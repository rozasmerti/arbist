<?
CModule::IncludeModule("iblock");

// ����� ��������� ��� ���������
if (preg_match('/PAGEN_1=1([^\d]|$)/', $_SERVER["REQUEST_URI"])
    || preg_match('/PAGEN_2=1([^\d]|$)/', $_SERVER["REQUEST_URI"])
) {
    $uri = str_replace(array('PAGEN_1=1', 'PAGEN_2=1'), '', $_SERVER["REQUEST_URI"]);
    $count = 1;
    while ($count)
        $uri = str_replace(array('?&', '&&'), array('?', '&'), $uri, $count);
    $uri = trim($uri, '?&');
    LocalRedirect($uri, false, 301);
}

$uri = GetPagePath(false,null);
$canonicalUrl = "https://" . SITE_SERVER_NAME . $uri;



use DebugBar\StandardDebugBar;

$debugbar = new StandardDebugBar();
$debugbarRenderer = $debugbar->getJavascriptRenderer();
$debugbarRenderer->setBaseUrl('/local/modules/vendor/maximebf/debugbar/src/DebugBar/Resources');




?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <base href="https://www.test.arbist.ru/">
    <meta name="viewport" content="initial-scale=1.0,width=device-width">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="canonical" href="<?=$canonicalUrl;?>">
	<? echo $debugbarRenderer->renderHead(); ?>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-N9F9HN');</script>
    <!-- End Google Tag Manager -->
    <!-- mda3 -->
    <meta name="cmsmagazine" content="ce3dd2eb434b96b3a51364fea7d4b7de"/>
    <meta name='yandex-verification' content='6263fd13e720d22b'/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700" rel="stylesheet">
    <? $page = 1;
    if (intval($_REQUEST['PAGEN_1']) > 1) $page = intval($_REQUEST['PAGEN_1']);
    if (intval($_REQUEST['PAGEN_2']) > 1) $page = intval($_REQUEST['PAGEN_2']);
    if (intval($_REQUEST['PAGEN_3']) > 1) $page = intval($_REQUEST['PAGEN_3']);

    if ($page > 1) {
        $pageTitle = '�������� ' . $page . ' - ';
    } ?>
    <title><?= $pageTitle; ?><? $APPLICATION->ShowTitle() ?></title>
    <? $APPLICATION->ShowHead(); ?>
    <? $APPLICATION->SetAdditionalCSS("/css/style.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/css/skin.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/css/jquery.lightbox-0.5.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/js/plugins/jquery-ui-1.8.16.custom/css/jquery-ui-1.8.16.custom.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/js/plugins/jquery-ui-1.8.16.custom/css/jquery.ui.button.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/js/plugins/jquery-ui-1.8.16.custom/css/jquery.ui.resizable.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/js/plugins/jquery-ui-1.8.16.custom/css/jquery.ui.dialog.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/js/plugins/jquery-ui-1.8.16.custom/css/jquery.ui.theme.css"); ?>

    <?// $APPLICATION->SetAdditionalCSS("/js/plugins/jquery-ui-1.9.2.custom/css/base/jquery-ui-1.9.2.custom.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/js/plugins/galleryview-2.1.1/galleryview.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/js/plugins/fc-trackbar-0.4.1/trackbar.css"); ?>
    <? $APPLICATION->SetAdditionalCSS("/css/font-awesome.min.css"); ?>
    <? $APPLICATION->AddHeadScript("/js/jquery-1.7.1.min.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.8.16.custom/js/jquery-ui-1.8.16.custom.min.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.8.16.custom/js/jquery.ui.mouse.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.8.16.custom/js/jquery.ui.position.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.8.16.custom/js/jquery.ui.draggable.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.8.16.custom/js/jquery.ui.resizable.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.8.16.custom/js/jquery.ui.button.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.8.16.custom/js/jquery.ui.dialog.js"); ?>
    <? //$APPLICATION->AddHeadScript("/js/plugins/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery.jcarousel.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/logic.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/galleryview-2.1.1/jquery.easing.1.3.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/galleryview-2.1.1/jquery.timers-1.2.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/galleryview-2.1.1/jquery.galleryview-2.1.1.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/jquery.simplemodal.1.4.1.min.js"); ?>
    <? $APPLICATION->AddHeadScript("/js/plugins/fc-trackbar-0.4.1/jquery.trackbar.js"); ?>

    <? $APPLICATION->AddHeadScript("/js/plugins/jquery.lightbox-0.5.js"); ?>

    <!--[if lt IE 8]>
    <link rel="stylesheet" media="screen" type="text/css" href="/css/ie67.css"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" media="screen" type="text/css" href="/css/ie68.css"/>
    <![endif]-->


    <?php 
    /*
    <script type="text/javascript">
    var __cs = __cs || [];
    __cs.push(["setCsAccount", "teAoY3EG9OIInGDGgUN8SvGgODedN3xg"]);
    __cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
    </script>
    <script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
    */
/*
    <script data-skip-moving="true">
        (function(w,d,u,b){
            s=d.createElement('script');r=(Date.now()/1000|0);s.async=1;s.src=u+'?'+r;
            h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b1989833/crm/site_button/loader_2_fdykgi.js');
    </script>
*/
        ?>
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N9F9HN"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
    "TYPE" => "toptop",
    "NOINDEX" => "Y",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "0"
),
    false
); ?>
<?
global $USER;
if($_REQUEST['dbg']=="show" && $USER->IsAdmin()) {
    echo "\n<!--\n";
    echo "\n===============================\n";
    \Bitrix\Main\Service\GeoIp\Manager::getRealIp();
    \Bitrix\Main\Service\GeoIp\Manager::useCookieToStoreInfo(true);
    $sypexGeo = \Bitrix\Main\Service\GeoIp\Manager::getDataResult($ipAddress, 'ru', array('cityName'));
    $geo = $sypexGeo->getGeoData();
    echo $geo;

    echo "\n===============================\n";
    echo "\n-->\n";
}
?>
<div id="wrapper" class="wrapper">
    <div class="header">
        <div class="inner clearfix">
            <div class="headerL">
                <div class="logo">
                    <?
                    if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php'/* || (!empty($_REQUEST["SECTION_MENU_ID"]) && $_REQUEST["alogo"] != "Y")*/) {
                        ?>
                        <? $APPLICATION->IncludeFile(
                            "/inc/logo.php",
                            Array(),
                            Array("MODE" => "HTML")
                        ); ?>
                        <?
                    } else {
                        ?>
                        <a href="/">
                            <? $APPLICATION->IncludeFile(
                                "/inc/logo.php",
                                Array(),
                                Array("MODE" => "HTML")
                            ); ?>
                        </a>
                        <?
                    }
                    ?>
                </div>
            </div>
            <div class="headerR">
                <div class="contacts">
                    <ul class="list">
                        <li style="height: 19px;">
                            <? /*<ul class="opt list">
								<li>
									<span class="skype">
										<?$APPLICATION->IncludeFile(
											"/inc/skype.php",
											Array(),
											Array("MODE"=>"HTML")
										);?>
									</span>
								</li>
								<li>
									<span class="icq">
										<?$APPLICATION->IncludeFile(
											"/inc/icq.php",
											Array(),
											Array("MODE"=>"HTML")
										);?>
									</span>
								</li>
							</ul>*/ ?>
                        </li>
                        <li>
                            <div class="phone ya-phone">
                                <? $APPLICATION->IncludeFile(
                                    "/inc/phone_top.php",
                                    Array(),
                                    Array("MODE" => "HTML")
                                ); ?>
                            </div>
                        </li>
                        <li>

                            <div class="call popup">
                                <div class="button0 button">
                                    <span class="helper"></span>
                                    <button class="trg" type="button" onclick="window.location.href='/callback/'">
                                        <i></i><b>�������� ������</b></button>
                                </div>
                            </div>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="headerC">
                <div class="gap0 nav aligned">
                    <? $APPLICATION->IncludeComponent("bitrix:menu", "arbist_top", array(
                        "ROOT_MENU_TYPE" => "top",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "360000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "top",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                        false
                    ); ?>
                    <span class="under"></span>
                </div>

                <? //if(CUser::IsAdmin()){
                ?>
                <? $APPLICATION->IncludeComponent("bitrix:search.title", "search_title", Array(
                    "NUM_CATEGORIES" => "1",    // ���������� ��������� ������
                    "TOP_COUNT" => "20",    // ���������� ����������� � ������ ���������
                    "ORDER" => "date",    // ���������� �����������
                    "USE_LANGUAGE_GUESS" => "Y",    // �������� ��������������� ��������� ����������
                    "CHECK_DATES" => "N",    // ������ ������ � �������� �� ���� ����������
                    "SHOW_OTHERS" => "N",    // ���������� ��������� "������"
                    "PAGE" => "#SITE_DIR#search/index.php",    // �������� ������ ����������� ������ (�������� ������ #SITE_DIR#)
                    "CATEGORY_0_TITLE" => "",    // �������� ���������
                    "CATEGORY_0" => array(    // ����������� ������� ������
                        0 => "main",
                        1 => "iblock_catalog",
                    ),
                    "CATEGORY_0_main" => "",    // ���� � ����� ���������� � ������ �� �������������
                    "CATEGORY_0_iblock_catalog" => array(    // ������ � �������������� ������ ���� "iblock_catalog"
                        0 => "all",
                    ),
                    "SHOW_INPUT" => "Y",    // ���������� ����� ����� ���������� �������
                    "INPUT_ID" => "title-search-input",    // ID ������ ����� ���������� �������
                    "CONTAINER_ID" => "title-search",    // ID ����������, �� ������ �������� ����� ���������� ����������
                ),
                    false
                ); ?>
                <?
                /*}
                else {
                    ?>
                    <?$APPLICATION->IncludeComponent("bitrix:search.form", "arbist_search", array(
                        "PAGE" => "#SITE_DIR#search/index.php",
                        "USE_SUGGEST" => "N"
                        ),
                        false
                    );?>
                    <?
                }*/ ?>

                <div class="row-a aligned">
                    <div class="item">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:sale.basket.basket.line",
                            "",
                            Array(
                                "PATH_TO_BASKET" => "/personal/basket.php",
                                "PATH_TO_PERSONAL" => "/personal/",
                                "SHOW_PERSONAL_LINK" => "Y"
                            ),
                            false
                        ); ?>
                    </div>
                    <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", ".default", array(
                        "REGISTER_URL" => "/register.php",
                        "FORGOT_PASSWORD_URL" => "/forgot.php",
                        "PROFILE_URL" => "/personal/profile/",
                        "SHOW_ERRORS" => "Y"
                    ),
                        false
                    ); ?>
                    <span class="under"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="inner">
            <div class="content clearfix">
                <?
                $pages = array(
                    '/personal/basket.php',
                    '/actions/zima2017/',
                    '/test/auth.php'
                );
                if (!in_array($APPLICATION->GetCurPage(), $pages)): ?>
                <div class="leftCol">
                    <? /*
	$APPLICATION->IncludeComponent("profline:menu", "tree", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "",
	"SECTION_ID" => $_REQUEST["SECTION_ID"],
	"SECTION_CODE" => "",
	"COUNT_ELEMENTS" => "Y",
	"TOP_DEPTH" => "2",
	"SECTION_URL" => "",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "3600",
	"DISPLAY_PANEL" => "N",
	"ADD_SECTIONS_CHAIN" => "Y"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);*/
                    ?>

                    <? $APPLICATION->IncludeComponent("itc:new_left.menu", ".default", array(
                        "FILTER_REQUEST" => $_REQUEST["arrFilter_pf"],
                        "BRAND_ID" => intval($_REQUEST["BRAND_ID"]),
                        "SECTION_ID" => $_REQUEST["SECTION_MENU_ID"],
                        "IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"],
                        "IBLOCK_TYPE" => "dynamic_content",
                        "IBLOCK_ID" => "47",
                        "PROPERTY_VALUE" => $_REQUEST["VALUE"],
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600000000000000000000000000000000000000000"
                    ),
                        false
                    ); ?>
                    <? if ($APPLICATION->GetCurPage() == '/' || !empty($_REQUEST["SECTION_MENU_ID"]) || !empty($_REQUEST["IBLOCK_CODE"])): ?>
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/leftBlock.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                            false
                        ); ?>
                    <? endif; ?>

                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>

                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left2",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>

                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left3",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left4",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left5",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left6",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left7",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left8",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left9",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left10",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left11",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left12",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>
                    <br/>
                    <br/>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", ".default", array(
                        "TYPE" => "left13",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                        false
                    ); ?>


                </div>
                <div class="rightCol"><a name="mainContent"></a>
                    <table>
                        <tr>
                            <td>
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:advertising.banner",
                                    "",
                                    Array(
                                        "TYPE" => "top_1",
                                        "NOINDEX" => "Y",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "0"
                                    ),
                                    false
                                ); ?>
                            </td>
                            <td>
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:advertising.banner",
                                    "",
                                    Array(
                                        "TYPE" => "top_2",
                                        "NOINDEX" => "Y",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "0"
                                    ),
                                    false
                                ); ?>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <?
                    $dir = $APPLICATION->GetCurDir();
                    $arrayDir = explode("/", $dir);
                    if ($dir != "/" && $arrayDir[1] != "catalog" && empty($_REQUEST["SECTION_MENU_ID"]) && $arrayDir[1] != "interior_doors") {
                        ?>
                        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "new_arbist_breadcrumb", array(
                            "START_FROM" => "0",
                            "PATH" => "",
                            "SITE_ID" => "-"
                        ),
                            false
                        ); ?>

                        <h1><? $APPLICATION->ShowTitle() ?></h1>

                    <? } ?>
                <?endif;?>
