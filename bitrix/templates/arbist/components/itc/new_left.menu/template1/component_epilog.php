<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(empty($arResult["REQUEST_IBLOCK_CODE"])) {
    if($arResult["REQUEST_ID_FLAG"] === true) {
        $APPLICATION->SetTitle($arResult["SEO_NAME"] . " - �������, ����, ���� | ������� ������� ������� " . $arResult["SEO_NAME"] . " - ��������-������� Arbist.ru");
        $APPLICATION->SetPageProperty("description", $arResult["SEO_NAME"] . " - ������� ������ � ��������-�������� Arbist, ������� �������� ����������� �������, ������� �������� � ��������.");
        $APPLICATION->SetPageProperty("h1", $arResult["SEO_NAME"]);
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/inc/seo/topText.php');
        $topText = ob_get_clean();
		$arFilter = Array('IBLOCK_ID'=>47, 'CODE'=>$arResult["REQUEST_ID"]);
		$arSelect = Array("NAME","UF_TITLE","IBLOCK_SECTION_ID");
		$ar_result = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, true, $arSelect);
		if($res=$ar_result->GetNext()){
			$title = $res['UF_TITLE'];
		}
		if ($title != '') {
			$APPLICATION->SetTitle($title);
		}

        
        //$APPLICATION->SetPageProperty("topText", "� ������� ������ �� ������� �" . $arResult["SEO_NAME"] . "� �� ������ ����� � ��������� ��������� � ����������������.<br><br>");
        $APPLICATION->SetPageProperty("topText", $topText);
        //$APPLICATION->SetPageProperty("botText", "� ����� ��������-�������� Arbist.ru ����������� ������� ����� �������� ��������� ������� ������� �������.");
    } else {
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/inc/seo/botText.php');
        $botText = ob_get_clean();
        
        $APPLICATION->SetTitle("��������������: ������ ���������� � ������������ ��������� �� �������� ����� � ��������-�������� ������");
        $APPLICATION->SetPageProperty("description", "��������-������� ���������� � ������������ ���������� Arbist � � ������� ���������� ��������� ������� �������������� �� ������ �����.");
        $APPLICATION->SetPageProperty("h1", "��������-������� ������������ � ���������� ����������");
        $APPLICATION->SetPageProperty("botText", $botText);
    }
}

if ($arResult["DEL_SECTIONS"] == "Y") {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
}
?>
