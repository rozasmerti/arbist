<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MENU_LEVEL;
$MENU_LEVEL = IntVal($MENU_LEVEL);
$leftBreak = '<tr><td></td></tr>';

if ($ITEM_TYPE=="D" || $MENU_LEVEL==0)
	$strBullet = '';
else
	$strBullet = '';


{  
	$sMenuProlog='<div align="left" class="catalog-section-list"><table width="100%" border="0" cellspacing="0" cellpadding="0">';
	$sMenuEpilog='</table></div>';
	if($MENU_LEVEL==1 && ($ITEM_INDEX > 0)) $sMenuBody = $leftBreak;
	if($MENU_LEVEL>0) $sMenuProlog = '<table width="200" border="0" cellspacing="0" cellpadding="0" style="margin-left: 24px">';

	if($SELECTED)
	{  
		$strU_beg = "";
		$strU_end = "";
		$strSubMenu = "";
		$CurMenu = new CMenu("left4");
		$CurMenu->Init($LINK);
		global $arUSED_MENU;
		if(!is_array($arUSED_MENU))
			$arUSED_MENU=Array();
		if(!in_array($CurMenu->MenuDir, $arUSED_MENU))
		{
			$arUSED_MENU[] = $CurMenu->MenuDir;
			$MENU_LEVEL++;
			$strSubMenu = $CurMenu->GetMenuHtml();
			$MENU_LEVEL--;
		}

		if(strlen($strSubMenu)>0)
			if($PERMISSION > "D")
				{
					$sMenuBody .= '<tr><td valign="top" style="padding-bottom: 2px"><a class="level2act" href="'.$LINK.'" id="dark">'.$strU_beg.$TEXT.$strU_end.'</a></td></tr>'. 
			'<tr><td colspan="2" style="padding-bottom: 6px">'.$strSubMenu.'</td></tr>';
				}
			else 
				{
					$sMenuBody = "";
				}
        else 			if($PERMISSION > "D") 
				{
					$sMenuBody .= '<tr><td valign="top" height="20" style="padding-bottom: 2px"><a class="level2act" href="'.$LINK.'" id="dark">'.$strU_beg.$TEXT.$strU_end.'</a></td></tr>';
				}
			else 
				{
					$sMenuBody = "";
				}
	}
	else { 		if($PERMISSION > "D")
			{
				$sMenuBody .= '<tr><td valign="top" height="20" style="padding-bottom: 2px"><a class="level2" href="'.$LINK.'" id="dark">'.$TEXT.'</a></td></tr>';
			}
		else 
			{
				$sMenuBody = "";
			}
		}
}
?>