<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//CPL::pr($arResult["ITEMS"]);
$arSelect = array(
	"ID",
	"NAME",
	"DETAIL_PICTURE",
	"PROPERTY_INTERIOR",
//	"",
//	"",
	);
foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $k=>$v) {
	$res = CIBlockElement::GetByID($v["PRODUCT_ID"]);
	if($arItem = $res->GetNext()) {
		if ($arItem["IBLOCK_ID"] == 12) {
			$type = explode(" -", $arItem["NAME"]);
			if (strpos($arItem["NAME"],"�������") === false)
				unset($arResult["ITEMS"]["AnDelCanBuy"][$k]["DETAIL_PAGE_URL"]);
		}
		$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"SIZE"));
		if($ar_props = $db_props->Fetch())
			$arResult["ITEMS"]["AnDelCanBuy"][$k]["SIZE"] = $ar_props["VALUE_ENUM"];
			
		$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"PACKING"));
		if($ar_props = $db_props->Fetch())
			$arResult["ITEMS"]["AnDelCanBuy"][$k]["PACKING"] = $ar_props["VALUE"];
		$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"CML2_BASE_UNIT"));
		if($ar_props = $db_props->Fetch())
			$arResult["ITEMS"]["AnDelCanBuy"][$k]["CHANGE_UNIT"] = $ar_props["VALUE"];
			
		$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"CML2_KRAT"));
		if($ar_props = $db_props->Fetch())
			$arResult["ITEMS"]["AnDelCanBuy"][$k]["CHANGE_KRAT"] = $ar_props["VALUE"];
			
		$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"CML2_KRAT_UNIT"));
		if($ar_props = $db_props->Fetch())
			$arResult["ITEMS"]["AnDelCanBuy"][$k]["CHANGE_KRAT_UNIT"] = $ar_props["VALUE"];
			
		$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"DOOR_LEAF"));
		if($ar_props = $db_props->Fetch())
			$arResult["ITEMS"]["AnDelCanBuy"][$k]["LEAF"] = $ar_props["VALUE_ENUM"];
		$db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"DELIVERY_DATE"));
		if($ar_props = $db_props->Fetch())
			$arResult["ITEMS"]["AnDelCanBuy"][$k]["DELIVERY_DATE"] = $ar_props["VALUE"];
	}
	$arResult["ITEMS"]["AnDelCanBuy"][$k]["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
		//CPL::pr($ar_res);
}
//CPL::pr($arResult["ITEMS"]["AnDelCanBuy"]);
?>
