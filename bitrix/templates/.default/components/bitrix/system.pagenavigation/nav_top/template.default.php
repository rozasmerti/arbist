<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
   if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
      return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

//echo "<pre>"; print_r($arResult); echo "</pre>";
?>

<ul class="paging paging-top">
	<?if($arResult["NavPageNomer"] != 1):?>
	 <li class="prev"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageNomer"]-1?>"></a></li>
	<?endif;?>
	 <?if(4 < $arResult["NavPageNomer"]):?>
	  <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a></li>
	  <li class="more">...</li>
	 <?endif;?>
	 <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
	  <?if(abs($arResult["nStartPage"] - $arResult["NavPageNomer"]) <= 2):?>
		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
		   <li class="current"><a href="#"><?=$arResult["nStartPage"]?></a></li>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
		   <li><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a></li>
		<?else:?>
		   <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a></li>
		<?endif?>
	  <?endif;?>
		<?$arResult["nStartPage"]++?>
	 <?endwhile?>
	 <?if(($arResult["NavPageNomer"] + 3) < $arResult["NavPageCount"]):?>
	  <li class="more">...</li>
	  <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a></li>
	 <?endif;?>
	 <?if($arResult["NavPageNomer"]!=$arResult["NavPageCount"]):?>
	 <li class="next"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageNomer"]+1?>"></a></li>
	 <?endif;?>
</ul>
