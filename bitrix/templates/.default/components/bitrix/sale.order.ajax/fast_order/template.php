<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
	if(isset($arResult["REDIRECT_URL"]) && !isset($_GET["ORDER_ID"])){
		LocalRedirect($arResult["REDIRECT_URL"]);
	}

	if(isset($_GET["ORDER_ID"])){
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/done.php");
	}else{
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/checkout.php");
	}
