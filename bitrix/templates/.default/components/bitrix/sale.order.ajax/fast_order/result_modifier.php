<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('local.lib');

$elementIDs = array();
foreach ($arResult['BASKET_ITEMS'] as $arItem) {
    $elementIDs[$arItem['PRODUCT_ID']] = $arItem['PRODUCT_ID'];
}
sort($elementIDs);

$arResult['PRODUCTS'] = array();
if (!empty($elementIDs)) {
    $arFilter = array(
        'ACTIVE' => 'Y',
        'ID' => $elementIDs
    );

    $arSort = array(
        'SORT' => 'ASC',
        'NAME' => 'ASC'
    );

    $rsElements = \CIBlockElement::GetList($arSort, $arFilter);

    while ($obElement = $rsElements->GetNextElement()) {
        $arElement = $obElement->GetFields();
        $arElement['PROPERTIES'] = $obElement->GetProperties();

        $image = new \Local\Lib\Parts\Image(array(
            'width' => 75,
            'height' => 75
        ));
        $image->addImage($arElement['PROPERTIES']['PHOTO_NAME']['VALUE']);
        $image->addImage($arElement['DETAIL_PICTURE']);
        $image->addImage($arElement['PREVIEW_PICTURE']);
        $arElement['IMAGE'] = $image->getFirstResized();

        $arResult['PRODUCTS'][$arElement['ID']] = $arElement;
    }
}