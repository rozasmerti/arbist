<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arResult["ERROR"]): ?>
    <div class="messages">
        <? foreach ($arResult["ERROR"] as $error): ?>
            <div><? print $error; ?></div>
        <? endforeach; ?>
        <div class="clear"></div>
    </div>
<? endif; ?>

<div class="basket_block">
    <div class="left_column">
        <ul class="tabs_basket">
            <li class="active">��� �����������</li>
            <li>��� ��������</li>
        </ul>
        <div class="tabs_basket_content">
            <div class="tabs_item active">
                <div class="block_text">
                    <button class="close">X</button>
                    <div class="title">���������� ������ ������ ����� ���������</div>
                    <div class="desc">������� ���� ��� � ���������� �������, � ������� ������ "��������"!���� ������
                        ����� ����������������, � � ������� 5 ����� ��� �������� �������� Arbist.RU, ����� �����������
                        ������ �� ���������� � ��������
                    </div>
                </div>
                <form action="<?=POST_FORM_ACTION_URI?>" method="post" name="ORDERFORM" class="buy_for_click">
                    <input type="hidden" name="PERSON_TYPE" value="1">
                    <input type="hidden" name="PAY_SYSTEM_ID" value="7">
                    <input type="hidden" name="DELIVERY_ID" value="1">

                    <label for="name">��� � ��� ���������� ?</label>
                    <input type="text" id="name" name="name" placeholder="���� ���" required/>
                    <label for="phone">���������� �������</label>
                    <input type="text" id="phone" name="phone" placeholder="�������" required/>
                    <label for="email">���� ����� <span>(�� �����������)</span></label>
                    <input type="text" id="email" name="email" placeholder="E-mail"/>

                    <?if(!$USER->IsAuthorized):?>
                        <input type="hidden" name="do_register" value="Y" />
                    <?endif?>
                    <input type="hidden" name="PERSON_TYPE_OLD" value="<?=$arResult["USER_VALS"]["PERSON_TYPE_ID"]?>">
                    <input type="hidden" name="confirmorder" value="Y" />

                    <input type="submit" value="��������">
                </form>
            </div>
            <div class="tabs_item"></div>
        </div>
    </div>
    <div class="right_column">
        <form action="/" class="order_products_form">
            <table class="order_tabel">
                <thead>
                <tr>
                    <td>����</td>
                    <td>��������</td>
                    <td>����</td>
                    <td>�������</td>
                </tr>
                </thead>
                <tbody>
                <?foreach ($arResult['BASKET_ITEMS'] as $arItem):?>
                    <?
                    $arProduct = array();
                    if(isset($arResult['PRODUCTS'][$arItem['PRODUCT_ID']])){
                        $arProduct =& $arResult['PRODUCTS'][$arItem['PRODUCT_ID']];
                    }
                    ?>
                    <tr>
                        <td class="photo_product">
                            <?if(!empty($arProduct['IMAGE']['SRC'])):?>
                                <img src="<?=$arProduct['IMAGE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                            <?endif;?>
                        </td>
                        <td class="name_product"><?=$arItem['NAME']?></td>
                        <td class="price_product"><?=$arItem['PRICE_FORMATED']?></td>
                        <td class="del_product">
                            <button class="delpos">X</button>
                        </td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </form>
        <div class="banner_phone">
            <div class="t1">��� �������</div>
            <div class="t2"><span>(495) </span>646-20-10</div>
        </div>
    </div>
</div>

<div style="clear: both"></div>
<?echo '<pre>'; print_r($arResult['ORDER_PROP']); echo '</pre>';?>