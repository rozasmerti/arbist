<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></b><br /><br />
	<table class="sale_order_full_table">
		<tr>
			<td>
				<?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER_ID"]))?><br /><br />
				<?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
			</td>
		</tr>
	</table>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>
		<br /><br />

		<table class="sale_order_full_table">
			<tr>
				<td>
					<?=GetMessage("SOA_TEMPL_PAY")?>: <?= $arResult["PAY_SYSTEM"]["NAME"] ?>
				</td>
			</tr>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?= $arResult["ORDER_ID"] ?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$arResult["ORDER_ID"])) ?>
							<?
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		<?
	}
	?>
	
	<?
	// ������ ��������������
	$location = '';
	$db_props = CSaleOrderProps::GetList(
		array("SORT" => "ASC"),
		array(
			"PERSON_TYPE_ID" => $arResult['ORDER']["PERSON_TYPE_ID"],
			"IS_LOCATION" => "Y"
		)
	);
	if ($arProps = $db_props->Fetch()) {
	
		$db_vals = CSaleOrderPropsValue::GetList(
			array("SORT" => "ASC"),
			array(
					"ORDER_ID" => $arResult['ORDER']['ID'],
					"ORDER_PROPS_ID" => $arProps["ID"]
				)
		);
		
		if ($arVals = $db_vals->Fetch()) $location = $arVals['VALUE'];
	}
	
	if(intval($location) > 0)
		$arLocs = CSaleLocation::GetByID($location,'en');
	
	
	// �������� �������� ������� ������
	$dbBasketItems = CSaleBasket::GetList(
		array("NAME" => "ASC","ID" => "ASC"),
		array(
			"LID" => SITE_ID,
			"ORDER_ID" => $arResult['ORDER']['ID']
		)
	);
	
	$arItems = array();
	while($basketItem = $dbBasketItems->fetch()){
		$arItems[] = $basketItem;
	}

	?>
	
	
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-20377562-1']);
	_gaq.push(['_trackPageview']);
		
	// ������
	_gaq.push(['_addTrans',
		'<?=$arResult['ORDER']['ID']?>',           // ����� ������
		'arbist.ru',   // �������� �������� ��� ��������
		'<?=$arResult['ORDER']['PRICE']?>',          // �������� ��������� ��������� ������
		'<?=$arResult['ORDER']['TAX_VALUE']?>',           // ������
		'',              // ��������� ��������
		'<?=$arLocs['CITY_NAME']?>',	     // ������ ��������
		'<?=$arLocs['CITY_NAME']?>',        	 // ������ ��������
		'<?=$arLocs['COUNTRY_NAME']?>'          // ������ ��������
	]);
		
	<?
	foreach($arItems as $item){
		?>
		_gaq.push(['_addItem',
			'<?=$arResult['ORDER']['ID']?>',           // ����� ������
			'<?=$item['PRODUCT_ID']?>',            // ��� ������ (��� SKU)
			'<?=$item['NAME']?>',  // �������� ������
			'',     // ��������� ��� ������
			'<?=$item['PRICE']?>',          // ���� �� �������
			'<?=round($item['QUANTITY'])?>'               // ���������� ������ ������
		]);
		<?
	}
	?>
		
	// �������� ������
	_gaq.push(['_trackTrans']);
		
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	
	
	var yaParams = 
	{
	  order_id: "<?=$arResult['ORDER']['ID']?>",
	  order_price: <?=$arResult['ORDER']['PRICE']?>,
	  currency: "RUR",
	  exchange_rate: 1,
	  goods: 
		[
		<?
		foreach($arItems as $item){
			?>
			{
			  id: "<?=$item['ID']?>", 
			  name: "<?=$item['NAME']?>", 
			  price: <?=$item['PRICE']?>,
			  quantity: <?=round($item['QUANTITY'])?>
			},
			<?
		}
		?>
		]
	};
	
	
	
	</script>
	
	<?
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ORDER_ID"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
