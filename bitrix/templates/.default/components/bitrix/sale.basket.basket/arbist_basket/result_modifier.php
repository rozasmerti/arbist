<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
//������� ��������� ���-�� ������, ����� �� ������� �����

function ReCount2tovar($count=0, $krat=0)
{
	$count = str_replace(",", ".", $count);
	$krat = floatval($krat);
	if($krat>0)
	{
		$ar = explode('.', (string)$count);
		$count = (float)($ar[0] . '.' . substr($ar[1], 0, 3));
		if($count < $krat && $count > 0 && $krat > 0)
			$count = $krat;
			
		$ck = $count / $krat;
		
		$ar = explode('.', (string)$ck);
		$osk = (float)('0.' . substr($ar[1], 0, 10));
		
		if($osk >= 0.5)
			$k = intval($ar[0])+1;
		else
			$k = intval($ar[0]);
			
		return $k * $krat;
	}
	else
		return round($count);
}
//DelDelCanBuy - ��������� ������
//nAnCanBuy - �����������

//echo "<pre>"; print_r($arResult); echo "</pre>";
//������� ���� (��-�� ����, ��� ������ ���� �������� � ���������� ��������� ����������, �� � ������� ���� � �������� �������)
// + ������ ��� ��������
$productsId = array();
$arResult["TOTAL_PRICE"] = 0;
foreach($arResult["ITEMS"] as $itemTypes) {
	foreach($itemTypes as $item) {
		$productsId[] = $item["PRODUCT_ID"];
	}
}

if(is_array($productsId) && count($productsId) > 0) {
	$arFilter = Array(
		"ACTIVE" => "Y",
		"ID" => $productsId
	);
	$arSelect = Array(
		"ID",
		"IBLOCK_CODE",
		"IBLOCK_SECTION_ID",
		"PROPERTY_CML2_KRAT",
		"PROPERTY_SIZE",
		"PROPERTY_INPACK",
		"PROPERTY_CHANGE_UNIT",
		"PREVIEW_PICTURE",
		"DETAIL_PICTURE",
		"XML_ID",

	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while ($ar_fields = $res->GetNext()) {

		if (!empty($ar_fields["XML_ID"]) && $r = CIBlockElement::GetList(array(), array("PROPERTY_COMPLECT" => $ar_fields["XML_ID"]), false, false, array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL"))->getnext()) {
			$products["PATH"][$ar_fields["ID"]] = $r["DETAIL_PAGE_URL"] . "?CID=" . $ar_fields["ID"];
			$products["PATH"][$ar_fields["ID"]] = $r["~DETAIL_PAGE_URL"] . "?CID=" . $ar_fields["ID"];
		} else {
			$products["PATH"][$ar_fields["ID"]] = "/catalog/" . $ar_fields["IBLOCK_CODE"] . "/" . $ar_fields["IBLOCK_SECTION_ID"] . "/" . $ar_fields["ID"] . "/";
		}

		$products["KRAT"][$ar_fields["ID"]] = $ar_fields["PROPERTY_CML2_KRAT_VALUE"];
		$products["CHANGE_UNIT"][$ar_fields["ID"]] = $ar_fields["PROPERTY_CHANGE_UNIT_VALUE"];
		//��������
		if (!empty($ar_fields["DETAIL_PICTURE"]))
			$products["PICTURE"][$ar_fields["ID"]] = $ar_fields["DETAIL_PICTURE"];
		else
			$products["PICTURE"][$ar_fields["ID"]] = $ar_fields["PREVIEW_PICTURE"];
		//����
		$products["SIZE"][$ar_fields["ID"]]["NAME"] = "�������";
		$products["SIZE"][$ar_fields["ID"]]["VALUE"] = $ar_fields["PROPERTY_SIZE_VALUE"];
		$products["INPACK"][$ar_fields["ID"]]["NAME"] = "� ��������";
		$products["INPACK"][$ar_fields["ID"]]["VALUE"] = $ar_fields["PROPERTY_INPACK_VALUE"];
	}

}

foreach ($arResult["ITEMS"] as $keyType => $itemTypes) {
	foreach ($itemTypes as $key => $item) {
		$arResult["ITEMS"][$keyType][$key]["DETAIL_PAGE_URL"] = $products["PATH"][$item["PRODUCT_ID"]];
		$quantity = ReCount2tovar($item["QUANTITY"], $products["KRAT"][$item["PRODUCT_ID"]]);
		$arResult["ITEMS"][$keyType][$key]["QUANTITY"] = $quantity;
		$arResult["ITEMS"][$keyType][$key]["KRAT"] = $products["KRAT"][$item["PRODUCT_ID"]];
		$arResult["ITEMS"][$keyType][$key]["CHANGE_UNIT"] = $products["CHANGE_UNIT"][$item["PRODUCT_ID"]];
		//��������
		$arResult["ITEMS"][$keyType][$key]["PICTURE"] = $products["PICTURE"][$item["PRODUCT_ID"]];
		//������� ��������
		if (!empty($products["PICTURE"][$item["PRODUCT_ID"]])) {
			$arResult["ITEMS"][$keyType][$key]["BIG_PICTURE"] = CFile::GetPath($products["PICTURE"][$item["PRODUCT_ID"]]);
		}
		//����
		$arResult["ITEMS"][$keyType][$key]["INFO"]["SIZE"] = $products["SIZE"][$item["PRODUCT_ID"]];
		$arResult["ITEMS"][$keyType][$key]["INFO"]["INPACK"] = $products["INPACK"][$item["PRODUCT_ID"]];
		/*if($keyType == "AnDelCanBuy")
            $arResult["TOTAL_PRICE"] += $item["PRICE"] * $quantity;*/
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>