<?
CModule::IncludeModule("sale");
$arBasketItems = array();

$dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE",
              "PRODUCT_ID", "QUANTITY", "DELAY",
              "CAN_BUY", "PRICE", "WEIGHT")
    );
$busketItemsArray = array();
while ($arItems = $dbBasketItems->Fetch()) {
    $busketItemsArray[] = $arItems["PRODUCT_ID"];
}

foreach($arResult["ALL_ITEM_IDES"] as $elementID) {
	if(in_array($elementID, $busketItemsArray)){
?>
		<script>
			$('#buy<?=$elementID?>').html('<div class="in-basket-yet"></div>');
			$('#buy<?=$elementID?>').closest('.aligned').addClass('in-basket-yet-margin');
		</script>
<?
	}
}

?>
