<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arResult
 */

?>

<?$this->SetViewTarget("collection_params");?>

<?$arCollection = false;
if(isset($_GET['id']) && intval($_GET['id']) > 0):
	foreach ($arResult["ITEMS"] as $arItem){
		if($arItem['ID'] == intval($_GET['id'])){
			$arCollection = $arItem;
			$arCollection['FROM_MARKET'] = true;
		}
	}
endif;

if(!is_array($arCollection) || empty($arCollection)):
	reset($arResult["ITEMS"]);
	$arCollection = current($arResult["ITEMS"]);
	$arCollection['FROM_MARKET'] = false;
endif;?>

<?
$arCollectionPropsID = array(
		"country", //������  16.05.2017 , sumato.shigoto@gmail.com - ������� country_text �� country
		"vendor", //�������������
		"view_text", //��� ������
		"uses_text", //����������
		"timing", //���� ��������
		"material_text", //��������
		"ed_izm", //�������� ���������
		"otgruz", //��������� ��������
		"garanty", //��������� ��������
);
//������������� ������, ������� ��������
$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>IBLOCK_vendor_ID,"XML_ID"=>$arCollection["PROPERTIES"]["vendor"]["VALUE"]), false, false, array("ID","NAME","CODE"));
if($ar = $res->Fetch())
	$arCollection["PROPERTIES"]["vendor"]["VALUE"] = $ar["NAME"];
//$arCollection["DETAIL_PICTURE"]["SRC"] = "http://www.arbist.ru/upload/resizer/4f/8023900_x274_4ff535bdc194ad79360528382167aaa3.jpg?1450304131";

?>
<div class="gap4 clearfix">
	<div class="img0 img" style="width: 422px; overflow: hidden; max-height: 400px;">

		<ul id="galleryd93" class="gallery">
			<div class="gallery-loader-img" style="display:none; position:absolute; left:150px; top:50px;"><img src="/images/ajax-loading_big.gif"/></div>

			<?foreach($arResult["GALLERY"]['SMALL'] as $key => $filePath):?>
                <li>
                    <!--68 x 50-->
                    <img src="<?=$filePath?>" alt=""/>
                    <div class="panel-content">
                        <!--400 x 296-->
                        <a class="lightboxx" rel="lightbox_gallery_interior" href="<?=$arResult["GALLERY"]['ORIGINAL'][$key]?>">
                            <img src="<?=$arResult["GALLERY"]['BIG'][$key]?>" alt=""/>
                        </a>
                    </div>
                </li>
			<?endforeach;?>
		</ul>
		<script>
			$(document).ready(function(){

				$('.lightboxx').lightBox();

			});
		</script>
	</div>
	<div class="txt">
		<table class="table0 table">
			<tbody>
			<?foreach($arCollection["PROPERTIES"] as $code=>$arValue):?>
			<?if(in_array($code,$arCollectionPropsID)):?>
			<tr>
				<td>
					<div class="char"><?=$arValue["NAME"]?>:</div>
				</td>
				<td>
					<div class="hl2">
						<?=$arValue["VALUE"]?>																		</div>
				</td>
			</tr>
			<?endif;?>
			<?endforeach;?>
			</tbody>
		</table>
	</div>
</div>
<?$this->EndViewTarget("collection_params");?>

<div class="catalog-section">

<table class="items_table complect-table b-complect-table">

    <?if($arCollection['FROM_MARKET']):?>
        <tbody>
        <tr>
            <td colspan="6" style="text-align: left; border:none;">
                <h2>�� ������� �� ������ �������:</h2>
            </td>

        </tr>

        <tr style="background: #fc0">
            <td><?
                if($arCollection["SELECTED"] == "Y")
                {
                    ?>
                    <b><?=$arCollection["NAME"]?><br/><span style="float:left; margin-top: 7px; text-decoration: underline;">���: <?=$arCollection["PROPERTIES"][PROPERTY_CODE]["VALUE"]?></span></b>
                    <div style='color:#c03;'><i>������ �� �������������� ���� �����.</i></div>
                    <?
                }else{?>
                    <?=$arCollection["NAME"]?><br/><span style="float:left; margin-top: 7px; text-decoration: underline;">���: <?=$arCollection["PROPERTIES"][PROPERTY_CODE]["VALUE"]?></span>
                <?}?>
            </td>
            <td>
                <?$val = $arCollection["PROPERTIES"]["power"]["VALUE"];?>
                <?if($val!="" && $val!="<>" && $val!="&lt;&gt;"):?>
                    <?=$val?>
                <?endif;?>
            </td>
            <td>
                <?$val = $arCollection["PROPERTIES"]["area"]["VALUE"];?>
                <?if($val!="" && $val!="<>" && $val!="&lt;&gt;"):?>
                    <?=$val?>
                <?endif;?>
            </td>
            <td>
                <?$val = $arCollection["PROPERTIES"]["ed_izm"]["VALUE"];?>
                <?if($val!="" && $val!="<>" && $val!="&lt;&gt;"):?>
                    <?=$val?>
                <?endif;?>
            </td>
            <td style="white-space: nowrap;">
                <?
                $price = array_shift($arCollection["PRICES"]);
                ?>
                <?=$price["PRINT_VALUE_VAT"]?>
            </td>
            <td>
                <form action="">
                    <input type="hidden" name="action" value="ADD2BASKET">
                    <input type="hidden" name="id" value="<?=$arCollection["ID"]?>">
                    <div class="buy<?=$arCollection["ID"]?> button4 button hover">
                        <i class="helper"></i>
                        <button type="button" onclick="$(this).closest('form').submit();"><i></i></button>
                    </div>
                </form>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left; border: none;">
                <br>
                <br>
                <br>
                <h2>�� ������ ������� � ������ �������:</h2>
            </td>
        </tr>
        </tbody>
    <?endif;?>

	<tbody>
	<tr>
		<th class="complect-table__first-th">������������ ������</th>
		<th><?=$arResult["ITEMS"][0]["PROPERTIES"]["power"]["NAME"]?></th>
		<th><?=$arResult["ITEMS"][0]["PROPERTIES"]["area"]["NAME"]?></th>
		<th><?=$arResult["ITEMS"][0]["PROPERTIES"]["ed_izm"]["NAME"]?></th>
		<th>����, ���.</th>
		<th style="width: 64px;" class="complect-table__last-td">&nbsp;</th>
	</tr>
	<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

    $style = '';
    if($arCollection['FROM_MARKET']){
        if($arElement['ID'] == $arCollection['ID']){
            $style = 'style="background: #fc0"';
        }
    }
    ?>
	<tr id="<?=$this->GetEditAreaId($arElement['ID']);?>" <?=$style?>>
		<td><?
			if($arElement["SELECTED"] == "Y")
			{
				?>
				<b><?=$arElement["NAME"]?><br/><span style="float:left; margin-top: 7px; text-decoration: underline;">���: <?=$arElement["PROPERTIES"][PROPERTY_CODE]["VALUE"]?></span></b>
				<div style='color:#c03;'><i>������ �� �������������� ���� �����.</i></div>
			<?
			}else{?>
				<?=$arElement["NAME"]?><br/><span style="float:left; margin-top: 7px; text-decoration: underline;">���: <?=$arElement["PROPERTIES"][PROPERTY_CODE]["VALUE"]?></span>
			<?}?>
		</td>
		<td>
			<?$val = $arElement["PROPERTIES"]["power"]["VALUE"];?>
			<?if($val!="" && $val!="<>" && $val!="&lt;&gt;"):?>
				<?=$val?>
			<?endif;?>
		</td>
		<td>
			<?$val = $arElement["PROPERTIES"]["area"]["VALUE"];?>
			<?if($val!="" && $val!="<>" && $val!="&lt;&gt;"):?>
				<?=$val?>
			<?endif;?>
		</td>
		<td>
			<?$val = $arElement["PROPERTIES"]["ed_izm"]["VALUE"];?>
			<?if($val!="" && $val!="<>" && $val!="&lt;&gt;"):?>
				<?=$val?>
			<?endif;?>
		</td>
		<td style="white-space: nowrap;">
			<?
			$price = array_shift($arElement["PRICES"]);
			?>
			<?=$price["PRINT_VALUE_VAT"]?>
		</td>
		<td>
			<form action="">
				<input type="hidden" name="action" value="ADD2BASKET">
				<input type="hidden" name="id" value="<?=$arElement["ID"]?>">
				<div id="buy<?=$arElement["ID"]?>" class="button4 button hover">
					<i class="helper"></i>
					<button type="button" onclick="$(this).closest('form').submit();"><i></i></button>
				</div>
			</form>
		</td>
	</tr>
	<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
	</tbody>
</table>

</div>
<script>
	$(document).ready(function(){
		$('.lightboxx').lightBox();
	});
</script>
<script>
	$(function(){
		$('.carousel').jcarousel({
			scroll: 1,
			auto: 4,
			animation: 'slow',
			wrap: "circular"
		});
	});
</script>
<script type="text/javascript">
	$('#galleryd93').each(function(){
		if ($('#' + $(this).attr('id') + ' > li').size() >= 1)   {
			$('#' + $(this).attr('id')).galleryView({
				transition_speed: 500,
				panel_width: 422,					//INT - width of gallery panel (in pixels)
				panel_height: 316,					//INT - height of gallery panel (in pixels)
				frame_width: 68,					//INT - width of filmstrip frames (in pixels)
				frame_height: 50,					//INT - width of filmstrip frames (in pixels)
				filmstrip_size: 4,
				frame_gap: 10,
				transition_interval: 1000000000,
				pointer_size: 0,
				filmstrip_position: 'bottom'
			});
		}
		else {
			$('#' + $(this).attr('id')).addClass('empt');
		}
	});

</script>
