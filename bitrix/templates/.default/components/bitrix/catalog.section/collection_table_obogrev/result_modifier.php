<?php
//������� ������� "� ���������" ����� ������, ��������� �� ����� ��������� ���������� ���������� Itc
$arResult["GALLERY"] = array(
    "FILE_NAMES" => array(),
    "SMALL" => array(),
    "BIG" => array(),
);

foreach($arResult["ITEMS"] as $key => $arItem){
	//����� ��� ����� � ��������� �� �������� ������, ����� ������ ���� ���� �� ������ ������
	if (empty($arResult["GALLERY"]["FILE_NAMES"])) {
		if(strlen($arItem["PROPERTIES"]["item_pic"]["VALUE"]) > 3){
			$file_path = $_SERVER['DOCUMENT_ROOT'].'/upload/products_foto/'.trim($arItem["PROPERTIES"]["item_pic"]["VALUE"]);
			if(is_file($file_path)){
				$arResult["GALLERY"]["FILE_NAMES"][] = $arItem["PROPERTIES"]["item_pic"]["VALUE"];
			}
		}
	} else {
		break;
	}
}

//������� ������� "� ���������" ����� ������, ��������� �� ����� ��������� ���������� ���������� Itc
$arSORT_ITEMS = array();
$LENGHT = true;
foreach($arResult["ITEMS"] as $key => $arItem){

	$arResult["ITEMS_ADD_INFO"]["IDS"][] = $arItem["ID"];//�� ���� ���������
	//���� ����� ��� �� �������� "������� ��������" ����� �������� <> ���� ���, �� ��� �����
	if($arItem["PROPERTIES"]["area"]["VALUE"] != "<>" && $arItem["PROPERTIES"]["area"]["VALUE"]!="&lt;&gt;" && (int)$arItem["PROPERTIES"]["area"]["VALUE"]>0)
	{
		$LENGHT = false;
	}
	$price = array_shift($arItem["PRICES"]); //��� ���������� �� ����
	$arSORT_ITEMS[$key] = $price["VALUE_VAT"]; //��� ���������� �� ����

	if(strlen($arItem["PROPERTIES"]["inter_pic"]["VALUE"])> 0 ) {
		//$interiorArray = explode(",", $arItem["PROPERTIES"]["INTERIOR"]["VALUE"]);
		// �� ��� ����� ���� ����������� � ;
		$interiorArray = preg_split("/[,;]+/", $arItem["PROPERTIES"]["inter_pic"]["VALUE"]);
		foreach ($interiorArray as $val) {
            if (!in_array($val, $arResult["GALLERY"]["FILE_NAMES"]))
                $arResult["GALLERY"]["FILE_NAMES"][] = $val;
		}
	}
}

//� ������� ���� ����� ����� ��� ID, ����������
$cp = $this->__component; // ������ ����������
if (is_object($cp))
{
	// ������� � arResult ���������� ��� ���� - MY_TITLE � IS_OBJECT
	$cp->arResult['ALL_ITEM_IDES'] = $arResult["ITEMS_ADD_INFO"]["IDS"];
	$cp->SetResultCacheKeys(array('ALL_ITEM_IDES'));
}
if($LENGHT)
{
	//� ����� ������ ��������� ���� ������ �� "�����"
	foreach ($arResult["ITEMS"] as $key => $item)
	{
		$arResult["ITEMS"][$key]["PROPERTIES"]["area"]["NAME"] = $arResult["ITEMS"][$key]["PROPERTIES"]["length"]["NAME"];
		$arResult["ITEMS"][$key]["PROPERTIES"]["area"]["VALUE"] = $arResult["ITEMS"][$key]["PROPERTIES"]["length"]["VALUE"];
	}
}
else
{
	$arResult["ITEMS"][0]["PROPERTIES"]["area"]["NAME"] .= ", �2";
}
/*echo "<pre>";
var_dump($arResult["GALLERY"]);
echo "</pre>";*/
//������� ������������� �� ���� ������. �.�. ��� ��������� � ���� ��������, ������ ��� catalog_group_10 �� ������� � ����������
asort($arSORT_ITEMS);

$SORTED_ITEMS = array();
foreach ($arSORT_ITEMS as $key => $val)
{
	$SORTED_ITEMS[] = $arResult["ITEMS"][$key];
}
$arResult["ITEMS"] = $SORTED_ITEMS;



foreach($arResult["GALLERY"]["FILE_NAMES"] as $key => $fileName)
{
    $fileName = trim($fileName);
	$file_path = '';
    $original = '';
    if(strlen($fileName) > 3) {
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $fileName)) {
            $original = '/images/catalog/' . $fileName;
            $file_path = $_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $fileName;
        } elseif (file_exists($_SERVER["DOCUMENT_ROOT"] . '/upload/products_foto/' . $fileName)) {
            $original = '/upload/products_foto/' . $fileName;
            $file_path = $_SERVER["DOCUMENT_ROOT"] . '/upload/products_foto/' . $fileName;
        }
    }

    //���� ���� ������ �� �����
    if(strlen($file_path) > 0){
        //������
        $destinationFile = $_SERVER["DOCUMENT_ROOT"] . '/upload/resize_cache/section/68_50_'.$fileName;
        $imgPath = '/upload/resize_cache/section/68_50_'.$fileName;
        $resized = \CFile::ResizeImageFile(
            $file_path,
            $destinationFile,
            array('width'=>68, 'height'=>50),
            BX_RESIZE_IMAGE_EXACT
        );

        if($resized){
            $arResult["GALLERY"]["SMALL"][$key] = $imgPath;
        }

        //�������� ��������
        $destinationFile = $_SERVER["DOCUMENT_ROOT"] . '/upload/resize_cache/section/400_296_'.$fileName;
        $imgPath = '/upload/resize_cache/section/400_296_'.$fileName;
        $resized = \CFile::ResizeImageFile(
            $file_path,
            $destinationFile,
            array('width'=>400, 'height'=>296),
            BX_RESIZE_IMAGE_EXACT
        );

        if($resized){
            $arResult["GALLERY"]["BIG"][$key] = $imgPath;
        }

        if(!isset($arResult["GALLERY"]["BIG"]) || !isset($arResult["GALLERY"]["SMALL"][$key])) {
            unset($arResult["GALLERY"]["SMALL"][$key]);
            unset($arResult["GALLERY"]["BIG"][$key]);
        } else {
            $arResult["GALLERY"]['ORIGINAL'][$key] = $original;
        }

    }
}