<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//echo "<pre>"; print_r($arResult); echo "</pre>";
//delayed function must return a string
/**
 * используется для формирования цепочки категорий, передаваемых в ecommerce datalayer (веб-аналитика)
 */

if(empty($arResult))
	return "";

$strReturn = '';


for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> "")
		$strReturn  .= $title.' / ';
	else
		$strReturn .= $title;
}
return $strReturn;
?>
