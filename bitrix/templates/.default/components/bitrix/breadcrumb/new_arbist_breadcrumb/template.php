<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//echo "<pre>"; print_r($arResult); echo "</pre>";
//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<ul class="breadcrumbs">';

for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> "")
		$strReturn .= '<li><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a></li>';
	else
		$strReturn .= '<li class="noLink">'.$title.'</li>';
}

$strReturn .= '</ul>';
return $strReturn;
?>
