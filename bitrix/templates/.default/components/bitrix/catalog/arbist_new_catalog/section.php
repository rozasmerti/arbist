<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h1><?$APPLICATION->ShowProperty("h1")?></h1>

<?if(!$_GET["PAGEN_1"]) $APPLICATION->ShowProperty("topSectionText")?>
<?/*$plitkaSections = $APPLICATION->IncludeComponent("itc:plitka.sections", ".default", array(
	"IBLOCK_TYPE" => "dynamic_content",
	"IBLOCK_ID" => "47",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600"
	),
	false
);*/?>

<?
CModule::IncludeModule("iblock");

$arFilter = Array(   
	"IBLOCK_ID" => 47,    
	"ACTIVE" => "Y",
	"XML_ID" => "main"
);
$db_list = CIBlockSection::GetList(Array(), $arFilter, false);  
if($ar_result = $db_list->GetNext()) {
	
	$arFilter = Array(   
		"IBLOCK_ID" => 47,    
		"ACTIVE" => "Y",
		"SECTION_ID" => $ar_result["ID"],
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("PROPERTY_IBLOCK_ID"));
	while($ar_fields = $res->GetNext()) {
		$plitkaSections[] = $ar_fields["PROPERTY_IBLOCK_ID_VALUE"];
	}
}

?>

<?/*
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"DISPLAY_PANEL" => "N",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	),
	$component
);*/?>
<br />

<?if($arParams["USE_FILTER"]=="Y"):?>
<?$APPLICATION->IncludeComponent(
	"itc:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
 		"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
		"PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
		"OFFERS_FIELD_CODE" => $arParams["FILTER_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["FILTER_OFFERS_PROPERTY_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
	),
	$component
);
?>
<br />
<?endif?>

<?

	global ${$arParams["FILTER_NAME"]};

	if(!empty(${$arParams["FILTER_NAME"]}['>=CATALOG_PRICE_10'])){
		${$arParams["FILTER_NAME"]}['>=CATALOG_PRICE_10'] = ${$arParams["FILTER_NAME"]}['>=CATALOG_PRICE_10'] - 0.5;
		${$arParams["FILTER_NAME"]}['<=CATALOG_PRICE_10'] = ${$arParams["FILTER_NAME"]}['<=CATALOG_PRICE_10'] + 0.499;
	}
	
	
?>

<?

$sectionActive = true;

if(!empty($arResult["VARIABLES"]["SECTION_ID"])) {

	$sectionDB = CIBlockSection::GetByID($arResult["VARIABLES"]["SECTION_ID"]);
	if($section = $sectionDB->GetNext()) 
		if($section['ACTIVE'] == 'N'){
			
			$section['NAME'] = trim(str_replace(
				Array(
					'(����� ���������)',
					'(�����)'
				), 
				'', 
				$section['NAME']
			));
			$sectionActive = false;		
			
			$items = CIBlockElement::GetList( 
				Array("SORT"=>"ASC"), 
				Array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => $arResult["VARIABLES"]["SECTION_ID"], '!PROPERTY_BRAND_GUID' => false), 
				false, 
				false, 
				Array('PROPERTY_BRAND_GUID')
			);
			
			if($anyItem = $items->fetch()){
				$brandID = $anyItem['PROPERTY_BRAND_GUID_VALUE'];
			}
			
			$brandName = '';
			if(intval($brandID) > 0){
				$brandDB = CIBlockElement::GetByID($brandID);
				if($brand = $brandDB->GetNext())  
					$brandName = $brand['NAME'];
			}
		
				
				$arInterior = array();
				
				$arItems = CIBlockElement::GetList( 
					Array(
						"SORT"=>"ASC"
					), 
					Array(
						'IBLOCK_ID' => $arParams["IBLOCK_ID"], 
						'SECTION_ID' => $arResult["VARIABLES"]["SECTION_ID"]
					), 
					false, 
					false, 
					Array(
						'PROPERTY_INTERIOR', 
						'NAME', 
						'IBLOCK_SECTION_ID',
						'DETAIL_PAGE_URL',
						'DETAIL_PICTURE',
						'PROPERTY_COUNTRY',
						'ID'
					)
				);
				
				$arProducts = array();
				
				while($item = $arItems->fetch()){
					if(!empty($item['PROPERTY_INTERIOR_VALUE'])) {

                        
						$interiorArray = explode(",", $item['PROPERTY_INTERIOR_VALUE']);
						foreach($interiorArray as $val) {
							if(!in_array($val, $arInterior))
								$arInterior[] = trim($val);
						}
					}
					
					$arProducts[] = $item;
					
					
				}
				?>
				
				<h3 class="link0 alt0">��������� <span class="catalog"><?=$section['NAME']?></span></h3>
				<?if(strlen($brandName) > 0):?><div class="fontReduced hl0">�������������: <?=$brandName?>, <?=$arProducts[0]['PROPERTY_COUNTRY_VALUE']?></div><?endif?>
			
				
				<div class="no-active-block">
					������������� ���� ����� ��� ��������� <b>������ �� ������������</b>.
					<?
					if(strlen($brandName) > 0) {
						?>
						<a href="/catalog/<?=$_REQUEST['IBLOCK_CODE']?>/vendor/<?=$brandName?>/">����������� ������ ����� �������������</a>
						<?
					}
					else {
						
						$parentCode = '';

						$iblocks = CIBlock::GetList(
							Array(),
							Array(
								'TYPE'=>'catalog',
								'ACTIVE'=>'Y', 
								"CODE"=>$_REQUEST['IBLOCK_CODE'],
							),
							true
						);

						if($iblock = $iblocks->Fetch()){	
							

							$menuItems = CIBlockElement::GetList( 
								Array("SORT"=>"ASC"), 
								Array('IBLOCK_ID' => 47, 'PROPERTY_IBLOCK_ID' => $iblock['ID']), 
								false, 
								false, 
								Array('IBLOCK_SECTION_ID')
							);
							
							if($menuItem = $menuItems->fetch()){
								$parentMenus = CIBlockSection::GetByID($menuItem["IBLOCK_SECTION_ID"]);
								if($parentMenu = $parentMenus->GetNext())  $parentCode = $parentMenu['CODE'];
							}
						}
						
						if(strlen($parentCode) == 0) $href="/catalog/".$_REQUEST['IBLOCK_CODE']."/"; else $href="/".$parentCode."/";
					
						?>
						<a href="<?=$href?>">����������� ������ ����� �������������</a>
						
						<?
					}
					?>
				</div>
				
				<?if(count($arInterior) > 0){
					?>
					<h4 class="alt0">� ���������</h4>
					<ul id="galleryd<?=$arResult["VARIABLES"]["SECTION_ID"]?>" class="gallery">
					<?foreach($arInterior as $pictureFullName):?>
						<?$pic = split("\.", $pictureFullName);?>

						<?if(fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$pictureFullName,'r')):?>
						<li>
							<!--68 x 50-->
							<img src="/rpic_path/c/<?=$pic[0]?>_h50_w68_crop.<?=$pic[1]?>" alt=""/>

							<div class="panel-content">
								<!--400 x 296-->
								<a class="lightboxx" rel="lightbox_gallery_interior" href="/rpic_path/c/<?=$pic[0]?>_h592_w800_auto.<?=$pic[1]?>">
									<img src="/rpic_path/c/<?=$pic[0]?>_h296_w400_crop.<?=$pic[1]?>" alt=""/>
								</a>
							</div>
						</li>
						<?endif?>
					<?endforeach;?>
					</ul>
					<?
				}
				?>
				
				<script>
				$(document).ready(function(){
				
					$('.lightboxx').lightBox();
					$('.nodecor').lightBox();
					
				});
				</script>
				
				
				<?
				if(count($arProducts) > 0){
					?>
					<div class="gap4" style="margin-top: 20px;">
						<div class="gap1 row">
							<?
							$i=0;
							foreach($arProducts as $pr){
								$detailPic = CFile::GetFileArray($pr['DETAIL_PICTURE']);
								if($i == 3) {
									echo '</div><div class="gap1 row">';
									$i=0;
								}
								?>
									<div class="item1 item">
										<a id="photo_<?=$pr["ID"]?>" class="nodecor" href="<?=$detailPic["SRC"]?>" rel="lightbox<?=$pr["ID"]?>">
											<div id="photo" class="image1 image">
												<img src="/rpic/c/<?=$detailPic["ID"]?>_h93_w136_auto.jpg" alt=""/>
											</div>
										</a>
										<!--<pre> <?print_r($pr)?> </pre>-->
										<?/*?>
										<h5 class="desc0 desc link3"><a href="/catalog/<?=$_REQUEST['IBLOCK_CODE']?>/goods/<?=$pr['ID']?>/"><?=$pr["NAME"]?></a></h5>
										<?/**/?>
										<h5 class="desc0 desc link3"><a href="/catalog/<?=$_REQUEST['IBLOCK_CODE']?>/<?=$pr['IBLOCK_SECTION_ID']?>/<?=$pr['ID']?>/"><?=$pr["NAME"]?></a></h5>
									</div>
								
								<?
								$i++;
							}
							?>
						</div>
					</div>
					
					
					<?
				}
		}
}

if(!$sectionActive) 
	echo '<div style="display: none;">';

		?><?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb", 
			"new_arbist_breadcrumb", 
			Array(),
			false
		);?><?
		if($arParams["USE_COMPARE"]=="Y")
		{
			?><?$APPLICATION->IncludeComponent(
				"bitrix:catalog.compare.list",
				"",
				Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"NAME" => $arParams["COMPARE_NAME"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
				),
				$component
			);?><br /><?
		}
		?><?

		$setFilter = $_REQUEST["set_filter"];
		if(empty($setFilter) && empty($arResult["VARIABLES"]["SECTION_ID"]) && in_array($arParams["IBLOCK_ID"], $plitkaSections))
		{

			?><?$APPLICATION->IncludeComponent(
				"itc:catalog.section.list",
				"",
				Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"ADD_SECTIONS_CHAIN" => "Y",
					"SET_TITLE" => "Y"
				),
				$component
			);?><?

		}
		else
		{
if (is_object($USER) && $USER->IsAdmin())
{
	//echo '<pre>';	print_r(${$arParams["FILTER_NAME"]});	echo '</pre>';
	//die();
}
	
			
			/**************************************************/
			/* � $arParams itc:catalog.section ������ ����    */
			/* �������� "PLITKA_SECTIONS" => $plitkaSections, */
			/**************************************************/
			?><?$APPLICATION->IncludeComponent(
				"itc:catalog.section",
				"",
				Array(
					"SHOW_COMPLECT" => $arParams["SHOW_COMPLECT"],
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
					"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					"PLITKA_SECTIONS" => $plitkaSections,
					"SID" => $arResult["VARIABLES"]["SECTION_ID"]
				),
				$component
			);?><?
		}
		?>
		
		<?
			if ($arParams["IBLOCK_ID"] == 55) // ������� �����
			{
				// ���� ��� ��������� ���� �������
				/*$arIblocks = array();
				$res = CIBlock::GetList(	
					Array(), 	
					Array(		
						'TYPE'=>'catalog', 		
						'SITE_ID'=>SITE_ID, 		
						'ACTIVE'=>'Y',
						'!ID' => array(12,29, $arParams['IBLOCK_ID']) // 12,29 - �������� ����������� ������
					), 
					false
				);
				while($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;
				
				foreach($arIblocks as $iblock)
				{	
					$arResult['NAV_BUTTONS'][] = array(
						'PATH' => '/catalog/'.$iblock['CODE'].'/',
						'NAME' => $iblock['NAME']
					);
				}*/
				
				if(!empty($arResult['NAV_BUTTONS'])) {
					?>
					<div style="width: 100%; text-align: center; margin-top: 30px;">
						<?
						foreach($arResult['NAV_BUTTONS'] as $button) {
							/*?>
							<div class="new_button">
								<button onclick="window.location.href='<?=$button['PATH']?>'" type="button"><?=$button['NAME']?></button>
							</div>
							<?*/
							?>
							<div class="new_button">
								<a href="<?=$button['PATH']?>" class="new_a"><?=$button['NAME']?></a>
							</div>
							<?
						}
						?>
						<div style="clear: both;"></div>
					</div>
					<?
				}
			}
		?>
		
		<?

		$APPLICATION->IncludeComponent(
			"itc:catalog.addchain", 
			".default", 
			array(
				"DISPLAY_IBLOCK_CHAIN" => (empty($arResult["VARIABLES"]["SECTION_ID"]) /*&& empty($setFilter)*/) ? "N": "Y",
				"PROP_IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"PROP_IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"],
				"SECTION_ID" => intval($arResult["VARIABLES"]["SECTION_ID"]),
				"ELEMENT_ID" => NULL,
				"IBLOCK_TYPE" => "dynamic_content",
				"IBLOCK_ID" => "47",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
			),
			$component
		);?><?
		if(!$_GET["PAGEN_1"]) $APPLICATION->ShowProperty("botSectionText");
	if(!$sectionActive) 
	echo '</div>';
?>
