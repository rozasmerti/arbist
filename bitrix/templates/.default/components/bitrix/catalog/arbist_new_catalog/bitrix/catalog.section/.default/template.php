<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? CModule::IncludeModule('itconstruct.resizer'); ?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?//echo "<pre>"; print_r($arResult); echo "</pre>";?>
<!--���� �����������-->
<?if(!empty($arResult["GALERY"])):?>
<div class="gap4 gal">
	<ul id="gallery">
	<?foreach($arResult["GALERY"] as $pictureId):?>
		<li>
			<!--68 x 50-->
			<img src="/rpic/c/<?=$pictureId?>_h50_w68_crop.jpg" alt=""/>

			<div class="panel-content">
				<!--400 x 296-->
				<img src="/rpic/c/<?=$pictureId?>_h296_w400_crop.jpg" alt=""/>
			</div>
		</li>
	<?endforeach;?>
	</ul>
</div>
<?endif;?>
<!---->
<div class="gap4" >
		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
		<?if($cell%$arParams["LINE_ELEMENT_COUNT"] == 0):?>
		<div class="gap1 row">
		<?endif;?>

		<div class="item1 item" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
			<div class="image1 image">
		<?//foreach($arElement["PRICES"] as $code=>$arPrice):?>
		<?$arPrice = array_shift($arElement["PRICES"])?>
			<?if($arPrice["PRINT_DISCOUNT_VALUE"] < $arPrice["PRINT_VALUE"]):?>
				<div class="labels">
					<div class="lab"><span>������</span></div>
				</div>
			<?endif;?>
				<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
                    <a href="<?=$arElement["DETAIL_PAGE_URL"]?>">
                        <img src="<?=itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'crop', 136, 93);?>" alt="<?=$arElement["NAME"];?>"/>
                    </a>
				<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'crop', 136, 93);?>" alt="<?=$arElement["NAME"];?>"/></a>
				<?else:?>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="/img/noimg0.jpg" alt="" width="136" height="93"/></a>
				<?endif?>
			</div>
			<h5 class="desc0 desc link3"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></h5>
			<div class="bot">
				<div class="aligned row-b">
					<div class="item">
						<div class="price0 price">
							<span class="val">
								<?if($arPrice["PRINT_DISCOUNT_VALUE"] < $arPrice["PRINT_VALUE"]) 
									echo $arPrice["PRINT_DISCOUNT_VALUE"];
								else
									echo $arPrice["PRINT_VALUE"];
								?>
							</span>
						</div>
					</div>
					<div class="item">
						<form action="#">
							<fieldset>
								<div class="button4 button">
									<i class="helper"></i>
									<button type="button" onclick="location.href='<?=$arElement["ADD_URL"]?>'"><i></i></button>
								</div>
							</fieldset>
						</form>
					</div>
					<span class="under"></span>
				</div>
				<?if($arPrice["PRINT_DISCOUNT_VALUE"] < $arPrice["PRINT_VALUE"]):?>
				<div class="old">
					<span class="price1 price"><span class="val"><?=$arPrice["PRINT_VALUE"]?></span></span> � ������ ����
				</div>
				<?endif;?>
			<?//endforeach;?>
			</div>
		</div>

		<?$cell++;
		if($cell%$arParams["LINE_ELEMENT_COUNT"] == 0):?>
			</div>
		<?endif?>

		<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>

		<?if($cell%$arParams["LINE_ELEMENT_COUNT"] != 0):?>
			</div>
		<?endif?>

</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<?=$arResult["DESCRIPTION"]?>
