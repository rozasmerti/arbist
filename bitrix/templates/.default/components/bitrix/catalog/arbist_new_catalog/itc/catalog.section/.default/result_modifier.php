<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

$currencyArray = array("RUB", "USD", "EUR");
foreach ($currencyArray as $val) {
    $result = CCurrencyLang::GetByID($val, "ru");
    $arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}

$brandID = false;
//D'L ��� ������ ������ ��������� � ������� ��� ����� ����
$arResult["ITEMS_ADD_INFO"]["IDS"] = array();
foreach ($arResult["ITEMS"] as $sid => $sElements) { //"PAGE_SECTIONS"
    //echo "<pre>"; print_r($sElements[0]); echo "</pre>";
    $arResult["ITEMS_ADD_INFO"]["IDS"][] = $sElements["ID"];
    $arResult["SECTIONS_COUNTRY"][$sid] = $sElements[0]["PROPERTIES"]["COUNTRY"]["VALUE"];
    $arResult["SECTIONS_BRAND_GUID"][$sid] = $sElements[0]["PROPERTIES"]["BRAND_GUID"]["VALUE"];
    $brandID = $sElements[0]["PROPERTIES"]["BRAND_GUID"]["VALUE"];


    //������ ��������� ����
    foreach ($sElements as $key => $arElement) {
        if (count($arElement['PRICES']) > 1) {
            if (
                isset($arElement['PRICES']['���������'])
                && isset($arElement['PRICES']['���� �� �����'])
            ) {
                $arResult["ITEMS"][$sid][$key]["PROPERTIES"]["STRIP_COUNT"]["VALUE"] = intval($arElement['PRICES']['���������']['DISCOUNT_VALUE']);
                $arResult["ITEMS"][$sid][$key]["OLD_PRICE"] = true;
                unset($arResult["ITEMS"][$sid][$key]['PRICES']['���������']);
            }
        }
    }
}
$arBrandGuidFilter = Array(
    "IBLOCK_CODE" => "brand_guid",
    "ACTIVE" => "Y",
    "ID" => $arResult["SECTIONS_BRAND_GUID"]
);
$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME", "CODE"));
while ($ar_fields = $res->GetNext()) {
    $brands[$ar_fields["ID"]] = $ar_fields; /**/
}
foreach ($arResult["SECTIONS_BRAND_GUID"] as $id => $value) {
    $arResult["SECTIONS_BRAND_GUID"][$id] = $brands[$value];
}


CModule::IncludeModule("catalog");
global $USER;
$userGroups = $USER->GetUserGroupArray();


$discountFlag = 0;
foreach ($GLOBALS["GROUP_STATUS"] as $groupId => $val) {
    if (in_array($groupId, $userGroups)) {
        $discountFlag = 1;
        break;
    }
}
/**
 * 2017-04-09, sumato.shigoto@gmail.com
 * ������! ���������, ��� � ����� /bitrix/php_interface/config.php
 * �������� ��� ������ ����� ������������� ���������� ���������, �������������� �������� ������ )
 * � � ���� �������, �� ��������� ����, �����������, ������ �� ������ �������� ������������ � ������ ������.
 * �, ���� �� ������, �� ������������ ����, ��� � ������������ ��� ������.
 * ����� �������, ����� ������-�� �� 2012�� ����. ������ �� ����, ������ ������� � config.php
 * ������ ��� ��, ��� ��� ������������ ����� ����� �� 4 ���������.
 * ������� �������� �� ����� ������ ���, �� ��� ����������� ������� �� ���������� ���������� ����������� ������
 * �� �������� ���������.
 * todo  |  ���� ���������� �������� ����, ������ ����������� ��������, ���� �� ������,
 * todo  |  � ����� �� ������� ������������ � ��� ������.
 *
 * todo  |  20.05.2017 , sumato.shigoto@gmail.com
 * todo  |  ����� ������ �� bitrix/components/itc/catalog.filter/component.php
 */

if ($discountFlag == 1) {
    foreach ($arResult["ITEMS"] as $sid => $sElements) {
        foreach ($sElements as $cell => $arElement) {
            $arResult["ITEMS"][$sid][$cell]["BASE_PRICES"] = $arResult["ITEMS"][$sid][$cell]["PRICES"]; //
            $optimalPrice = CCatalogProduct::GetOptimalPrice($arElement["ID"], 1, $userGroups);
            $arResult["ITEMS"][$sid][$cell]["PRICES"] = array(
                array(
                    "VALUE" => $optimalPrice["PRICE"]["PRICE"],
                    "DISCOUNT_VALUE" => $optimalPrice["DISCOUNT_PRICE"],
                    "CURRENCY" => $optimalPrice["PRICE"]["CURRENCY"],
                ),
            );
        }
    }
}

// ���� �� �������� ���������
if (count($arResult["SECTIONS"]) == 1) {

    $iblockName = false;
    $res = CIBlock::GetByID($arResult['IBLOCK_ID']);
    if ($ar_res = $res->GetNext())
        $iblockName = $ar_res['NAME'];

    if ($arResult["SECTIONS_BRAND_GUID"][$arResult['ID']] && $iblockName && !empty($arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['CODE'])) {
        $arResult['NAV_BUTTONS'][] = array(
            'PATH' => str_replace('-', '_', str_replace(' ', '_', strtolower('/catalog/' . $arResult['IBLOCK_CODE'] . '/vendor/' . $arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['CODE'] . '/'))),
            //'NAME' => '��� ��������� '.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['NAME'].' '.mb_strtolower($iblockName)
            'NAME' => $iblockName . ' ' . $arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['NAME']
        );
    }

    if ($brandID && $arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]) {
        // ���� ��� ��������� ���� �������
        $arIblocks = array();
        $res = CIBlock::GetList(
            Array(),
            Array(
                'TYPE' => 'catalog',
                'SITE_ID' => SITE_ID,
                'ACTIVE' => 'Y',
                '!CODE' => $arResult['IBLOCK_CODE'],
                '!ID' => array(12, 29) // �������� ����������� ������
            ),
            false
        );
        while ($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;

        //��� ������� ��������� ���� �� ��� ������ ����� ������
        foreach ($arIblocks as $iblock) {

            $res = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock['ID'], "CODE" => 'BRAND_GUID'));
            if ($res->fetch()) {
                $res = CIBlockElement::GetList(
                    Array("SORT" => "ASC"),
                    Array('IBLOCK_ID' => $iblock['ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_GUID' => $brandID),
                    false,
                    false,
                    Array('ID')
                );
                if ($res->SelectedRowsCount()) {
                    global $USER;

                    if (!empty($arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['CODE']))
                        $arResult['NAV_BUTTONS'][] = array(
                            'PATH' => str_replace('-', '_', str_replace(' ', '_', strtolower('/catalog/' . $iblock['CODE'] . '/vendor/' . $arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['CODE'] . '/'))),
                            //'NAME' => '��� ��������� '.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['NAME'].' '.mb_strtolower($iblock['NAME'])
                            'NAME' => $iblock['NAME'] . ' ' . $arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['NAME']
                        );
                }
            }
        }

    }

}

//D'L � ��� � ������� �������� ���, ����������, � ������ �� ������������ ���������?
//������ ��� ��� (������� ��������) ������������� � ������ ������ ���� ������� � ������� ���� ������ � �� ���������

$res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arResult["ITEMS_ADD_INFO"]["IDS"]), false, false, array("ID", "IBLOCK_ID", "PROPERTY_" . PROPERTY_CODE));
while ($ar = $res->Fetch()) {
    if (strlen($ar["PROPERTY_" . PROPERTY_CODE . "_VALUE"]) > 2) {
        $arResult["ITEMS_ADD_INFO"]["ART"][$ar["ID"]] = $ar["PROPERTY_" . PROPERTY_CODE . "_VALUE"];
    }
}

if($USER->IsAdmin()) {
 //   echo "PRICES<pre>" . print_r($arResult['PRICES']) . "</pre>";
 //   echo "PRICES<pre>" . print_r($arResult["ITEMS"]) . "</pre>";
}

