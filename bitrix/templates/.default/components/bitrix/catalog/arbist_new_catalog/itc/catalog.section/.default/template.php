<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? CModule::IncludeModule('itconstruct.resizer'); ?>
<?global $USER?>
<?
//if ($USER-IsAdmin()){echo '<pre>'; print_r($arResult); echo '</pre>';}
?>
<div class="catalog-area">
<?if(!empty($arResult["SECTIONS"])) { ?> <!--$arResult["PAGE_SECTIONS"]-->
<? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
    <?= $arResult["NAV_STRING_TOP"] ?><br/>
<?endif; ?>

<? $sectionsCount = count($arResult["SECTIONS"]) ?> <!--$arResult["PAGE_SECTIONS"]--> <?
foreach ($arResult["ITEMS"] as $sid => $sElements): ?> <!--$arResult["PAGE_SECTIONS"]-->
<?
$message = NULL;
if (count($sElements) != $arResult["SECTION_ELEMENT_COUNT"][$sid])
    $message = "Y";
?>
<? if ($sectionsCount == 1):?>
    <a name="sectionTop"></a>
    <div class="gap1" style="position: relative;">


        <h3 class="link0 alt0">
            ��������� <span class="catalog"><?= $arResult["SECTION_NAME"][$sid] ?></span>
        </h3>
        <? if ($arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"]) {
            ?>
            <div class="fontReduced hl0">������������� <a class="light-grey-link vendor-link-on-top"
                                                          href="<?= $arResult["SECTION_PARENT_PATH"][$sid] . 'vendor/' . $arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"] ?>/"><?= $arResult["SECTIONS_BRAND_GUID"][$sid]["NAME"] ?></a>, <?= $arResult["SECTIONS_COUNTRY"][$sid] ?>
            </div>
        <?
        } ?>
    </div>
<?
else:?>
    <a name="sectionTop"></a>
    <div class="gap1" style="position: relative;">
        <h3 class="link0 alt0">
            ���������
            <a href="<?= $arResult["SECTION_PATH"][$sid] ?>">
                <?= $arResult["SECTION_NAME"][$sid] ?>
            </a>
        </h3>
        <? if ($arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"]) {
            ?>
            <div class="fontReduced hl0">������������� <a class="light-grey-link vendor-link-on-top"
                                                          href="<?= $arResult["SECTION_PARENT_PATH"][$sid] . 'vendor/' . $arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"] ?>/"><?= $arResult["SECTIONS_BRAND_GUID"][$sid]["NAME"] ?></a>, <?= $arResult["SECTIONS_COUNTRY"][$sid] ?>
            </div> <!--���������, �������������-->
        <?
        } ?>
    </div>
<?endif; ?>
<? if ($message == "Y"):?>
    <div class="noted">
        <div class="note link4">
            �������� ������ ������, ��������������� ������ �������.
            <a href="<?= $arResult["SECTION_PATH"][$sid] ?>">�������� ��� ���������</a>
        </div>
        <div style="width: 100%; height: 10px;"></div>
    </div>
<?endif; ?>

<?
// ��� ��������������� ��������-�������� ������ ����������� ��������� ������������ ������ ������
if (in_array('5', CUser::GetUserGroup($USER->GetID())) || in_array('1', CUser::GetUserGroup($USER->GetID()))) :
    ?>
    <div style="overflow: auto; padding-bottom: 10px;">
        <? CMain::IncludeFile('/inc/avail_form.php', array(
            'avail_type' => 'section',
            'id' => $sid,
            'date' => '',
            'note' => '',
            'element' => null
        ), array('SHOW_BORDER' => false)); ?>
        <? CMain::IncludeFile('/inc/price_form.php', array(
            'price_type' => 'section',
            'id' => $sid,
            'date' => '',
            'prices' => null,
            'element' => null
        ), array('SHOW_BORDER' => false)); ?>
    </div>
<? endif; ?>

<? if (!empty($arResult["DESCRIPTIONS"][$sid])):?>
    <div class="top-text" style="overflow: hidden;">
        <?= $arResult["DESCRIPTIONS"][$sid] ?>
    </div>
<?endif; ?>

<!--���� �����������-->
<?
$totalCount = count($arResult["GALLERY"][$sid]);
$realCount = 0;
foreach ($arResult["GALLERY"][$sid] as $key => $pictureFullName) {
    if (fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $pictureFullName, 'r'))
        $realCount++;
}

// ��� �� ������ ������� ��� ����� � ������� ���������
$labels = array();
foreach ($sElements as $cell => $arElement)
    if (($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "987617dd-090b-11e0-87a1-00166f1a5311"
            || $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "d536afec-2269-11e0-9088-0018f34aa38f"
            || $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "a379a7ca-9a4a-11e0-b2a0-0018f34aa38f")
        && !in_array($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE"], $labels)
    )
        $labels[] = $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE"];
?>

<? if (!empty($arResult["GALLERY"][$sid]) /*&& $sectionsCount == 1*/ && in_array($arParams["IBLOCK_ID"], $arParams["PLITKA_SECTIONS"]) && $realCount != 0):?>

    <h4 class="alt0">� ���������</h4>

    <div class="gap4 gal" style="height:318px">
        <? if (!empty($labels)) : ?>
            <div class="labels">
                <div class="lab"><span><?= implode(', ', $labels) ?></span></div>
            </div>
        <? endif; ?>

        <div class="gallery-loader-img"
             style="display:none; position:absolute; left:150px; top:50px; cursor : url('img/lens.cur'), auto;"><img
                src="/images/ajax-loading_big.gif"/></div>

        <ul id="galleryd<?= $sid ?>" class="gallery">
            <? foreach ($arResult["GALLERY"][$sid] as $pictureFullName):?>
                <? $pic = split("\.", $pictureFullName); ?>
                <? if (fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $pictureFullName, 'r')):?>
                    <li>
                        <!--68 x 50-->
                        <img src="/rpic_path/c/<?= $pic[0] ?>_h50_w68_crop.<?= $pic[1] ?>" alt=""/>

                        <div class="panel-content">
                            <!--400 x 296-->
                            <a class="lightboxx" rel="lightbox_gallery_interior"
                               href="/rpic_path/c/<?= $pic[0] ?>_h592_w800_auto.<?= $pic[1] ?>"
                               style="cursor:url('/img/lens.cur'), auto;">
                                <img src="/rpic_path/c/<?= $pic[0] ?>_h296_w400_portrait.<?= $pic[1] ?>"
                                     alt="<? echo $arResult["NAME"] . ' ' . $arResult["~NAME"]; ?>"/>
                            </a>
                        </div>
                    </li>
                <?endif ?>
            <?endforeach; ?>
        </ul>

        <script>
            $(document).ready(function () {

                $('.lightboxx').lightBox();

            });
        </script>

    </div>
<?endif; ?>
<!---->

<div class="gap4 b-gap4">

    <? foreach ($sElements as $cell => $arElement): ?>

<?

$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
?>
<? if ($cell % $arParams["LINE_ELEMENT_COUNT"] == 0): ?>

    <div class="gap1 row">
        <?endif;
        ?>
        <div class="item1 item b-item3-on-row" id="<?= $this->GetEditAreaId($arElement['ID']); ?>">
            <?
            $arPrice = array_shift($arElement["PRICES"]);

            if($arPrice['DISCOUNT_VALUE'] > 20 && $arPrice['DISCOUNT_VALUE'] < $arPrice['VALUE'] ){
                $actionPrice = true;
                $priceStyle = "action-category-price_bg";
            } else {
                $actionPrice = false;
                $priceStyle = "minimal-category-price_bg";
            }

            ?>
            <? if (isset($arResult["ITEMS_ADD_INFO"]["ART"][$arElement['ID']]) && strlen($arResult["ITEMS_ADD_INFO"]["ART"][$arElement['ID']]) > 2):?>
                    ���: <?= $arResult["ITEMS_ADD_INFO"]["ART"][$arElement['ID']] ?>
            <?endif; ?>
            <a name="product<?= $arElement["ID"] ?>"></a>
            <?

            ?>
            <? if (is_array($arElement["DETAIL_PICTURE"])): ?>
            <a id="photo_<?= $arElement["ID"] ?>" class="nodecor" href="<?= $arElement["DETAIL_PICTURE"]["SRC"] ?>"
               rel="lightbox<?= $arElement["ID"] ?>">
                <? elseif (is_array($arElement["PREVIEW_PICTURE"])): ?>
                <a id="photo_<?= $arElement["ID"] ?>" class="nodecor" href="<?= $arElement["PREVIEW_PICTURE"]["SRC"] ?>"
                   rel="lightbox<?= $arElement["ID"] ?>">
                    <? else: ?>
                    <a id="photo_<?= $arElement["ID"] ?>" class="nodecor" href="<?= $arElement["DETAIL_PAGE_URL"] ?>"
                       rel="lightbox<?= $arElement["ID"] ?>">
                        <?endif;
                        ?>
                        <div id="photo" class="image1 image b-light-border ceramic-img">

                            <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                <div class="labels">
                                    <div class="lab"><span>������</span></div>
                                </div>
                            <?
                            elseif ($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "987617dd-090b-11e0-87a1-00166f1a5311" ||
                                $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "d536afec-2269-11e0-9088-0018f34aa38f" ||
                                $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "a379a7ca-9a4a-11e0-b2a0-0018f34aa38f"
                            ):?>
                                <!--div class="labels">
							<div class="lab"><span><?= $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE"] ?></span></div>
						</div-->
                            <?endif; ?>
                            <? if (is_array($arElement["PREVIEW_PICTURE"])):?><?//=$arElement["DETAIL_PAGE_URL"]
                                ?>
                                <? if ($arElement["PREVIEW_PICTURE"]["HEIGHT"] > 93 || $arElement["PREVIEW_PICTURE"]["WIDTH"] > 136):?>
                                <? if (($arElement["PREVIEW_PICTURE"]["HEIGHT"] - 93) > ($arElement["PREVIEW_PICTURE"]["WIDTH"] - 136)):?>
                            <img
                                src="<?= itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'height', null, 152); ?>"
                                alt="<?= $arElement["NAME"]; ?>"/>
                            <? else:?>
                            <img src="<?= itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'width', 185); ?>"
                                 alt="<?= $arElement["NAME"]; ?>"/>
                            <?endif;
                                ?>
                            <? else:?>
                            <img src="<?= itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'auto', 185, 152); ?>"
                                 alt="<?= $arElement["NAME"]; ?>"/>
                            <?endif;
                            ?>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#photo_<?=$arElement["ID"]?>').lightBox();
                                    });
                                </script>
                            <? elseif (is_array($arElement["DETAIL_PICTURE"])): ?>
                            <? if ($arElement["DETAIL_PICTURE"]["HEIGHT"] > 93 || $arElement["DETAIL_PICTURE"]["WIDTH"] > 136): ?>
                            <? if (($arElement["DETAIL_PICTURE"]["HEIGHT"] - 93) > ($arElement["DETAIL_PICTURE"]["WIDTH"] - 136)): ?>
                            <img src="<?= itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'height', null, 93); ?>"
                                 alt="<?= $arElement["NAME"]; ?>"/>
                            <? else:?>
                            <img src="<?= itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'width', 185); ?>"
                                 alt="<?= $arElement["NAME"]; ?>"/>
                            <?endif;
                                ?>
                            <? else:?>
                            <img src="<?= itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'auto', 185, 152); ?>"
                                 alt="<?= $arElement["NAME"]; ?>"/>
                            <?endif;
                            ?>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#photo_<?=$arElement["ID"]?>').lightBox();
                                    });
                                </script>
                            <? else: ?>
                            <img src="/img/noimg0.jpg" alt="" width="136" height="93"/><?/*</a>*/
                                ?>
                            <?endif ?>
                        </div>
                    </a>
                    <h5 class="desc0 desc link3"><a
                            href="<?= $arElement["DETAIL_PAGE_URL"] ?>"><?= $arElement["NAME"] ?></a></h5>
                    <div class="bot">
                        <div class="aligned row-b">
                            <? if (canBuyElement($arElement) && (intVal($arPrice["VALUE"]) > 20)) { ?>
                                <div class="item"><?
                                    if (!empty($arPrice["DISCOUNT_VALUE"]) && !empty($arPrice["VALUE"]) && $arPrice["VALUE"] > 5) {
                                        ?>
                                    <div class="price0 price <?= $priceStyle ?>">
                                        <span class="val">
											<? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
                                                $val = round($arPrice["DISCOUNT_VALUE"]);
                                                if (!empty($arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"]))
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]) . "&nbsp;��&nbsp;" . $arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"];
                                                else
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]);
                                                echo str_replace("���", '<span class="b-rub">�</span>', $priceVal);
                                            } else {
                                                $val = round($arPrice["VALUE"]);
                                                if (!empty($arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"]))
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]) . "&nbsp;��&nbsp;" . $arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"];
                                                else
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]);
                                                echo str_replace("���", '<span class="b-rub">�</span>', $priceVal);;
                                            }
                                            ?>
										</span>
                                        </div><?
                                    }
                                    ?></div>
                                <?
                                if (isset($arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"])) {
                                    $dataVendor = ' data-vendor-code="' . $arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"] . '" data-vendor-name="' . $arResult["SECTIONS_BRAND_GUID"][$sid]["NAME"] . '"  data-vendor-id="' . $arResult["SECTIONS_BRAND_GUID"][$sid]["ID"] . '"';
                                } else {
                                    $dataVendor = '';
                                }
                                ?>
                                <div class="item">
                                    <form action="#">
                                        <fieldset>
                                            <div id="buy<?= $arElement["ID"] ?>" class="button4 button">
                                                <i class="helper"></i>
                                                <?
                                                $arElement["ADD_URL"] .= "#product" . $arElement["ID"];
                                                ?>
                                                <button type="button"
                                                        data-id="<?= $arElement['ID'] ?>" <?= $dataVendor; ?>
                                                        data-price="<?= round($arPrice["VALUE"]); ?>"
                                                        data-name="<?= $arElement["NAME"] ?>"
                                                        onclick="addToCart(this);"><i></i></button>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            <?
                            } else { ?>
                                <div class="item">
                                    <div class="new_button get_price">
                                        <a class="a-button" href="/price-request/?productId=<?= $arElement['ID'] ?>"
                                           type="button" target="_blank">�������� ������� � ����</a>
                                    </div>
                                </div>
                            <?
                            } ?>
                            <span class="under"></span>
                        </div>
                        <?
                        /*
                        if (($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "987617dd-090b-11e0-87a1-00166f1a5311"
                                || $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "d536afec-2269-11e0-9088-0018f34aa38f"
                                || $arElement["OLD_PRICE"] == true )
                            && ($arElement["PROPERTIES"]["STRIP_COUNT"]["VALUE"] != '')
                            && intval($arElement["PROPERTIES"]["STRIP_COUNT"]["VALUE"])) {
                        */
                        if($actionPrice) {
                         ?>
                        <div class="old">
							<span class="price1 price"><span
                                    class="val"><?=round($arElement['CATALOG_PRICE_10']);?></span>
							<span class="b-rub b-old-ruble">�</span> &mdash; ������ ����</span>
                        </div>
                        <?
                        }
                        ?>
					</div>
				</div>

				<?$cell++;
				if($cell%$arParams["LINE_ELEMENT_COUNT"] == 0):?>
					</div>
				<?endif?>

				<?endforeach;?>

				<?if($cell%$arParams["LINE_ELEMENT_COUNT"] != 0){
				    echo '</div>';
                }?>

		</div>
	<?endforeach;?>

	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING_BOT"]?>
	<?endif;?>
	
	<?if($sectionsCount == 1):?>
	<?
	if(!empty($arResult['NAV_BUTTONS'])) {
		?>
		<div style="width: 100%; text-align: center; margin-bottom: 20px;">
			<?
			foreach($arResult['NAV_BUTTONS'] as $button) {
				?>
				<div class="new_button<?/* width50*/?>">
                    <?/*<button onclick="window.location.href='<?=$button['PATH']?>'" type="button"><?=$button['NAME']?></button>*/?>
                    <a class="a-button" href="<?=ToLower(TranslateIt($button['PATH']))?>" type="button"><?=$button['NAME']?></a>
				</div>
				<?
			}
			?>
			<div style="clear: both;"></div>
		</div>
		<?
	}	
	?>
	<?endif;?>
<?
} else {
	echo "�� ������� ������� ������ �� �������. �������� �� ������ ���������� �����, ������ ����� ���������. ���������� �������� �������� ������.";
}
?>
<script>
    var cats = [];
    var breadcrumb = '';
    $('ul.breadcrumbs li').each(function(i, val){ if(i > 0) { cats.push($(this).text()) }});
    cats.pop();
    var breadcrumb = cats.join('/','');


	function addToCart(obj) {
        var item = $(obj);

		var filters = window.location.search.substring(1);
		var pathname = window.location.pathname;

		var blockCode = '<?=$arParams["IBLOCK_ID"]?>';
		var vendorName = item.data('vendor-name');
		var vendorId   = item.data('vendor-id');
		var price      = item.data('price');
		var pname      = item.data('name');
		var pid        = item.data('id');

		var newHref = pathname +'?action=ADD2BASKET&id='+pid+'&IBLOCK_CODE='+blockCode;

		if(vendorId > 0) {
			newHref+='&BRAND_ID='+vendorId;
		}

		if(filters.length>0) {
			newHref+='&'+filters;
		}

		newHref+='#product'+pid;

		window.dataLayer.push({
			"ecommerce": {
				"add": {
					"products": [
						{
							"id": pid,
							"name": pname,
							"price": price,
							"brand": vendorName,
							"category": window.breadcrumb,
							"quantity": 1
						}
					]
				}
			}
		});
		window.location.href = newHref.replace(/amp;/g,'');
	}
</script>
</div>