<?
CModule::IncludeModule("sale");
$arBasketItems = array();

$dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE", 
              "PRODUCT_ID", "QUANTITY", "DELAY", 
              "CAN_BUY", "PRICE", "WEIGHT")
    );
$busketItemsArray = array();
while ($arItems = $dbBasketItems->Fetch()) {
    $busketItemsArray[] = $arItems["PRODUCT_ID"];
}
foreach($arResult["ELEMENTS"] as $sElements) {
    foreach($sElements as $arElementID) {
        if(in_array($arElementID, $busketItemsArray)){
?>
            <script>
				//$('#buy<?=$arElementID?>').removeClass("button4 button");
				$('#buy<?=$arElementID?>').html('<div class="in-basket-yet"></div>');
				$('#buy<?=$arElementID?>').closest('.aligned').addClass('in-basket-yet-margin'); 
			</script>
<?
        }
    }
}
?>