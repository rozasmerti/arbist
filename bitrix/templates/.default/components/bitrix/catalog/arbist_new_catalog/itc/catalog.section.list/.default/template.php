<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? CModule::IncludeModule('itconstruct.resizer'); ?>

<?
   $sSectionName = $APPLICATION->GetTitle(false); 
?>


<div class="b-gap4">
<?
global $USER;
if(!empty($arResult["NAV_TOP"]))
	echo $arResult["NAV_TOP"];


$i = 0;

foreach($arResult["SECTIONS"] as $arSection){
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));

if($USER->IsAdmin()){
  //  print_r($arSection);
}
?>
	<?if($i == 0) {?> 
		<div class="gap1 row"> 
	<?}?>
			<div class="item1 item b-item3-on-row" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
				<div class="image1 image b-light-border ceramic-img">
					 <? if (!empty($arSection['LABELS'])) : ?>
					 <div class="labels">
						 <div class="lab"><span><?= implode(', ', $arSection['LABELS']) ?></span></div>
					 </div>
					 <? endif; ?>
					 
					<a  href="<?=$arSection["SECTION_PAGE_URL"]?>">
					<?if(!empty($arSection["PICTURE_PATH"]) &&  file_exists($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$arSection["PICTURE_PATH"])):?> <?/*PICTURE*/?>
						<?$pic = split("\.", $arSection["PICTURE_PATH"]);?>
                        <img src="/rpic_path/c/<?=$pic[0]?>_h152_w185_crop.<?=$pic[1]?>" alt="<?=$arSection["NAME"]?>"/>
					<?else:?>
						<img src="/img/noimg0.jpg" alt="" width="136" height="93"/>
					<?endif;?>
					</a> 
				</div>
				<div class="desc">
                    <a class="light-red-link" href="<?=$arSection["SECTION_PAGE_URL"]?>"><span class="small-seo-text"><?=$sSectionName;?></span><br/><?=$arSection["NAME"]?></a>

                    <div class="minimal-category-price category-vendor-title hl0"><? if (!empty($arSection["BRAND"]) ) { ?>�������������: <a class="light-grey-link" href="<?=strtok($_SERVER['REQUEST_URI'], '?');?>vendor/<?=$arSection["BRAND_CODE"];?>/"><?=$arSection["BRAND"];?></a><?}?></div>
                    <? if (!empty($arSection["MIN_PRICE"]) && ($arSection['PROPERTY_CHANGE_UNIT'] == '�2' || $arSection['PROPERTY_CHANGE_UNIT'] == '���')) { ?> <div class="minimal-category-price minimal-category-price_bg hl0">�� <?=round($arSection["MIN_PRICE"]);?> <span class="b-rub">�</span> �� <?=$arSection['PROPERTY_CHANGE_UNIT'];?></div> <?}?>
				</div>
			</div>
	<?if($i == 2) {
		$i = 0;
	?>
		</div> 
	<?
	} else {
		$i++;
	}
	?>
<?
}
if($i != 0) {
?>
	</div>
<?
}
?>
</div>
<?
if(!empty($arResult["NAV_BOT"]))
	echo $arResult["NAV_BOT"];
?>
