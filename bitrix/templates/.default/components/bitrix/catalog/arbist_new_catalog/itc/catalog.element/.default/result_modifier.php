<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
CModule::IncludeModule('iblock');
?>
<?/*
//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
//echo $_REQUEST["BRAND_ID"];
$res = CIBlock::GetList(Array("ID"=>"DESC"), Array("ACTIVE"=>"Y", "CODE"=>$_REQUEST["IBLOCK_CODE"]), false);
if($ar_res = $res->Fetch()){
	$IBLOCK_ID = $ar_res["ID"];
}
$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ID'=>$_REQUEST["BRAND_ID"], 'GLOBAL_ACTIVE'=>'Y');  
$db_list = CIBlockSection::GetList(Array('ID'=>'DESC'), $arFilter, false, array('UF_GALLERY'));
if($section = $db_list->GetNext())
	$arResult["GALLERY"] = $section["UF_GALLERY"];
//echo "<pre>"; print_r($section); echo "</pre>";
*/?>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}

$depressedTypes = Array(
    "������",
    "������� ��������",
    "�������",
    "������",
    "�������",
    "������� �������",
    "������ ���������",
    "�����������",
    "����",
    "������",
    "�������",
    "�����",
    "����� ���������",
    "������",
    "����� ����������",
    "������� �������",
    "������� ���������",
    "������������ �����������",
    "�������",
    "�������",
    "������������",
    "�������� �������",
    "������, ������������",
    "�������-������������",
    "�������",
    "������",
    "��������"
);
?>

<?
global $USER;
if(in_array($arParams["IBLOCK_ID"], Array(14,20,22,23,24,82))) {
    /**
     * 01.08.2017 , sumato.shigoto@gmail.com
     * todo ���� ������� �������������� ���� ��� ����������, ����� ����� ���� ��������� ����������� ����� ������ � �������
     * ��������� � ���� ����� ���� ����������� ����������� ����� ������ � 2500�. ��� ��� ������,
     * ����������� ��� ������, � �����.
     */
    $arResult["DISPLAY_PROPERTIES"]["MINIMAL_ORDER"]["NAME"] = "���. ����� ��";
    $arResult["DISPLAY_PROPERTIES"]["MINIMAL_ORDER"]["DISPLAY_VALUE"] = "2500 �.";
}
if(!in_array($arParams["IBLOCK_ID"], $arParams["PLITKA_SECTIONS"])) {
	 if(!empty($arResult["PROPERTIES"]["INTERIOR"]["VALUE"])) {
		//$interiorArray = explode(",", $arResult["PROPERTIES"]["INTERIOR"]["VALUE"]);
		// �� ��� ����� ���� ����������� � ;
		$interiorArray = preg_split( "/[,;]+/", $arResult["PROPERTIES"]["INTERIOR"]["VALUE"]);
		foreach($interiorArray as $val) {
			if(!in_array($val, $arResult["GALLERY"]))
				$arResult["GALLERY"][] = trim($val);
		}
	}
} else {
    /**
     * 01.08.2017 , sumato.shigoto@gmail.com
     * ��� ������ ������ ������� - ���������� �������� ��� ��������� ����� �������,
     * ����� ������� ������������� �� ���� �� ������ �������. �����
     * �� ���������� ���� �� ������, ������� ��������� ��������, � �� ��������
     * �� ������ �� ����� ����������.
     *
     * $depressedTypes ������ ����������� �� /bitrix/php_interface/include/catalog_export/yandex_custom_run.php
     * todo ���� ������� ���������� ���� �������� �� �������, ��� �������
     */

    if(in_array($arResult["PROPERTIES"]["APPLICATION"]["~VALUE"],$depressedTypes)) {
        $arResult["PROPERTIES"]["DELIVERY_COST"]["VALUE"] = "<b>���������</b>";
        $arResult["DISPLAY_PROPERTIES"]["DELIVERY_COST"]["DISPLAY_VALUE"] = "<b>���������</b>";
    }

	$arSelect = Array("ID", "NAME", "PROPERTY_INTERIOR");
	$arFilter = Array(
		"IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"], 
		"SECTION_ID" => $_REQUEST["BRAND_ID"],
		"!PROPERTY_INTERIOR" => false,
		"ACTIVE" => "Y"
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ar_res = $res->GetNext()) {
		if(!empty($ar_res["PROPERTY_INTERIOR_VALUE"])) {
			//$interiorArray = explode(",", $ar_res["PROPERTY_INTERIOR_VALUE"]);
			// �� ��� ����� ���� ����������� � ;
			$interiorArray = preg_split( "/[,;]+/", $ar_res["PROPERTY_INTERIOR_VALUE"]);
			foreach($interiorArray as $val) {
				if(!in_array($val, $arResult["GALLERY"]))
					$arResult["GALLERY"][] = trim($val);
			}
		}
	}
}




CModule::IncludeModule("catalog");
global $USER;
$optimalPrice = CCatalogProduct::GetOptimalPrice($arResult["ID"],1,$USER->GetUserGroupArray());
$arResult["PRICES"] = array(
	array(
		"VALUE" => $optimalPrice["PRICE"]["PRICE"],
		"DISCOUNT_VALUE"=> $optimalPrice["DISCOUNT_PRICE"],
		"CURRENCY"=> $optimalPrice["PRICE"]["CURRENCY"],
	),
);

if($USER->IsAdmin()) {


}



$arResult['NAV_BUTTONS'] = array();
$sectionDB = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID']);
if($section = $sectionDB->GetNext())  {
	$sectionActive = $section['ACTIVE'];
	$arResult['NAV_BUTTONS'][] = array(
		'PATH' => '/catalog/'.$section['IBLOCK_CODE'].'/'.$section['ID'].'/',
		//'NAME' => '������� � ��������� '.$section['NAME']
		'NAME' => '��������� '.$section['NAME']
	);
}
if($sectionActive == 'N'){
	$arResult['ACTIVE'] = 'N';
}

$arVendor = false;
if($arResult['PROPERTIES']['BRAND_GUID']['VALUE']) {
	$res = CIBlockElement::GetByID($arResult['PROPERTIES']['BRAND_GUID']['VALUE']);
	if($ar_res = $res->GetNext())  {
		$arVendor = $ar_res;
	}
}
$iblockName = false;
$res = CIBlock::GetByID($arResult['IBLOCK_ID']);
if($ar_res = $res->GetNext())
  $iblockName = $ar_res['NAME'];

if($arVendor && !empty($arVendor['CODE']) && $iblockName) {
	$arResult['NAV_BUTTONS'][] = array(
		'PATH' => str_replace(' ','_',strtolower('/catalog/'.$arResult['IBLOCK_CODE'].'/vendor/'.$arVendor['CODE'])).'/',
		//'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblockName)
		'NAME' => $iblockName . ' ' . $arVendor['NAME'] 
	);
}

if($arVendor && !empty($arVendor['CODE'])) {
	// ���� ��� ��������� ���� �������
	$arIblocks = array();
	$res = CIBlock::GetList(	
		Array(), 	
		Array(		
			'TYPE'=>'catalog', 		
			'SITE_ID'=>SITE_ID, 		
			'ACTIVE'=>'Y',
			'!CODE' => $arResult['IBLOCK_CODE'],
			'!ID' => array(12,29) // �������� ����������� ������
		), 
		false
	);
	while($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;
	
	//��� ������� ��������� ���� �� ��� ������ ����� ������
	foreach($arIblocks as $iblock) {
		
		$res = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock['ID'], "CODE" => 'BRAND_GUID'));
		if($res->fetch()) {
			$res = CIBlockElement::GetList( 
				Array("SORT"=>"ASC"), 
				Array('IBLOCK_ID' => $iblock['ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_GUID' => $arVendor['ID']), 
				false, 
				false, 
				Array('ID')
			);	
			if($res->SelectedRowsCount()) {
				$arResult['NAV_BUTTONS'][] = array(
					'PATH' => str_replace(' ','_',strtolower('/catalog/'.$iblock['CODE'].'/vendor/'.$arVendor['CODE'])).'/',
					//'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblock['NAME'])
					'NAME' => $iblock['NAME'] . ' ' . $arVendor['NAME']
				);			
			}
		}
	}
}


//����� ��� ������� �������� � �����-��������. ��� ���� � ���, ��� ����������� ������� �������� ���� ���������
//� ������ ����� ����������� �������� � ������_���������, ��� � ���������.
$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ID"=>$arResult["ID"]), false, false, array("ID","IBLOCK_ID","PROPERTY_".PROPERTY_CODE));
$arResult["ADD_INFO"] = array();
while ($ar = $res->Fetch())
{
	$arResult["ADD_INFO"]["PROPERTY_CODE"] = $ar["PROPERTY_".PROPERTY_CODE."_VALUE"];
}


if($arParams['IBLOCK_ID'] == 55){
    if(is_array($arResult['PROPERTIES']['COMPLECT']['VALUE']) && count($arResult['PROPERTIES']['COMPLECT']['VALUE']) > 0){
		$arSelect = array(
			'ID',
			'IBLOCK_ID',
		);

		$arFilter = array(
			'IBLOCK_TYPE' => 'catalog',
			'XML_ID' => $arResult['PROPERTIES']['COMPLECT']['VALUE'],
		);

		$rsElements = \CIBlockElement::GetList(
			array(),
			$arFilter,
			false,
			array(
				'nTopCount' => 20
			),
			$arSelect
		);
		$rsElements->SetUrlTemplates();

		$iblockIDs = array();
		$arResult['COMPLECT'] = array(
			'IBLOCKS' => array(),
			'ITEMS' => array(),
			'FILTER' => array(
				'ID' => array()
			)
		);
		while($arElement = $rsElements->GetNext())
		{
			$iblockIDs[$arElement['IBLOCK_ID']] = $arElement['IBLOCK_ID'];
			$arResult['COMPLECT']['ITEMS'][$arElement['IBLOCK_ID']][$arElement['ID']] = $arElement;
			$arResult['COMPLECT']['FILTER']['ID'][$arElement['ID']] = $arElement['ID'];
		}

		if(count($iblockIDs) > 0){
			$rsIBlocks = CIBlock::GetList(
				Array(),
				Array(
					'TYPE'=>'catalog',
					'ID' => $iblockIDs
				), true
			);

			while ($arIBlock = $rsIBlocks->Fetch())
			{
				$arResult['COMPLECT']['IBLOCKS'][$arIBlock['ID']] = $arIBlock;
			}
		}


	}
}

$arPrice = reset($arResult['PRICES']);
$arResult['ORIGINAL_PRICE'] = array();
if(
	!empty($arResult['CATALOG_PRICE_10'])
	&& intval($arResult['CATALOG_PRICE_10']) > 0
	&& $arPrice['DISCOUNT_VALUE'] != $arResult['CATALOG_PRICE_10']
) {
	$arResult['ORIGINAL_PRICE']['VALUE'] = $arResult['CATALOG_PRICE_10'];
	$arResult['ORIGINAL_PRICE']['FORMATED'] = CurrencyFormat($arResult['CATALOG_PRICE_10'], $arResult['CATALOG_CURRENCY_10']);

}

if($USER->IsAdmin()) {


}
