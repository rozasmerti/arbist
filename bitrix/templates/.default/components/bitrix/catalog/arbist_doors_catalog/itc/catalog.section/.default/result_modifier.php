<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}

$brandID = false;

foreach($arResult["ITEMS"] as $sid => $sElements) { //"PAGE_SECTIONS"
	//echo "<pre>"; print_r($sElements[0]); echo "</pre>";
	$arResult["SECTIONS_COUNTRY"][$sid] = $sElements[0]["PROPERTIES"]["COUNTRY"]["VALUE"];
	$arResult["SECTIONS_BRAND_GUID"][$sid] = $sElements[0]["PROPERTIES"]["BRAND_GUID"]["VALUE"];
	$brandID = $sElements[0]["PROPERTIES"]["BRAND_GUID"]["VALUE"];
}
$arBrandGuidFilter = Array(   
	"IBLOCK_CODE" => "brand_guid",
	"ACTIVE" => "Y",
	"ID" => $arResult["SECTIONS_BRAND_GUID"]
);
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME", "CODE"));
while($ar_fields = $res->GetNext()) {
	$brands[$ar_fields["ID"]] = $ar_fields; /**/
}
foreach($arResult["SECTIONS_BRAND_GUID"] as $id => $value) {
	$arResult["SECTIONS_BRAND_GUID"][$id] = $brands[$value];
}

/////////////////////////////////////////////////////////////////////////////////// �������� ��������� ����
CModule::IncludeModule("catalog");
$COMPLECT_IBLOCK_ID = intval($arParams["COMPLECT_IBLOCK_ID"]);



$polotnoXmlIds = $arParams["XML_ID_COMPLECT"];

$complectTypePolotno = CIBlockPropertyEnum::GetList(array(),array("IBLOCK_ID"=>$COMPLECT_IBLOCK_ID,"CODE"=>"COMPLECT_TYPE","XML_ID"=>$polotnoXmlIds))->fetch();// �������

$basePriceType = CCatalogGroup::GetBaseGroup();
$basePriceTypeID = intval($basePriceType["ID"]);
if($basePriceTypeID > 0)
{
	foreach($arResult["ITEMS"] as $sid=>$sElements)
	{
		foreach($sElements as $cell=>$arElement)
		{	
			$xmlIds = $arElement["PROPERTIES"]["COMPLECT"]["VALUE"];

			if(is_array($xmlIds) && count($xmlIds) > 0)
			{
				$filter = array(
					"IBLOCK_ID"=>$COMPLECT_IBLOCK_ID,
					"ACTIVE"=>"Y",
					"XML_ID"=>$xmlIds,
					"PROPERTY_COMPLECT_TYPE"=>$complectTypePolotno["ID"],
				);
				
				//PR($xmlIds);
				
				
				if($minPriceComplect = CIBlockElement::GetList(array("CATALOG_PRICE_".$basePriceTypeID=>"ASC"),$filter,false,array("nTopCount"=>1),array("ID","IBLOCK_ID","XML_ID","PROPERTY_CHANGE_UNIT","CATALOG_GROUP_".$basePriceTypeID))->fetch())
				{
					/*
					PR(array(
						"XML_ID"=>$minPriceComplect["XML_ID"],
						"PRICE"=>$minPriceComplect["CATALOG_PRICE_".$basePriceTypeID],
					));
					*/
					
					$price = floatval($minPriceComplect["CATALOG_PRICE_10"]);
					$price_f = number_format($price, 0,"."," ");
					$arResult["ITEMS"][$sid][$cell]["PRICE_FROM"] = $price;
					$arResult["ITEMS"][$sid][$cell]["PRICE_FROM_F"] = $price_f;
					$arResult["ITEMS"][$sid][$cell]["CHANGE_UNIT"] = $minPriceComplect["PROPERTY_CHANGE_UNIT_VALUE"];
				}
			}
			
			
			
			
		}
	}
}

// ���� �� �������� ���������
if(count($arResult["SECTIONS"]) == 1 ) {

	$iblockName = false;
	$res = CIBlock::GetByID($arResult['IBLOCK_ID']);
	if($ar_res = $res->GetNext())
	  $iblockName = $ar_res['NAME'];

      
	if($arResult["SECTIONS_BRAND_GUID"][$arResult['ID']] && $iblockName) {
		$arResult['NAV_BUTTONS'][] = array(
			//'PATH' => '/catalog/'.$arResult['IBLOCK_CODE'].'/vendor/'.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']].'/',
			//'NAME' => '��� ��������� '.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']].' '.mb_strtolower($iblockName)
            'PATH' => '/catalog/'.$arResult['IBLOCK_CODE'].'/vendor/'.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['CODE'].'/',// Vanes 2014-11-05 issue #9313
			'NAME' => '��� ��������� ������ ������� "'.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['NAME'].'"'/*.mb_strtolower($iblockName)*/ // Vanes 2014-11-05 issue #9313
		);
	}
	
	if($brandID && $arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]) {
		// ���� ��� ��������� ���� �������
		$arIblocks = array();
		$res = CIBlock::GetList(	
			Array(), 	
			Array(		
				'TYPE'=>'catalog', 		
				'SITE_ID'=>SITE_ID, 		
				'ACTIVE'=>'Y',
				'!CODE' => $arResult['IBLOCK_CODE'],
				'!ID' => array(12,29) // �������� ����������� ������
			), 
			false
		);
		while($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;
		
		//��� ������� ��������� ���� �� ��� ������ ����� ������
		foreach($arIblocks as $iblock) {
			
			$res = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock['ID'], "CODE" => 'BRAND_GUID'));
			if($res->fetch()) {
				$res = CIBlockElement::GetList( 
					Array("SORT"=>"ASC"), 
					Array('IBLOCK_ID' => $iblock['ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_GUID' => $brandID), 
					false, 
					false, 
					Array('ID')
				);	
				if($res->SelectedRowsCount()) {
					$arResult['NAV_BUTTONS'][] = array(
						//'PATH' => '/catalog/'.$iblock['CODE'].'/vendor/'.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']].'/',
						//'NAME' => '��� ��������� '.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']].' '.mb_strtolower($iblock['NAME'])
						'PATH' => '/catalog/'.$iblock['CODE'].'/vendor/'.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['CODE'].'/',// Vanes 2014-11-05 issue #9313
						'NAME' => '��� ��������� ������ ������� "'.$arResult["SECTIONS_BRAND_GUID"][$arResult['ID']]['NAME'].'"'/*.mb_strtolower($iblock['NAME'])*/ // Vanes 2014-11-05 issue #9313                      
					);			
				}
			}
		}
	}
	
}


?>
