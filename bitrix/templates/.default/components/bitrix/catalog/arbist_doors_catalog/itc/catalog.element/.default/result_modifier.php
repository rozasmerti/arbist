<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?/*
//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
//echo $_REQUEST["BRAND_ID"];
$res = CIBlock::GetList(Array("ID"=>"DESC"), Array("ACTIVE"=>"Y", "CODE"=>$_REQUEST["IBLOCK_CODE"]), false);
if($ar_res = $res->Fetch()){
	$IBLOCK_ID = $ar_res["ID"];
}
$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ID'=>$_REQUEST["BRAND_ID"], 'GLOBAL_ACTIVE'=>'Y');  
$db_list = CIBlockSection::GetList(Array('ID'=>'DESC'), $arFilter, false, array('UF_GALLERY'));
if($section = $db_list->GetNext())
	$arResult["GALLERY"] = $section["UF_GALLERY"];
//echo "<pre>"; print_r($section); echo "</pre>";
*/?>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}
?>

<?

//echo "<pre>"; print_r($arResult); echo "</pre>";
if(!in_array($arParams["IBLOCK_ID"], $arParams["PLITKA_SECTIONS"])) {
	
	if(!empty($arResult["PROPERTIES"]["INTERIOR"]["VALUE"])) {
		$interiorArray = explode(",", $arResult["PROPERTIES"]["INTERIOR"]["VALUE"]);
		foreach($interiorArray as $val) {
			if(!in_array($val, $arResult["GALLERY"]))
				$arResult["GALLERY"][] = trim($val);
		}
	}
} else {
	
	$arSelect = Array("ID", "NAME", "PROPERTY_INTERIOR");
	$arFilter = Array(
		"IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"], 
		"SECTION_ID" => $_REQUEST["BRAND_ID"],
		"!PROPERTY_INTERIOR" => false,
		"ACTIVE" => "Y"
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ar_res = $res->GetNext()) {
		if(!empty($ar_res["PROPERTY_INTERIOR_VALUE"])) {
			$interiorArray = explode(",", $ar_res["PROPERTY_INTERIOR_VALUE"]);
			foreach($interiorArray as $val) {
				if(!in_array($val, $arResult["GALLERY"]))
					$arResult["GALLERY"][] = trim($val);
			}
		}
	}
}


CModule::IncludeModule("catalog");
/*
global $USER;
$optimalPrice = CCatalogProduct::GetOptimalPrice($arResult["ID"],1,$USER->GetUserGroupArray());
$arResult["PRICES"] = array(
	array(
		"VALUE" => $optimalPrice["PRICE"]["PRICE"],
		"DISCOUNT_VALUE"=> $optimalPrice["DISCOUNT_PRICE"],
		"CURRENCY"=> $optimalPrice["PRICE"]["CURRENCY"],
	),
);
*/


$COMPLECT_IBLOCK_ID = intval($arParams["COMPLECT_IBLOCK_ID"]);
$COMPLECT_ELEMENT_ID = intval($arParams["COMPLECT_ELEMENT_ID"]);

$complects = array();
$complectsCache = array();
$cIds = array();
$cVals = $arResult["PROPERTIES"]["COMPLECT"]["VALUE"];
if(is_array($cVals) && count($cVals) > 0)
{
	foreach($cVals as $k=>$v)
	{
		$v = trim($v);
		if(empty($v))unset($cVals[$k]);
	}
	
	if(count($cVals) > 0 && $COMPLECT_IBLOCK_ID > 0)
	{
		$fltr = array(
			"IBLOCK_ID"=>$COMPLECT_IBLOCK_ID,
			"ACTIVE"=>"Y",
			"XML_ID"=>$cVals
		);
		$rs = CIBlockElement::GetList(array(),$fltr);
		while($obj = $rs->getnextelement())
		{
			$c = $obj->getfields();
			$c["PROPS"] = $obj->getproperties();
			$c["PRICE"] = CCatalogProduct::GetOptimalPrice($c["ID"],1,CUser::GetUserGroupArray());
			$c["PRICE_F"] = number_format(round(floatval($c["PRICE"]["DISCOUNT_PRICE"])),0,"."," ");
			$c["BUY_URL"] = $APPLICATION->GetCurPageParam("action=ADD2BASKET&id=".$c["ID"],array("action","id"));
			
			if($COMPLECT_ELEMENT_ID == $c["ID"])
			{
				$c["SELECTED"] = "Y";
			}
			
			$complects[] = $c;
			$complectsCache[$c["ID"]] = array(
				"BUY_URL"=>$c["BUY_URL"],
				"PRICE_F"=>$c["PRICE_F"],
				"PRICE"=>floatval($c["PRICE"]["DISCOUNT_PRICE"]),
			);
			$cIds[] = $c["ID"];
		}
	}
}



/*
$polotno = array();
foreach($complects as $k=>$v)
{
	if($v["PROPS"]["COMPLECT_TYPE"]["VALUE_XML_ID"] == "53ec260f-c448-11e1-8a68-14dae9a723c8")// �������
	{
		$polotno[] = $v;
		unset($complects[$k]);
	}
}
$complects = array_merge($polotno, $complects);
*/

$complectEnumSorted = array();
$rs = CIBlockPropertyEnum::GetList(array("SORT"=>"ASC"),array("IBLOCK_ID"=>$COMPLECT_IBLOCK_ID,"PROPERTY_ID"=>"COMPLECT_TYPE"));
while($r = $rs->fetch())
{
	$complectEnumSorted[] = $r["XML_ID"];
}


$tpmComplects = array();
foreach($complects as $k=>$v)
{
	$cid = $v["PROPS"]["COMPLECT_TYPE"]["VALUE_XML_ID"];
	$tpmComplects[$cid][] = $v;
}

$complects = array();
foreach($complectEnumSorted as $cid)
{
	if(is_array($tpmComplects[$cid]))
	{
		foreach($tpmComplects[$cid] as $c)
		{
			$complects[] = $c;
		}
	}
}


$arResult["COMPLECT"] = $complects;
$arResult["CIDS"] = $cIds;
$arResult["COMPLECT_CACHE"] = $complectsCache;

$arResult['NAV_BUTTONS'] = array();

$sectionDB = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID']);
if($section = $sectionDB->GetNext())  {
	$sectionActive = $section['ACTIVE'];
	$arResult['NAV_BUTTONS'][] = array(
		'PATH' => '/catalog/'.$section['IBLOCK_CODE'].'/'.$section['ID'].'/',
		'NAME' => '������� � ��������� '.$section['NAME']
	);	
}
if($sectionActive == 'N'){
	$arResult['ACTIVE'] = 'N';
}

$arVendor = false;
if($arResult['PROPERTIES']['BRAND_GUID']['VALUE']) {
	$res = CIBlockElement::GetByID($arResult['PROPERTIES']['BRAND_GUID']['VALUE']);
	if($ar_res = $res->GetNext())  {
		$arVendor = $ar_res;
	}
}
$iblockName = false;
$res = CIBlock::GetByID($arResult['IBLOCK_ID']);
if($ar_res = $res->GetNext())
  $iblockName = $ar_res['NAME'];

if($arVendor && $iblockName) {
	$arResult['NAV_BUTTONS'][] = array(
		'PATH' => '/catalog/'.$arResult['IBLOCK_CODE'].'/vendor/'.$arVendor['NAME'].'/',
		'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblockName)
	);
}

if($arVendor) {
	// ���� ��� ��������� ���� �������
	$arIblocks = array();
	$res = CIBlock::GetList(	
		Array(), 	
		Array(		
			'TYPE'=>'catalog', 		
			'SITE_ID'=>SITE_ID, 		
			'ACTIVE'=>'Y',
			'!CODE' => $arResult['IBLOCK_CODE'],
			'!ID' => array(12,29) // �������� ����������� ������
		), 
		false
	);
	while($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;
	
	//��� ������� ��������� ���� �� ��� ������ ����� ������
	foreach($arIblocks as $iblock) {
		
		$res = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock['ID'], "CODE" => 'BRAND_GUID'));
		if($res->fetch()) {
			$res = CIBlockElement::GetList( 
				Array("SORT"=>"ASC"), 
				Array('IBLOCK_ID' => $iblock['ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_GUID' => $arVendor['ID']), 
				false, 
				false, 
				Array('ID')
			);	
			if($res->SelectedRowsCount()) {
				$arResult['NAV_BUTTONS'][] = array(
					'PATH' => '/catalog/'.$iblock['CODE'].'/vendor/'.$arVendor['NAME'].'/',
					'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblock['NAME'])
				);			
			}
		}
	}
}


$this->__component->SetResultCacheKeys(array("ID","CIDS","COMPLECT_CACHE","PREVIEW_TEXT","DETAIL_TEXT","GALLERY"));

//if(CUser::isAdmin())PR($arResult["GALLERY"]);







?>