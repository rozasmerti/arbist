<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? CModule::IncludeModule('itconstruct.resizer');?>

<style>
.noted .note:before{
	left: -10px;
}
.noted .note {
	position: static;
	display: block;
	margin-left: 0px !important;
}
</style>


<?if(!empty($arResult["SECTIONS"])) {?> <!--$arResult["PAGE_SECTIONS"]-->
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING_TOP"]?><br />
	<?endif;?>

	<?$sectionsCount = count($arResult["SECTIONS"])?> <!--$arResult["PAGE_SECTIONS"]-->
	<?foreach($arResult["ITEMS"] as $sid=>$sElements):?> <!--$arResult["PAGE_SECTIONS"]-->
		<?
		$message = NULL;
		if(count($sElements) != $arResult["SECTION_ELEMENT_COUNT"][$sid])
			$message = "Y";
		?>
		<?if($sectionsCount == 1):?>
<div class="anchor" id="<?=$sid?>"></div>
		<div class="gap1" style="position: relative;">
			<h3 class="link0 alt0">
				��������� <span class="catalog"><?=$arResult["SECTION_NAME"][$sid]?></span>
			</h3>
			<div class="fontReduced hl0">�������������: <a class="light-grey-link vendor-link-on-top" href="<?=$arResult["SECTION_PARENT_PATH"][$sid].'vendor/'. $arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"]?>/"><?=$arResult["SECTIONS_BRAND_GUID"][$sid]["NAME"]?></a>, <?=$arResult["SECTIONS_COUNTRY"][$sid]?></div>
		</div>
		<?else:?>
		<div class="gap1" style="position: relative;">
			<h3 class="link0 alt0">
				��������� 
				<a href="<?=$arResult["SECTION_PATH"][$sid]?>#<?=$sid?>">
					<?=$arResult["SECTION_NAME"][$sid]?>
				</a>
			</h3>
			<div class="fontReduced hl0">�������������: <a class="light-grey-link vendor-link-on-top" href="<?=$arResult["SECTION_PARENT_PATH"][$sid].'vendor/'. $arResult["SECTIONS_BRAND_GUID"][$sid]["CODE"]?>/"><?=$arResult["SECTIONS_BRAND_GUID"][$sid]["NAME"]?></a>, <?=$arResult["SECTIONS_COUNTRY"][$sid]?></div> <!--���������, �������������-->
		</div>
		<?endif;?>

		<?if($message == "Y"):?>
		<div class="noted">
			<div class="note link4">
				�������� ������ ������, ��������������� ������ �������.
				<a href="<?=$arResult["SECTION_PATH"][$sid]?>">�������� ��� ���������</a>
			</div>
			<div style="width: 100%; height: 10px;"></div>
		</div>
		<?endif;?>
		<?if(!empty($arResult["DESCRIPTIONS"][$sid])):?>
			<div class="top-text">
				<?=$arResult["DESCRIPTIONS"][$sid]?>
			</div>
		<?endif;?>
		
		<!--���� �����������-->
		
		<?
		$totalCount = count($arResult["GALLERY"][$sid]);
		$realCount = 0;
		foreach($arResult["GALLERY"][$sid] as $key=>$pictureFullName){
		
			if(fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$pictureFullName,'r')){
				$realCount++;
				
			}
		
		
		}
		?>
			
		<?if(!empty($arResult["GALLERY"][$sid]) /*&& $sectionsCount == 1*/ && in_array($arParams["IBLOCK_ID"], $arParams["PLITKA_SECTIONS"]) && $realCount!=0 ):?>
		
			<h4 class="alt0">� ���������</h4>
			
			<div class="gap4 gal" style="height:318px">
				
				<div class="gallery-loader-img" style="display:none; position:absolute; left:150px; top:50px;"><img src="/images/ajax-loading_big.gif"/></div>

				<ul id="galleryd<?=$sid?>" class="gallery">
				<?foreach($arResult["GALLERY"][$sid] as $pictureFullName):?>
					<?$pic = split("\.", $pictureFullName);?>

					<?if(fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$pictureFullName,'r')):?>
					<li>
						<!--68 x 50-->
						<img src="/rpic_path/c/<?=$pic[0]?>_h50_w68_crop.<?=$pic[1]?>" alt=""/>

						<div class="panel-content">
							<!--400 x 296-->
							<a class="lightboxx" rel="lightbox_gallery_interior" href="/rpic_path/c/<?=$pic[0]?>_h592_w800_auto.<?=$pic[1]?>">
								<img src="/rpic_path/c/<?=$pic[0]?>_h296_w400_crop.<?=$pic[1]?>" alt=""/>
							</a>
						</div>
					</li>
					<?endif?>
				<?endforeach;?>
				</ul>
				
				<script>
				$(document).ready(function(){
					$('.lightboxx').lightBox();
				});
				</script>
				
			</div>
			
		<?endif;?>
		<!---->
		<div class="gap4 b-gap4">
				<?foreach($sElements as $cell=>$arElement):?>
				<?
				$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
				?>
				<?if($cell%$arParams["LINE_ELEMENT_COUNT"] == 0):?>
				<div class="gap1 row">
				<?endif;?>
				<div class="item1 item" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
				<a name="product<?=$arElement["ID"]?>"></a>
				<?if(is_array($arElement["DETAIL_PICTURE"])):?>
					<a id="photo_<?=$arElement["ID"]?>" class="nodecor" href="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>" rel="lightbox<?=$arElement["ID"]?>">
				<?elseif(is_array($arElement["PREVIEW_PICTURE"])):?>
					<a id="photo_<?=$arElement["ID"]?>" class="nodecor" href="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" rel="lightbox<?=$arElement["ID"]?>">
				<?else:?>
					<a id="photo_<?=$arElement["ID"]?>" class="nodecor" href="<?=$arElement["DETAIL_PAGE_URL"]?>" rel="lightbox<?=$arElement["ID"]?>">
				<?endif;?>
					<div id="photo" class="image1 image door-img" style="height:140px;">
				<?$arPrice = array_shift($arElement["PRICES"])?>
						<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
							<div class="labels">
								<div class="lab"><span>������</span></div>
							</div>
						<?elseif($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "987617dd-090b-11e0-87a1-00166f1a5311" || 
								 $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "d536afec-2269-11e0-9088-0018f34aa38f" ||  
								 $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "a379a7ca-9a4a-11e0-b2a0-0018f34aa38f"):?>
							<div class="labels">
								<div class="lab"><span><?=$arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE"]?></span></div>
							</div>
						<?endif;?>

						<?if(is_array($arElement["PREVIEW_PICTURE"])):?> 
                            <img src="<?=itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'auto', 190, 200);?>" alt="<?=$arElement["NAME"];?>"/>
							<script type="text/javascript">	$(function() {$('#photo_<?=$arElement["ID"]?>').lightBox();});</script>
						<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
							<img src="<?=itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'auto', 136, 140);?>" alt="<?=$arElement["NAME"];?>"/>
							<script type="text/javascript">$(function() {$('#photo_<?=$arElement["ID"]?>').lightBox();});</script>
						<?else:?>
							<img class="b-empty-img" src="/img/noimg0.jpg" alt="" width="136" height="93" />
						<?endif?>
					</div>
					</a>
					<h5 class="desc0 desc link3"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></h5>
					<div class="bot price-positon">
						<div class="aligned row-b">
							<div>
								<div class="price0 price minimal-category-price_bg">
									<span class="val">
									<?
									if(intval($arElement["PRICE_FROM"]) > 0)
									{
										?>�� <?=$arElement["PRICE_FROM_F"]?> <span class="b-rub">�</span><?
										if(!empty($arElement["CHANGE_UNIT"]))
										{
											echo "&nbsp;��&nbsp;".$arElement["CHANGE_UNIT"];
										}
									}
									?>
									</span>

								</div>
							</div>
							<span class="under"></span>
						</div>
					</div>
				</div>

				<?$cell++;
				if($cell%$arParams["LINE_ELEMENT_COUNT"] == 0):?>
					</div>
				<?endif?>

				<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>

				<?if($cell%$arParams["LINE_ELEMENT_COUNT"] != 0):?>
					</div>
				<?endif?>

		</div>
	<?endforeach;?>

	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING_BOT"]?>
	<?endif;?>
	
	<?if($sectionsCount == 1):?>
	
	<?
		// ���� ������ ���� ��������� - ������ ��������� �� ����� �������
		foreach ($arResult["ITEMS"] as $sid => $sElements)
		{
			$APPLICATION->SetTitle('������������ ����� '.$arResult["SECTION_NAME"][$sid].' '.$arResult["SECTIONS_BRAND_GUID"][$sid]["NAME"].' ������ �� ��������������� ���� � ������');
			$APPLICATION->SetPageProperty("description", '������� ������������ ������ '.$arResult["SECTION_NAME"][$sid].' '.$arResult["SECTIONS_BRAND_GUID"][$sid]["NAME"].' � ���������� ���������������� � ������.');
			$APPLICATION->SetPageProperty("h1", '������������ ����� '.$arResult["SECTION_NAME"][$sid]);
		}
	?>
	
	<?
	if(!empty($arResult['NAV_BUTTONS'])) {
		?>
		<div style="width: 100%; text-align: center; margin-bottom: 20px;">
			<?
			foreach($arResult['NAV_BUTTONS'] as $button) {
				?>
				<div class="new_button<?/* width50*/?>">
					<?/*?><button onclick="window.location.href='<?=$button['PATH']?>'" type="button"><?=$button['NAME']?></button><?*/ // Vanes 2014-11-05 issue #9313?>
                    <a class="a-button" href="<?=$button['PATH']?>"><?=$button['NAME']?></a>
				</div>
				<?
			}
			?>
			<div style="clear: both;"></div>
		</div>
		<?
	}	
	?>	
	<?endif;?>
<?
} else {
	echo "�� ������� ������� ������ �� �������. �������� �� ������ ���������� �����, ������ ����� ���������. ��������� �������� �������� ������.";
}
?>
