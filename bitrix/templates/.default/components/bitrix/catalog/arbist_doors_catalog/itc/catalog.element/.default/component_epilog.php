<?
$APPLICATION->SetTitle($arResult["NAME"]." ������, ���� � ������ - ��������-������� ������");
$APPLICATION->SetPageProperty("description", "� ������� " . $arResult["NAME"] . " �� ������ �����, �� ������ ������� � ������ ������ - Arbist");

CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
$arBasketList = GetBasketListSimple();

$basketList = array();
while($bi = $arBasketList->fetch())
{
	$basketList[$bi["PRODUCT_ID"]] = $bi;
}

StartCaptureUncachedContent('buy-link-cid');
$CID = intval($arParams["COMPLECT_ELEMENT_ID"]);
if($CID > 0 && in_array($CID,$arResult["CIDS"]))
{
	if($cItemObj = CIBlockElement::GetByID($CID)->getnextelement())
	{
		$cItem = $cItemObj->GetFields();
		$cItemProps = $cItemObj->GetProperties();
		if(in_array($cItemProps["COMPLECT_TYPE"]["VALUE_XML_ID"],$arParams["XML_ID_COMPLECT"]))
		{// �������!
			$APPLICATION->SetTitle($cItem["NAME"]);
			$arPrice = CCatalogProduct::GetOptimalPrice($CID,1,CUser::GetUserGroupArray());
			$price = floatval($arPrice["DISCOUNT_PRICE"]);
			$price_f = number_format($price,0,"."," ");
			$buy_url = $APPLICATION->GetCurPageParam("action=ADD2BASKET&id=".$CID,array("action","id"));
			if($price > 0)
			{
				?>
				<div class="gap1 pricing">
					<div class="row row-b">
						<div class="item4 item">
							<strong class="hl3">����:</strong>
							<span class="price3 price">
								<span class="val">
									<?=$price_f?> �.<?
									$ed = $cItemProps["CHANGE_UNIT"]["VALUE"];
									if(!empty($ed))echo "/".$ed;
									?>
								</span>
							</span>
						</div>
						<div class="item4 item">
						<form action="#">
							<fieldset>
								<div class="button5 button">
									<?
									if(is_array($basketList[$CID]))
									{
										?><img src="/img/in_basket.png"><?
									}
									else
									{
										?>
										<i class="helper"></i>
										<button type="button" onclick="location.href='<?=$buy_url?>'"><span class="button-txt">������</span><i></i></button>
										<?
									}
									?>
								</div>
							</fieldset>
						</form>
						</div>
						<span class="under"></span>
					</div>
				</div>
				<?
			}
		}
	}
}
EndCaptureUncachedContent();
if(is_array($arResult["CIDS"]))
{
	foreach($arResult["CIDS"] as $cid)
	{
		$item = $arResult["COMPLECT_CACHE"][$cid];
		if(is_array($item))
		{
			StartCaptureUncachedContent('buy-link-'.$cid);
			if($item["PRICE"] > 0)
			{
				if(is_array($basketList[$cid]))
				{
					?><a href="/personal/basket.php" title="������� � �������" class="a-in-basket"><img src="/img/in_basket.png"/></a><?
				}
				else
				{
					?>
					
					<form action="">
						<input type="hidden" name="action" value="ADD2BASKET"/>
						<input type="hidden" name="id" value="<?=$cid?>"/>
						<div id="buy<?=$cid?>" class="button4 button">
							<i class="helper"></i>
							<button type="button" onclick="$(this).closest('form').submit();"><i></i></button>
						</div>
					</form>
					<?
					/*
					<a style='color:#c03;' href="<?=$item["BUY_URL"]?>#complects">������</a>
					*/
				}
			}
			EndCaptureUncachedContent();
		}
	}
}






?>
<br/>
<br/>
<?
if(!empty($_SESSION["ITC_BACKURL"]))
{
	/*?>
	<div class="back">
		<a href="<?=$_SESSION["ITC_BACKURL"]?>">
			<div class="panel3 panel">
				<span class="helper"></span>
				<div class="panel-cont">
					��������� � ���������
				</div>
			</div>
		</a>
	</div>
	<?*/
}
else
{
	CModule::IncludeModule("iblock");
	$element = CIBlockElement::GetByID($arResult["ID"])->fetch();
	$sid = intval($element["IBLOCK_SECTION_ID"]);
	if($sid > 0)
	{
		if($sec = CIBlockSection::GetByID($sid)->getnext())
		{
			?>
			<div class="back">
				<a href="<?=$sec["SECTION_PAGE_URL"]?>">
					<div class="panel3 panel">
						<span class="helper"></span>
						<div class="panel-cont">
							��������� � ���������
						</div>
					</div>
				</a>
			</div>
			<?
		}
	}
}
?>

<?if(!empty($arResult["PREVIEW_TEXT"]) || !empty($arResult["DETAIL_TEXT"])):?>
	<div class="clearfix"></div>
	<br>
	<?if(!empty($arResult["PREVIEW_TEXT"])) echo "<p>" . $arResult["PREVIEW_TEXT"] . "</p>";?>
	<?if(!empty($arResult["DETAIL_TEXT"])) echo "<p>" . $arResult["DETAIL_TEXT"] . "</p>";?>
<?endif;?>




<?if(!empty($arResult["GALLERY"])):?>
<h4 class="alt0">� ���������</h4>
<div class="gap4 gal">
	<ul id="gallery" class="gallery">
	<?foreach($arResult["GALLERY"] as $pictureFullName):?>
		<?$pic = split("\.", $pictureFullName);?>
		<?if(fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$pictureFullName,'r')):?>
		
		<li>
			<!--68 x 50-->
			<img src="/rpic_path/c/<?=$pic[0]?>_h50_w68_crop.<?=$pic[1]?>" alt=""/>

			<div class="panel-content">
				<!--400 x 296-->
				<a class="lightboxx" rel="lightbox_gallery_interior" href="/rpic_path/c/<?=$pic[0]?>_h592_w800_auto.<?=$pic[1]?>">
					<img src="/rpic_path/c/<?=$pic[0]?>_h296_w400_crop.<?=$pic[1]?>" alt=""/>
				</a>
			</div>
		</li>
		
		<?endif?>
		
		
	<?endforeach;?>
	</ul>
</div>
<?endif;?>