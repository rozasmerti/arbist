<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"COMPLECT_IBLOCK_ID" => Array(
		"NAME" => "COMPLECT_IBLOCK_ID",
		"TYPE" => "STRING",
	),
	
	"COMPLECT_ELEMENT_ID" => Array(
		"NAME" => "COMPLECT_ELEMENT_ID",
		"TYPE" => "STRING",
	),
	
	"FILTER_PROPERTY_CODE_COMPLECT" => Array(
		"NAME" => "FILTER_PROPERTY_CODE_COMPLECT",
		"TYPE" => "STRING",
		"MULTIPLE"=>"Y",
	),
	
	"XML_ID_COMPLECT" => array(
		"PARENT" => "BASE",
		"NAME" => "XML_ID_COMPLECT",
		"TYPE" => "STRING",
		"DEFAULT" => "",
		"MULTIPLE" => "Y"
	),
);
?>
