<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"COMPLECT_IBLOCK_ID" => Array(
		"NAME" => "COMPLECT_IBLOCK_ID",
		"TYPE" => "STRING",
	),	
	"XML_ID_COMPLECT" => array(
		"PARENT" => "BASE",
		"NAME" => "XML_ID_COMPLECT",
		"TYPE" => "STRING",
		"DEFAULT" => "",
		"MULTIPLE" => "Y"
	),
);
?>
