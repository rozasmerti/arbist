<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h1><?$APPLICATION->ShowProperty("h1")?></h1>

<?$APPLICATION->ShowProperty("topSectionText")?>
<?/*$plitkaSections = $APPLICATION->IncludeComponent("itc:plitka.sections", ".default", array(
	"IBLOCK_TYPE" => "dynamic_content",
	"IBLOCK_ID" => "47",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600"
	),
	false
);*/?>

<?
CModule::IncludeModule("iblock");

$arFilter = Array(   
	"IBLOCK_ID" => 47,    
	"ACTIVE" => "Y",
	"XML_ID" => "main"
);
$db_list = CIBlockSection::GetList(Array(), $arFilter, false);  
if($ar_result = $db_list->GetNext()) {
	
	$arFilter = Array(   
		"IBLOCK_ID" => 47,    
		"ACTIVE" => "Y",
		"SECTION_ID" => $ar_result["ID"],
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("PROPERTY_IBLOCK_ID"));
	while($ar_fields = $res->GetNext()) {
		$plitkaSections[] = $ar_fields["PROPERTY_IBLOCK_ID_VALUE"];
	}
}

?>

<?/*
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"DISPLAY_PANEL" => "N",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	),
	$component
);*/?>
<br />

<?if($arParams["USE_FILTER"]=="Y"):?>
<?$APPLICATION->IncludeComponent(
	"itc:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
 		"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
		"PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
		"OFFERS_FIELD_CODE" => $arParams["FILTER_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["FILTER_OFFERS_PROPERTY_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
	),
	$component
);
?>
<br />
<?endif?>

<?

	global ${$arParams["FILTER_NAME"]};
	
	if(!empty(${$arParams["FILTER_NAME"]}['>=CATALOG_PRICE_10'])){
		${$arParams["FILTER_NAME"]}['>=CATALOG_PRICE_10'] = ${$arParams["FILTER_NAME"]}['>=CATALOG_PRICE_10'] - 0.5;
		${$arParams["FILTER_NAME"]}['<=CATALOG_PRICE_10'] = ${$arParams["FILTER_NAME"]}['<=CATALOG_PRICE_10'] + 0.499;
	}
	
	
?>

<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "new_arbist_breadcrumb", Array(),false);?> 

<?if($arParams["USE_COMPARE"]=="Y"):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.compare.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NAME" => $arParams["COMPARE_NAME"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
	),
	$component
);?>
<br />
<?endif?>
<!---->

<?$setFilter = $_REQUEST["set_filter"];?>
<?if(empty($setFilter) && empty($arResult["VARIABLES"]["SECTION_ID"]) && in_array($arParams["IBLOCK_ID"], $plitkaSections)):?>
	<?$APPLICATION->IncludeComponent(
		"itc:catalog.section.list",
		"",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"ADD_SECTIONS_CHAIN" => "Y",
            "SET_TITLE" => "Y"
		),
		$component
	);
	?>
<?else:?>
<?
/**************************************************/
/* � $arParams itc:catalog.section ������ ����    */
/* �������� "PLITKA_SECTIONS" => $plitkaSections, */
/**************************************************/
	?>

	
	<?$APPLICATION->IncludeComponent(
		"itc:catalog.section",
		"",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
			"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
			"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
			"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
			"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
			"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
			"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_FILTER" => $arParams["CACHE_FILTER"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_TITLE" => $arParams["SET_TITLE"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
			"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
			"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],

			"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE" => $arParams["PAGER_TITLE"],
			"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
			"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
			"PLITKA_SECTIONS" => $plitkaSections,
		),
		$component
	);
	?>
<?endif;?>

<?$APPLICATION->IncludeComponent("itc:catalog.addchain", ".default", array(
	"DISPLAY_IBLOCK_CHAIN" => (empty($arResult["VARIABLES"]["SECTION_ID"]) && empty($setFilter)) ? "N": "Y",
	"PROP_IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"PROP_IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"],
	"IBLOCK_TYPE" => "dynamic_content",
	"IBLOCK_ID" => "47",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	),
	$component
);?>
<?$APPLICATION->ShowProperty("botSectionText")?>