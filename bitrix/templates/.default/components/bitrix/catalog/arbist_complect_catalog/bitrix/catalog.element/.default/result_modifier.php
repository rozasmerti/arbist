<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?/*
//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
//echo $_REQUEST["BRAND_ID"];
$res = CIBlock::GetList(Array("ID"=>"DESC"), Array("ACTIVE"=>"Y", "CODE"=>$_REQUEST["IBLOCK_CODE"]), false);
if($ar_res = $res->Fetch()){
	$IBLOCK_ID = $ar_res["ID"];
}
$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ID'=>$_REQUEST["BRAND_ID"], 'GLOBAL_ACTIVE'=>'Y');  
$db_list = CIBlockSection::GetList(Array('ID'=>'DESC'), $arFilter, false, array('UF_GALLERY'));
if($section = $db_list->GetNext())
	$arResult["GALLERY"] = $section["UF_GALLERY"];
//echo "<pre>"; print_r($section); echo "</pre>";
*/?>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}
?>

<?
//echo "<pre>"; print_r($arResult); echo "</pre>";
if(!in_array($arParams["IBLOCK_ID"], $arParams["PLITKA_SECTIONS"])) {
	 if(!empty($arResult["PROPERTIES"]["INTERIOR"]["VALUE"])) {
		$interiorArray = explode(",", $arResult["PROPERTIES"]["INTERIOR"]["VALUE"]);
		foreach($interiorArray as $val) {
			if(!in_array($val, $arResult["GALLERY"]))
				$arResult["GALLERY"][] = trim($val);
		}
	}
} else {
	$arSelect = Array("ID", "NAME", "PROPERTY_INTERIOR");
	$arFilter = Array(
		"IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"], 
		"SECTION_ID" => $_REQUEST["BRAND_ID"],
		"!PROPERTY_INTERIOR" => false,
		"ACTIVE" => "Y"
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ar_res = $res->GetNext()) {
		if(!empty($ar_res["PROPERTY_INTERIOR_VALUE"])) {
			$interiorArray = explode(",", $ar_res["PROPERTY_INTERIOR_VALUE"]);
			foreach($interiorArray as $val) {
				if(!in_array($val, $arResult["GALLERY"]))
					$arResult["GALLERY"][] = trim($val);
			}
		}
	}
}




CModule::IncludeModule("catalog");
global $USER;
$optimalPrice = CCatalogProduct::GetOptimalPrice($arResult["ID"],1,$USER->GetUserGroupArray());
$arResult["PRICES"] = array(
	array(
		"VALUE" => $optimalPrice["PRICE"]["PRICE"],
		"DISCOUNT_VALUE"=> $optimalPrice["DISCOUNT_PRICE"],
		"CURRENCY"=> $optimalPrice["PRICE"]["CURRENCY"],
	),
);










?>