<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(!empty($arResult["SECTIONS"])):
    foreach($arResult["SECTIONS"] as $arSection):
        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
?>
        <div id="<?=$this->GetEditAreaId($arSection['ID']);?>" class="gap1" style="position: relative; margin-bottom: 5px !important;">
            <h3 class="link0 alt0">
                ��������� 
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>">
                   <?=$arSection["NAME"]?>
                </a>
            </h3>
            <?/*<div class="fontReduced hl0">�������������: <?=$arResult["SECTIONS_BRAND_GUID"][$sid]?>, <?=$arResult["SECTIONS_COUNTRY"][$sid]?></div> <!--���������, �������������-->*/?>
        </div>
    <?endforeach?>
    <br>
<?endif;?>

