<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<ul class="list clearfix">
<?
	$lastEl = count($arResult) - 1;
	foreach($arResult as $key => $arItem) {
?>
	<li <? if($arItem['SELECTED']) {?>class="selected <? if($lastEl == $key) { ?>last<? } ?>"<?} else {?><? if($lastEl == $key) { ?>class="last"<? } }?>><a href="<?=$arItem['LINK']; ?>"><span><?=$arItem['TEXT']; ?></span><i class="iefix"></i></a></li>
<?
	}
?>
</ul>
<?endif; ?>