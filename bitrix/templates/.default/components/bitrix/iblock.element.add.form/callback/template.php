<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

<div <?if (strlen($arResult["MESSAGE"]) > 0):?>class="hidden"<?endif?>>
<p class="work_time">
����� ������ ���������� ��������:<br>
- ��������� � 9 �� 20 �����
</p>
<p>
� ���� ��� �������� �� ��� �������������� ������� ������, ��� � ��� ������ �����������. �� ��������� � ���� ����� �������� � ������ ������� ������� ������� �������, ������� �����������.
</p><p>
���� �� �� ������ ����������� - ������ ��� ��� ���������� ��������� (�����������, ��� �� ����� ������), ���������� ��� ��������������� ������� - "��������� ���!". ��� ����� ���������, ����������, ��������������� �����.
</p><p>
� ������� ���� � ������� ����� ��������-�������� ���� ��������� �������� � ����. </p><p>�������� ��������� �� ��������� ����������.
</p>
<br></div>

<?if (count($arResult["ERRORS"])):?>
	<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
    <?//=ShowNote($arResult["MESSAGE"])?>
    ���� ������: <b>� <?=$arResult["ELEMENT"]["ID"]?></b> �������.<br><br>
    � ������� ���� ���� ��������� �������� � ���� �� ��������: <b><?=$arResult["ELEMENT"]["NAME"]?></b><br> � ������� �� ��� ������.<br><br>
    �������, ��� ��������������� ����� ��������-��������� � <a href="http://arbist.ru"><b>ARBIST.RU</b></a></p>
<?endif?>

<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

	<?=bitrix_sessid_post()?>

	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
    
    <div <?if (strlen($arResult["MESSAGE"]) > 0):?>class="hidden"<?endif?>>
	<table class="data-table callback" cellspacing="0" cellpadding="0">
		<tbody>
            <tr>
                <td>��� ������</td><td>
                    <select name="PROPERTY[3039]">
                        <option <?if($_REQUEST["PROPERTY"][3039] == 76310){?>selected<?}?> value="76310">�� ������</option>
                        <option <?if($_REQUEST["PROPERTY"][3039] == 76311){?>selected<?}?> value="76311">�� ��������� ���������</option>
                        <option <?if($_REQUEST["PROPERTY"][3039] == 76312){?>selected<?}?> value="76312">�� ������</option>
                        <option <?if($_REQUEST["PROPERTY"][3039] == 76313){?>selected<?}?> value="76313">�� ������</option>
                        <option <?if($_REQUEST["PROPERTY"][3039] == 76314){?>selected<?}?> value="76314">�� ��������</option>
                        <option <?if($_REQUEST["PROPERTY"][3039] == 76315){?>selected<?}?> value="76315">������</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>���</td><td>
                    <input type="text" size="40" name="PROPERTY[3040][0]" value="<?if($_REQUEST["PROPERTY"][3040][0]){echo $_REQUEST["PROPERTY"][3040][0];}?>" />
                </td>
            </tr>
            <tr>
                <td>�������<span class="starrequired">*</span></td><td>
                    <input type="text" size="40" name="PROPERTY[NAME][0]" value="<?if($_REQUEST["PROPERTY"]["NAME"][0]){echo $_REQUEST["PROPERTY"]["NAME"][0];}?>">
                </td>
            </tr>
            <tr>
                <td>������� ����� ������</td><td>
                    <input type="text" size="40" name="PROPERTY[3041][0]" value="<?if($_REQUEST["PROPERTY"][3041][0]){echo $_REQUEST["PROPERTY"][3041][0];}?>">		
                </td>
            </tr>
            <tr>
                <td>����� ������ (���� ��������)</td><td>
                    <input type="text" size="40" name="PROPERTY[3042][0]" value="<?if($_REQUEST["PROPERTY"][3042][0]){echo $_REQUEST["PROPERTY"][3042][0];}?>" >
                </td>
            </tr>
            <tr>
                <td>������<span class="starrequired">*</span></td><td>
                    <textarea name="PROPERTY[PREVIEW_TEXT][0]" rows="5" cols="40"><?if($_REQUEST["PROPERTY"]["PREVIEW_TEXT"][0]){echo $_REQUEST["PROPERTY"]["PREVIEW_TEXT"][0];}?></textarea>
                </td>
            </tr>

			<?/*if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
				<tr>
					<td><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></td>
					<td>
						<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
					</td>
				</tr>
				<tr>
					<td><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</td>
					<td><input type="text" name="captcha_word" maxlength="50" value=""></td>
				</tr>
			<?endif*/?>
		</tbody>
		<tfoot>
			<tr>
				<td>&nbsp;</td><td>
					<?/*<input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
					<?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):*/?>
                    <input type="submit" name="iblock_submit" value="���������" />
                    <?//endif?>
					<?/*<input type="reset" value="<?=GetMessage("IBLOCK_FORM_RESET")?>" />*/?>
				</td>
			</tr>
		</tfoot>
	</table>
	<br />
    <p class="small">* &mdash; ���� ������������ ��� ����������</p>
	<?if (strlen($arParams["LIST_URL"]) > 0):?><a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a><?endif?>
    </div>
</form>
