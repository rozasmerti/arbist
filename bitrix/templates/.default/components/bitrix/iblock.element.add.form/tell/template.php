<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>
<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
<fieldset>
	<div class="call popup">
		<div class="button0 button">
			<span class="helper"></span>
			<button class="trg" type="button"><i></i>�������� ������</button>
		</div>
		<div class="pop">
			<div class="block0">
				<div class="r-border-shape">
					<div class="tb">
						<div class="cn l"></div>
						<div class="cn r"></div>
					</div>
					<div class="d dl"><i></i></div>
					<div class="d dr"><i></i></div>
					<div class="d dc"><i></i></div>
					<div class="cnt">
						<?if (count($arResult["ERRORS"])):?>
							<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
						<?endif?>
						<?if (strlen($arResult["MESSAGE"]) > 0):?>
							<?=ShowNote($arResult["MESSAGE"])?>
						<?endif?>

						<?=bitrix_sessid_post()?>
						
						<div class="gap0">
							<label class="label0" for="phone">�������� ��� ��� ����� ��������, � ���� ������������ �������� � ����.</label>
							<div class="block1">
								<div class="block-wrap">
									<div class="r-border-shape">
										<div class="tb">
											<div class="cn l"></div>
											<div class="cn r"></div>
										</div>
										<div class="d dl"><i></i></div>
										<div class="d dr"><i></i></div>
										<div class="d dc"><i></i></div>
										<div class="cnt">
											<div class="input0 input inputHinted">
												<label class="hint" for="phone">�������:</label>
												<input id="phone" type="text" name="PROPERTY[NAME][0]" value=""/>
											</div>
										</div>

										<div class="bb">
											<div class="cn l"></div>
											<div class="cn r"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row-btn row">
							<div class="item">
								<div class="button0 button">
									<span class="helper"></span>
									<button type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>"><i></i>��������� ������</button>
								</div>    
							</div>    
						</div>

						<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
					</div>
					<div class="bb">
						<div class="cn l"></div>
						<div class="cn r"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</fieldset>
</form>