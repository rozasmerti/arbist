<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["FORM_TYPE"] == "login"):?>

<div class="item">
	<div class="login popup">
		<a class="trg" href="#">����� � ������ �������</a>
		<div class="sign pop">
			<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
			<fieldset>
			<?if($arResult["BACKURL"] <> ''):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
			<?foreach ($arResult["POST"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
				<input type="hidden" name="AUTH_FORM" value="Y" />
				<input type="hidden" name="TYPE" value="AUTH" />
			<!--<form action="#">-->
					<div class="block0">
						<div class="r-border-shape">
							<div class="tb">
								<div class="cn l"></div>
								<div class="cn r"></div>
							</div>
							<div class="d dl"><i></i></div>
							<div class="d dr"><i></i></div>
							<div class="d dc"><i></i></div>
							<div class="cnt">
								<div class="gap2">
									<div class="block1">
										<div class="block-wrap">
											<div class="r-border-shape">
												<div class="tb">
													<div class="cn l"></div>
													<div class="cn r"></div>
												</div>
												<div class="d dl"><i></i></div>
												<div class="d dr"><i></i></div>
												<div class="d dc"><i></i></div>
												<div class="cnt">
													<div class="input0 input inputHinted">
														<label class="hint" for="email">E-mail:</label>
														<input type="text" id="email" name="USER_LOGIN" maxlength="50" />
													</div>
												</div>

												<div class="bb">
													<div class="cn l"></div>
													<div class="cn r"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gap2">
									<div class="block1">
										<div class="block-wrap">
											<div class="r-border-shape">
												<div class="tb">
													<div class="cn l"></div>
													<div class="cn r"></div>
												</div>
												<div class="d dl"><i></i></div>
												<div class="d dr"><i></i></div>
												<div class="d dc"><i></i></div>
												<div class="cnt">
													<div class="input0 input inputHinted">
														<label class="hint" for="pass">������:</label>
														<input type="password" id="pass" name="USER_PASSWORD" maxlength="50" />
													</div>
												</div>

												<div class="bb">
													<div class="cn l"></div>
													<div class="cn r"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="gap2">
									<div class="aligned">
										<div class="item">
											<label class="check0 check-b" for="USER_REMEMBER_frm">
												<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y">
												<span class="labelTxt">
													<span>��������� ����</span>
												</span>
											</label>
										</div>
										<div class="item">
											<a class="forgot" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow">������ ������?</a>
										</div>
										<span class="under"></span>
									</div>
								</div>
								<div class="gap2">
									<div class="row-b aligned">
										<div class="item">
											<label class="check1 check-b" for="USER_FIRST_TIME_frm">
												<input type="checkbox" id="USER_FIRST_TIME_frm" name="USER_FIRST_TIME" value="Y">
												<span class="labelTxt">
													<span>� ����� �������</span>
												</span>
											</label>
										</div>
										<div class="item">
											<div class="button0 button">
												<span class="helper"></span>
												<button type="submit"><i></i>����� �� ����</button>
											</div>
										</div>
										<span class="under"></span>
									</div>
								</div>
							</div>

							<div class="bb">
								<div class="cn l"></div>
								<div class="cn r"></div>
							</div>
						</div>
					</div>
				</fieldset>
			<!--</form>-->
		</div>
	</div>
</form>
</div>
<div class="item" style="padding-left: 62%;">
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>
</div>
<?if($arResult["AUTH_SERVICES"]):?>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
<?endif?>

<?
//if($arResult["FORM_TYPE"] == "login")
else:
?>
    <?php
    /*
    ?>
<div class="item">
	<div class="rating">
		������
		<span class="rating-b">
			<?=$arResult["STATUS_STRING"]?>
		</span>
	</div>
</div>
    <?php
    */
    ?>
<div class="item">
	<div class="personal">
		<a class="trg" href="<?=$arResult["PROFILE_URL"]?>">������ �������</a>
		<div class="menu-m">
			<div class="block2">
				<div class="r-border-shape">
					<div class="tb">
						<div class="cn l"></div>
						<div class="cn r"></div>
					</div>
					<div class="d dl"><i></i></div>
					<div class="d dr"><i></i></div>
					<div class="d dc"><i></i></div>
					<div class="cnt">
						<ul class="list">
							<li><a href="<?=$arResult["PROFILE_URL"]?>">������������� ������</a></li>
							<li><a href="/personal/order/">������ �������</a></li>
							<li><a href="/personal/basket.php">�������</a></li>
						</ul>
					</div>

					<div class="bb">
						<div class="cn l"></div>
						<div class="cn r"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="item">
	<form action="<?=$arResult["AUTH_URL"]?>" id="logout_form">
	<?foreach ($arResult["GET"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
		<input type="hidden" name="logout" value="yes" />
	</form>
	<div class="logout"><a href="#" onclick="$('#logout_form').submit(); return false;">�����</a></div>
</div>
<?endif?>

