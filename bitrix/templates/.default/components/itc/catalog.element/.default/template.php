<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	// ������ ����� ��������, ��� �� 1� ������������ ����� � ! � ������ �������� ������
	// ������ ����� ������ �� ������ ���� �� �����, �� ����� �� ������ ���� ����
	if (strpos($arResult["NAME"], '!') === 0)
		$arResult["NAME"] = substr($arResult["NAME"], 1);
?>
<?//echo "<pre>"; print_r($arResult); echo "</pre>";?>
<?CModule::IncludeModule('itconstruct.resizer');?>
<?
ob_start();
include($_SERVER['DOCUMENT_ROOT'] . '/inc/seo/topElementText.php');
$topElementText = ob_get_clean();

//������������� ������, ������� ��������
$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>IBLOCK_vendor_ID,"XML_ID"=>$arResult["PROPERTIES"]["vendor"]["VALUE"]), false, false, array("ID","NAME","CODE"));
if($ar = $res->Fetch())
{
	$arResult["PROPERTIES"]["VENDOR"]["VALUE"] = $ar["NAME"];
	$arResult["DISPLAY_PROPERTIES"]["vendor"]["DISPLAY_VALUE"] = $ar["NAME"];
}

?>
<div class="fontReducedElement hl0">�������������: <?=preg_replace("/<a.*>(.*)<\/a>/", "$1", $arResult["PROPERTIES"]["VENDOR"]["VALUE"])?>, <?=$arResult["PROPERTIES"]["country_text"]["VALUE"]?></div>
<div style="padding-bottom: 10px;"><?=$topElementText?></div>

<?
	// ��� ��������������� ��������-�������� ������ ����������� ��������� ������������ ������ ������
	if (in_array('5', CUser::GetUserGroup($USER->GetID())) || in_array('1', CUser::GetUserGroup($USER->GetID()))) :
?>
<div style="overflow: auto; padding-bottom: 10px;">
	<? CMain::IncludeFile('/inc/avail_form.php', array(
		'avail_type' => 'element',
		'date' => $arResult["PROPERTIES"]["AVAIL_DATE"]["VALUE"],
		'note' => $arResult["PROPERTIES"]["AVAIL_NOTE"]["VALUE"],
		'id' => $arResult['ID'],
		'element' => $arResult
	), array('SHOW_BORDER' => false)); ?>
	<? CMain::IncludeFile('/inc/price_form.php', array(
		'price_type' => 'element',
		'date' => $arResult["PROPERTIES"]["PRICE_DATE"]["VALUE"],
		'prices' => $arResult["PRICES"],
		'id' => $arResult['ID'],
		'element' => $arResult
	), array('SHOW_BORDER' => false)); ?>
</div>
<? endif; ?>

<div class="gap4 clearfix">
<?if(is_array($arResult["PREVIEW_PICTURE"]) || is_array($arResult["DETAIL_PICTURE"]) || !empty($arResult["IMAGE"])):?>
	<div class="img0 img">
		<?if(strlen($arResult["ADD_INFO"]["PROPERTY_CODE"])>2):?>
				<div style="height: 10px; margin-top: 5px;" class="char">���: <?=$arResult["ADD_INFO"]["PROPERTY_CODE"]?></div>
		<?endif;?>
		<div class="image3 image b-light-border">
			<?/*<img src="/rpic/c/<?=$arResult["DETAIL_PICTURE"]["ID"]?>_h200_w198_crop.jpg" alt="">*/?>
		<?if(!empty($arResult["IMAGE"])):?>
			<img src="<?=$arResult["IMAGE"]?>" alt="<?=$arResult["NAME"];?>" />
		<?elseif(is_array($arResult["DETAIL_PICTURE"])):?>
			<?/*<img src="/rpic/c/<?=$arResult["DETAIL_PICTURE"]["ID"]?>_h200_w198_crop.jpg" alt="">*/?>
			<?if($arResult["DETAIL_PICTURE"]["HEIGHT"] > 200 || $arResult["DETAIL_PICTURE"]["WIDTH"] > 198):?>
				<?if(($arResult["DETAIL_PICTURE"]["HEIGHT"] - 200) > ($arResult["DETAIL_PICTURE"]["WIDTH"] - 198)):?>
					<a class="lightboxx" rel="detail" href="<?=itc\Resizer::get($arResult["DETAIL_PICTURE"]["ID"], 'width', 800); ?>">
					<img src="<?=itc\Resizer::get($arResult["DETAIL_PICTURE"]["ID"], 'height', null, 243) ?>" alt="<?=$arResult["NAME"];?>"/>
					</a>
				<?else:?>
					<a class="lightboxx" rel="detail" href="<?=itc\Resizer::get($arResult["DETAIL_PICTURE"]["ID"], 'width', 800); ?>">
						<img src="<?=itc\Resizer::get($arResult["DETAIL_PICTURE"]["ID"], 'width', 198); ?>" alt="<?=$arResult["NAME"];?>"/>
					</a>
				<?endif;?>
			<?else:?>
				<a class="lightboxx" rel="detail" href="<?=itc\Resizer::get($arResult["DETAIL_PICTURE"]["ID"], 'width', 800); ?>">
					<img src="<?=itc\Resizer::get($arResult["DETAIL_PICTURE"]["ID"], 'auto', 198, 243); ?>" alt="<?=$arResult["NAME"];?>"/>
				</a>
			<?endif;?>
		<?elseif(is_array($arResult["PREVIEW_PICTURE"])):?>
			<?/*<img src="/rpic/c/<?=$arResult["PREVIEW_PICTURE"]["ID"]?>_h200_w198_crop.jpg" alt="">*/?>
			<?if($arResult["PREVIEW_PICTURE"]["HEIGHT"] > 200 || $arResult["PREVIEW_PICTURE"]["WIDTH"] > 198):?>
				<?if(($arResult["PREVIEW_PICTURE"]["HEIGHT"] - 200) > ($arResult["PREVIEW_PICTURE"]["WIDTH"] - 198)):?>
					<a class="lightboxx" rel="detail" href="/rpic/c/<?=$arResult["PREVIEW_PICTURE"]["ID"]?>_w800_landscape.jpg">
						<img src="<?=itc\Resizer::get($arResult["PREVIEW_PICTURE"]["ID"], 'height', null, 243) ?>" alt="<?=$arResult["NAME"];?>"/>
					</a>
				<?else:?>
					<a class="lightboxx" rel="detail" href="<?itc\Resizer::get($arResult["PREVIEW_PICTURE"]["ID"], 'width', 800); ?>">
						<img src="<?=itc\Resizer::get($arResult["PREVIEW_PICTURE"]["ID"], 'width', 198); ?>" alt="<?=$arResult["NAME"];?>"/>
					</a>
				<?endif;?>
			<?else:?>
				<a class="lightboxx" rel="detail" href="<?itc\Resizer::get($arResult["PREVIEW_PICTURE"]["ID"], 'width', 800); ?>">
					<img src="<?=itc\Resizer::get($arResult["PREVIEW_PICTURE"]["ID"], 'auto', 198, 243); ?>" alt="<?=$arResult["NAME"];?>"/>
				</a>
			<?endif;?>
		<?endif;?>
			
		</div>
	</div>
	
<?endif;?>

<?

$arPrice = array_shift($arResult["PRICES"]);
if(!empty($arPrice["DISCOUNT_VALUE"]) && !empty($arPrice["VALUE"])) {
	if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
		//$price = $arPrice["PRINT_DISCOUNT_VALUE"];
		/*if(abs(round($arPrice["DISCOUNT_VALUE"]) - $arPrice["DISCOUNT_VALUE"]) < 0.1) {
			$val = number_format($arPrice["DISCOUNT_VALUE"]);
		} else {
			$val = number_format($arPrice["DISCOUNT_VALUE"], 2, '.', '');
		}*/
		$val = round($arPrice["DISCOUNT_VALUE"]);
		if(!empty($arResult["PROPERTIES"]["CHANGE_UNIT"]["VALUE"]))
			$priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]) . "/" . $arResult["PROPERTIES"]["CHANGE_UNIT"]["VALUE"];
		else
			$priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]);
		$price = $priceVal;
	} else {
		//$price = $arPrice["PRINT_VALUE"];
		/*if(abs(round($arPrice["VALUE"]) - $arPrice["VALUE"]) < 0.1) {
			$val = number_format($arPrice["VALUE"], 0, '.', ' ');
		} else {
			$val = number_format($arPrice["VALUE"], 2, '.', ' ');
		}*/
		$val = round($arPrice["VALUE"]);
		if(!empty($arResult["PROPERTIES"]["CHANGE_UNIT"]["VALUE"]))
			$priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]) . "/" . $arResult["PROPERTIES"]["CHANGE_UNIT"]["VALUE"];
		else
			$priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]);
		$price = $priceVal;
	}
}
?>
	<div class="txt">
	
	

	<?if($arResult['ACTIVE'] != 'Y'){
		
		?>
		
	
		<style>
		
			.no-active-block-small {
				width: 435px;
				height: 80px;
				background: no-repeat url(/images/no-active-bg-small.png);
				padding-top: 20px;
				text-align: center;
				margin-bottom: 20px;
			}
			
			.no-active-block-small a {
				display: block;
				margin-top: 5px;
				color: #cc6600;
				text-decoration: underline;
			}
		
		</style>
		
		<?
		$brandID = $arResult['PROPERTIES']['BRAND_GUID']['VALUE'];
		$brandName = '';
		if(intval($brandID) > 0){
			$brandDB = CIBlockElement::GetByID($brandID);
			if($brand = $brandDB->GetNext())  $brandName = $brand['NAME'];
		}
		?>
		
		<div class="no-active-block-small">
			������������� ���� ����� ��� ���������<br/>
			<b>������ �� ������������</b>.
			<?if(strlen($brandName) > 0):?>
				<a href="/catalog/<?=$arResult['IBLOCK_CODE']?>/vendor/<?=$brandName?>/">����������� ������ ����� �������������</a>
			<?else:?>
				<a href="/catalog/<?=$arResult['IBLOCK_CODE']?>/">����������� ������ ����� �������������</a>
			<?endif?>
		</div>
		
		<?
	}
	else {
		?>
		
		<div class="gap1 pricing">
			<div class="row row-b">
				<? if(canBuyElement($arResult) and $price) : ?>
				<div class="item4 item">
					<strong class="hl3">����:</strong> <span class="price3 price"><span class="val"><?=$price?></span></span>
				</div>
				<div class="item4 item">
					<form action="#">
						<fieldset>
							<div class="button5 button">
								<i class="helper"></i>
								<button type="button" onclick="location.href='<?=$arResult["ADD_URL"]?>'"><span class="button-txt">������</span><i></i></button>
							</div>
						</fieldset>
					</form>
				</div>
				<? else : ?>

				<? endif; ?>
				<span class="under"></span>
			</div>
		</div>
		
		<?
		
	}
	
	?>

	
		<table class="table0 table">
		<?
		//na
		//CML2_KRAT
		//CHANGE_UNIT
		/*global $USER;
		if($USER->IsAdmin()) {
			echo "<pre>"; print_r($arResult["DISPLAY_PROPERTIES"]); echo "</pre>";
		}*/
		?>
		<?
		global $USER;

		foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<?
			if($arProperty['CODE'] == 'COLLECTION') 
				$COLLECTION = $arProperty['DISPLAY_VALUE'];
			if($arProperty['CODE'] == 'BRAND_GUID') 
				$BRAND = $arProperty['DISPLAY_VALUE'];
			if($arProperty['CODE'] == 'SIZE') {
				$arSize = explode('�', $arProperty['DISPLAY_VALUE']);
				$SIZE_WIDTH = $arSize[1];
				$SIZE_HEIGTH = $arSize[0];
			}

			if($arProperty["DISPLAY_VALUE"] != "&lt;&gt;"):?>
			<tr>
				<td>
					<div class="char"><?=$arProperty["NAME"]?>:</div>
				</td>
				<td>
					<div class="hl2">
					<?if(is_array($arProperty["DISPLAY_VALUE"])):
						echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
					elseif($pid=="MANUAL"):
						?><a href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a><?
					elseif($pid == "CML2_KRAT"):
						echo round(preg_replace("/<a.*>(.*)<\/a>/", "$1", $arProperty["DISPLAY_VALUE"]), 3);
					else:
						echo preg_replace("/<a.*>(.*)<\/a>/", "$1", $arProperty["DISPLAY_VALUE"]);?>
					<?endif;?>

					</div>
				</td>
			</tr>
			<?endif;?>
		<?endforeach;?>
		</table>
	</div>
</div>


<?
if(!empty($arResult['NAV_BUTTONS']) && 2>9) {
	?>
	<div style="width: 100%; text-align: center; margin-bottom: 20px;">
		<?
		foreach($arResult['NAV_BUTTONS'] as $button) {
			?>
			<div class="new_button<?/* width50*/?>">
				<?/*<button onclick="window.location.href='<?=$button['PATH']?>'" type="button"><?=$button['NAME']?></button>*/?>
				<a class="a-button" href="<?=$button['PATH']?>" type="button"><?=$button['NAME']?></a>
			</div>
			<?
		}
		?>
		<div style="clear: both;"></div>
	</div>
	<?
}
?>

<?//echo "<pre>"; print_r($arResult["GALLERY"]); echo "</pre>";?>
<?if(!empty($arResult["GALLERY"])):?>
<h4 class="alt0">� ���������</h4>
<div class="gap4 gal">
	<ul id="gallery" class="gallery">
	<?foreach($arResult["GALLERY"] as $pictureFullName):?>
		<?$pic = split("\.", $pictureFullName);?>
		
		
		<?if(fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$pictureFullName,'r')):?>
		
		<li>
			<!--68 x 50-->
			<img src="/rpic_path/c/<?=$pic[0]?>_h50_w68_crop.<?=$pic[1]?>" alt="<?=str_replace('"', '', $arResult["NAME"]);?>"/>

			<div class="panel-content">
				<!--400 x 296-->
				<a class="lightboxx" rel="lightbox_gallery_interior" href="/rpic_path/c/<?=$pic[0]?>_h592_w800_auto.<?=$pic[1]?>">
					<img src="/rpic_path/c/<?=$pic[0]?>_h296_w400_crop.<?=$pic[1]?>" alt=<?=$arResult["NAME"]?>""/>
				</a>
			</div>
		</li>
		
		<?endif?>
		
		
	<?endforeach;?>
	</ul>
</div>
<?endif;?>


<?if(!empty($arResult["PREVIEW_TEXT"]) || !empty($arResult["DETAIL_TEXT"])):?>
	<div class="clearfix"></div>
	<br>
	<?if(!empty($arResult["PREVIEW_TEXT"])) echo "<p>" . $arResult["PREVIEW_TEXT"] . "</p>";?>
	<?if(!empty($arResult["DETAIL_TEXT"])) echo "<p>" . $arResult["DETAIL_TEXT"] . "</p>";?>
<?endif;?>

<?/*if(is_array($arResult["SECTION"])):?>
<div class="back">
	<a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>" onclick="history.back(); return false;">
		<div class="panel3 panel">
			<span class="helper"></span>
			<div class="panel-cont">
				��������� � ���������
			</div>
		</div>
	</a>
</div>
<?endif;*/?>


<script>
$(document).ready(function(){

	$('.lightboxx').lightBox();
	
});
</script>
<?
$BRAND = strip_tags($BRAND);
?>
<div itemscope itemtype="http://schema.org/Product" style="display:none;"><?
	if(strlen($arResult["NAME"])){
		?><span itemprop="name"><?=$arResult["NAME"]?></span><?
	}
	if(strlen($price)){
		?><div itemprop="offers" itemscope itemtype="http://schema.org/Offer"><?
			?><span itemprop="price"><?=$price?></span>
			<span itemprop="priceCurrency">RUB</span><?
		?></div><?
	}
	if(strlen($topElementText)){
		?><span itemprop="description"><?=$topElementText?></span><?
	}
	if(strlen($COLLECTION)){
		?><span itemprop="model"><?=$COLLECTION?></span><?
	}
	if(strlen($BRAND)){
		?><span itemprop="brand"><?=$BRAND?></span><?
	}
	if(strlen($SIZE_WIDTH)){
		?><span itemprop="width"><?=$SIZE_WIDTH?></span><?
	}
	if(strlen($SIZE_HEIGTH)){
		?><span itemprop="height"><?=$SIZE_HEIGTH?></span><?
	}
?></div>