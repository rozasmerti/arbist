<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
CModule::IncludeModule('iblock');
?>
<??>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}
?>

<?
//echo "<pre>"; print_r($arResult); echo "</pre>";
if(!in_array($arParams["IBLOCK_ID"], $arParams["PLITKA_SECTIONS"])) {
	 if(!empty($arResult["PROPERTIES"]["INTERIOR"]["VALUE"])) {
		//$interiorArray = explode(",", $arResult["PROPERTIES"]["INTERIOR"]["VALUE"]);
		// �� ��� ����� ���� ����������� � ;
		$interiorArray = preg_split( "/[,;]+/", $arResult["PROPERTIES"]["INTERIOR"]["VALUE"]);
		foreach($interiorArray as $val) {
			if(!in_array($val, $arResult["GALLERY"]))
				$arResult["GALLERY"][] = trim($val);
		}
	}
} else {
	$arSelect = Array("ID", "NAME", "PROPERTY_INTERIOR");
	$arFilter = Array(
		"IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"], 
		"SECTION_ID" => $_REQUEST["BRAND_ID"],
		"!PROPERTY_INTERIOR" => false,
		"ACTIVE" => "Y"
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ar_res = $res->GetNext()) {
		if(!empty($ar_res["PROPERTY_INTERIOR_VALUE"])) {
			//$interiorArray = explode(",", $ar_res["PROPERTY_INTERIOR_VALUE"]);
			// �� ��� ����� ���� ����������� � ;
			$interiorArray = preg_split( "/[,;]+/", $ar_res["PROPERTY_INTERIOR_VALUE"]);
			foreach($interiorArray as $val) {
				if(!in_array($val, $arResult["GALLERY"]))
					$arResult["GALLERY"][] = trim($val);
			}
		}
	}
}




CModule::IncludeModule("catalog");
global $USER;
$optimalPrice = CCatalogProduct::GetOptimalPrice($arResult["ID"],1,$USER->GetUserGroupArray());
$arResult["PRICES"] = array(
	array(
		"VALUE" => $optimalPrice["PRICE"]["PRICE"],
		"DISCOUNT_VALUE"=> $optimalPrice["DISCOUNT_PRICE"],
		"CURRENCY"=> $optimalPrice["PRICE"]["CURRENCY"],
	),
);


$arResult['NAV_BUTTONS'] = array();
$sectionDB = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID']);
if($section = $sectionDB->GetNext())  {
	$sectionActive = $section['ACTIVE'];
	$arResult['NAV_BUTTONS'][] = array(
		'PATH' => '/catalog/'.$section['IBLOCK_CODE'].'/'.$section['ID'].'/',
		//'NAME' => '������� � ��������� '.$section['NAME']
		'NAME' => '��������� '.$section['NAME']
	);
}
if($sectionActive == 'N'){
	$arResult['ACTIVE'] = 'N';
}

$arVendor = false;
if($arResult['PROPERTIES']['BRAND_GUID']['VALUE']) {
	$res = CIBlockElement::GetByID($arResult['PROPERTIES']['BRAND_GUID']['VALUE']);
	if($ar_res = $res->GetNext())  {
		$arVendor = $ar_res;
	}
}
$iblockName = false;
$res = CIBlock::GetByID($arResult['IBLOCK_ID']);
if($ar_res = $res->GetNext())
  $iblockName = $ar_res['NAME'];

if($arVendor && !empty($arVendor['CODE']) && $iblockName) {
	$arResult['NAV_BUTTONS'][] = array(
		'PATH' => str_replace(' ','_',strtolower('/catalog/'.$arResult['IBLOCK_CODE'].'/vendor/'.$arVendor['CODE'])).'/',
		//'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblockName)
		'NAME' => $iblockName . ' ' . $arVendor['NAME'] 
	);
}

if($arVendor && !empty($arVendor['CODE'])) {
	// ���� ��� ��������� ���� �������
	$arIblocks = array();
	$res = CIBlock::GetList(	
		Array(), 	
		Array(		
			'TYPE'=>'catalog', 		
			'SITE_ID'=>SITE_ID, 		
			'ACTIVE'=>'Y',
			'!CODE' => $arResult['IBLOCK_CODE'],
			'!ID' => array(12,29) // �������� ����������� ������
		), 
		false
	);
	while($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;
	
	//��� ������� ��������� ���� �� ��� ������ ����� ������
	foreach($arIblocks as $iblock) {
		
		$res = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock['ID'], "CODE" => 'BRAND_GUID'));
		if($res->fetch()) {
			$res = CIBlockElement::GetList( 
				Array("SORT"=>"ASC"), 
				Array('IBLOCK_ID' => $iblock['ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_GUID' => $arVendor['ID']), 
				false, 
				false, 
				Array('ID')
			);	
			if($res->SelectedRowsCount()) {
				$arResult['NAV_BUTTONS'][] = array(
					'PATH' => str_replace(' ','_',strtolower('/catalog/'.$iblock['CODE'].'/vendor/'.$arVendor['CODE'])).'/',
					//'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblock['NAME'])
					'NAME' => $iblock['NAME'] . ' ' . $arVendor['NAME']
				);			
			}
		}
	}
}


//����� ��� ������� �������� � �����-��������. ��� ���� � ���, ��� ����������� ������� �������� ���� ���������
//� ������ ����� ����������� �������� � ������_���������, ��� � ���������.
$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ID"=>$arResult["ID"]), false, false, array("ID","IBLOCK_ID","PROPERTY_".PROPERTY_CODE));
$arResult["ADD_INFO"] = array();
while ($ar = $res->Fetch())
{
	$arResult["ADD_INFO"]["PROPERTY_CODE"] = $ar["PROPERTY_".PROPERTY_CODE."_VALUE"];
}


//���������� �������� - �����������, ����������� � ���������������� ���������
$arResult['IMAGE'] = '';
if(strlen($arResult['PROPERTIES']['item_pic']['VALUE']) > 0){
	$fileName = trim($arResult['PROPERTIES']['item_pic']['VALUE']);
	$file_path = '';
	if(strlen($fileName) > 3) {
		if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $fileName)) {
			$original = '/images/catalog/' . $fileName;
			$file_path = $_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $fileName;
		} elseif (file_exists($_SERVER["DOCUMENT_ROOT"] . '/upload/products_foto/' . $fileName)) {
			$original = '/upload/products_foto/' . $fileName;
			$file_path = $_SERVER["DOCUMENT_ROOT"] . '/upload/products_foto/' . $fileName;
		}
	}

	if(strlen($file_path) > 0){
		//������
		$destinationFile = $_SERVER["DOCUMENT_ROOT"] . '/upload/resize_cache/product/210_220_'.$fileName;
		$imgPath = '/upload/resize_cache/product/210_220_'.$fileName;
		$resized = \CFile::ResizeImageFile(
			$file_path,
			$destinationFile,
			array('width'=>200, 'height'=>220),
			BX_RESIZE_IMAGE_PROPORTIONAL
		);

		if($resized){
			$arResult['IMAGE'] = $imgPath;
		}
	}
}

?>