<?
CModule::AddAutoloadClasses(
	"local.lib",
    $a = array(
        'Local\Lib\Helpers\Files' => 'lib/helpers/files.php',
        'Local\Lib\Helpers\Validation' => 'lib/helpers/validation.php',
        'Local\Lib\Parts\Image' => 'lib/parts/image.php',
    )
);
?>