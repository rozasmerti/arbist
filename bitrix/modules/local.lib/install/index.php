<?
IncludeModuleLangFile(__FILE__);
Class local_lib extends CModule
{
	const MODULE_ID = 'local.lib';
	var $MODULE_ID = 'local.lib'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("local.lib_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("local.lib_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("local.lib_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("local.lib_PARTNER_URI");
	}

	function DoInstall()
	{
		global $APPLICATION;
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
	}
}
?>
