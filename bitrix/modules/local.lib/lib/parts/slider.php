<?
namespace Local\Lib\Parts;

use Local\Lib\Helpers\Files;

class Slider
{

    /**
     * Images or image ids without resize
     * @var array
     */
    protected $slides = array();

    /**
     * Ready images by size
     * @var array
     */
    protected $arSlider = array(
        'ORIGINAL' => array(),
    );

    /**
     * @var array
     */
    protected $slideResizeMethods = array();

    /**
     * @var array
     */
    protected $slideParams = array();

    protected $defaultResizeMethod = BX_RESIZE_IMAGE_PROPORTIONAL;

    /**
     * Sizes list with options
     * @var array
     */
    protected $arSizes = array();

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * @var bool|array
     */
    protected $arImageFilters = false;

    /**
     * Slider constructor.
     * @param $arSizes
     * @param array|bool $arImageFilters
     */
    public function __construct($arSizes, $arImageFilters = false)
    {
        foreach ($arSizes as $key => $size) {
            if (isset($size['width']) && isset($size['height'])) {
                if (intval($size['width']) > 0 && intval($size['height']) > 0) {
                    $this->arSizes[$key] = $size;
                } else {
                    $this->errors[] = 'Wrong sizes given';
                }
            } else {
                $this->errors[] = 'Wrong sizes given';
            }
        }

        $this->arImageFilters = $arImageFilters;
    }

    /**
     * @param int|array $slide
     * @param int|bool $resizeMethod
     * @param array $params
     * @return bool
     */
    public function addSlide($slide, $resizeMethod = false, $params = array())
    {
        if (!empty($slide)) {
            $this->slides[] = $slide;
            if (!empty($resizeMethod)) {
                end($this->slides);
                $lastKey = key($this->slides);
                $this->slideResizeMethods[$lastKey] = $resizeMethod;
            }
            if (!empty($params)) {
                end($this->slides);
                $lastKey = key($this->slides);
                $this->slideParams[$lastKey] = $params;
            }
        } else {
            $this->errors[] = 'Empty slide';
        }

        return true;
    }

    /**
     * @param array $arSlides
     * @param int|bool $resizeMethod
     * @param array $params
     * @return bool
     */
    public function addSlidesList($arSlides, $resizeMethod = false, $params = array())
    {
        if (is_array($arSlides)) {
            foreach ($arSlides as $slide) {
                $this->addSlide($slide, $resizeMethod, $params);
            }
        } else {
            $this->errors[] = 'Empty slides list';
        }

        return true;
    }

    /**
     * Make slider and resort images
     * @return array
     */
    public function getSlider()
    {
        $this->makeSlider();
        $this->resortSlider();
        return $this->arSlider;
    }

    /**
     * Getting all original images and resize them to given sizes
     */
    protected function makeSlider()
    {
        foreach ($this->slides as $key => $img) {
            $original = Files::getOriginal($img);
            if (is_array($original)) {
                $this->arSlider['ORIGINAL'][$key] = $original;

                foreach ($this->arSizes as $code => $size) {
                    if (isset($this->slideResizeMethods[$key])) {
                        $method = $this->slideResizeMethods[$key];
                    } elseif (isset($size['method'])) {
                        $method = $size['method'];
                    } else {
                        $method = $this->defaultResizeMethod;
                    }
                    $this->arSlider[$code][$key] = Files::resizeImage($original, $size['width'], $size['height'], $method);
                }
            } else {
                $this->errors[] = 'Image ' . $img . ' not found';
            }

            //add slides data
            if (isset($this->slideParams[$key]) && is_array($this->slideParams[$key])) {
                if(!empty($this->arSlider['ORIGINAL'][$key])){
                    foreach ($this->arSlider as $type => $arSlides){
                        $this->arSlider[$type][$key] = array_merge($this->arSlider[$type][$key], $this->slideParams[$key]);
                    }
                }
            }
        }
    }

    /**
     * Make slide keys continuous. Remove gaps in slides list
     */
    protected function resortSlider()
    {
        $tmpSlider = $this->arSlider;
        $this->arSlider = array();

        foreach ($tmpSlider as $code => $arType) {
            foreach ($arType as $arSlide) {
                $this->arSlider[$code][] = $arSlide;
            }
        }
    }

}