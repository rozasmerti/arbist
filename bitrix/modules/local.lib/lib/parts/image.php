<?
namespace Local\Lib\Parts;

class Image
{

	/**
	 * @var array
	 */
	protected $images = array();

	/**
	 * @var int
	 */
	protected $width;

	/**
	 * @var int
	 */
	protected $height;

    /**
     * @var int
     */
    protected $method = BX_RESIZE_IMAGE_PROPORTIONAL;

	/**
	 * @var array
	 */
	protected $errors = array();

	/**
	 * Slider constructor.
	 * @param array $arSizes
	 */
	public function __construct($arSizes)
	{
		if(isset($arSizes['width']))
			$this->width = $arSizes['width'];

		if(isset($arSizes['height']))
			$this->height = $arSizes['height'];

        if(isset($arSizes['method']))
            $this->method = $arSizes['method'];
	}
	
	public function addImage($image)
	{
		if(!empty($image))
			$this->images[] = $image;
		else {
			$this->errors[] = 'Empty image';
		}
		
		return true;
	}
	
	public function addImagesList($arImages)
	{
		if(is_array($arImages)){
			foreach ($arImages as $image){
				$this->addImage($image);
			}
		} else {
			$this->errors[] = 'Empty slides list';
		}
		
		return true;
	}

	public function getFirstResized()
	{
		if(
			intval($this->width) > 0 && intval($this->height) > 0
		) {
			foreach ($this->images as $key => $img) {
				$result = \Local\Lib\Helpers\Files::universalResize(
				    $img,
                    array(
                        'width' => $this->width,
                        'height' => $this->height,
                        'method' => $this->method
                    )
                );
				if(is_array($result)){
					return $result;
				}
			}
		} else {
			$this->errors[] = 'Check sizes';
		}

		return false;
	}

	public function getFirstOriginal()
	{

		foreach ($this->images as $key => $img) {
			$result = \Local\Lib\Helpers\Files::getOriginal($img);
			if(is_array($result)){
				return $result;
			}
		}

		return false;
	}

}