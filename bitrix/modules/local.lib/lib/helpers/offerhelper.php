<?
namespace Local\Lib\Helpers;

use Bitrix\Main\Loader;

class OfferHelper
{

    /**
     * @var array
     */
    public $errors;

    /**
     * @var string
     */
	protected $currency = 'RUB';

    /**
     * @var int
     */
	protected $product_id;

    /**
     * @var array
     */
    protected $price;

    /**
     * @var array
     */
    protected $storesQuantity;

    /**
     * @var array
     */
    protected $refCatalogPriceID;

    /**
     * @var array
     */
    protected $barcode;

    /**
     * @var int
     */
    protected $measureId;

    /**
     * @var int
     */
    protected $quantityAll;

    /**
     * @var float
     */
    protected $weight;

    /**
     * @var \CPrice
     */
    protected $CPrice;

    /**
     * @var \CCatalogProduct
     */
    protected $CCatalogProduct;

    /**
     * @var array
     */
    protected $measuresList = array();

    static private $instance;

    private function __clone() { /* ... @return Singleton */ }  // Защищаем от создания через клонирование
    private function __wakeup() { /* ... @return Singleton */ }  // Защищаем от создания через unserialize

    static public function getInstance() {
        if (empty(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct()
    {
        if(!Loader::includeModule('catalog')){
            die('No catalog module');
        }

        $this->CPrice = new \CPrice();
        $this->CCatalogProduct = new \CCatalogProduct();

        $this->clear();

        $dbPriceType = \CCatalogGroup::GetList(
            array("SORT" => "ASC"),
            array()
        );
        while ($arPriceType = $dbPriceType->Fetch()) {
            $this->refCatalogPriceID[$arPriceType["XML_ID"]] = $arPriceType["ID"];
        }


        $rsMeasures = \CCatalogMeasure::getList();
        while($arMeasure = $rsMeasures->Fetch()){
            $this->measuresList['BY_ID'][$arMeasure['ID']] = $arMeasure;
            $this->measuresList['BY_CODE'][$arMeasure['CODE']] =& $this->measuresList['BY_ID'][$arMeasure['ID']];
        }
    }
	
    
    public function clear()
    {
        $this->errors = array();
        $this->price = false;
        $this->product_id = false;
        $this->storesQuantity = false;
        $this->barcode = false;
        $this->measureId = false;
        $this->quantityAll = false;
        $this->weight = false;
    }

    /**
     * @param int $PRODUCT_ID
     * @return bool
     */
    public function setProduct($PRODUCT_ID)
    {
        $this->product_id = intval($PRODUCT_ID);
        return true;
    }

    /**
     * @param array|int $QUANTITY
     * @return bool
     */
    public function setQuantity($QUANTITY)
    {
        /*
        if(is_array($QUANTITY)){
            foreach($QUANTITY as $storeID => $amount){
                $storeID = intval($storeID);
                if($storeID > 0){
                    if(isset($this->data->storesList['BY_ID'][$storeID]))
                        $this->storesQuantity[$storeID] = floatval($amount);
                }
            }
        } else {
            $this->storesQuantity[1] = floatval($QUANTITY);
        }
        */

        $this->quantityAll = $QUANTITY;
    }

    /**
     * Setting price
     * @param $XML_ID string
     * @param $PRICE float
     * @return bool
     */
    public function setPrice($XML_ID, $PRICE)
    {
        if (empty($this->refCatalogPriceID[$XML_ID])) {
            $this->errors[] = 'Price type not found';
            return false;
        }

        $this->price[$this->refCatalogPriceID[$XML_ID]] = $PRICE;
        return true;
    }

    public function setBarcode($BARCODE)
    {
        $this->barcode[] = $BARCODE;
        return true;
    }

    public function setWeight($WEIGHT)
    {
        if (gettype($WEIGHT) == 'string')
            $WEIGHT = str_replace(",", ".", $WEIGHT);
        $this->weight = floatval($WEIGHT);
        return true;
    }

    public function setMeasure($measureCode)
    {
        if(intval($measureCode) > 0) {
            if(isset($this->measuresList['BY_CODE'][$measureCode])) {
                $this->measureId = $this->measuresList['BY_CODE'][$measureCode]['ID'];
            }
        }
        return true;
    }

    public function getMeasureByCode($measureCode)
    {
        if(intval($measureCode) > 0) {
            if(isset($this->measuresList['BY_CODE'][$measureCode])) {
                return $this->measuresList['BY_CODE'][$measureCode];
            }
        }

        return array();
    }

    public function getMeasureById($measureId)
    {
        if(intval($measureId) > 0) {
            if(isset($this->measuresList['BY_ID'][$measureId])) {
                return $this->measuresList['BY_ID'][$measureId];
            }
        }

        return array();
    }

    public function run()
    {
        if ($this->product_id !== false) {

            if ($this->price !== false)
                $this->addPrice();

            if ($this->storesQuantity !== false)
                $this->addStoreQuantity();

            if ($this->barcode !== false)
                $this->addBarcode();

            $this->updateCatalog();

        } else {
            $this->errors[] = "No product ID";
        }

        if(empty($this->errors))
            return true;
        else
            return false;
    }


    protected function addStoreQuantity()
    {
        $quantityAll = 0;
        $quantityChanged = false;

        $rsStore = \CCatalogStoreProduct::GetList(
            array(),
            array('PRODUCT_ID' => $this->product_id),
            false,
            false,
            array('ID', 'AMOUNT', 'STORE_ID')
        );

        while ($arStore = $rsStore->Fetch()) {
            if(isset($this->storesQuantity[$arStore['STORE_ID']]) && !empty($this->storesQuantity[$arStore['STORE_ID']])) {
                $quantityAll = $quantityAll + floatval($this->storesQuantity[$arStore['STORE_ID']]);
            }

            if(floatval($this->storesQuantity[$arStore['STORE_ID']]) == 0){
                $quantityChanged = true;
                \CCatalogStoreProduct::Delete($arStore['ID']);
                unset($this->storesQuantity[$arStore['STORE_ID']]);
            } elseif(
                !isset($this->storesQuantity[$arStore['STORE_ID']])
                || $arStore['AMOUNT'] != floatval($this->storesQuantity[$arStore['STORE_ID']])
            ){
                $quantityChanged = true;

                $arFieldsStore = Array(
                    "PRODUCT_ID" => $this->product_id,
                    "STORE_ID" => $arStore['STORE_ID'],
                    "AMOUNT" => $this->storesQuantity[$arStore['STORE_ID']]
                );

                \CCatalogStoreProduct::Update($arStore['ID'], $arFieldsStore);
                unset($this->storesQuantity[$arStore['STORE_ID']]);
            }
        }

        if(is_array($this->storesQuantity) && count($this->storesQuantity) > 0) {
            foreach ($this->storesQuantity as $storeID => $amount) {
                $amount = floatval($amount);
                $quantityChanged = true;
                $arFieldsStore = Array(
                    "PRODUCT_ID" => $this->product_id,
                    "STORE_ID" => $storeID,
                    "AMOUNT" => $amount
                );
                \CCatalogStoreProduct::Add($arFieldsStore);
            }
        }

        if($quantityChanged){
            $this->quantityAll = $quantityAll;
        }
    }


    protected function addPrice()
    {
        global $APPLICATION;
        $oldPrices = array();
        $dbProductPrice = $this->CPrice->GetListEx(
            array(),
            array("PRODUCT_ID" => $this->product_id),
            false,
            false,
            array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
        );
        while ($arProductPrice = $dbProductPrice->Fetch()) {
            $oldPrices[$arProductPrice["CATALOG_GROUP_ID"]] = $arProductPrice;
        }

        foreach ($this->price as $catalogGroupID => $price) {
            if (isset($oldPrices[$catalogGroupID]) && $price == $oldPrices[$catalogGroupID]["PRICE"])
                continue;

            $arFields = Array(
                "PRODUCT_ID" => $this->product_id,
                "CATALOG_GROUP_ID" => $catalogGroupID,
                "PRICE" => $price,
                "CURRENCY" => $this->currency,
            );

            if (isset($oldPrices[$catalogGroupID])) {
                $result = $this->CPrice->Update($oldPrices[$catalogGroupID]["ID"], $arFields);
            } else {
                $result = $this->CPrice->Add($arFields);
            }

            if (!$result) {
                $this->errors[] = $APPLICATION->GetException()->msg . " (" . $APPLICATION->GetException()->id . ")";
            }
        }
    }

    protected function addBarcode()
    {
        foreach ($this->barcode as $barcode) {
            \CCatalogStoreBarCode::add(
                array(
                    'PRODUCT_ID' => $this->product_id,
                    'BARCODE' => $barcode,
                )
            );
        }
    }

    protected function updateCatalog()
    {
        $arFieldsCatalog = array();

        if ($this->quantityAll !== false) {
            $productData = $this->CCatalogProduct->GetByID($this->product_id);
            $arFieldsCatalog['QUANTITY'] = $this->quantityAll - $productData['QUANTITY_RESERVED'];
        }

        if ($this->measureId !== false)
            $arFieldsCatalog["MEASURE"] = $this->measureId;

        if ($this->weight !== false)
            $arFieldsCatalog["WEIGHT"] = $this->weight;

        if(!empty($arFieldsCatalog)) {
            $arFieldsCatalog['ID'] = $this->product_id;
            $this->CCatalogProduct->Add($arFieldsCatalog);
        }
    }
}