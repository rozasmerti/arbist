<?php
namespace Local\Lib\Helpers;

use Bitrix\Main\Loader;

class Catalog
{

    public static function changeCurrencyView($arMinPrice)
    {
        if(is_string($arMinPrice)){
            $arMinPrice = str_replace('Р.', '<span class="rub">&#8381;</span>', $arMinPrice);
        } else {

            foreach ($arMinPrice as $key => $val) {
                $arMinPrice[$key] = str_replace('Р.', '<span class="rub">&#8381;</span>', $val);
            }
        }

        return $arMinPrice;
    }

    public static function changeMeasureView($measure)
    {
        $result = str_replace('м2', 'м<span class="sup">2</span>', $measure);
        return $result;
    }
}