<?php
namespace Local\Lib\Helpers;

class Video
{
    
    public static function getVideoHtml($arElement)
    {
        if(!is_array($arElement)){
            $arElement = self::getVideoElement($arElement);
        }

        $result = '';
        
        if(strlen($arElement['PROPERTIES']['YOUTUBE']['VALUE']) > 0){
            $result = self::getYoutubeVideoHtml($arElement['PROPERTIES']['YOUTUBE']['VALUE']);
        }
        
        return $result;
    }
    
    protected static function getYoutubeVideoHtml($videoId)
    {
        $result = '';
        if(strlen($videoId) > 0) {
            $result = '<iframe src="https://www.youtube.com/embed/' . $videoId . '" frameborder="0" allowfullscreen></iframe>';
        }
        
        return $result;
    }

    protected static function getVideoElement($elementID)
    {
        $arSelect = array(
            'ID',
            'IBLOCK_ID',
        );

        $arFilter = array(
            'IBLOCK_ID' => IBLOCK_ID__VIDEO,
            'ID' => $elementID
        );

        $arElement = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)
            ->Fetch();

        if(is_array($arElement)) {
            $arElement['PROPERTIES']['YOUTUBE'] = \CIBlockElement::GetProperty(
                $arElement['IBLOCK_ID'],
                $arElement['ID'],
                array(),
                array(
                    'CODE' => 'YOUTUBE'
                )
            )
                ->Fetch();
        }

        return $arElement;
    }
    
}