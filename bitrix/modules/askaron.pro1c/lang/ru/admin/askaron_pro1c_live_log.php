<?
$MESS ['askaron_pro1c_live_log_title'] = "����� ���";
$MESS ['askaron_pro1c_live_log_pull_not_installed'] = '������ Push and Pull �� ����������!';
$MESS ['askaron_pro1c_live_log_alert_message'] = '�������� ��������� ������ ���������� 5 ������. ����������, ��������� ������ � �� ���������� ��� ��������.';
$MESS ['askaron_pro1c_live_log_alert_message_nginx'] = '������ ��������� �� ������ nginx. ����� ������ ���� �����.';
$MESS ['askaron_pro1c_live_log_alert_message_nginx_error'] = '����� �� nginx �� ������ � ������� 5 ������. ���������� �������� �������� � ��������� ��� ���. ���� �� ��������, ������ ����� �� �������� �� �������� nginx. ������ ���������, ��������� ����� ������ Push and Pull ��� ������� ���������� "C����� ��������" (nginx-push-stream-module)� � ���������� ������� � ������ ��� nginx.';

$MESS ['askaron_pro1c_live_log_message'] = '����� ��� ��������� � �������� ������� ��������� �� ������������ �������.';

$MESS ['askaron_pro1c_live_log_warning'] = '
	<strong>��������:</strong> �������� ��������� ������ ��  ������� ���������� 5 ������.<br /><br />����� ������ Push and Pull ��� ������� ���������� "C����� ��������" (nginx-push-stream-module)� ���������.
	<br /><br />����� ����� ��� ������� ��� ��������, ������������� � <a href="settings.php?lang=#LANG#&amp;mid=pull&amp;mid_menu=1"> ��������� ������ Push and Pull</a> �������� ����� <em>��� ������� ���������� "������ ��������" (nginx-push-stream-module)�</em>.
	<br /><br />�����: ��������� nginx-push-stream-module �� ����� ������� � ������������ ������������ ������ �� ������. nginx-push-stream-module ��� �������� �� ����������� ������ �������� 4.2 � ����.';

$MESS ['askaron_pro1c_live_log_warning_nginx'] = '
	����� ������ Push and Pull <em>��� ������� ���������� "C����� ��������" (nginx-push-stream-module)�</em> <strong>��������</strong>.
	<br /><br />���� ����� ��� �� ��������, �� ������ ����� �� ����� ������� �� �������� nginx.
	<br /><br />��������� ����� <em>��� ������� ���������� "C����� ��������" (nginx-push-stream-module)�</em> � <a href="settings.php?lang=#LANG#&amp;mid=pull&amp;mid_menu=1"> ��������� ������ Push and Pull</a> � ����������� ������� � ������ ��� nginx.
	<br /><br />�����: ��������� nginx-push-stream-module �� ����� ������� � ������������ ������������ ������ �� ������. nginx-push-stream-module ��� �������� �� ����������� ������ �������� 4.2 � ����.';


$MESS ['askaron_pro1c_live_log_test'] = '��������� ������ ������ ����';
$MESS ['askaron_pro1c_live_log_off'] = '����� ��� ��������';
$MESS ['askaron_pro1c_live_log_off_details'] = '�������� ����� ��� � <a href="settings.php?mid=askaron.pro1c&amp;lang=#LANG#&amp;mid_menu=2">���������� ������</a> (������ ������� ������).';
$MESS ['askaron_pro1c_live_log_clean'] = '�������� �������';
$MESS ['askaron_pro1c_live_log_counter'] = '�������: ';
$MESS ['askaron_pro1c_live_log_pull_install'] = '<a href="module_admin.php?lang=#LANG#">���������� ������</a> Push and Pull';

$MESS ['askaron_pro1c_live_log_settings'] = '��������� ������ �<a href="settings.php?mid=askaron.pro1c&amp;lang=#LANG#&amp;mid_menu=2">����������� ����� � 1�</a>�.';

?>
