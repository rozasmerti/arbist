<?
$MESS ['askaron_pro1c_tab_debug'] = "������� ������";

$MESS ['askaron_pro1c_header_files'] = "������ ������� ������ � 1�, ��� ������� ����������� ������";
$MESS ['askaron_pro1c_header_clear_cache'] = "��������� �����. ���������� ����� ������������ ���� ����������";
$MESS ['askaron_pro1c_header_failure_resistance'] = "������������������ ������";

$MESS ['askaron_pro1c_header_fast'] = "��������� ������";

$MESS ['askaron_pro1c_header_debug'] = "������� ������";
//$MESS ['askaron_pro1c_header_products'] = "�������� �������";


$MESS ['askaron_pro1c_more'] = "���";

$MESS['askaron_pro1c_settings_help'] = "������� ������, ��� ������� ����������� ������. ��������:
	<br /><br /><strong>/bitrix/admin/1c_exchange.php</strong> - ����������� �������� ������;
	<br /><br /><strong>/bitrix/admin/askaron_pro1c_exchange.php</strong> - ����� ����������� �������� ������. 1� � ��� �������� ����� ��� ��, ��� � <strong>/bitrix/admin/1c_exchange.php</strong>
	<br><br><strong>/crm/1c_exchange.php</strong> - �������� ������ ������� ��� �������24 � �������.

	";

$MESS ['askaron_pro1c_field_active']		= "����������";
$MESS ['askaron_pro1c_field_name']			= "A���� ��������";
$MESS ['askaron_pro1c_field_skip_product']	= "��������� ������ ���� � �������<br />(���� � ��������� ������� ����� ��������)";

$MESS ['askaron_pro1c_managed_cache_off'] = '����������� ��� <a href="cache.php?lang='.LANGUAGE_ID.'&amp;tabControl_active_tab=fedit4">��������</a>';


$MESS ['askaron_pro1c_header_clear_cache_help'] = '���������������� ���� �� ����������� ����������� ����� �������� �����
	�������� �� ����� ������ � 1� ��-�� ����,
	��� ��� �������� ������� ��������� ������������ ����������� ��� ���������.
	����������� ��� ��������� ������������ ����� ��� ���� ������� ���������.
	<br><br>���������� ����������� ������ ���� ��� ������ ��������� ����������� �������� ����.
	 <br><br>
	 ������ ��������� ���������
	 ����� ���� ��������������� �� ����� ������ � ��������� ����� �� ��������� ����� ����� � ������� ������.
	<br><br>
	<a href="http://askaron.ru/api_help/course1/lesson160/" target="_blank">��������� �������� ����� � ������������</a>
	 ';

$MESS ['askaron_pro1c_disable_clear_tag_cache_for_script'] = "��������� ����� ������������ ���� ����������<br>(��� ������ ��������� ������� ������ ����)";
$MESS ['askaron_pro1c_disable_clear_tag_cache_for_script_help'] = '
����� ��������� ���������� ����� ���� � ��������� ��� �������� �������.<br><br>
�������������.
������ ����� �������� ��� ����������� �������, ����� ��� ������������ �������� CIBlock::clearIblockTagCache().
<br><br>���� �����-������ ��� ���������� ���������� � ���� ������ ������ <nobr>$CACHE_MANAGER->ClearByTag("ibclock_id_4"),</nobr> �� ��� ������� ��������� �� CIBlock::clearIblockTagCache(4).
';

$MESS ['askaron_pro1c_clear_tag_cache_agent_enabled'] = "������������ ���������� ����������� ��� ���������� � ������� ������";
$MESS ['askaron_pro1c_clear_tag_cache_agent_enabled_help'] = '����� ������������ ������� ��� ����������, �� �� �� ������. ����� ���������, � ����� ���������� ���������� ���� ���������, ���� ������� ��� ��������.

<br><br>����� ��������� ��������: <strong>#LAST_DATE#</strong>';

$MESS ['askaron_pro1c_clear_tag_cache_agent_main_page'] = "����������� ��� �� ������� �������� ����� ����� ������ ���� �������";
$MESS ['askaron_pro1c_clear_tag_cache_agent_main_page_help'] = '
����� ������ ���� ���������� ����� ������ �� ������� �������� ����� � ������� ��� ��� ����������������� ������������.
<br><br>
��� ������� �������� ��������, ����� ����� ������ ���� � ������-������ ������������ ������ ������������ ��� ���� � ������ ����������� �������.
';



$MESS ['askaron_pro1c_clear_tag_cache_agent_interval'] = "�������� ������� ������";
$MESS ['askaron_pro1c_clear_tag_cache_agent_interval_2'] = "�����";

$MESS ['askaron_pro1c_clear_tag_cache_agent_interval_help'] = '�� ����� ������ ������� ������� ����� (������ ������������������) ��� ������� ����� (����� ������������ ����������� ������ �� �����). ���������� � ��������� 5-30 �����.';

$MESS ['askaron_pro1c_import_pause'] = "�������� ����� ������ ��� ������� �������";
$MESS ['askaron_pro1c_import_pause_2'] = "������";
$MESS ['askaron_pro1c_import_pause_help'] = '���������� ���������� ��������� ����� ������ ����������� ������� �������� �� ������ � ��������� ��������� �������������� ���������� ������� ���� �� ����� ������ �������. ��� ���� ������, ��� ������ �������: 2, 5, 10, 20 ������.<br /><br />����� ������ ���� ������������� <a href="1c_admin.php?lang=#LANG#">� ���������� ������ � 1�</a>. ������ 30 ������, �� �� ������ ��������� ������������� ���������.';

$MESS ['askaron_pro1c_time_limit'] = "������������ ����� ���������� ������ ����";
$MESS ['askaron_pro1c_time_limit_2'] = '������';
$MESS ['askaron_pro1c_time_limit_help'] = '�� ���� �������� max_execution_time=#TIME_LIMIT#.<br /><br />���������� ������� ����������� ����� ��� ���������� ������ ���� ������. ��������, 180 ��� 300. 0 � �������������� ����� ����. ���� ����� �� ������, max_execution_time ��������������� �� �����.';

$MESS ['askaron_pro1c_memory_limit'] = "������a����� ����� ������ ��������� ���� �������";
$MESS ['askaron_pro1c_memory_limit_2'] = '��������';
$MESS ['askaron_pro1c_memory_limit_help'] = '�� ���� �������� memory_limit=#MEMORY_LIMIT#.<br /><br />���������� ������� ����������� ���������� ������ ��� ���������� ������ ���� ������. ��������, 512 ��� 1024 ��������. -1 � �������������� ����� ����������� ������. ���� ����� �� ������, memory_limit ��������������� �� �����.';


$MESS ['askaron_pro1c_forbidden'] = "��������� ���������� �������";
$MESS ['askaron_pro1c_forbidden_help'] = '���������� ���� � ������� �����������, ���� ��� ���� �������� ��� ��������� ����� � 1�. ������ ��� �������� ����� �� ������ ������, ����� �� ��������� ����������������.';

$MESS ['askaron_pro1c_additional_settings'] = "�������������� ���������";

$MESS ['askaron_pro1c_header_quantity'] = "��������� ����������";


$MESS ['askaron_pro1c_quantity_set_to_zero'] = "���� ���������o �� ������ �� 1�, �� ������������� 0";
$MESS ['askaron_pro1c_quantity_set_to_zero_help'] = '����� ���������� �������� ���� �� ��������� ������, ��� �� ������� 1� ������������ ������ ������ ������ ������. � ����� ������ �������� �������� ������ ����������, � ������� ������� �� ����� �� ������������
	<br /><br />
	<a href="http://askaron.ru/api_help/course1/lesson99/" target="_blank">��������� �������� ����� � ������������</a>.';


$MESS ['askaron_pro1c_log'] = "���������� ��� ���� � ������� ���-����";

$MESS ['askaron_pro1c_log_trace'] = "���������� � ���-���� ������� ������ �������";
$MESS ['askaron_pro1c_log_trace_help'] = "���������� ����� ������, ������� ������������� ������� � ���-����";

$MESS ['askaron_pro1c_log_max_size'] = "������������ ������ ���-�����";
$MESS ['askaron_pro1c_log_max_size_2'] = "��������";

$MESS ['askaron_pro1c_log_max_size_help'] = "��� ���������� ������������� ������� ���-���� ����� ������.
��� �����, ����� ����� �� ����� �� ����������.";


//$MESS ['askaron_pro1c_log_help_warning'] = '<strong>��������! </strong> �� ���������� ��� ����� ��������� ����������. ���-���� �� �������� ����� ����� ����� �������.';

$MESS ['askaron_pro1c_log_help'] = '������ � ��� �������� ����������� ��������
<a href="http://dev.1c-bitrix.ru/api_help/main/functions/debug/addmessage2log.php">AddMessage2Log</a> � ���� #LOG_FILENAME#.
<br><br>
<a href="askaron_pro1c_log_view.php?lang='.LANGUAGE_ID.'" target="_blank">C������� ��������� ���-�����</a> (� ����� �������)
';



//$MESS ['askaron_pro1c_log_help_not_defined'] = '�� ������ ������� ������ ����, ���� ���������� ��������� LOG_FILENAME � /bitrix/php_interface/dbconn.php.';
//
//$MESS ['askaron_pro1c_log_help_not_exist'] = '���-���� �� ����������';
//$MESS ['askaron_pro1c_log_help_file_info'] = '<a href="#URL#" target="_blank">������� ���-����</a>
//	<br />������� <strong>#DATE#</strong>
//	<br />������ <strong>#BYTE#</strong> ����</a>';

//$MESS ['askaron_pro1c_log_clear'] = "�������� ���-����";


//$MESS ['askaron_pro1c_import_date'] = '���� ���������� ��������� ������� �������� �������';
//$MESS ['askaron_pro1c_offers_date'] = '���� ���������� ��������� ������� ��� � ��������';

$MESS ['askaron_pro1c_live_log'] = '����� ���';
//$MESS ['askaron_pro1c_live_log_title'] = '����� (�������������) ���';
$MESS ['askaron_pro1c_live_log_help'] = '����� ��� ��������� � �������� ������� ��������� �� ������� � ������ ������ ����� ��� 1�. ������������ ������ �������� <a href="http://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=41&amp;LESSON_ID=2033">Push and Pull</a>.';

$MESS ['askaron_pro1c_live_log_version'] = '<strong>��������!</strong> ��� ������ ������ ���� ������ ���� ���������� ������ �������� <a href="http://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=41&amp;LESSON_ID=2033">Push and Pull</a> ������ �� ���� 14.0.0. ������ #CURRENT_VERSION#. <a href="update_system.php?lang=#LANG#">��������</a> Push and Pull.';

$MESS ['askaron_pro1c_live_log_open'] = '<a href="askaron_pro1c_live_log.php?lang=#LANG#" target="_blank">������� ����� ���</a> (� ����� �������)';

$MESS ['askaron_pro1c_pull_not_installed'] = '<strong>������ Push and Pull �� ����������!</strong>';

$MESS ['askaron_pro1c_pull_install'] = '<a href="module_admin.php?lang=#LANG#">���������� ������</a> Push and Pull';


$MESS ['askaron_pro1c_pull_notice'] = '<strong>��������!</strong>
	<br /><br />����� ����� ��� ������� ��� �������� ������������� � <a href="settings.php?lang=#LANG#&amp;mid=pull&amp;mid_menu=1">��������� ������ Push and Pull</a> �������� ����� <em>��� ������� ���������� "������ ��������" (nginx-push-stream-module)�</em>.	
	<br /><br />�����: ��������� nginx-push-stream-module �� ����� ������� � ������������ ������������ ������ �� ������. nginx-push-stream-module ��� �������� �� ����������� ������ �������� 4.2 � ����.';


$MESS ['askaron_pro1c_copy_exchange_files'] = "���������� XML-����� ������ � ����� ������";
$MESS ['askaron_pro1c_copy_exchange_files_help'] = '����� ������� ���� �� ��������� 1�, ������� ������� ����� ������ �� �����. ��������, �� 11.1.10.199 (��� ����������) ��� �������� ����� ��� ������� ���� �������� �������.
<br><br>
�� ��������� ����� ���������. ��� ����� ���� ������������ ��� ��������� � ������ ������. ������ �� ������ �� �������� �����.
<br><br>
����� ������ <a target="_blank" href="/bitrix/admin/fileman_admin.php?lang='.LANGUAGE_ID.'&amp;path='.urlencode("/upload/1c_catalog_copy_askaron_pro1c").'">/upload/1c_catalog_copy_askaron_pro1c</a>
';


$MESS ['askaron_pro1c_fast_write'] = "������� ������ ������� �������";
$MESS ['askaron_pro1c_fast_write_help'] = '� ����������, ��� ���������� ������� ������� ������, ������ ����� ������������ �� ������� � ������. � ���-����� ����� ������� ����� ������ ��������.
	<br /><br />� ����� ������� ����� ����� �������� � ��������� ��� �� ���� ������ ������ ������������ �������. ����� ������� �������� ������ �� ��� ������, ��� � ��������� ����� �������.
	<br /><br />�����: �������� ����� ������ �������� � ���-����� �� � ����� ��������� �����.
	<br /><br /><a href="http://askaron.ru/api_help/course1/lesson78/" target="_blank">��������� �������� ����� � ������������</a>.';

$MESS ['askaron_pro1c_error_save'] = "������ �������� �� ��� ��������";
$MESS ['askaron_pro1c_error_save_header'] = "������ ��� ���������� ������";


?>
