<?
$MESS ['askaron_pro1c_last_title'] = "��������� �����";
$MESS ['askaron_pro1c_last_exchange_catalog'] = "�������� ������� �� 1�";
$MESS ['askaron_pro1c_last_exchange_catalog_import'] = "�������� �������";
$MESS ['askaron_pro1c_last_exchange_catalog_offers'] = "�������� �����������";
$MESS ['askaron_pro1c_last_exchange_catalog_prices'] = "����";
$MESS ['askaron_pro1c_last_exchange_catalog_rests'] = "�������";
$MESS ['askaron_pro1c_last_exchange_catalog_notes'] = 
"�� ������ ������� ������ CML 2.08 ���� � ������� ����������� � ����� ����� � ��������� ������������� offers.xml.
<br><br>
������� � ������ CML 2.08 (� ��������� �������� 4.0.0.0 ��� 1�, ������ 2014 ����) ������� � ���� ����������� � ��������� ������ prices.xml � rests.xml.";


$MESS ['askaron_pro1c_last_exchange_order'] = '������� ������� � ����� � 1�';

$MESS ['askaron_pro1c_last_exchange_order_pages'] = '�������� ������ ����';
$MESS ['askaron_pro1c_last_exchange_order_page'] = '�������� ������';
$MESS ['askaron_pro1c_last_exchange_order_for_example'] = '��������';
$MESS ['askaron_pro1c_last_exchange_order_time'] = '����� ��������� ������� ��������� ������ � ����� � 1�';
$MESS ['askaron_pro1c_last_exchange_order_time_committed'] = '����� ��������� �������� ������� ��������� ������ � ����� � 1�, ����� ������� 1� ���������� �������� �������';

$MESS ['askaron_pro1c_last_exchange_order_com'] = '��������';

$MESS ['askaron_pro1c_last_exchange_order_button'] = '��������';


$MESS ['askaron_pro1c_last_exchange_order_notes'] = 
	'�� ������ ��������� �������� ��������� �������� ������� � ����� � 1�.
	<br><br>
	���� ����� ��������, �� ��� ��������� ������ ���� �������� ��� ������, ������� ����.<br>
	���� ���������� ������������ �����, �� ���� �������� ������ ��������� ��� ���������� ����� ����� ������� ������.';



$MESS ['askaron_pro1c_last_settings'] = '������ �������� ������ ����� ������� � ���������� ������ �<a href="settings.php?mid=askaron.pro1c&amp;lang=#LANG#">����������� ����� � 1�</a>�.';


?>
