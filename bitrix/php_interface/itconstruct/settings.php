<?
define('SEO_COMMON_VENDOR_TEXT_IBLOCK_ID', 67);
define('SEO_INTERIOR_DOORS_IBLOCK_ID', 68);
define('INTERIOR_DOORS_IBLOCK_ID', 51);
define('BRANDS_IBLOCK_ID', 46);
define('INTERIOR_DOORS_PAGE_PROP_URL', '/interior_doors/');

//������ �������������� �� ������ � �������, ������������ �� �������� �����
global $arAdjectiveCountry;
$arAdjectiveCountry = array(
    '������' => array(
        'male' => array(
            'tp' => '�����������', 
            'vp' => '����������'
        ),
        'female' => array(
            'tp' => '����������', 
            'vp' => '����������'
        ),
        'plural' => '����������'
    ),
    '������' => array(
        'male' => array(
            'tp' => '������������', 
            'vp' => '�����������'
        ),
        'female' => array(
            'tp' => '�����������', 
            'vp' => '�����������'
        ),
        'plural' => '�����������'
    ),
    '�������' => array(
        'male' => array(
            'tp' => '����������', 
            'vp' => '���������'
        ),
        'female' => array(
            'tp' => '���������', 
            'vp' => '���������'
        ),
        'plural' => '���������'
    ),
    '�������' => array(
        'male' => array(
            'tp' => '�����������', 
            'vp' => '����������'
        ),
        'female' => array(
            'tp' => '����������', 
            'vp' => '����������'
        )
    ),
    '�����' => array(
        'male' => array(
            'tp' => '����������', 
            'vp' => '���������'
        ),
        'female' => array(
            'tp' => '���������', 
            'vp' => '���������'
        )
    ),
    '������-�����' => array(
        'male' => array(
            'tp' => '�����-����������', 
            'vp' => '�����-���������'
        ),
        'female' => array(
            'tp' => '�����-���������', 
            'vp' => '�����-���������'
        )
    ),
    '������' => array(
        'male' => array(
            'tp' => '���������', 
            'vp' => '��������'
        ),
        'female' => array(
            'tp' => '��������', 
            'vp' => '��������'
        ),
        'plural' => '��������'
    ),
    '����������' => array(
        'male' => array(
            'tp' => '������������', 
            'vp' => '�����������'
        ),
        'female' => array(
            'tp' => '�����������', 
            'vp' => '�����������'
        )
    ),
    '������' => array(
        'male' => array(
            'tp' => '���������', 
            'vp' => '��������'
        ),
        'female' => array(
            'tp' => '��������', 
            'vp' => '��������'
        )
    ),    
    '������' => array('male'   => array('tp' => '���������',
                                        'vp' => '��������'),
                      'female' => array('tp' => '��������',
                                        'vp' => '��������'),
                      'plural' => '��������'),
    '����������' => array('plural' => '�������������'),
    '����'       => array('plural' => '����������'),
    '���������'  => array('plural' => '�������������'),
    '���������'  => array('male'    => array('tp' => '������������',
                                            'vp' => '�����������'),
                          'female' => array('tp' => '�����������',
                                            'vp' => '�����������'),
                          'plural' => '�����������'),    
    '��������'   => array('plural' => '�����������'),
    '�������'    => array('plural' => '�����������'),
    '�����'      => array('male'   => array('tp' => '��������',
                                            'vp' => '�������'),
                          'female' => array('tp' => '�������',
                                            'vp' => '�������'),
                          'plural' => '�������'),    
    '���������'  => array('male'   => array('tp' => '��������',
                                            'vp' => '�������'),
                          'female' => array('tp' => '�������',
                                            'vp' => '�������'),
                          'plural' => '�������'),    
    '��������'   => array('male'   => array('tp' => '�����������',
                                            'vp' => '����������'),
                          'female' => array('tp' => '����������',
                                            'vp' => '����������'),
                          'plural' => '����������'),
    '��������' => array(
        'male' => array(
            'tp' => '���������', 
            'vp' => '��������'
        ),
        'female' => array(
            'tp' => '��������', 
            'vp' => '��������'
        )
    )    
);

global $arPurposeCyr;
$arPurposeCyr = array(
    '������ ��� �����' => "plitka-dlya-vanny'",
    '������ ��� �����' => 'plitka-dlya-kuxni',
    '������ ��� ����' => 'plitka-dlya-pola',
    '������ ��� ����' => 'plitka-dlya-sten',
    '������ ��� �����' => "plitka-dlya-uliczy'",
    '������ ��� ��������' => 'plitka-dlya-bassejna'
);
?>