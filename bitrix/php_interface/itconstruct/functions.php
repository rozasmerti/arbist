<?
// updated 10.04.2014 Vad
namespace itc;

function createSiteMap(){
    if(\CModule::IncludeModule('search')){	
        $NS = Array();
        $sm_max_execution_time = 0;            
        $sm_record_limit = 5000;	
        
        do {		
            $cSiteMap = new \CSiteMap;	
            $NS = $cSiteMap->Create("s2", array($sm_max_execution_time, $sm_record_limit), $NS);
        } while(is_array($NS));
    }
	
	addSeoSiteMap('s2');
	
    return 'itc\createSiteMap();';
}

function addSeoSiteMap($siteId, $filePrefix = 'sitemap_', $arOptions = array()) {
	
	$menuIblockId = 47;
	
	if(!\CModule::IncludeModule('search')) {
		return;
	}
	
	if(empty($siteId)) {
		return;
	}
	
	if(!$arSite = \CSite::GetByID($siteId)->Fetch()) {
		return;
	}
	
	$siteMapIndexFilePath = $arSite['ABS_DOC_ROOT'] . $arSite['DIR'] . $filePrefix . 'index.xml';
	
	if(!file_exists($siteMapIndexFilePath)) {
		return;
	}
	
	if(is_array($arOptions) && ($arOptions['USE_HTTPS'] == 'Y')) {
		$strProto = 'https://';
	} else {
		$strProto = 'http://';
	}
	
	// add sitemap_seo.xml in sitemap_index.xml
	$cSiteMap = new \CSiteMap;	
	
	$indexXml = simplexml_load_file($siteMapIndexFilePath);

	$strSeoFile = $arSite['DIR'] . $filePrefix . 'seo.xml';
	$strSeoTime = $cSiteMap->TimeEncode(filemtime($siteMapIndexFilePath));
	
	$newSitmap = $indexXml->addChild('sitemap', '');
	$newSitmap->addChild('loc', $strProto . $arSite['SERVER_NAME'] . $strSeoFile);
	$newSitmap->addChild('lastmod', $strSeoTime);
	
	file_put_contents($siteMapIndexFilePath, $indexXml->asXML());
	
	// generating sitemap_seo.xml
	\CModule::IncludeModule('iblock');	
	$arFilter = array(
		'IBLOCK_ID' => $menuIblockId,
        'ACTIVE' => 'Y',
	);  
	
	$arSelect = array(
		'ID', 
		'NAME', 
		'PROPERTY_IBLOCK_ID'
	);
	
	$res = \CIBlockElement::GetList(array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);  
	while($arRes = $res->GetNext()) {
		$tempIblockIds[] = $arRes['PROPERTY_IBLOCK_ID_VALUE'];
	}
    
	if(empty($tempIblockIds)) {
		return;
	}
	
	$arFilter = array(
		'TYPE' => 'catalog', 
		'SITE_ID' => $siteId, 
		'ACTIVE' => 'Y', 
		'ID' => $tempIblockIds
	);
	$res = \CIBlock::GetList(array('SORT' => 'ASC'),	$arFilter, false);
	while($arRes = $res->Fetch()) {
		$seoData['IBLOCKS_INFO'][$arRes['ID']] = $arRes;
	}
    global $DB;
    $query = '
		SELECT 
			`seo_association`.`element_id` as EID, 
			`seo_association`.`vendor_name` as VN, 
			`seo_association`.`country_name` as CN, 
			`seo_association`.`purpose_name` as PN, 
			`b_iblock_element`.`IBLOCK_ID` as IBLOCK_ID 
		FROM 
			`seo_association` 
		INNER JOIN 
			`b_iblock_element` 
		ON 
			`seo_association`.`element_id` = `b_iblock_element`.`ID`;
		';



        
    $res = $DB->Query($query);
    while($arRes = $res->Fetch()) {
        if(!empty($arRes['VN']) && $arRes['VN'] != '&lt;&gt;') {
            if(!in_array($arRes['IBLOCK_ID'], $seoData['VENDOR_LIST'][$arRes['VN']])) {
                $seoData['VENDOR_LIST'][$arRes['VN']][] = $arRes['IBLOCK_ID'];
            }
        }
        if(!empty($arRes['CN']) && $arRes['CN'] != '&lt;&gt;') {
             if(!in_array($arRes['IBLOCK_ID'], $seoData['COUNTRY_LIST'][$arRes['CN']]))
                $seoData['COUNTRY_LIST'][$arRes['CN']][] = $arRes['IBLOCK_ID'];
        }
        if(!empty($arRes['PN']) && $arRes['PN'] != '&lt;&gt;') {
            if(!in_array($arRes['IBLOCK_ID'], $seoData['PURPOSE_LIST'][$arRes['PN']]))
                $seoData['PURPOSE_LIST'][$arRes['PN']][] = $arRes['IBLOCK_ID'];
        }
    }
    
    $arBrandGuidFilter = Array(   
        'IBLOCK_CODE' => 'brand_guid',
        'ACTIVE' => 'Y',
        '!NAME' => '<>',
    );
	
	$arSelect = array(
		'ID', 
		'NAME', 
		'CODE'
	);

    $allBrands = array();
    $res = \CIBlockElement::GetList(array('NAME' => 'ASC'), $arBrandGuidFilter, false, false, $arSelect);
    while($ar_fields = $res->GetNext()) {
        $allBrands[$ar_fields['NAME']] = $ar_fields['CODE'];
    }

    $seoData['ALL_BRANDS'] = $allBrands;
	
	file_put_contents($arSite['ABS_DOC_ROOT'] . $strSeoFile, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
	
	foreach($seoData['IBLOCKS_INFO'] as $iblock) {
		$strURL = $cSiteMap->LocationEncode($strProto . $arSite['SERVER_NAME'] . $cSiteMap->URLEncode('/catalog/' . $iblock['CODE'] . '/', 'UTF-8'));
		$strTime = $cSiteMap->TimeEncode(time());

		$strToWrite = "\t<url>\n\t\t<loc>" . $strURL . "</loc>\n\t\t<lastmod>" . $strTime . "</lastmod>\n\t</url>\n";
		file_put_contents($arSite['ABS_DOC_ROOT'] . $strSeoFile, $strToWrite, FILE_APPEND);
	}
	
	
	foreach($seoData['COUNTRY_LIST'] as $name => $iblocks) {
		foreach($iblocks as $key => $iblockId) {
			if(empty($seoData['IBLOCKS_INFO'][$iblockId]['CODE']) || empty($name)) {
				continue;
			}
			$strURL = $cSiteMap->LocationEncode($strProto . $arSite['SERVER_NAME'] . $cSiteMap->URLEncode('/catalog/' . $seoData['IBLOCKS_INFO'][$iblockId]['CODE'] . '/country/' . TranslateIt($name) . '/', 'UTF-8'));
			$strTime = $cSiteMap->TimeEncode(time());

			$strToWrite = "\t<url>\n\t\t<loc>" . $strURL . "</loc>\n\t\t<lastmod>" . $strTime . "</lastmod>\n\t</url>\n";
			file_put_contents($arSite['ABS_DOC_ROOT'] . $strSeoFile, $strToWrite, FILE_APPEND);
		}
	}
	
	foreach($seoData['PURPOSE_LIST'] as $name => $iblocks) {
		$purposeVal = $name;
		$purposeVal = strtolower(str_replace(' ', '-', TranslateIt($name, 'ru_en')));
		foreach($iblocks as $key => $iblockId) {
			if(empty($seoData['IBLOCKS_INFO'][$iblockId]['CODE']) || empty($purposeVal)) {
				continue;
			}
			$strURL = $cSiteMap->LocationEncode($strProto . $arSite['SERVER_NAME'] . $cSiteMap->URLEncode('/catalog/' . $seoData['IBLOCKS_INFO'][$iblockId]['CODE'] . '/purpose/' . $purposeVal . '/', 'UTF-8'));
			$strTime = $cSiteMap->TimeEncode(time());

			$strToWrite = "\t<url>\n\t\t<loc>" . $strURL . "</loc>\n\t\t<lastmod>" . $strTime . "</lastmod>\n\t</url>\n";
			file_put_contents($arSite['ABS_DOC_ROOT'] . $strSeoFile, $strToWrite, FILE_APPEND);
		}
	}
	
	foreach($seoData['VENDOR_LIST'] as $name => $iblocks) {
		foreach($iblocks as $key => $iblockId) {
			if(empty($seoData['IBLOCKS_INFO'][$iblockId]['CODE']) || empty($seoData['ALL_BRANDS'][$name])) {
				continue;
			}
			$strURL = $cSiteMap->LocationEncode($strProto . $arSite['SERVER_NAME'] . $cSiteMap->URLEncode('/catalog/' . $seoData['IBLOCKS_INFO'][$iblockId]['CODE'] . '/vendor/' . $seoData['ALL_BRANDS'][$name] . '/', 'UTF-8'));
			$strTime = $cSiteMap->TimeEncode(time());

			$strToWrite = "\t<url>\n\t\t<loc>" . $strURL . "</loc>\n\t\t<lastmod>" . $strTime . "</lastmod>\n\t</url>\n";
			file_put_contents($arSite['ABS_DOC_ROOT'] . $strSeoFile, $strToWrite, FILE_APPEND);
		}
	}
	file_put_contents($arSite['ABS_DOC_ROOT'] . $strSeoFile, "</urlset>\n", FILE_APPEND);
	
	/*
	echo '<pre>';	
	print_r($seoData);
	echo '</pre>';
	*/
	
	return true;
}

//
function getInteriorDoorsPageProps(){
    
    \CModule::IncludeModule('iblock');
    
    $arPageProperties = array();
        
    $interiorDoorsUrl = INTERIOR_DOORS_PAGE_PROP_URL;
    
     $rsProperties = \CIBlockProperty::GetList(
        array(
            "SORT" => "ASC"
        ),
        array(
            "IBLOCK_ID" => INTERIOR_DOORS_IBLOCK_ID
        )
    );
    
    while($arProperty = $rsProperties -> GetNext()){
        if(strpos($arProperty["CODE"], 'PAGE_') === 0)
        {
            $propertyName = $arProperty["NAME"];
            
            $propertyName = substr($propertyName, strpos($propertyName, ':'), strlen($propertyName) );
            $propertyName = ltrim($propertyName, ':');
            $propertyName = rtrim($propertyName, '.');
            $propertyName = trim($propertyName);
            
            $propCode = $arProperty["CODE"];
            $propCode = str_replace('PAGE_', '', $propCode);
            $propCode = strtolower($propCode);
            
            $arProperty["NAME"] = $propertyName;
            $arProperty["DETAIL_PAGE_URL"] = $interiorDoorsUrl . $propCode . '/'; 
            $arPageProperties[ $propCode ] = $arProperty;
        }    
    }
    
    return $arPageProperties;
}
?>