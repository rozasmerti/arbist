<?
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/constants.php"))
    require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/constants.php";

//if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/arbist/market/vendor/autoload.php"))
//    require_once $_SERVER['DOCUMENT_ROOT'] . '/arbist/market/vendor/autoload.php';
//ini_set('display_errors', 1);
//error_reporting(E_ALL);


AddEventHandler("main", "OnProlog", Array("MyClass", "OnPrologHandler"));

class MyClass
{
    function OnPrologHandler()
    {
        // AgentRemoveResponserTasks();
        //��������� �� ���� �� �������� ������������ ����
        //KAA ������� �������� ��������� ��� ��� ��� ����� ��������
        if (ACCESS_TO_AVEKOM_COMPONENT && $_REQUEST["MODE"] == "CONVERT" && $_REQUEST["CONFIG"]["invoice"]["active"] == "Y") {
            $_SESSION["LAST_CONVERT_DEAL_ID"] = $_REQUEST["ENTITY_ID"];
        }
    }
}


AddEventHandler('catalog', 'OnBeforePriceUpdate', 'arbistOnBeforePriceUpdate');
function arbistOnBeforePriceUpdate($ID, &$arFields)
{
    // @$_REQUEST['mode']=='import' - ��������� ������� ������� �� 1�
    if (@$_REQUEST['mode'] == 'import' && !empty($ID) && array_key_exists('PRODUCT_ID', $arFields) && CModule::IncludeModule("catalog")) {
        $res = CIBlockElement::GetList(array(), array('ID' => $arFields['PRODUCT_ID']), false, false, array('PROPERTY_PRICE_DATE'));
        if (($ar_res = $res->GetNext())
            && (isset($ar_res['PROPERTY_PRICE_DATE_VALUE']))
            && (strtotime($ar_res['PROPERTY_PRICE_DATE_VALUE']) > time())
        ) {
            $price = CPrice::GetByID($ID);
            $arFields['PRICE'] = $price['PRICE'];
        }
    }
}

CModule::IncludeModule('itconstruct.resizer');
CModule::IncludeModule('itconstruct.uncachedarea');

$arFiles = array(
    'itconstruct/settings.php',
    'itconstruct/pre.php',
    'itconstruct/functions.php',

    'property/property_iblockproperties.php',
    'property/property_iblocks.php',

    'uncachedcontent.php',
    'trailing_slashes.php'

    //'itconstruct/module_events.php'
);

foreach ($arFiles as $file) {
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/' . $file))
        require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/' . $file);
}
/*
if (file_exists($_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/itconstruct/functions.php")) {
    require_once $_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/itconstruct/functions.php";
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/property/property_iblockproperties.php")) {
    require_once $_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/property/property_iblockproperties.php";
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/property/property_iblocks.php")) {
    require_once $_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/property/property_iblocks.php";
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/uncachedcontent.php")) {
    require_once $_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/uncachedcontent.php";
}

if (file_exists($_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/trailing_slashes.php")) {
    require_once $_SERVER["DOCUMENT_ROOT"]. "/bitrix/php_interface/trailing_slashes.php";
}
*/
/*$_SERVER['HTTP_HOST'] = 'arbist.ru';
$_SERVER['SERVER_NAME'] = 'test.arbist.ru';
$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_REAL_IP2'];*/

//exit;
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/config.php"))
    require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/config.php";

require("ClassPL.php");

AddEventHandler("main", "OnBeforeUserLogin", Array("UserClass", "BeforeUserLogin"));
AddEventHandler("main", "OnBeforeUserRegister", Array("UserClass", "BeforeUserRegister"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("UserClass", "OnBeforeUserUpdateHandler"));
AddEventHandler("main", "OnBeforeUserAdd", Array("UserClass", "OnBeforeUserAddHandler"));

AddEventHandler("iblock", "OnAfterIBlockSectionAdd", Array("SectionClass", "UF_YA_Update"));

class SectionClass
{
    function UF_YA_Update(&$arFields)
    {
        if ($arFields["ID"] > 0) {
            $arField = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_" . $arFields["IBLOCK_ID"] . "_SECTION");
            $obEnum = new CUserFieldEnum;
            $rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => $arField["UF_YA"]["ID"]));
            if ($arEnum = $rsEnum->GetNext()) {
                $obSection = new CIBlockSection;
                $obSection->Update($arFields["ID"], array("UF_YA" => $arEnum["ID"]));
                /*global $USER;
                if($USER->GetID() == 549) {
                    echo "<pre>"; print_r($arFields); echo "</pre>";
                    echo "<pre>"; print_r($arField); echo "</pre>";
                    echo "<pre>"; print_r($arEnum); echo "</pre>"; die("upd");
                    
                }*/
            }
            /*}*/
        }
    }
}

class UserClass
{
    function beforeUserLogin($arFields)
    {
        if (SITE_ID == "s2" && $_REQUEST["USER_FIRST_TIME"] == "Y") {
            $user = new CUser;
            $arUserFields = Array(
                "EMAIL" => $arFields["LOGIN"],
                "LOGIN" => $arFields["LOGIN"],
                "LID" => SITE_ID,
                "ACTIVE" => "Y",
                "GROUP_ID" => array(3),
                "PASSWORD" => $arFields["PASSWORD"],
                "CONFIRM_PASSWORD" => $arFields["PASSWORD"],
            );

            $ID = $user->Add($arUserFields);
            if (intval($ID) == 0) {
                global $APPLICATION;
                $APPLICATION->ThrowException($user->LAST_ERROR);
                return false;
            }
        }
    }

    // ������� ���������� ������� "OnBeforeUserAdd"
    function OnBeforeUserAddHandler(&$arFields)
    {

        global $APPLICATION;
        $dir = $APPLICATION->GetCurDir();
        $adminka = "/bitrix/";
        $publ = "Y";
        if (strncasecmp($dir, $adminka, 8) == 0) {
            $publ = "N";
        } else {
            $publ = "Y";
        }

        if (SITE_ID == "s2" && $publ == "Y") {
            $arFields["LOGIN"] = $arFields["EMAIL"];
        }
    }


    function BeforeUserRegister(&$arFields)
    {
        // ��������� ����� �� e-mail
        if (SITE_ID == "s2") {
            //echo '<pre>';print_r($arFields);echo '</pre>';
            //die();
            $arFields['LOGIN'] = $arFields['EMAIL'];
        }
    }

    // ������� ���������� ������� "OnBeforeUserUpdate"
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        if (SITE_ID == "s2") {
            $filter = Array("ID" => $arFields["ID"]);
            $rsUsers = CUser::GetList(($by = "ID"), ($order = "desc"), $filter, Array("SELECT" => Array("UF_*")));
            if ($arUser = $rsUsers->Fetch()) {
                //echo "CODE: " . $arUser["UF_CODE"];
                $code = $arUser["UF_CODE"];
            }
            if ($code != $arFields["UF_CODE"] && !empty($arFields["UF_CODE"])) {
                $arSelect = Array("ID", "NAME", "PROPERTY_GROUP_ID");
                $arFilter = Array("IBLOCK_ID" => 48, "ACTIVE" => "Y", "NAME" => $arFields["UF_CODE"]);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if ($ar_res = $res->GetNext()) {
                    //������� ������ ������������ � �������� � ��� ������, ��������� � ����
                    $arGroups = CUser::GetUserGroup($arFields["ID"]);
                    //������ ��� ������ ������, ������� ���� �� �����
                    foreach ($GLOBALS["GROUP_STATUS"] as $key => $val) {
                        if ($res = array_search($key, $arGroups))
                            unset($arGroups[$res]);
                    }
                    $arGroups[] = $ar_res["PROPERTY_GROUP_ID_VALUE"];
                    CUser::SetUserGroup($arFields["ID"], $arGroups);

                    //����� ��� ���������� (14.05.2012 �� ������� ������� ������ ����������� �����.)
                    /*$el = new CIBlockElement;
                    $arLoadProductArray = Array(
                        "ACTIVE" => "N"
                    );
                    $res = $el->Update($ar_res["ID"], $arLoadProductArray);*/
                } else {
                    global $APPLICATION;
                    $APPLICATION->ThrowException("�������� ���� ��� �� �����");
                    return false;
                }
            }
            //echo "<pre>"; print_r($arFields); echo "</pre>"; die();
        }
    }
}

AddEventHandler('iblock', 'OnBeforeIBlockPropertyAdd', 'itcOnCatalogVendorPropertyAdd');

function itcOnCatalogVendorPropertyAdd($arFields)
{
    if ($arFields['NAME'] != '�������������GUID') {
        return;
    }

    $arFields['PROPERTY_TYPE'] = 'E';
    $arFields['LINK_IBLOCK_ID'] = 46;
}


function PR($o)
{
    $bt = debug_backtrace();
    $bt = $bt[0];
    $dRoot = $_SERVER["DOCUMENT_ROOT"];
    $dRoot = str_replace("/", "\\", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $dRoot = str_replace("\\", "/", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    ?>
    <div style='font-size:9pt; color:#000; background:#fff; border:1px dashed #000;'>
        <div style='padding:3px 5px; background:#99CCFF; font-weight:bold;'>File: <?= $bt["file"] ?> [<?= $bt["line"] ?>
            ]
        </div>
        <pre style='padding:10px;'><? print_r($o) ?></pre>
    </div>
    <?
}

function PRA($o)
{
    if (CUser::isAdmin()) {
        $bt = debug_backtrace();
        $bt = $bt[0];
        $dRoot = $_SERVER["DOCUMENT_ROOT"];
        $dRoot = str_replace("/", "\\", $dRoot);
        $bt["file"] = str_replace($dRoot, "", $bt["file"]);
        $dRoot = str_replace("\\", "/", $dRoot);
        $bt["file"] = str_replace($dRoot, "", $bt["file"]);
        ?>
        <div style='font-size:9pt; color:#000; background:#fff; border:1px dashed #000;'>
            <div style='padding:3px 5px; background:#99CCFF; font-weight:bold;'>File: <?= $bt["file"] ?>
                [<?= $bt["line"] ?>]
            </div>
            <pre style='padding:10px;'><? print_r($o) ?></pre>
        </div>
        <?
    }
}


define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/logs/default_log.txt");


AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("ITC", "OnAfterIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("ITC", "OnAfterIBlockElementUpdateHandler"));

class ITC
{
    // ������� ���������� ������� "OnAfterIBlockElementUpdate"
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {

        if (isset($arFields["PREVIEW_PICTURE"]) || isset($arFields["DETAIL_PICTURE"]))
        {
            $arElement = CIBlockElement::GetByID($arFields["ID"])->Fetch();
            if ($arElement)
            {
                $pictNo = IntVal($arElement["DETAIL_PICTURE"]);
                if ($pictNo <= 0) $pictNo = IntVal($arElement["PREVIEW_PICTURE"]);
                $arPictInfo = CFile::GetFileArray($pictNo);
                if (is_array($arPictInfo)) {
                    if(file_exists($_SERVER['DOCUMENT_ROOT'] . $arPictInfo['SRC'] )) {
                        $imageHash = md5_file($_SERVER['DOCUMENT_ROOT'] . $arPictInfo['SRC']);
                    } else
                        $imageHash = false;

                    CIBlockElement::SetPropertyValuesEx($arElement["ID"], false, array("PICTURE_HASH"=>$imageHash));
                }
            }
        }

        if (isset($arFields['NO_UPDATE']) && $arFields['NO_UPDATE'] == 'Y') return;
        global $USER;

        $res = CIBlockElement::GetList(
        //Array("SORT"=>"ASC"),
            array(),
            Array('ID' => $arFields['ID'], 'IBLOCK_ID' => $arFields['IBLOCK_ID']),
            false,
            array('nTopCount' => 1),
            Array('ID', 'IBLOCK_ID', 'PROPERTY_PHOTO_NAME')
        );

        if ($item = $res->fetch()) {

            $pic = $item['PROPERTY_PHOTO_NAME_VALUE'];

            if (strlen($pic) != 0) {
                if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/upload/products_foto/" . $pic)) { // Vanes 2014-11-24 issue #9848
                    $arLoadProductArray = Array(
                        "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/upload/products_foto/" . $pic),
                        "PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/upload/products_foto/" . $pic),
                        "NO_UPDATE" => 'Y'
                    );

                    $el = new CIBlockElement;

                    $res = $el->Update($item['ID'], $arLoadProductArray);

                    // Vanes 2014-11-24 issue #9848
                    // �������� ������ ������� � ���������� ������� �� 1� ftp://arbist.ru/httpdocs/bitrix/components/itc/catalog.import.1c/component.php
                    //if($res){
                    //unlink($_SERVER["DOCUMENT_ROOT"]."/upload/products_foto/".$pic);
                    //}

                    //if($res) AddMessage2Log('IBLOCK_ID: '.$item['IBLOCK_ID'].'  ELEMENT_ID: '.$item['ID'].' has updated', "iblock");
                    //else AddMessage2Log('IBLOCK_ID: '.$item['IBLOCK_ID'].'  ELEMENT_ID: '.$item['ID'].' has not updated', "iblock");
                }
            }

            //else AddMessage2Log('IBLOCK_ID: '.$item['IBLOCK_ID'].'  ELEMENT_ID: '.$item['ID'].' has not updated. Picture not found', "iblock");

        }

        //else AddMessage2Log('IBLOCK_ID: '.$arFields['IBLOCK_ID'].'  ELEMENT_ID: '.$arFields['ID'].' has not updated', "iblock");

    }


}

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("SeoAssociation", "addUpdateElement"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("SeoAssociation", "addUpdateElement"));
AddEventHandler("iblock", "OnIBlockElementDelete", Array("SeoAssociation", "deleteElement"));

class SeoAssociation
{
    function addUpdateElement($arFields)
    {
        $ibRes = CIBlock::GetByID($arFields["IBLOCK_ID"]);
        if ($arIbRes = $ibRes->GetNext())
            $iblockType = $arIbRes["IBLOCK_TYPE_ID"];

        if ($iblockType == 'catalog' && $arFields["ACTIVE"] == "Y" && $arFields["RESULT"]) {
            $arFilter = Array(
                "IBLOCK_ID" => $arFields["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "ID" => $arFields["ID"]
            );
            $elementRes = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array("ID", "PROPERTY_COUNTRY", "PROPERTY_DESTINATION", "PROPERTY_BRAND_GUID"));
            if ($arElementFields = $elementRes->GetNext()) {
                $propArray = array(
                    "COUNTRY" => $arElementFields["PROPERTY_COUNTRY_VALUE"],
                    "DESTINATION" => $arElementFields["PROPERTY_DESTINATION_VALUE"],
                    "BRAND_GUID" => $arElementFields["PROPERTY_BRAND_GUID_VALUE"],
                );
            }

            // ������ �������� ��� ����������/������� ��������
            $addUpdateArray = array(
                "ELEMENT_ID" => $arFields["ID"]
            );

            // ������� ��� ��������(������, ��������� ��������, �����, ����������)
            $arFilter = Array(
                "IBLOCK_ID" => 47,
                "ACTIVE" => "Y",
                "PROPERTY_IBLOCK_ID" => $arFields["IBLOCK_ID"]
            );
            $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array("IBLOCK_SECTION_ID"));
            if ($arElementFields = $res->GetNext()) {
                $iblockTypeSectionId = $arElementFields["IBLOCK_SECTION_ID"];
            }
            if (!empty($iblockTypeSectionId)) {
                $sectionFilter = array(
                    "IBLOCK_ID" => 47,
                    "ID" => $iblockTypeSectionId
                );
                $sectionRes = CIBlockSection::GetList(Array("SORT" => "ASC"), $sectionFilter, false, Array("CODE"));
                if ($arSectionRes = $sectionRes->GetNext()) {
                    $addUpdateArray["TYPE"] = $arSectionRes["CODE"];
                }
            }

            // ������� �������� ������
            $addUpdateArray["COUNTRY"] = $propArray["COUNTRY"];

            // ������� �������� ����������
            $addUpdateArray["DESTINATION"] = $propArray["DESTINATION"];

            // ������� �������� �������������
            if (!empty($propArray["BRAND_GUID"])) {
                $arFilter = Array(
                    "IBLOCK_ID" => 46,
                    "ACTIVE" => "Y",
                    "ID" => $propArray["BRAND_GUID"]
                );
                $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array("NAME"));
                if ($arElementFields = $res->GetNext()) {
                    $addUpdateArray["BRAND_GUID"] = $arElementFields["NAME"];
                }
            }


            global $DB;
            $resQuery = $DB->Query("SELECT id FROM seo_association WHERE element_id = '" . $arFields["ID"] . "'");
            $arQueryFields = array(
                "element_id" => $arFields["ID"],
                "type" => "'" . $DB->ForSql($addUpdateArray["TYPE"]) . "'",
                "vendor_name" => "'" . $DB->ForSql($addUpdateArray["BRAND_GUID"]) . "'",
                "country_name" => "'" . $DB->ForSql($addUpdateArray["COUNTRY"]) . "'",
                "purpose_name" => "'" . $DB->ForSql($addUpdateArray["DESTINATION"]) . "'",
            );
            if ($arQuery = $resQuery->Fetch()) {
                $DB->Update("seo_association", $arQueryFields, "WHERE element_id='" . $arFields["ID"] . "'");
            } else {
                $DB->Insert("seo_association", $arQueryFields);
            }
        }
    }

    function deleteElement($id)
    {
        global $DB;
        $DB->Query("DELETE FROM seo_association WHERE element_id = '" . $id . "'");
    }
}

function itcYaExportUpdater()
{
    // �������������� ��������� ����� "�������������� � ������.������" ��� �������� ���������
    global $DB;
    $q = "UPDATE b_catalog_iblock SET YANDEX_EXPORT='Y' WHERE YANDEX_EXPORT='N' AND IBLOCK_ID IN (select ID from b_iblock where IBLOCK_TYPE_ID='catalog' AND ACTIVE='Y')";
    $DB->Query($q);
    return "itcYaExportUpdater()";
}


if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/triggers.php"))
    require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/triggers.php";


if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/itconstruct/eladdev.php"))
    require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/itconstruct/eladdev.php";

function TranslateIt($text, $direct = 'ru_en')
{


    $L['ru'] = array(
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�', '�', '�', '�', '�',
        '�', '�', '�'
    );


    $L['en'] = array(
        "YO", "ZH", "CZ", "CH", "SHH", "SH", "Y'",
        "E'", "YU", "YA", "yo", "zh", "cz", "ch",
        "sh", "shh", "y'", "e'", "yu", "ya", "A",
        "B", "V", "G", "D", "E", "Z", "I",
        "J", "K", "L", "M", "N", "O", "P",
        "R", "S", "T", "U", "F", "X", "''",
        "'", "a", "b", "v", "g", "d", "e",
        "z", "i", "j", "k", "l", "m", "n",
        "o", "p", "r", "s", "t", "u", "f",
        "x", "''", "'"
    );


    // ������������ ����� � �������� � ������� �������...
    if ($direct == 'en_ru') {
        $translated = str_replace($L['en'], $L['ru'], $text);

        // ������ �������� ��������� ������� ������� � �������� ������.
        $translated = preg_replace('/(?<=[�-��])�/', '�', $translated);
        $translated = preg_replace('/(?<=[�-��])�/', '�', $translated);
    } else // � ��������
        $translated = str_replace($L['ru'], $L['en'], $text);
    // ���������� ����������.
    return $translated;
}

function checkAvailDate($arElement)
{
    if ($arElement
        && isset($arElement["PROPERTIES"]["AVAIL_DATE"])
        && !empty($arElement["PROPERTIES"]["AVAIL_DATE"]["VALUE"])
        && strtotime($arElement["PROPERTIES"]["AVAIL_DATE"]["VALUE"]) > time()
    )
        return false;
    else
        return true;
}

function checkPriceDate($arElement)
{
    if ($arElement
        && isset($arElement["PROPERTIES"]["PRICE_DATE"])
        && !empty($arElement["PROPERTIES"]["PRICE_DATE"]["VALUE"])
        && strtotime($arElement["PROPERTIES"]["PRICE_DATE"]["VALUE"]) > time()
    )
        return false;
    else
        return true;
}

function canBuyElement($arElement)
{
    if ($arElement["CAN_BUY"])
        return checkAvailDate($arElement);
    else
        return false;
}

function dump($ar)
{
    echo('<pre>');
    print_r($ar);
    echo("</pre>");
}


AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");
function OnBeforeUserRegisterHandler(&$arFields)
{
    if(isset($_REQUEST['action_code']) && strlen($_REQUEST['action_code']) > 0){
        $availableCodes = array(
            'zima2017'
        );

        //�������� ���� �����
        $actionCodeValid = false;
        if (in_array(strtolower($_REQUEST['action_code']), $availableCodes)) {
            $actionCodeValid = true;
        }


        if($actionCodeValid) {
            //���� ��� ����� ������ �����, �� ��������� � ������ ������
            if(is_array($arFields['GROUP_ID'])){
                $arFields['GROUP_ID'][] = 9;
            }
        } else {
            //���� �������, �� ���������� ������
            $GLOBALS['APPLICATION']->ThrowException('��� ����� ������ �������');
            return false;
        }
    }

    return true;
}
