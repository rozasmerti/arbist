<?
/*����� 03.05.2012*/
/*
AddEventHandler("main", "OnBeforeUserUpdate", array("CPL", "OnBeforeUserUpdateHandler"));
AddEventHandler("main", "OnBeforeUserAdd", array("CPL", "OnBeforeUserAddHandler"));
*/
class CPL extends CAllPL
{
	/**
	 * ��������� ������� ���� ����� ������������ ������� ��� ���������� ������
	 *
	 * @param array $arFields
	 */
	/*����� 03.05.2012*/
	/*
	function OnBeforeUserUpdateHandler(&$arFields)
	{
		if ($arFields["NAME"]=="" && $arFields["LAST_NAME"]=="")
		{
			$arFields["NAME"] = $arFields["LOGIN"];
		}
	}
	*/
	/**
	 * ��������� ������� ���� ����� ������������ ������� ��� ���������� ������
	 *
	 * @param array $arFields
	 */
	/*����� 03.05.2012*/
	/*
	function OnBeforeUserAddHandler(&$arFields)
	{
		if ($arFields["NAME"]=="" && $arFields["LAST_NAME"]=="")
		{
			$arFields["NAME"] = $arFields["LOGIN"];
		}
	}
	*/
	/**
	 * ��������� �������������� ������������ � �������.
	 *
	 * @param array $idGroups
	 * @return boolean
	 */
	function IsUserGroups($idGroups = array())
	{

		if (is_array($idGroups) && count($idGroups))
		{
			global $USER;
			$arGroups = array();
			$res = CUser::GetUserGroupEx($USER->GetId());
			while ($arGroup = $res->Fetch()){
			$arGroups[] = $arGroup["GROUP_ID"];
			}
			foreach ($idGroups as $gid)
			{
				if(!in_array($gid, $arGroups)) return false;
			}
		}
		elseif($idGroups == intval($idGroups))
		{
			global $USER;
			$arGroups = array();
			$res = CUser::GetUserGroupEx($USER->GetId());
			while ($arGroup = $res->Fetch()){
				$arGroups[] = $arGroup["GROUP_ID"];
			}
			if(!in_array($idGroups, $arGroups)) return false;
		}
		else
		{
			return false;
		}
		return true;
	}

	/**
	 * ��������� �������������� ������������ � ������.
	 *
	 * @param int $idGroups
	 * @return boolean
	 */
	function IsUserGroup($idGroups = array())
	{
		$isIn = false;
		if (is_array($idGroups) && count($idGroups))
		{
			global $USER;
			$arGroups = array();
			$res = CUser::GetUserGroupEx($USER->GetId());
			while ($arGroup = $res->Fetch()){
			$arGroups[] = $arGroup["GROUP_ID"];
			}
			foreach ($idGroups as $gid)
			{
				if(in_array($gid, $arGroups)) $isIn =  true;
			}

		}
		elseif($idGroups == intval($idGroups))
		{
			global $USER;
			$arGroups = array();
			$res = CUser::GetUserGroupEx($USER->GetId());
			while ($arGroup = $res->Fetch()){
				$arGroups[] = $arGroup["GROUP_ID"];
			}
			if(in_array($idGroups, $arGroups)) $isIn =  true;
		}
		else
		{
			return false;
		}
		return $isIn;
	}

	/**
	 * �������� ID ������ ���������� ���� ������������.
	 *
	 * @param int $useID
	 * @return integer
	 */
	function GetUserGroupsSocNet($userID = 0)
	{
		if (!$GLOBALS["USER"]->IsAuthorized() && $userID == 0)
			return;
		if ($GLOBALS["USER"]->IsAuthorized() && $userID == 0)
			$userID = $GLOBALS["USER"]->GetID();
		$arGroupFilter = array(
			"USER_ID" => $userID,
			"<=ROLE" => SONET_ROLES_USER,
			"GROUP_SITE_ID" => SITE_ID,
		);

		$dbGroups = CSocNetUserToGroup::GetList(
			array("GROUP_NAME" => "ASC"),
			$arGroupFilter,
			false,
			false
			//array("ID", "GROUP_ID", "GROUP_NAME")
		);

		while($arGroup = $dbGroups->GetNext())
		{
			return $arGroup["GROUP_ID"];
		}
		return false;
	}



	/**
	 * ��������� �������������� ������������ � ������ ���������� ����.
	 *
	 * @param int $idGroups
	 * @return boolean
	 */
	function IsUserGroupsSocNet($idGroups = array())
	{
		if (!$GLOBALS["USER"]->IsAuthorized())
			return;

		$arGroupFilter = array(
			"USER_ID" => $GLOBALS["USER"]->GetID(),
			"<=ROLE" => SONET_ROLES_USER,
			"GROUP_SITE_ID" => SITE_ID,
		);

		$dbGroups = CSocNetUserToGroup::GetList(
			array("GROUP_NAME" => "ASC"),
			$arGroupFilter,
			false,
			false,
			array("ID", "GROUP_ID", "GROUP_NAME")
		);

		while($arGroup = $dbGroups->GetNext())
		{
			$arGroups[] = $arGroup["ID"];
		}

		if (count($arGroups))
		{
			if (is_array($idGroups) && count($idGroups))
			{
				foreach ($idGroups as $gid)
				{
					if(!in_array($gid, $arGroups)) return false;
				}
			}
			elseif($idGroups == intval($idGroups))
			{
				if(!in_array($idGroups, $arGroups)) return false;
			}
			else
			{
				return false;
			}
		}
		else return false;

		return true;
	}

	/**
	 * ��������� ������������ �� �������������� � ��������� ������ (�������) � �������������� � ��������� ������� �������������
	 *
	 * @param int $arSocNetGroups
	 * @param array $arUserGroups
	 * @return boolean
	 */
	function InGroups($arSocNetGroups = array(), $arUserGroups = array())
	{
		$arGroups=array();

		if (is_array($arSocNetGroups) && count($arSocNetGroups))
		{
			foreach($arSocNetGroups as $sngid)
			{
				if (intval($sngid))
					$arGroups["SOCNET_G"][] = intval($sngid);
			}
		}
		elseif($arSocNetGroups == intval($arSocNetGroups))
		{
			$arGroups["SOCNET_G"][] = intval($arSocNetGroups);
		}
		else return false;

		if (is_array($arUserGroups) && count($arUserGroups))
		{
			foreach($arUserGroups as $ugid)
			{
				if (intval($ugid))
					$arGroups["USER_G"][] = intval($ugid);
			}
		}
		elseif($arUserGroups == intval($arUserGroups))
		{
			$arGroups["USER_G"][] = intval($arUserGroups);
		}
		else return false;

		if (count($arGroups["SOCNET_G"])>0 && count($arGroups["USER_G"])>0)
		{
			if(CPL::IsUserGroupsSocNet($arGroups["SOCNET_G"]) && CPL::IsUserGroup($arGroups["USER_G"])) return true; else return false;
		}
		else return false;
	}

	/**
	 * ���������� ������ ���� ����� ��������
	 *
	 * @param int $CompanyId
	 * @param int $IBLOCK_ID
	 * @return int
	 */
	function GetFileSize2Company($CompanyId = false, $IBLOCK_ID = false)
	{

		//if (intval($IBLOCK_ID)>0)
		{
			$CompanyId = intval($CompanyId);
			$arFilter = Array(
				"CODE"=>$CompanyId,
				"!PROPERTY_FILE_VALUE" => false,
			);
			if ($IBLOCK_ID)
				$arFilter["IBLOCK_ID"] = intval($IBLOCK_ID);
			$arSelect = array("ID", "PROPERTY_FILE",  "PROPERTY_VIDEO");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			$returnSize=0;
			if($res->SelectedRowsCount())
			{
				//$returnSize=0;
				while($arFile = $res->GetNext()){
				
					$arrFile = array();
					$arrFile = CFile::GetFileArray($arFile["PROPERTY_FILE_VALUE"]);
					//CPL::pr($arrFile);
					$returnSize = $returnSize + $arrFile["FILE_SIZE"];
				}
			}
			unset($arFilter["!PROPERTY_FILE_VALUE"]);
			$arFilter["!PROPERTY_video_VALUE"] = false;
			$arSelect = array("ID", "PROPERTY_video");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			//$returnSize=0;
			if($res->SelectedRowsCount())
			{
				//$returnSize=0;
				while($arFile = $res->GetNext()){
					$arrFile = array();
					$arrFile = CFile::GetFileArray($arFile["PROPERTY_VIDEO_VALUE"]);
					if ($arrFile) 
						$returnSize = $returnSize + $arrFile["FILE_SIZE"];
					$arrFile = CFile::GetFileArray($arFile["PROPERTY_FILE_VALUE"]);
					if ($arrFile) 
						$returnSize = $returnSize + $arrFile["FILE_SIZE"];
				}
			}
				return $returnSize;
		}
		return false;
	}

	/**
	*  ��������� ������� ���� ����� ���������� ������������
	*  echo '��� ��� '.declension('<b>20</b>','��� ���� ���').'!';
	*  ������� ���� ��� <b>20</b> ���!�
	*
	*/
	/**
	 * ��������� ������� ���� ����� ���������� ������������
	 * echo '��� ��� '.declension('<b>20</b>','��� ���� ���').'!';
	 * ������� ���� ��� <b>20</b> ���!�
	 *
	 * @param string/int $digit �����
	 * @param string $expr ���������� �����
	 * @param boolean $onlyword ���������� ������ ���������
	 * @return string
	 */
	function declension($digit,$expr,$onlyword=false)
	{
		if(!is_array($expr))
			$expr = array_filter(explode(' ', $expr));
		if(empty($expr[2]))
			$expr[2]=$expr[1];
		$i=preg_replace('/[^0-9]+/s','',$digit)%100; //intval �� ������ ��������� ��������
		if($onlyword) $digit='';
		if($i>=5 && $i<=20)
			$res=$digit.' '.$expr[2];
		else
		{
			$i%=10;
			if($i==1) $res=$digit.' '.$expr[0];
			elseif($i>=2 && $i<=4) $res=$digit.' '.$expr[1];
			else $res=$digit.' '.$expr[2];
		}
		return trim($res);
	}

	/**
	 * ���������� ���������� �� $_POST ��� $_REQUEST
	 *
	 * @param string $key
	 * @return string
	 */
	function make($key) {
		$res = array();
		if (isset($key) && isset($_POST[$key]) && is_array($_POST[$key])) {
			$res = array_keys($_POST[$key]);
		}

		if(!empty($res))
			return $res;
		elseif(isset($_REQUEST[$key]))
			return $_REQUEST[$key];
		else
			return false;
	}

	/**
	 * ���������� ���� ���������� ��� �������
	 *
	 * @param any $var ����������
	 * @param string $name ?
	 */
	function pr($var, $name = '') {

		//if ($_SERVER['REMOTE_ADDR'] != '87.236.29.54') return;
		//if (!$USER->IsAdmin)
		//	return;
		echo '<pre>';
		$type = gettype($var);

		$backtrace = debug_backtrace();
		if (isset($backtrace[1]['function']) && $backtrace[1]['function'] == 'pred') {
			$caller = array_shift($backtrace);
		}
		$caller = array_shift($backtrace);
		echo $caller['file'].', '.$caller['line']."\n";

		echo (is_numeric($name) || $name ? $name : '').'('.$type.') = ';
		if ($type == 'boolean') {
			echo ($var === true) ? 'true' : 'false';
		}
		elseif (empty($var)) {
			$arr = array('string', 'array', 'object', 'resource');
			$var = ($type == 'NULL') ? 'NULL' : $var;
			$var = (in_array($type, $arr)) ? 'empty!' : $var;
			echo $var;
		}
		else {
			($type == 'object') ? var_dump($var) : print_r($var);
		}
		echo '</pre>';
	}

	/**
	 * ����� �����������
	 *
	 * @param int $_imgId ID ��������
	 * @param boolean $_saveImg ��������� �� ����������� � ����
	 * @param int $_width ������������ ������ �����������
	 * @param int $_height ������������ ������ �����������
	 * @param string $_str ������� �� ��������
	 * @param string $_addon �������������� ��������� ���� <img>
	 * @param boolean $_showHtml ���������� html-���
	 * @param boolean $_tux ������� ����������� � ������
	 * @return string
	 */
	function showPreviewImage($_imgId, $_saveImg, $_width, $_height, $_str, $_addon, $_showHtml, $_tux)
	{

		$_imgId=intval($_imgId); //ID �����������
		if ($_imgId>0){

			$_width=intval($_width); //������
			$_height=intval($_height); //������
			$_saveImg = strToUpper($_saveImg)!="Y"? "N": "Y"; //��������� ����������� ��� ������ ��� �� ���� ? ����
			$arImg=CFile::GetFileArray($_imgId);
			$is_image = CFile::IsImage($arImg["FILE_NAME"], $arImg["CONTENT_TYPE"]);
			if ($is_image)
			{
				$arResultImg = array();
				CPL::pr(stat($_SERVER["DOCUMENT_ROOT"]."/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"]));
				if(is_file($_SERVER["DOCUMENT_ROOT"]."/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"]))
				{
					//�������� ��� ����!
					$arResultImg["IMPATCH"]="/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
					$arResultImg["SIZE"]=array();
					$arResultImg["SIZE"] = @getimagesize($_SERVER["DOCUMENT_ROOT"].$arResultImg["IMPATCH"]);
					$arResultImg["ADDON"] = " ".trim($_addon)." ";

					//return "/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
				}
				else
				{
					//�������� ���
					$newImageFile=$_SERVER["DOCUMENT_ROOT"]."/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];

					$arResize = Array (
						"METHOD" => "resample",
						"COMPRESSION" => "100",
						"WIDTH" => $_width,
						"HEIGHT" => $_height,
					);
					$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arImg["SRC"]);
		//---------------------������ ��������------------------------------------------------
					$file = $arFile["tmp_name"];
					if(file_exists($file) and is_file($file))
					{
						$width = intval($arResize["WIDTH"]);
						$height = intval($arResize["HEIGHT"]);

						$orig = @getimagesize($file);
						$img_width = $widht;
						$img_height = $height;
						if(is_array($orig))
						{

							if(($width > 0 && $orig[0] > $width) || ($height > 0 && $orig[1] > $height))
							{

								$width_orig = $orig[0];
								$height_orig = $orig[1];

								if($width <= 0)
								{
									$width = $width_orig;
								}

								if($height <= 0)
								{
									$height = $height_orig;
								}

								$height_new = $height_orig;
								if($width_orig > $width)
								{
									$height_new = ($width / $width_orig) * $height_orig;
									$height_new = ($width / $width_orig) * $height_orig;
								}

								if($height_new > $height)
								{
									$width = ($height / $height_orig) * $width_orig;
								}
								else
								{
									$height = $height_new;
								}
///echo 1111111111111111111111;
							}
							else
							{
								$width = $orig[0];
								$height = $orig[1];
								$width_orig = $orig[0];
								$height_orig = $orig[1];
							}
								$image_type = $orig[2];


								if($image_type == IMAGETYPE_JPEG)
								{
									$image = imagecreatefromjpeg($file);
								}
								elseif($image_type == IMAGETYPE_GIF)
								{
									$image = imagecreatefromgif($file);
								}
								elseif($image_type == IMAGETYPE_PNG)
								{
									$image = @imagecreatefrompng($file);
									imagealphablending($image, true);
									imagesavealpha($image, true);
								}
								else
									break;

								if ($_tux == true) {
									$image_p = imagecreatetruecolor($_width, $_height);
									$white = imagecolorallocate($image_p, 255, 255, 255);
									imagefill($image_p, 0, 0, $white);
									$x = ($_width - $width) / 2;
									$y = ($_height - $height) / 2;
								} else {
									$image_p = imagecreatetruecolor($width, $height);
									$x=0;
									$y=0;
								}
								if($image_type == IMAGETYPE_JPEG)
								{
									if($arResize["METHOD"] === "resample"){
															$arResultImg["SIZE2"][]=$width;
								//$arResultImg["SIZE2"][]=$height;
								//$arResultImg["SIZE3"][]=$width_orig;
								//$arResultImg["SIZE3"][]=$height_orig;
									//CPL::pr($img_width);
									//CPL::pr($img_height);
										imagecopyresampled($image_p, $image, $x, $y, 0, 0, $width, $height, $width_orig, $height_orig);
									}else
										imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
									if (strlen($_str)>0){
										// ���������� ���������� ������ ������
										$size = 2; // ������ ������
										$str=$_str;
										$x_text = $width-imagefontwidth($size)*strlen($str)-3;
										$y_text = $height-imagefontheight($size)-3;

										// ���������� ����� ������ �� ����� ���� �������� �����
										$white = imagecolorallocatealpha($image_p, 255, 255, 255, 50);
										$black = imagecolorallocatealpha($image_p, 0, 0, 0, 90);
										imagestring($image_p, $size, $x_text-1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text, $y_text, $str, $white);
										unset ($str);
									}
									if($arResize["COMPRESSION"] > 0)
									{
										CheckDirPath(GetDirPath($newImageFile),true);
										imagejpeg($image_p, $newImageFile, $arResize["COMPRESSION"]);
									}
									else
									{
										CheckDirPath(GetDirPath($newImageFile),true);
										imagejpeg($image_p, $newImageFile);
									}
								}
								elseif($image_type == IMAGETYPE_GIF && function_exists("imagegif"))
								{
									imagetruecolortopalette($image_p, true, imagecolorstotal($image));
									imagepalettecopy($image_p, $image);

									//Save transparency for GIFs
									$transparentcolor = imagecolortransparent($image);
									if($transparentcolor >= 0 && $transparentcolor < imagecolorstotal($image))
									{
										$transparentcolor = imagecolortransparent($image_p, $transparentcolor);
										imagefilledrectangle($image_p, 0, 0, $width, $height, $transparentcolor);
									}

									if($arResize["METHOD"] === "resample")
										imagecopyresampled($image_p, $image, $x, $y, 0, 0, $width, $height, $width_orig, $height_orig);
									else
										imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
									if (strlen($_str)>0){
										// ���������� ���������� ������ ������
										$size = 2; // ������ ������
										$str=$_str;
										$x_text = $width-imagefontwidth($size)*strlen($str)-3;
										$y_text = $height-imagefontheight($size)-3;

										// ���������� ����� ������ �� ����� ���� �������� �����
										$white = imagecolorallocatealpha($image_p, 255, 255, 255, 50);
										$black = imagecolorallocatealpha($image_p, 0, 0, 0, 90);
										imagestring($image_p, $size, $x_text-1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text, $y_text, $str, $white);
										unset ($str);
									}
									CheckDirPath(GetDirPath($newImageFile),true);
									imagegif($image_p, $newImageFile);
								}
								else
								{
   									//Save transparency for PNG
									$transparentcolor = imagecolorallocate($image_p, 255, 255, 255);
									$transparentcolor = imagecolortransparent($image_p, $transparentcolor);
									imagealphablending($image_p, true);
									imagesavealpha($image_p, true);

									if($arResize["METHOD"] === "resample")
										imagecopyresampled($image_p, $image, $x, $y, 0, 0, $width, $height, $width_orig, $height_orig);
									else
										imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
									if (strlen($_str)>0){
										// ���������� ���������� ������ ������
										$size = 2; // ������ ������
										$str=$_str;
										$x_text = $width-imagefontwidth($size)*strlen($str)-3;
										$y_text = $height-imagefontheight($size)-3;

										// ���������� ����� ������ �� ����� ���� �������� �����
										$white = imagecolorallocatealpha($image_p, 255, 255, 255, 50);
										$black = imagecolorallocatealpha($image_p, 0, 0, 0, 90);
										imagestring($image_p, $size, $x_text-1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text, $y_text, $str, $white);
										unset ($str);
									}
									CheckDirPath(GetDirPath($newImageFile),true);
									imagepng($image_p, $newImageFile);
								}

								imagedestroy($image);
								imagedestroy($image_p);

								$arFile["size"] = filesize($newImageFile);

						}
					}
					$arResultImg["IMPATCH"]="/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
					$arResultImg["SIZE"]=array();
					$arResultImg["SIZE"] = @getimagesize($_SERVER["DOCUMENT_ROOT"].$arResultImg["IMPATCH"]);
					$arResultImg["ADDON"] = " ".trim($_addon)." ";
					//return "/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
		//--------------------------------------------------------------------------------------------------------
				}
			}
			if (is_array($arResultImg)){
				if ($_showHtml=="Y"){
					return "<img src=\"".$arResultImg["IMPATCH"]."\" width=\"".$arResultImg["SIZE"][0]."\" height=\"".$arResultImg["SIZE"][1]."\"".$arResultImg["ADDON"]." />";
				}
				else
				{
					return $arResultImg;
				}
			}
		}
	}


	/**
	 * ����� �����������
	 *
	 * @param int $_imgId ID ��������
	 * @param boolean $_saveImg ��������� �� ����������� � ����
	 * @param int $_width ������������ ������ �����������
	 * @param int $_height ������������ ������ �����������
	 * @param string $_str ������� �� ��������
	 * @param string $_addon �������������� ��������� ���� <img>
	 * @param boolean $_showHtml ���������� html-���
	 * @param boolean $_tux ������� ����������� � ������
	 * @return string
	 */
	function showPreviewImageFromFile($_imgId, $_saveImg, $_width, $_height, $_str, $_addon, $_showHtml, $_tux)
	{

		{
			$tmp = getimagesize($_SERVER["DOCUMENT_ROOT"].$_imgId);
			//CPL::pr(getimagesize($_SERVER["DOCUMENT_ROOT"].$_imgId));
			//exit;
			$arImg = array(
					"WIDTH" => $tmp[0],
					"HEIGHT" => $tmp[1],
					"CONTENT_TYPE" => $tmp["mime"],
					"FILE_NAME" => basename($_SERVER["DOCUMENT_ROOT"].$_imgId),
					"SUBDIR" => "fromfile",
					"SRC" =>$_imgId
				);
			$_width=intval($_width); //������
			$_height=intval($_height); //������
			$_saveImg = strToUpper($_saveImg)!="Y"? "N": "Y"; //��������� ����������� ��� ������ ��� �� ���� ? ����
			//$arImg=CFile::GetFileArray($_imgId);
			$is_image = CFile::IsImage($arImg["FILE_NAME"], $arImg["CONTENT_TYPE"]);
			//CPL::pr($is_image);
			//exit;
			if ($is_image)
			{
				$arResultImg = array();
				$go=1;
				//CPL::pr(stat($_SERVER["DOCUMENT_ROOT"]."/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"]));
				if(is_file($_SERVER["DOCUMENT_ROOT"]."/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"]))
				{
					$go=0;
					$is = stat($_SERVER["DOCUMENT_ROOT"]."/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"]);
					$new = stat($_SERVER["DOCUMENT_ROOT"].$_imgId);
					if ($is['ctime']<$new['ctime']) {
						$go=1;
					} else {
						//�������� ��� ����!
						$arResultImg["IMPATCH"]="/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
						$arResultImg["SIZE"]=array();
						$arResultImg["SIZE"] = @getimagesize($_SERVER["DOCUMENT_ROOT"].$arResultImg["IMPATCH"]);
						$arResultImg["ADDON"] = " ".trim($_addon)." ";
					}
					//return "/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
				}
				if ($go==1)
				{
					//�������� ���
					$newImageFile=$_SERVER["DOCUMENT_ROOT"]."/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];

					$arResize = Array (
						"METHOD" => "resample",
						"COMPRESSION" => "100",
						"WIDTH" => $_width,
						"HEIGHT" => $_height,
					);
					$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arImg["SRC"]);
		//---------------------������ ��������------------------------------------------------
					$file = $arFile["tmp_name"];
					if(file_exists($file) and is_file($file))
					{
						$width = intval($arResize["WIDTH"]);
						$height = intval($arResize["HEIGHT"]);

						$orig = @getimagesize($file);
						$img_width = $widht;
						$img_height = $height;
						if(is_array($orig))
						{

							if(($width > 0 && $orig[0] > $width) || ($height > 0 && $orig[1] > $height))
							{

								$width_orig = $orig[0];
								$height_orig = $orig[1];

								if($width <= 0)
								{
									$width = $width_orig;
								}

								if($height <= 0)
								{
									$height = $height_orig;
								}

								$height_new = $height_orig;
								if($width_orig > $width)
								{
									$height_new = ($width / $width_orig) * $height_orig;
									$height_new = ($width / $width_orig) * $height_orig;
								}

								if($height_new > $height)
								{
									$width = ($height / $height_orig) * $width_orig;
								}
								else
								{
									$height = $height_new;
								}
///echo 1111111111111111111111;
							}
							else
							{
								$width = $orig[0];
								$height = $orig[1];
								$width_orig = $orig[0];
								$height_orig = $orig[1];
							}
								$image_type = $orig[2];


								if($image_type == IMAGETYPE_JPEG)
								{
									$image = imagecreatefromjpeg($file);
								}
								elseif($image_type == IMAGETYPE_GIF)
								{
									$image = imagecreatefromgif($file);
								}
								elseif($image_type == IMAGETYPE_PNG)
								{
									$image = @imagecreatefrompng($file);
									imagealphablending($image, true);
									imagesavealpha($image, true);
								}
								else
									break;

								if ($_tux == true) {
									$image_p = imagecreatetruecolor($_width, $_height);
									$white = imagecolorallocate($image_p, 255, 255, 255);
									imagefill($image_p, 0, 0, $white);
									$x = ($_width - $width) / 2;
									$y = ($_height - $height) / 2;
								} else {
									$image_p = imagecreatetruecolor($width, $height);
									$x=0;
									$y=0;
								}
								if($image_type == IMAGETYPE_JPEG)
								{
									if($arResize["METHOD"] === "resample"){
															$arResultImg["SIZE2"][]=$width;
								//$arResultImg["SIZE2"][]=$height;
								//$arResultImg["SIZE3"][]=$width_orig;
								//$arResultImg["SIZE3"][]=$height_orig;
									//CPL::pr($img_width);
									//CPL::pr($img_height);
										imagecopyresampled($image_p, $image, $x, $y, 0, 0, $width, $height, $width_orig, $height_orig);
									}else
										imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
									if (strlen($_str)>0){
										// ���������� ���������� ������ ������
										$size = 2; // ������ ������
										$str=$_str;
										$x_text = $width-imagefontwidth($size)*strlen($str)-3;
										$y_text = $height-imagefontheight($size)-3;

										// ���������� ����� ������ �� ����� ���� �������� �����
										$white = imagecolorallocatealpha($image_p, 255, 255, 255, 50);
										$black = imagecolorallocatealpha($image_p, 0, 0, 0, 90);
										imagestring($image_p, $size, $x_text-1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text, $y_text, $str, $white);
										unset ($str);
									}
									if($arResize["COMPRESSION"] > 0)
									{
										CheckDirPath(GetDirPath($newImageFile),true);
										imagejpeg($image_p, $newImageFile, $arResize["COMPRESSION"]);
									}
									else
									{
										CheckDirPath(GetDirPath($newImageFile),true);
										imagejpeg($image_p, $newImageFile);
									}
								}
								elseif($image_type == IMAGETYPE_GIF && function_exists("imagegif"))
								{
									imagetruecolortopalette($image_p, true, imagecolorstotal($image));
									imagepalettecopy($image_p, $image);

									//Save transparency for GIFs
									$transparentcolor = imagecolortransparent($image);
									if($transparentcolor >= 0 && $transparentcolor < imagecolorstotal($image))
									{
										$transparentcolor = imagecolortransparent($image_p, $transparentcolor);
										imagefilledrectangle($image_p, 0, 0, $width, $height, $transparentcolor);
									}

									if($arResize["METHOD"] === "resample")
										imagecopyresampled($image_p, $image, $x, $y, 0, 0, $width, $height, $width_orig, $height_orig);
									else
										imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
									if (strlen($_str)>0){
										// ���������� ���������� ������ ������
										$size = 2; // ������ ������
										$str=$_str;
										$x_text = $width-imagefontwidth($size)*strlen($str)-3;
										$y_text = $height-imagefontheight($size)-3;

										// ���������� ����� ������ �� ����� ���� �������� �����
										$white = imagecolorallocatealpha($image_p, 255, 255, 255, 50);
										$black = imagecolorallocatealpha($image_p, 0, 0, 0, 90);
										imagestring($image_p, $size, $x_text-1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text, $y_text, $str, $white);
										unset ($str);
									}
									CheckDirPath(GetDirPath($newImageFile),true);
									imagegif($image_p, $newImageFile);
								}
								else
								{
   									//Save transparency for PNG
									$transparentcolor = imagecolorallocate($image_p, 255, 255, 255);
									$transparentcolor = imagecolortransparent($image_p, $transparentcolor);
									imagealphablending($image_p, true);
									imagesavealpha($image_p, true);

									if($arResize["METHOD"] === "resample")
										imagecopyresampled($image_p, $image, $x, $y, 0, 0, $width, $height, $width_orig, $height_orig);
									else
										imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
									if (strlen($_str)>0){
										// ���������� ���������� ������ ������
										$size = 2; // ������ ������
										$str=$_str;
										$x_text = $width-imagefontwidth($size)*strlen($str)-3;
										$y_text = $height-imagefontheight($size)-3;

										// ���������� ����� ������ �� ����� ���� �������� �����
										$white = imagecolorallocatealpha($image_p, 255, 255, 255, 50);
										$black = imagecolorallocatealpha($image_p, 0, 0, 0, 90);
										imagestring($image_p, $size, $x_text-1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text-1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text+1, $y_text,   $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text-1, $str, $black);
										imagestring($image_p, $size, $x_text,   $y_text+1, $str, $black);
										imagestring($image_p, $size, $x_text, $y_text, $str, $white);
										unset ($str);
									}
									CheckDirPath(GetDirPath($newImageFile),true);
									imagepng($image_p, $newImageFile);
								}

								imagedestroy($image);
								imagedestroy($image_p);

								$arFile["size"] = filesize($newImageFile);

						}
					}
					$arResultImg["IMPATCH"]="/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
					$arResultImg["SIZE"]=array();
					$arResultImg["SIZE"] = @getimagesize($_SERVER["DOCUMENT_ROOT"].$arResultImg["IMPATCH"]);
					$arResultImg["ADDON"] = " ".trim($_addon)." ";
					//return "/upload/PREVIEW/".$_width."x".$_height."/".$arImg["SUBDIR"]."/".$arImg["FILE_NAME"];
		//--------------------------------------------------------------------------------------------------------
				}
			}
			if (is_array($arResultImg)){
				if ($_showHtml=="Y"){
					return "<img src=\"".$arResultImg["IMPATCH"]."\" width=\"".$arResultImg["SIZE"][0]."\" height=\"".$arResultImg["SIZE"][1]."\"".$arResultImg["ADDON"]." />";
				}
				else
				{
					return $arResultImg;
				}
			}
		}
	}

	/**
	 * ����������� ���������� �������
	 *
	 * @param array $array �������� ������
	 * @param string $sortdirection ����������� ���������� (SORT_ASC/SORT_DESC)
	 * @param string $sortfield ���� ��� ����������
	 * @return array
	 */
	function multisort($array, $sortdirection =  'SORT_ASC', $sortfield = 'NAME')
	{
		//$sort['direction'] = 'SORT_ASC';
	    //$sort['field']     = 'name';

	    $sort_arr = array();
	    foreach($array AS $uniqid => $row){
	        foreach($row AS $key=>$value){
	            $sort_arr[$key][$uniqid] = $value;
	        }
	    }

	    if($sortdirection){
	        array_multisort($sort_arr[$sortfield], constant($sortdirection), $array);
	    }


	    return $array;
	}

	/**
	* ���������, �������� �� $password ������� ������� ������������.
	*
	* @param int $userId
	* @param string $password
	*
	* @return bool
	*/
	function isUserPassword($userId, $password)
	{
	    $userData = CUser::GetByID($userId)->Fetch();

	    $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

	    $realPassword = substr($userData['PASSWORD'], -32);
	    $password = md5($salt.$password);

	    return ($password == $realPassword);
	}
	
	
	
	/**
	* Improperly formatted javascript being add to tags and the limit of 15 instences of recursion 
	* before memory allocation runs out are some of the concerns involved in coding.  
	* Here is the code that I created to leave tags intact but strip scripting from only inside the tags... 
	*
	*
	*/
	function strip_javascript($filter) {

		// realign javascript href to onclick
		$filter = preg_replace("/href=(['\"]).*?javascript:(.*)?
		\\1/i", "onclick=' $2 '", $filter);

		//remove javascript from tags
		while( preg_match("/<(.*)?javascript.*?\(.*?((?>[^()]+)
		|(?R)).*?\)?\)(.*)?>/i", $filter))
		$filter = preg_replace("/<(.*)?javascript.*?\(.*?((?>
		[^()]+)|(?R)).*?\)?\)(.*)?>/i", "<$1$3$4$5>", $filter);

		// dump expressions from contibuted content
		if(0) $filter = preg_replace("/:expression\(.*?((?>[^
		(.*?)]+)|(?R)).*?\)\)/i", "", $filter);

		while( preg_match("/<(.*)?:expr.*?\(.*?((?>[^()]+)|(?
		R)).*?\)?\)(.*)?>/i", $filter))
		$filter = preg_replace("/<(.*)?:expr.*?\(.*?((?>[^()]
		+)|(?R)).*?\)?\)(.*)?>/i", "<$1$3$4$5>", $filter);

		// remove all on* events   
		while( preg_match("/<(.*)?\s?on.+?=?\s?.+?(['\"]).*?\\2
		\s?(.*)?>/i", $filter) )
		$filter = preg_replace("/<(.*)?\s?on.+?=?\s?.+?
		(['\"]).*?\\2\s?(.*)?>/i", "<$1$3>", $filter);

		return $filter;
	} 
	
	
	/**
	* ������� ������ �� �������������� �������� (html, JS)
	*
	* @param string $text
	*
	* @return string
	*/
	function clearText($text = "") {
		return nl2br(htmlspecialchars(strip_tags(CPL::strip_javascript($text))));
	}



	/**
	* ���������� ����� ����� � ����� �� ��
	*
	* @param string $CURRENCY  ��� ������
	*
	* @return string
	**/
	function GetRateFromCBR($CURRENCY) {
		global $DB;
		global $APPLICATION;

		$DATE_RATE=date("d.m.Y");//�������
		$QUERY_STR = "date_req=".$DB->FormatDate($DATE_RATE, CLang::GetDateFormat("SHORT", $lang), "D.M.Y");

		//������ ������ � www.cbr.ru � �������� ������ ���� �� �������� ����          
		$strQueryText = QueryGetData("www.cbr.ru", 80, "/scripts/XML_daily.asp", $QUERY_STR, $errno, $errstr);

		//�������� XML � ������������ � ��������� �����          
		$charset = "windows-1251";
		if (preg_match("/<"."\?XML[^>]{1,}encoding=[\"']([^>\"']{1,})[\"'][^>]{0,}\?".">/i", $strQueryText, $matches))
		{
			$charset = Trim($matches[1]);
		}
		$strQueryText = eregi_replace("<!DOCTYPE[^>]{1,}>", "", $strQueryText);
		$strQueryText = eregi_replace("<"."\?XML[^>]{1,}\?".">", "", $strQueryText);
		$strQueryText = $APPLICATION->ConvertCharset($strQueryText, $charset, SITE_CHARSET);

		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");

		//������ XML
		$objXML = new CDataXML();
		$res = $objXML->LoadString($strQueryText);
		if($res !== false)
		$arData = $objXML->GetArray();
		else
		$arData = false;

		$NEW_RATE=Array();

		//�������� ���� ������ ������ $CURRENCY
		if (is_array($arData) && count($arData["ValCurs"]["#"]["Valute"])>0)
		{
			for ($j1 = 0; $j1<count($arData["ValCurs"]["#"]["Valute"]); $j1++)
			{
				if ($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"]==$CURRENCY)
				{
					$NEW_RATE['CURRENCY']=$CURRENCY;
					$NEW_RATE['RATE_CNT'] = IntVal($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Nominal"][0]["#"]);
					$NEW_RATE['RATE'] = DoubleVal(str_replace(",", ".", $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Value"][0]["#"]));
					$NEW_RATE['DATE_RATE']=$DATE_RATE;
					break;
				}
			}
		}

		if ((isset($NEW_RATE['RATE']))&&(isset($NEW_RATE['RATE_CNT'])))
		{

			//���� ��������, ��������, ���� �� �������� ���� ��� ���� �� �����, ���������
			CModule::IncludeModule('currency');
			$arFilter = array(
				"CURRENCY" => $NEW_RATE['CURRENCY'],
				"DATE_RATE"=>$NEW_RATE['DATE_RATE']
				);
			$by = "date";
			$order = "desc";

			$db_rate = CCurrencyRates::GetList($by, $order, $arFilter);
			
			//������ ����� ���, ������ ���� �� �������� ����
			if(!$ar_rate = $db_rate->Fetch())
			CCurrencyRates::Add($NEW_RATE);

		}

		//���������� ��� ������ �������, ����� ����� �� "������"
		return 'GetRateFromCBR("'.$CURRENCY.'");';
	}
	
	function updateFilter($arrFilter) {
		if (CPL::getRemoteCountry($_SERVER['REMOTE_ADDR']) == "ru") {
			$arrFilter["URK"] = 1;
		}
		return $arrFilter;
	}
	
	function getRemoteCountry($ip = "") {
		if ($ip!="") {
			$sock = fsockopen ("whois.ripe.net",43,$errno,$errstr);
			if ($sock) {
				fputs ($sock, $ip."\r\n");
				while (!feof($sock)) {
					$str.=trim(fgets ($sock,128)." <br>");
				}
			}
			else {
				$str.="$errno($errstr)";
				return;
			}
			fclose ($sock);
		}
		$need = "country:";
		$pos = strpos($str,$need);
		$search = substr($str,$pos,18);
		$excount = explode(":", $search);

		$country = trim($excount[1]);
		return $country;
	}


	/**
	* 
	*
	*
	*
	*/
	function getIBlockByCode($code, $type = null) {
		$filter = array(
			'CODE' => $code,
			'SITE_ID' => SITE_ID
			);

		if (is_string($type)) {
			$filter['TYPE'] = $type;
		}
		$res = CIBlock::GetList(false, $filter);
		
		return $res->Fetch();
	}
	

	/**
	* 
	*
	*
	*
	*/
	function db2Array($dbResult, $method = null) {
		if (!is_object($dbResult)) {
			return array();
		}
		if (!is_string($method)) {
			$method = 'Fetch';
		}

		$result = array();
		while (false !== ($result[] = $dbResult->$method()));
			array_pop($result);

		return $result;
	}
	
	
	function rec_file_size($ss)
	{
		$mb = (1024 * 1024);
		if ($ss > $mb) 
		{
			return sprintf ("%01.2f", ($ss / $mb)) . " ��";
		} 
		elseif ($ss >= 1024) 
		{
			return sprintf ("%01.0f", ($ss / 1024)) . " ��";
		} 
		else 
		{
			return sprintf("%01.0f", $ss)." ����";
		}
	}

}


class CAllPL
{
	function getClassInfo()
	{
		echo "Class ProfLine v0.8.1 build 20091209";
	}
}
?>
