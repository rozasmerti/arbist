<?php
define("IBLOCK_LJUKI_ID",71);
define("country",4798);
define("vendor",4733);
define("material",4807);
define("view",4770);
define("schema",4766);
define("uses",4777);
define("VENDORS_IBLOCK_ID", 46);
define("IBLOCK_vendor_ID", 46);
define("IBLOCK_JELEKTRICHESKIE_TEPLYE_POLY_ID", 77);
define("IBLOCK_OBOGREV_KROVLI_I_PLOSHADOK_ID", 72);
define("IBLOCK_OBOGREV_TRUB_VODOPROVODA_ID", 74);
define("IBLOCK_SPECIALNIE_NAGREVATELNIE_KABELI_ID", 75);
define("IBLOCK_TERMOREGULYATORI", 76);
//справочники
define("SPR_COUNTRY",78);
define("SPR_MATERIAL",79);
define("SPR_VIEW",80);
define("SPR_SCHEMA",81);
//свойства люков, они какого-то... без кодов
//общие для коллекций
define("PROPERTY_LJUKI_COUNTRY",4798);
define("PROPERTY_LJUKI_MNF",4733);
define("PROPERTY_LJUKI_VIEW",4770);
define("PROPERTY_LJUKI_USES",4777);
define("PROPERTY_LJUKI_TIMING",4792);
define("PROPERTY_LJUKI_SCHEMA",4766);
define("PROPERTY_LJUKI_MATERIAL",4807);
define("PROPERTY_LJUKI_TOLSHINA",4732);
define("PROPERTY_LJUKI_GARANTY",4756);
define("PROPERTY_LJUKI_EDIZM",4774);
define("PROPERTY_LJUKI_CRATNOST",4762);
//индивидуальные
define("PROPERTY_LJUKI_WIDTH",4760);
define("PROPERTY_LJUKI_HEIGHT",4785);
define("PROPERTY_LJUKI_SIZE",4754);
define("PROPERTY_LJUKI_HIGHLOAD",4744);
//фотки интерьера
define("PROPERTY_LJUKI_INTERIER",4804);
define("PROPERTY_CODE","DL_ARTICLE"); //Код товара, выводимый сверху


define('IBLOCK_ID__OBOI', 82);