<?

// ������������ ����������
AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
// ������� ���������� ������� "BeforeIndex"
function BeforeIndexHandler($arFields)
{
	if(!CModule::IncludeModule("iblock")) // ���������� ������
		return $arFields;

	if($arFields["MODULE_ID"] == "iblock")
	{
		global $USER;

		$db_props = CIBlockElement::GetProperty(    // �������� �������� �������������� ��������
			$arFields["PARAM2"],    // BLOCK_ID �������������� ��������
			$arFields["ITEM_ID"],     // ID �������������� ��������
			array("sort" => "asc"),  // ���������� (����� ��������)
			Array("CODE"=>PROPERTY_CODE)); // CODE �������� (� ������ ������ ��� ������)

		if($ar_props = $db_props->Fetch())
		{
			if($USER->IsAdmin())
			{
				$arFields["TITLE"] .= " ".$ar_props["VALUE"];   // ������� �������� � ����� ��������� �������������� ��������
				/*echo "<pre>";
				var_dump($arFields["TITLE"]);
				echo "</pre>";
				die;*/
			}
		}

	}
	return $arFields; // ������ ���������
}

AddEventHandler("iblock", "OnBeforeIBlockUpdate","DL_SCANER");
// ������� ���������� ������� "OnBeforeIBlockUpdate"
function DL_SCANER(&$arFields)
{
	/*echo "<pre>";
	var_dump($arFields);
	echo "</pre>";
	die;*/
}
//��� ���������� ������ �� 1� ����� ���������� ��������� ���� � �����������
AddEventHandler("iblock", "OnAfterIBlockElementUpdate","DL_1C_SPR_CONVERTOR");
AddEventHandler("iblock", "OnAfterIBlockElementAdd","DL_1C_SPR_CONVERTOR");

function DL_CHECK_AND_WRITE_SPR($name,$SPR_ID)
{
	CModule::IncludeModule('iblock');
	//������������� ��������
	//$name = $prop["VALUE"];
	$arParams = array("replace_space"=>"-","replace_other"=>"");
	$trans = Cutil::translit($name,"ru",$arParams);
	//��������� �� �������� � ����� ��������� ���� �� ����� ������� � �����������
	$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$SPR_ID, "NAME"=>$name), false, false, array("ID"));
	if(!$ar = $res->Fetch()) //��� ������ ��������
	{
		//������ ���� ��������
		$arFieldsItem = array(
			"IBLOCK_ID"=> $SPR_ID,
			"NAME" => $name,
			"CODE" => $trans
		);
		$ibe = new CIBlockElement;
		$ID_SPR_ELEM = $ibe->Add($arFieldsItem);
	}else
	{
		$ID_SPR_ELEM = $ar["ID"];
	}
	if((int) $ID_SPR_ELEM > 0)
		return $ID_SPR_ELEM;
	else
		return false;
}

function DL_1C_SPR_CONVERTOR($arFields,$ID)
{
	$arIBLOCKS = array(
		IBLOCK_LJUKI_ID,
		IBLOCK_OBOGREV_KROVLI_I_PLOSHADOK_ID,
		IBLOCK_TERMOREGULYATORI,
		IBLOCK_JELEKTRICHESKIE_TEPLYE_POLY_ID,
		IBLOCK_SPECIALNIE_NAGREVATELNIE_KABELI_ID,
		IBLOCK_OBOGREV_TRUB_VODOPROVODA_ID
	);
	if(in_array($arFields["IBLOCK_ID"],$arIBLOCKS))
	{
		//������ ��� ���� ��� ��������������� ������� � ��� ��� � ����� ���� � ������ ��� ������� ����������
		$res = CIBlockElement::GetByID($arFields["ID"]);
		$ar = $res->GetNextElement();
		$arProp = $ar->GetProperties();
		$new_fields = array();
		foreach ($arProp as $prop)
		{
			if($prop["NAME"] == "��������")
			{
				if($res = DL_CHECK_AND_WRITE_SPR($prop["VALUE"],SPR_MATERIAL))
					$new_fields["material"] = $res;
			}
			if($prop["NAME"] == "������")
			{
				if($res = DL_CHECK_AND_WRITE_SPR($prop["VALUE"],SPR_COUNTRY))
					$new_fields["country"] = $res;
			}
			if($prop["NAME"] == "����� ����������")
			{
				if($res = DL_CHECK_AND_WRITE_SPR($prop["VALUE"],SPR_SCHEMA))
					$new_fields["schema"] = $res;
			}
			if($prop["NAME"] == "��� ������")
			{
				if($res = DL_CHECK_AND_WRITE_SPR($prop["VALUE"],SPR_VIEW))
					$new_fields["view"] = $res;
			}
		}
		if(!empty($new_fields))
		{
			$res = CIBlockElement::SetPropertyValuesEx(
				$arFields["ID"],
				$arFields["IBLOCK_ID"],
				$new_fields,
				false
			);
		}
	}
}