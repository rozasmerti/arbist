<?
/*-------------*/
//�������� �� ����������� �������
if (!defined("B_PROLOG_INCLUDED")) require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$minimalBids = false;

$mediumBids = false;

$extraBids = true;
$extraRate = 1.25;


$categoryRate = 0.245;

$deliverySchedule = Array(
    "Monday" => 24,
    "Tuesday" => 24,
    "Wednesday" => 24,
    "Thursday" => 24,
    "Friday" => 12,
    "Saturday" => 0,
    "Sunday" => 24
);

$idleDays = Array(
    "Saturday", "Sunday"
);

$dayOfWeek = date("l");
$hour = intval(date('H'));

$orderBefore = $deliverySchedule[$dayOfWeek];

$plitkaBlocks = Array(14,22,23,24);

$oboiBlocks = Array(82);

$furnitureBlocks = Array(55, 56, 57, 58, 59, 60, 61, 62, 63);

$forbiddenBlocks = Array(85, 86, 87, 88, 89, 90, 91, 92, 93);

$depressedTypes = Array(
    "������",
    "������� ��������",
    "�������",
    "������",
    "�������",
    "������� �������",
    "������ ���������",
    "�����������",
    "����",
    "������",
    "�������",
    "�����",
    "����� ���������",
    "������",
    "����� ����������",
    "������� �������",
    "������� ���������",
    "������������ �����������",
    "�������",
    "�������",
    "������������",
    "�������� �������",
    "������, ������������",
    "�������-������������",
    "�������",
    "������",
    "��������"
);

$additionalType = "";

$surfaceGloss = Array(
"���������","����������","�������������"
);
$surfaceMate = Array(
"�������","�����������"
);

$marketCats = Array(
    1 => Array("parentId" => 0, "Name" => "������� �������"),
    2 => Array("parentId" => 0, "Name" => "������������"),
    3 => Array("parentId" => 0, "Name" => "������ ��� ���� � ����"),
    4 => Array("parentId" => 1, "Name" => "������� ��� ����"),
    6 => Array("parentId" => 4, "Name" => "����������� ������� ����� ���"),
    7 => Array("parentId" => 6, "Name" => "������� ���������� ��������", "IBLOCK_ID" => 76),
    8 => Array("parentId" => 2, "Name" => "������� ������ � ������������"),
    9 => Array("parentId" => 8, "Name" => "����� � ���������", "IBLOCK_ID" => 73),
    10 => Array("parentId" => 3, "Name" => "������"),
    11 => Array("parentId" => 10, "Name" => "������� ��������� ������"),
    12 => Array("parentId" => 11, "Name" => "������� ��������� ��� ������ ������", "IBLOCK_ID" => 35),
    13 => Array("parentId" => 3, "Name" => "������ ��� ������������� � �������"),
    14 => Array("parentId" => 13, "Name" => "�����, ����, �������� �����"),
    15 => Array("parentId" => 14, "Name" => "���������� ��� ���� � ������", "IBLOCK_ID" => 64),
    16 => Array("parentId" => 15, "Name" => "�����, �������, �������", "IBLOCK_ID" => Array(57, 60, 61, 62, 63)),
    17 => Array("parentId" => 15, "Name" => "����� �������", "IBLOCK_ID" => 56),
    18 => Array("parentId" => 14, "Name" => "������� �����", "IBLOCK_ID" => Array(29, 50)),
    19 => Array("parentId" => 14, "Name" => "������������ �����", "IBLOCK_ID" => Array(12, 51)),
    20 => Array("parentId" => 13, "Name" => "���������� � �������������", "IBLOCK_ID" => 40),
    21 => Array("parentId" => 20, "Name" => "�����", "IBLOCK_ID" => 29),
    22 => Array("parentId" => 20, "Name" => "���� � ������� ������"),
    23 => Array("parentId" => 22, "Name" => "������� ������", "IBLOCK_ID" => 37),
    24 => Array("parentId" => 22, "Name" => "����, ������� ������, ���������", "IBLOCK_ID" => 38),
    25 => Array("parentId" => 20, "Name" => "��������, �����������", "IBLOCK_ID" => 33),
    26 => Array("parentId" => 20, "Name" => "����������� ����", "IBLOCK_ID" => 71),
    27 => Array("parentId" => 20, "Name" => "���������", "IBLOCK_ID" => 36),
    28 => Array("parentId" => 20, "Name" => "����� ��� ��������", "IBLOCK_ID" => 35),
    29 => Array("parentId" => 20, "Name" => "�������, ��������, ����", "IBLOCK_ID" => 34),
    30 => Array("parentId" => 13, "Name" => "������� ��������� � ����������"),
    31 => Array("parentId" => 30, "Name" => "�����������������", "IBLOCK_ID" => 39),
    32 => Array("parentId" => 30, "Name" => "������������� ������ ���", "IBLOCK_ID" => 77),
    33 => Array("parentId" => 13, "Name" => "������������ � ���������� ���������"),
    34 => Array("parentId" => 33, "Name" => "���������	 ��������"),
    35 => Array("parentId" => 34, "Name" => "�������", "IBLOCK_ID" => 15),
    36 => Array("parentId" => 34, "Name" => "��������� �����", "IBLOCK_ID" => 16),
    37 => Array("parentId" => 34, "Name" => "��������� �����", "IBLOCK_ID" => Array(17, 54)),
    38 => Array("parentId" => 34, "Name" => "��������� ���", "IBLOCK_ID" => 43),
    39 => Array("parentId" => 33, "Name" => "���������� ���������"),
    40 => Array("parentId" => 39, "Name" => "������ �� �������������", "IBLOCK_ID" => 14),
    41 => Array("parentId" => 39, "Name" => "������������ ������", "IBLOCK_ID" => 22),
    42 => Array("parentId" => 39, "Name" => "�������", "IBLOCK_ID" => 23),
    43 => Array("parentId" => 39, "Name" => "���������� ������", "IBLOCK_ID" => 24),
    44 => Array("parentId" => 39, "Name" => "����", "IBLOCK_ID" => 82),
    45 => Array("parentId" => 39, "Name" => "�������", "IBLOCK_ID" => Array(85, 86, 89, 91)),
    46 => Array("parentId" => 39, "Name" => "�������� ������", "IBLOCK_ID" => Array(87, 88, 90)),
    47 => Array("parentId" => 33, "Name" => "������������ ���������"),
    48 => Array("parentId" => 47, "Name" => "������� � ����� ������������ �����"),
    49 => Array("parentId" => 48, "Name" => "������������ ����", "IBLOCK_ID" => 83,20),
    50 => Array("parentId" => 47, "Name" => "���������� � ����������������� ���������"),
    51 => Array("parentId" => 50, "Name" => "���������", "IBLOCK_ID" => 92),
    52 => Array("parentId" => 47, "Name" => "��������� �������"),
    53 => Array("parentId" => 52, "Name" => "����������� � ��������", "IBLOCK_ID" => 93),
    54 => Array("parentId" => 52, "Name" => "��������� �����", "IBLOCK_ID" => 93),
    55 => Array("parentId" => 52, "Name" => "���������� � ������������� ��� ��������� ������", "IBLOCK_ID" => 93),
    56 => Array("parentId" => 13, "Name" => "���������"),
    57 => Array("parentId" => 56, "Name" => "������� ������ � �������������", "IBLOCK_ID" => Array(72, 74, 75)),
    58 => Array("parentId" => 15, "Name" => "����� �������", "IBLOCK_ID" => Array(55, 58, 59))
);

$mappingRules = Array();

foreach ($marketCats as $branch => $lists) {
    if (is_array($lists["IBLOCK_ID"])) {
        foreach ($lists["IBLOCK_ID"] as $item) {
            $mappingRules[$item] = $branch;
        }
    } else {
        if ($lists["IBLOCK_ID"] > 0)
            $mappingRules[$lists["IBLOCK_ID"]] = $branch;
    }
}

$treeXML = '<categories>
<category id="1">������� �������</category>
<category id="2">������������</category>
<category id="3">������ ��� ���� � ����</category>
<category id="4" parentId="1">������� ��� ����</category>
<category id="6" parentId="4">����������� ������� ����� ���</category>
<category id="7" parentId="6">������� ���������� ��������</category>
<category id="8" parentId="2">������� ������ � ������������</category>
<category id="9" parentId="8">����� � ���������</category>
<category id="10" parentId="3">������</category>
<category id="11" parentId="10">������� ��������� ������</category>
<category id="12" parentId="11">������� ��������� ��� ������ ������</category>
<category id="13" parentId="3">������ ��� ������������� � �������</category>
<category id="14" parentId="13">�����, ����, �������� �����</category>
<category id="15" parentId="14">���������� ��� ���� � ������</category>
<category id="16" parentId="15">�����, �������, �������</category>
<category id="17" parentId="15">����� �������</category>
<category id="18" parentId="14">������� �����</category>
<category id="19" parentId="14">������������ �����</category>
<category id="20" parentId="13">���������� � �������������</category>
<category id="21" parentId="20">�����</category>
<category id="22" parentId="20">���� � ������� ������</category>
<category id="23" parentId="22">������� ������</category>
<category id="24" parentId="22">����, ������� ������, ���������</category>
<category id="25" parentId="20">��������, �����������</category>
<category id="26" parentId="20">����������� ����</category>
<category id="27" parentId="20">���������</category>
<category id="28" parentId="20">����� ��� ��������</category>
<category id="29" parentId="20">�������, ��������, ����</category>
<category id="30" parentId="13">������� ��������� � ����������</category>
<category id="31" parentId="30">�����������������</category>
<category id="32" parentId="30">������������� ������ ���</category>
<category id="33" parentId="13">������������ � ���������� ���������</category>
<category id="34" parentId="33">��������� ��������</category>
<category id="35" parentId="34">�������</category>
<category id="36" parentId="34">��������� �����</category>
<category id="37" parentId="34">��������� �����</category>
<category id="38" parentId="34">��������� ���</category>
<category id="39" parentId="33">���������� ���������</category>
<category id="40" parentId="39">������ �� �������������</category>
<category id="41" parentId="39">������������ ������</category>
<category id="42" parentId="39">�������</category>
<category id="43" parentId="39">���������� ������</category>
<category id="44" parentId="39">����</category>
<category id="45" parentId="39">�������</category>
<category id="46" parentId="39">�������� ������</category>
<category id="47" parentId="33">������������ ���������</category>
<category id="48" parentId="47">������� � ����� ������������ �����</category>
<category id="49" parentId="48">������������ ����</category>
<category id="50" parentId="47">���������� � ����������������� ���������</category>
<category id="51" parentId="50">���������</category>
<category id="52" parentId="47">��������� �������</category>
<category id="53" parentId="52">����������� � ��������</category>
<category id="54" parentId="52">��������� �����</category>
<category id="55" parentId="52">���������� � ������������� ��� ��������� ������</category>
<category id="56" parentId="13">���������</category>
<category id="57" parentId="56">������� ������ � �������������</category>
<category id="58" parentId="15">����� �������</category>
</categories>';

$deliveryOptions = "<delivery-options>
<option cost=\"1400\" days=\"2-3\" order-before=\"14\"/>
</delivery-options>";

$bidsArray = Array(
    "12" => Array("NAME" => "����� ������������", "BID" => 9, "BRAND" => Array()),
    "14" => Array("NAME" => "������������", "BID" => 150, "BRAND" => Array(
        "ABK" => 25,
        "Absolut keramika" => 60,
        "Adex" => 80,
        "Aparici" => 180,
        "Apavisa" => 50,
        "Ape" => 40,
        "Ariana" => 45,
        "Arcana Cerami�a" => 25,
        "Ariostea" => 80,
        "Atlas concorde Russia" => 200,
        "Atlas concorde" => 70,
        "Azori" => 20,
        "Azulejos Borja" => 50,
        "Azulev" => 55,
        "Baldocer" => 45,
        "Belani" => 100,
        "Benadresa" => 40,
        "Bestile" => 50,
        "Brennero" => 40,
        "Caesar" => 30,
        "Casabella" => 55,
        "Ceracasa" => 75,
        "Cerdisa" => 25,
        "Cerdomus" => 60,
        "Cerim" => 35,
        "Cersanit" => 150,
        "Cerrad" => 25,
        "Cicogres" => 45,
        "Cifre" => 45,
        "Cimic" => 40,
        "Cir" => 60,
        "Cisa" => 25,
        "Codicer" => 40,
        "ColiseumGres" => 50,
        "Colorker" => 40,
        "Dado" => 25,
        "Del Conca" => 60,
        "Dom" => 50,
        "Dongpeng" => 20,
        "Dual Gres" => 175,
        "Dune" => 45,
        "Durstone" => 20,
        "Eagle" => 25,
        "Ecoceramic" => 45,
        "Elios" => 75,
        "El-Molino" => 25,
        "Emigres" => 155,
        "Emotion" => 25,
        "Epoca" => 40,
        "Equipe" => 55,
        "Estilker" => 25,
        "Estima" => 175,
        "Europa Ceramica" => 45,
        "Evolve" => 70,
        "Exagres" => 75,
        "Expotile" => 40,
        "Fanal" => 35,
        "Fap" => 35,
        "Flamenco" => 40,
        "Fiorano" => 40,
        "FreeLite decor" => 25,
        "Frls" => 35,
        "Gambarelli" => 60,
        "Gayafores" => 90,
        "Goldencer" => 30,
        "Gracia Ceramica" => 175,
        "Grasaro" => 50,
        "Grazia" => 20,
        "Gresart" => 75,
        "Grespania" => 40,
        "Halcon Ceramicas" => 60,
        "Ibero" => 20,
        "IL Cavallino EnergieKer" => 45,
        "Imola Ceramica" => 75,
        "Impronta" => 25,
        "Inalco" => 50,
        "Infinity Ceramic Tiles" => 60,
        "Italon" => 200,
        "Kale" => 40,
        "Kavarti" => 50,
        "Keope" => 50,
        "Kerama Marazzi" => 300,
        "Kerasol" => 50,
        "Kerlife" => 55,
        "Keros" => 50,
        "Kerranova" => 175,
        "Konskie" => 50,
        "La Fabbrica" => 50,
        "La Faenza" => 35,
        "La Platera" => 75,
        "Lantic Colonial" => 35,
        "Lasselsberger" => 200,
        "LB-Ceramics" => 60,
        "Lightgres" => 20,
        "Mapisa" => 25,
        "Marazzi Italy" => 30,
        "Marca Corona" => 45,
        "Marmocer" => 60,
        "Mijares" => 30,
        "Mirage" => 55,
        "Moneli Decor" => 40,
        "Monopole" => 60,
        "Nabel" => 35,
        "Natucer" => 60,
        "Navarti" => 40,
        "Oset" => 150,
        "Pamesa" => 150,
        "Panaria" => 25,
        "Paradyz" => 45,
        "Pastorelli" => 50,
        "Paul Ceramiche" => 60,
        "Piemmegres" => 45,
        "PiezaROSA" => 50,
        "Plaza" => 50,
        "Polis" => 35,
        "Porcelanicos HDC" => 50,
        "Porcelanite Dos" => 25,
        "Porcellana Di Rocca" => 60,
        "Porcelanosa" => 45,
        "Porselastone" => 45,
        "Ragno" => 60,
        "Realistik" => 45,
        "Realonda" => 80,
        "Rex" => 60,
        "RHS (Rondine Group)" => 75,
        "Rocersa" => 75,
        "Saime" => 35,
        "Saloni" => 20,
        "Savoia" => 20,
        "Serenissima" => 70,
        "Settecento" => 55,
        "Sintesi" => 40,
        "Stone4Home" => 50,
        "Stylnul (STN Ceramica)" => 55,
        "Tau ceramica" => 20,
        "Undefasa" => 25,
        "Venatto" => 50,
        "Venis" => 25,
        "Venus" => 25,
        "Vitra" => 60,
        "Viva" => 20,
        "�������� ��������" => 20,
        "�������" => 85,
        "�-�������" => 50,
        "������������" => 25,
        "��������� ������" => 150
    )),
    "15" => Array("NAME" => "�������", "BID" => 9, "BRAND" => Array()),
    "16" => Array("NAME" => "��������� �����", "BID" => 9, "BRAND" => Array()),
    "17" => Array("NAME" => "��������� �����", "BID" => 9, "BRAND" => Array()),
    "20" => Array("NAME" => "������������� ������", "BID" => 25, "BRAND" => Array(
        "Litokol" => 95,
    )),
    "22" => Array("NAME" => "������������ ������", "BID" => 125, "BRAND" => Array(
        "Absolut keramika" => 80,
        "Adex Studio" => 50,
        "Adex" => 50,
        "Alaplana" => 25,
        "Alcor" => 25,
        "Alma" => 50,
        "Almera Ceramica" => 90,
        "Alta" => 50,
        "Altacera" => 125,
        "Amadis Fine Tiles" => 25,
        "Aparici" => 210,
        "Ape" => 80,
        "Architeza" => 50,
        "Argenta" => 50,
        "Ariostea" => 25,
        "ArtiCer" => 40,
        "Atlantic" => 25,
        "Atlas concorde Russia" => 170,
        "Atlas concorde" => 120,
        "AVA" => 25,
        "Azori" => 75,
        "Azteca" => 40,
        "Azulejos Borja" => 25,
        "Azulejos Benadresa" => 25,
        "Azulejo Espanol" => 25,
        "Azulev" => 30,
        "Azuliber" => 25,
        "Azulindus Marti" => 25,
        "Azuvi" => 25,
        "Baldocer" => 50,
        "BayKer" => 30,
        "Belani" => 70,
        "Belarti" => 45,
        "Benadresa" => 40,
        "Bonaparte" => 170,
        "Brennero" => 25,
        "Capri" => 25,
        "Caramelle Mosaic" => 110,
        "Carmen" => 40,
        "Cas" => 25,
        "Ceracasa" => 25,
        "Ceradim" => 60,
        "Ceramicalcora" => 25,
        "Ceramica Classic" => 60,
        "Ceramica Colli Di Sassuolo SPA" => 25,
        "Cerasarda" => 25,
        "Ceresit" => 60,
        "Cerpa" => 40,
        "Cerrad" => 25,
        "Cerrol" => 80,
        "Cersanit" => 150,
        "Cicogres" => 150,
        "Cifre" => 40,
        "Cir" => 25,
        "Cisa" => 25,
        "Cobsa" => 25,
        "ColiseumGres" => 40,
        "Colori Viva" => 70,
        "Colorker" => 60,
        "Cristacer (Cristal ceramica)" => 40,
        "Dado" => 25,
        "Decocer" => 110,
        "Del Conca" => 80,
        "Domino" => 40,
        "Domus Linea" => 40,
        "Dual Gres" => 150,
        "Dune" => 25,
        "Ecoceramic" => 25,
        "Elios" => 25,
        "El-Molino" => 25,
        "Emigres" => 185,
        "Equipe" => 45,
        "Estima" => 150,
        "Europa Ceramica" => 25,
        "Exagres" => 80,
        "Expotile" => 25,
        "Fabresa" => 45,
        "Fanal" => 60,
        "Fap" => 75,
        "Gambarelli" => 50,
        "Gardenia Orchidea" => 25,
        "Gayafores" => 45,
        "Gemma" => 25,
        "Geotiles" => 25,
        "Glazurker" => 25,
        "Golden Tile" => 60,
        "Goldencer" => 50,
        "Gomez" => 25,
        "Gracia Ceramica" => 170,
        "Grasaro" => 100,
        "Grazia" => 25,
        "Gres de Aragon" => 25,
        "Gres De Valls" => 25,
        "Gresan" => 150,
        "Grespania" => 25,
        "Halcon Ceramicas" => 60,
        "Herberia" => 60,
        "Ibero" => 65,
        "IL Cavallino EnergieKer" => 25,
        "Imola Ceramica" => 25,
        "Impronta" => 80,
        "Infinity Ceramic Tiles" => 45,
        "Interbau" => 110,
        "InterCerama" => 70,
        "IRIS" => 25,
        "Italon" => 150,
        "Itt Ceramica" => 25,
        "Keraben" => 25,
        "Kerama Marazzi" => 300,
        "Keramex" => 25,
        "Kerasol" => 25,
        "Keratile" => 25,
        "Kerlife" => 50,
        "Keros" => 45,
        "Kerranova" => 190,
        "Konskie (Ceramika Color)" => 50,
        "La Platera" => 50,
        "Lantic Colonial" => 25,
        "Lasselsberger" => 215,
        "Latina Ceramica" => 50,
        "LB-Ceramics" => 65,
        "Litokol" => 25,
        "Lotus" => 25,
        "Magna" => 25,
        "Mainzu" => 175,
        "Mapelastic" => 25,
        "Mapisa" => 40,
        "Marazzi Italy" => 50,
        "Marca Corona" => 25,
        "Marmocer" => 25,
        "Mayolica" => 25,
        "Mijares" => 25,
        "Modus Keramika" => 25,
        "Moneli Decor" => 50,
        "Monocolor" => 75,
        "Monopole" => 70,
        "Natural" => 70,
        "Navarti" => 60,
        "Naxos" => 25,
        "Newker" => 60,
        "Novogres" => 50,
        "Onix" => 50,
        "Opoczno" => 65,
        "Oset" => 55,
        "Pamesa" => 200,
        "Paradyz" => 85,
        "Piemme" => 25,
        "Plaza" => 80,
        "Polcolorit" => 25,
        "Polis" => 40,
        "Porcelanicos HDC" => 25,
        "Porcelanite Dos" => 50,
        "Porcelanosa" => 100,
        "Primacolore" => 25,
        "Prissmacer" => 25,
        "Profiplas" => 25,
        "Ragno" => 25,
        "Rako" => 25,
        "Realistik" => 50,
        "Realonda" => 25,
        "Remocolor / Cezar" => 25,
        "RHS (Rondine Group)" => 40,
        "Ribesalbes" => 25,
        "Roca ceramica" => 25,
        "Rocersa" => 90,
        "Rodnoe" => 25,
        "Rondine Group" => 90,
        "Saloni" => 45,
        "Sanchis" => 25,
        "Serenissima" => 50,
        "Sintesi" => 25,
        "Sticks" => 25,
        "Stone4Home" => 50,
        "Stylnul (STN Ceramica)" => 50,
        "Tecniceramica" => 25,
        "Tubadzin" => 45,
        "Tonalite" => 45,
        "Undefasa" => 40,
        "Unicer" => 25,
        "Unis" => 60,
        "Vallelunga" => 25,
        "Venatto" => 70,
        "Venis" => 70,
        "Venus" => 50,
        "Vidrepur" => 50,
        "Vitra" => 40,
        "Westerhof" => 60,
        "Zirconio" => 25,
        "�����������" => 25,
        "�������" => 90,
        "������ ��������" => 40,
        "���" => 60,
        "���������" => 25,
        "�������� ��������" => 120,
        "�������" => 25,
        "�������" => 40,
        "�������" => 100,
        "�-�������" => 40,
        "���� ��������" => 75,
        "������" => 45,
        "����" => 60,
        "�������� �" => 40,
        "�������" => 25,
        "��������" => 70,
        "�����" => 55,
        "������������" => 250,
        "��������� ������" => 220

    )),
    "23" => Array("NAME" => "�������", "BID" => 155, "BRAND" => Array(
        "Alma" => 75,
        "Architeza" => 75,
        "Arezia" => 35,
        "Bonaparte" => 150,
        "Caramelle Mosaic" => 110,
        "Colori Viva" => 50,
        "Dune" => 45,
        "Fap" => 40,
        "Gaudi" => 40,
        "Kerama Marazzi" => 200,
        "Kerion" => 45,
        "Lantic Colonial" => 50,
        "L Antic Colonial" => 50,
        "Mosavit" => 45,
        "Natural" => 75,
        "Primacolore" => 90,
        "Q-Stones" => 45,
        "Skalini" => 60,
        "Stone4Home" => 75,
        "Vidrepur" => 60,
        "Vidromar" => 45,
        "Vitra" => 50,
        "Vitrex" => 45,
        "���������" => 75
    )),
    "24" => Array("NAME" => "�������", "BID" => 145, "BRAND" => Array(
        "Cehimosa" => 125,
        "Cerrad" => 125,
        "Exagres" => 140,
        "Gresan" => 145,
        "Gresmanc" => 145,
        "Gres de Aragon" => 125,
        "Gres de Breda" => 125,
        "Italon" => 160,
        "Keros" => 125,
        "Mayor" => 125,
        "Natucer" => 125,
        "Opoczno" => 125,
        "Paradyz" => 145,
        "RosaGres" => 145,
        "Realistik" => 125,
        "Sierragres" => 130,
        "Vitra" => 120
    )),
    "29" => Array("NAME" => "����� �������", "BID" => 9, "BRAND" => Array()),
    "32" => Array("NAME" => "�����", "BID" => 9, "BRAND" => Array()),
    "33" => Array("NAME" => "��������", "BID" => 9, "BRAND" => Array()),
    "34" => Array("NAME" => "������� � ����", "BID" => 9, "BRAND" => Array()),
    "35" => Array("NAME" => "������ ��� ����", "BID" => 9, "BRAND" => Array()),
    "36" => Array("NAME" => "���������", "BID" => 9, "BRAND" => Array()),
    "37" => Array("NAME" => "������� ������", "BID" => 9, "BRAND" => Array()),
    "38" => Array("NAME" => "������� �������", "BID" => 9, "BRAND" => Array()),
    "39" => Array("NAME" => "�����������������", "BID" => 9, "BRAND" => Array()),
    "40" => Array("NAME" => "���������� ��� ����������", "BID" => 9, "BRAND" => Array()),
    "43" => Array("NAME" => "��������� ��������", "BID" => 9, "BRAND" => Array()),
    "50" => Array("NAME" => "������� �����", "BID" => 9, "BRAND" => Array()),
    "51" => Array("NAME" => "������������ �����", "BID" => 9, "BRAND" => Array()),
    "54" => Array("NAME" => "���������� �����", "BID" => 9, "BRAND" => Array()),
    "55" => Array("NAME" => "������� �����", "BID" => 150, "BRAND" => Array(
        "Adden Bau" => 80,
        "ARCHIE" => 45,
        "ARCHIE SILLUR" => 150,
        "Armadillo" => 150,
        "Bussare" => 45,
        "Fuaro" => 80,
        "Genesis" => 25,
        "Melodia" => 75,
        "Morelli" => 50,
        "Morelli Luxury" => 50,
        "Orlando" => 50,
        "Punto" => 100,
        "Rucetti" => 50
    )),
    "56" => Array("NAME" => "������� �����", "BID" => 60, "BRAND" => Array()),
    "57" => Array("NAME" => "������� �����", "BID" => 100, "BRAND" => Array()),
    "58" => Array("NAME" => "�������� �������", "BID" => 9, "BRAND" => Array()),
    "59" => Array("NAME" => "�������� �������", "BID" => 9, "BRAND" => Array()),
    "60" => Array("NAME" => "����������� ���������", "BID" => 30, "BRAND" => Array()),
    "61" => Array("NAME" => "������� ������������", "BID" => 9, "BRAND" => Array()),
    "62" => Array("NAME" => "������", "BID" => 9, "BRAND" => Array()),
    "63" => Array("NAME" => "���������� ���������", "BID" => 9, "BRAND" => Array()),
    "64" => Array("NAME" => "����������", "BID" => 9, "BRAND" => Array()),
    "71" => Array("NAME" => "���� �����������", "BID" => 50, "BRAND" => Array()),
    "72" => Array("NAME" => "������� ������ � ��������", "BID" => 9, "BRAND" => Array()),
    "73" => Array("NAME" => "��������� �������", "BID" => 9, "BRAND" => Array()),
    "74" => Array("NAME" => "������� ���� �����������", "BID" => 9, "BRAND" => Array()),
    "75" => Array("NAME" => "����������� �������������� ������", "BID" => 9, "BRAND" => Array()),
    "76" => Array("NAME" => "���������������", "BID" => 9, "BRAND" => Array()),
    "77" => Array("NAME" => "������������� ������ ����", "BID" => 9, "BRAND" => Array()),
    "82" => Array("NAME" => "����", "BID" => 195, "BRAND" => Array(
        "Andrea Rossi" => 90,
        "Armani/Casa" => 35,
        "Artdecorium" => 45,
        "AS Creation" => 150,
        "Atlas" => 75,
        "BN International" => 150,
        "Calcutta" => 50,
        "Casa Mia" => 150,
        "Casadeco" => 60,
        "Caselio" => 60,
        "Cristiana Masi" => 60,
        "Decoro Pareti" => 35,
        "Domus Parati (Limonta)" => 125,
        "Erismann" => 90,
        "Esedra" => 60,
        "Estro" => 45,
        "Eurodecor" => 90,
        "Filpassion" => 60,
        "G L Design" => 35,
        "Grandeco (Ideco)" => 120,
        "Graham Brown" => 60,
        "Limonta" => 170,
        "Loymina" => 50,
        "Marburg" => 150,
        "MaxWall" => 50,
        "Milassa" => 90,
        "Novamur" => 25,
        "OVK Design" => 35,
        "P+S" => 90,
        "Paper Ink" => 45,
        "Parato by Cristiana Masi" => 90,
        "Portofino" => 90,
        "Pufas" => 50,
        "Rasch" => 150,
        "Rasch Textil" => 60,
        "Rips" => 60,
        "Quarta Parete" => 45,
        "Sangiorgio" => 40,
        "Sirpi" => 200,
        "Ugepa" => 50,
        "Victoria Stenova" => 75,
        "Wallquest" => 95,
        "Zambaiti" => 190,
        "�������� (���)" => 60,
        "���������" => 75

    )),
    "83" => Array("NAME" => "���� ��� �����", "BID" => 25, "BRAND" => Array()),
    "84" => Array("NAME" => "���������� ��� �����", "BID" => 25, "BRAND" => Array()),
    "85" => Array("NAME" => "���� ����", "BID" => 25, "BRAND" => Array()),
    "86" => Array("NAME" => "������� ���������", "BID" => 25, "BRAND" => Array()),
    "87" => Array("NAME" => "������� ��������������", "BID" => 25, "BRAND" => Array()),
    "88" => Array("NAME" => "������� ���������", "BID" => 25, "BRAND" => Array()),
    "89" => Array("NAME" => "������", "BID" => 25, "BRAND" => Array()),
    "90" => Array("NAME" => "�������� ������", "BID" => 25, "BRAND" => Array()),
    "91" => Array("NAME" => "��������� � ��������", "BID" => 25, "BRAND" => Array()),
    "92" => Array("NAME" => "����������� �������", "BID" => 25, "BRAND" => Array()),
    "93" => Array("NAME" => "��������� �������", "BID" => 25, "BRAND" => Array())
);


$saveOriginalDetailPage = array(
    71,//����������� ����
    77,//������������� ������ ����
    72,//������� ������ � ��������
    74,//������� ���� �����������
    75,//����������� �������������� ������
    76,//���������������
    73,//��������� �������
);



$stopValues = Array(
    '��� ������',
    '&lt;&gt;',
    ' ',
    '<>'
);
define("NOT_CHECK_PERMISSIONS", true);
CModule::IncludeModule("currency");
CModule::IncludeModule("catalog");
global $APPLICATION;

include(GetLangFileName($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/catalog/lang/", "/export_setup_templ.php"));
set_time_limit(0);
global $USER;
$bTmpUserCreated = False;
if (!is_object($USER)) {
    $bTmpUserCreated = True;
    $GLOBALS['USER'] = new CUser;
}

function yandex_replace_special($arg)
{
    if (in_array($arg[0], array("&quot;", "&amp;", "&lt;", "&gt;")))
        return $arg[0];
    else
        return " ";
}

function yandex_text2xml($text, $bHSC = false)
{
    $text = $GLOBALS['APPLICATION']->ConvertCharset($text, LANG_CHARSET, 'windows-1251');
    if ($bHSC)
        $text = htmlspecialchars($text);
    $text = preg_replace('/[\x01-\x08\x0B-\x0C\x0E-\x1F]/', "", $text);
    $text = str_replace("'", "&apos;", $text);
    $text = str_replace(array("\r", "\n"), '', $text);
    return $text;
}

function getBid($ibid, $brand)
{
    $bid = 0;
    global $bidsArray;
    if (isset($brand, $bidsArray[$ibid]["BRAND"][$brand])) {
        $bid = intval($bidsArray[$ibid]["BRAND"][$brand]);
    } else {
        $bid = intval($bidsArray[$ibid]["BID"]);
    }
    if (!empty($bid)) {
        $bid = $bid + rand(1,5);
        return $bid;
    } else {
        return false;
    }
}

function getYandexCategory($ibid)
{
    global $mappingRules;
    if ($mappingRules[$ibid] > 0) {
        return $mappingRules[$ibid];
    } else {
        return false;
    }
}

$strExportErrorMessage = "";

if (empty($SETUP_FILE_NAME))
    $SETUP_FILE_NAME = "/yml/yandex_tmp.php";
/*-------------*/
$SETUP_SERVER_NAME = trim($SETUP_SERVER_NAME);
$SETUP_FILE_NAME = Rel2Abs("/", $SETUP_FILE_NAME);


if (strlen($strExportErrorMessage) <= 0) {
    if (!$fp = @fopen($_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, "wb")) {
        $strExportErrorMessage .= str_replace('#FILE#', $_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_OPEN_WRITING')) . "\n";
    } else {
        if (!@fwrite($fp, '<?if (!isset($_GET["referer1"]) || strlen($_GET["referer1"])<=0) $_GET["referer1"] = "yandext"?>')) {
            $strExportErrorMessage .= str_replace('#FILE#', $_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_WRITE')) . "\n";
            @fclose($fp);
        }
    }
}

if (strlen($strExportErrorMessage) <= 0) {

    @fwrite($fp, '<? header("Content-Type: text/xml; charset=windows-1251");?>');
    @fwrite($fp, '<? echo "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">"?>');
    @fwrite($fp, "\n<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");
    @fwrite($fp, "<yml_catalog date=\"" . Date("Y-m-d H:i") . "\">\n");
    @fwrite($fp, "<shop>\n");
    @fwrite($fp, "<name>" . htmlspecialchars($APPLICATION->ConvertCharset(COption::GetOptionString("main", "site_name", ""), LANG_CHARSET, 'windows-1251')) . "</name>\n");
    @fwrite($fp, "<company>" . htmlspecialchars($APPLICATION->ConvertCharset(COption::GetOptionString("main", "site_name", ""), LANG_CHARSET, 'windows-1251')) . "</company>\n");
    @fwrite($fp, "<url>https://" . htmlspecialchars(strlen($SETUP_SERVER_NAME) > 0 ? $SETUP_SERVER_NAME : COption::GetOptionString("main", "server_name", "")) . "</url>\n");

    $db_acc = CCurrency::GetList(($by = "sort"), ($order = "asc"));
    $strTmp = "<currencies>\n";
    $arCurrencyAllowed = array('RUR', 'RUB', 'USD', 'EUR', 'UAH');
    while ($arAcc = $db_acc->Fetch()) {
        if (in_array($arAcc['CURRENCY'], $arCurrencyAllowed))
            $strTmp .= "<currency id=\"" . $arAcc["CURRENCY"] . "\" rate=\"" . $arAcc["AMOUNT"] . "\"/>\n";
    }
    $strTmp .= "</currencies>\n";

    @fwrite($fp, $strTmp);

    $arSelect = array(
        "ID",
        "LID",
        "IBLOCK_ID",
        "IBLOCK_CODE",
        "IBLOCK_SECTION_ID",
        "ACTIVE",
        "ACTIVE_FROM",
        "ACTIVE_TO",
        "NAME",
        "PREVIEW_PICTURE",
        "PREVIEW_TEXT",
        "PREVIEW_TEXT_TYPE",
        "DETAIL_PICTURE",
        "LANG_DIR",
        "DETAIL_PAGE_URL",
        "PROPERTY_APPLICATION",
        "PROPERTY_DESTINATION",
        "PROPERTY_INPACK",
        "PROPERTY_PACKING",
        "PROPERTY_SURFPROC",
        "PROPERTY_BRAND",
        "PROPERTY_BRAND_GUID",
        "PROPERTY_COLLECTION",
        "PROPERTY_NAME_COLLECTION",
        "PROPERTY_COUNTRY",
        "PROPERTY_MATERIAL",
        "PROPERTY_COLOR",
        "PROPERTY_SUPPLIER",
        "PROPERTY_DELIVERY_DATE",
        "PROPERTY_DELIVERY_COST",
        "PROPERTY_COMPLECT_TYPE",
        "PROPERTY_GUARANTEE",
        "XML_ID",
        "PROPERTY_CML2_KRAT",
        "PROPERTY_CML2_BASE_UNIT"
    );

    $strTmpCat = "";
    $strTmpOff = "";

    if (empty($YANDEX_EXPORT)) {
        $iblockFilter = Array(
            "ACTIVE" => "Y",
            "YANDEX_EXPORT" => "Y"
        );
        $res = CCatalog::GetList(array(), $iblockFilter);
        while ($ar_res = $res->Fetch()) {
            // if(!in_array($ar_res["IBLOCK_ID"],$forbiddenBlocks))
            $YANDEX_EXPORT[] = $ar_res["IBLOCK_ID"];
        }
    }

    if (is_array($YANDEX_EXPORT)) {
        $arSiteServers = array();
        foreach ($YANDEX_EXPORT as $ykey => $yvalue) {
            $filter = Array("IBLOCK_ID" => IntVal($yvalue), "ACTIVE" => "Y", "IBLOCK_ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y");
            /*-------------*/
            //�������� ������ �� ����������������� �������� �������
            //���� �������� �����, �� �� ���������
            $filter["!UF_YA"] = false;
            /*-------------*/
            $db_acc = CIBlockSection::GetList(array("left_margin" => "asc"), $filter);
            $arAvailGroups = array();
            while ($arAcc = $db_acc->Fetch()) {
                $strTmpCat .= "<category id=\"" . $arAcc["ID"] . "\"" . (IntVal($arAcc["IBLOCK_SECTION_ID"]) > 0 ? " parentId=\"" . $arAcc["IBLOCK_SECTION_ID"] . "\"" : "") . ">" . yandex_text2xml($arAcc["NAME"], true) . "</category>\n";
                $arAvailGroups[] = IntVal($arAcc["ID"]);
            }

            //*****************************************//
            /*-------------*/
            /*������ �������*/
            $filter = Array("IBLOCK_ID" => IntVal($yvalue), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
            /*-���������� �� �������!-*/
            $filter["PROPERTY_YA_MARKET_VALUE"] = "��%";
            //$filter["NAME"] = "%Cersanit%";
            $filter["SECTION_ID"] = $arAvailGroups;
            $filter[] = array(
                "LOGIC" => "OR",
                "PROPERTY_AVAIL_DATE" => false,
                "<=PROPERTY_AVAIL_DATE" => date("Y-m-d") . ' 23:59:59'
            );

            //$arNavStart = Array("nTopCount"=>5);
            $arNavStart = false;

            /*-------------*/
            $res = CIBlockElement::GetList(array(), $filter, false, $arNavStart, $arSelect);

            /**
             * 2017.05.02, sumato.shigoto@gmail.com
             * ����� �� ���������� � ������ ������ �����, �������� �������� �������� �� ������������ �������� ( �� ����)
             * � ���� �������.
             * $images_hash ��� ����������� ��� ������������������� ��������
             * $product_names ��� ����������� ��� ������������������� ���� �������
             */
            // ��� �������������������
            $exported = array();
            $images_hash = array();
            $product_names = array();

            while ($arAcc = $res->GetNext()) {
                $breakCycle = false;

                if($arAcc["PROPERTY_SUPPLIER_VALUE"] == "������������ ���") continue;
                if($arAcc["PROPERTY_SUPPLIER_VALUE"] == "������-����� ���") continue;
                if($arAcc["PROPERTY_SUPPLIER_VALUE"] == "������") continue;

                if (in_array($arAcc["ID"], $exported))
                    continue;
                else
                    $exported[] = $arAcc["ID"];

                /**
                 * @param string $brandUtmString ������������ ��� ���������� ��������� ������ UTM ������
                 * @param string $vendor ������������ ��� ��� �������������
                 *
                 * 07.08.2017 , sumato.shigoto@gmail.com
                 * ���� � �������� ��� �������� BRAND (��� ��� ����� "<>"),
                 * �� ���� �� ������ �������� BRAND_GUID, �������� �������� BRAND_GUID � BRAND.
                 * ���� ����� ����� � �������� ��� ����� ��� ������������ �������������, �������� ��������.
                 */

                unset($brandUtmString,$vendor);

                if(
                    (empty($arAcc["PROPERTY_BRAND_VALUE"]) || $arAcc["PROPERTY_BRAND_VALUE"] == "&lt;&gt;")
                    && !empty($arAcc["PROPERTY_BRAND_GUID_VALUE"])
                    && $arAcc["PROPERTY_BRAND_GUID_VALUE"] != "&lt;&gt;"
                ) {
                    $brandElement = CIBlockElement::GetByID($arAcc["PROPERTY_BRAND_GUID_VALUE"]);
                    if ($br_res = $brandElement->GetNext()) {
                        $arAcc["PROPERTY_BRAND_VALUE"] = $br_res['NAME'];
                    }
                }
                if(empty($arAcc["PROPERTY_BRAND_VALUE"])) continue;

                $brandUtmString = "&amp;utm_content=" . ucfirst(rawurlencode($arAcc["PROPERTY_BRAND_VALUE"]));
                $vendor = yandex_text2xml($arAcc["PROPERTY_BRAND_VALUE"], true);

                /**
                 * 2017.04.04 ������� �������� "������" ���� � �������� ������
                 * sumato.shigoto@gmail.com
                 * todo �������� ������, ������ ������� ��������� �����������
                 */

                $tmpPrName = yandex_text2xml($arAcc["NAME"], true);
                if (stristr($tmpPrName, "�����") OR stristr($tmpPrName, "�����")) continue;

                /**
                 * 2017.04.04 ������� ��� �������� ���������������� ����� � ������ ������, ���� ����� ��������� �� 1�
                 * sumato.shigoto@gmail.com
                 *
                 * todo �������� ������, ������ ������� ��������� �����������
                 */

                if (strpos($tmpPrName, '!') === 0)
                    $tmpPrName = substr($tmpPrName, 1);

                $productName = $tmpPrName;

                /**
                 * @param bool $breakCycle
                 * 2017.05.02 ��������� ��� �������� �� ������������. ���� �� ���������, ��������� ���� breakCycle,
                 * ������� ����� ����� ����������� ��� ���������� �����.
                 * sumato.shigoto@gmail.com
                 */
                if (in_array($productName, $product_names)) {
                    $breakCycle = true;
                } else {
                    $product_names[] = $productName;
                }

                if (strlen($SETUP_SERVER_NAME) <= 0) {
                    if (!array_key_exists($arAcc['LID'], $arSiteServers)) {
                        $rsSite = CSite::GetList(($b = "sort"), ($o = "asc"), array("LID" => $arAcc["LID"]));
                        if ($arSite = $rsSite->Fetch())
                            $arAcc["SERVER_NAME"] = $arSite["SERVER_NAME"];
                        if (strlen($arAcc["SERVER_NAME"]) <= 0 && defined("SITE_SERVER_NAME"))
                            $arAcc["SERVER_NAME"] = SITE_SERVER_NAME;
                        if (strlen($arAcc["SERVER_NAME"]) <= 0)
                            $arAcc["SERVER_NAME"] = COption::GetOptionString("main", "server_name", "");

                        $arSiteServers[$arAcc['LID']] = $arAcc['SERVER_NAME'];
                    } else {
                        $arAcc['SERVER_NAME'] = $arSiteServers[$arAcc['LID']];
                    }
                } else {
                    $arAcc['SERVER_NAME'] = $SETUP_SERVER_NAME;
                }

                if (IntVal($arAcc["DETAIL_PICTURE"]) > 0 || IntVal($arAcc["PREVIEW_PICTURE"]) > 0) {

                    $pictNo = IntVal($arAcc["DETAIL_PICTURE"]);
                    if ($pictNo <= 0) $pictNo = IntVal($arAcc["PREVIEW_PICTURE"]);
                    $arPictInfo = CFile::GetFileArray($pictNo);
                    if (is_array($arPictInfo)) {
                        if (substr($arPictInfo["SRC"], 0, 1) == "/") {
                            $strFile = "https://" . $arAcc['SERVER_NAME'] . implode("/", array_map("rawurlencode", explode("/", $arPictInfo["SRC"])));
                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $arPictInfo['SRC'])) {
                                $imageHash = md5_file($_SERVER['DOCUMENT_ROOT'] . $arPictInfo['SRC']);
                            } else { // 2017.05.02, sumato.shigoto@gmail.com  , ���� � ���� �������� ����, � ���� �� ������, ���� ���� ��������� ����
                                $breakCycle = true;
                            }

                        } elseif (preg_match("/^(http|https):\\/\\/(.*?)\\/(.*)\$/", $arPictInfo["SRC"], $match)) {
                            $strFile = "https://" . $match[2] . implode("/", array_map("rawurlencode", explode("/", $match[3])));
                            $imageHash = $strFile; // 2017.05.02, sumato.shigoto@gmail.com, ���� ����� � ���� ����� http ������ �� ��������, �� ��������� �� ������������ ������.
                        } else {
                            $strFile = $arPictInfo["SRC"]; //2017.05.02, sumato.shigoto@gmail.com, ����� ��� ��������, ������� ������ ����� � ����� ����� (�� ���������� �� ����� � �� �������� http
                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/" . $arPictInfo['SRC'])) {
                                $imageHash = md5_file($_SERVER['DOCUMENT_ROOT'] . "/" . $arPictInfo['SRC']);
                            } else { // 2017.05.02, sumato.shigoto@gmail.com  , ���� � ���� �������� ����, � ���� �� ������, ���� ���� ��������� ����
                                $breakCycle = true;
                            }
                        }
                    }
                    /**
                     * 2017.05.02 ��������� ��� �������� ��� ������ �� ��� �� ������������. ���� �� ���������, ��������� ���� breakCycle,
                     * ������� ����� ����� ����������� ��� ���������� �����.
                     * sumato.shigoto@gmail.com
                     */
                    if ($imageHash) {
                        if (in_array($imageHash, $images_hash)) {
                            $breakCycle = true;
                        } else {
                            $images_hash[] = $imageHash;
                        }
                    }
                } else {
                    /**
                     * 2017.05.02, sumato.shigoto@gmail.com
                     * ����� � ������� ���� �� ���������� ������ �� ����������� ��������, �������� ������ ������
                     */
                    unset($imageHash, $strFile);
                }

                /**
                 * 2017.05.02, sumato.shigoto@gmail.com, ���� ������ ���� �������� ��������� ��������, �������� ���������
                 */
                if ($breakCycle) continue;
                if (empty($strFile)) continue;

                unset($basePrice, $minPrice);
                $optimalPrice = CCatalogProduct::GetOptimalPrice($arAcc["ID"],1,$USER->GetUserGroupArray());
                if( !empty($optimalPrice['DISCOUNT_PRICE'])  && $optimalPrice['DISCOUNT_PRICE'] < $optimalPrice['PRICE']['PRICE']) {
                    $basePrice = round($optimalPrice['PRICE']['PRICE']);
                    $minPrice  = round($optimalPrice['DISCOUNT_PRICE']);
                } else {
                    $minPrice = round($optimalPrice['PRICE']['PRICE']);
                }

                $currency = $optimalPrice['PRICE']['CURRENCY'];

                if($minPrice < 20) continue;

                $str_QUANTITY = DoubleVal($arAcc["CATALOG_QUANTITY"]);
                $str_QUANTITY_TRACE = $arAcc["CATALOG_QUANTITY_TRACE"];
                if (($str_QUANTITY <= 0) && ($str_QUANTITY_TRACE == "Y")) {
                    $available = false;
                    $str_AVAILABLE = ' available="false"';
                } else {
                    $available = true;
                    $str_AVAILABLE = ' available="true"';
                }

                /* begin 05.07.2012 �������� ������ "�� �����" ��� ����� �������� �� 3 � ����� ���� */

                if (!empty($arAcc["PROPERTY_DELIVERY_DATE_VALUE"]) && $arAcc["PROPERTY_DELIVERY_DATE_VALUE"] >= 3) {
                    $available = false;
                    $str_AVAILABLE = ' available="false"';
                }


                $bNoActiveGroup = True;
                $strTmpOff_tmp = "";
                $db_res1 = CIBlockElement::GetElementGroups($arAcc["ID"]);
                while ($ar_res1 = $db_res1->Fetch()) {
                    if (in_array(IntVal($ar_res1["ID"]), $arAvailGroups)) {
                        $strTmpOff_tmp .= "<categoryId>" . $ar_res1["ID"] . "</categoryId>\n";
                        $bNoActiveGroup = False;
                    }
                }

                if ($bNoActiveGroup) continue;


                if (strlen($arAcc['DETAIL_PAGE_URL']) <= 0)
                    $arAcc['DETAIL_PAGE_URL'] = '/';
                else
                    $arAcc['DETAIL_PAGE_URL'] = str_replace(' ', '%20', $arAcc['DETAIL_PAGE_URL']);

                if (!in_array($yvalue, $saveOriginalDetailPage)) {
                    /*-���� ������-*/
                    $arAcc['DETAIL_PAGE_URL'] = "/catalog/" . $arAcc["IBLOCK_CODE"] . "/" . $arAcc["IBLOCK_SECTION_ID"] . "/" . $arAcc["ID"] . "/";
                    $arAcc['~DETAIL_PAGE_URL'] = "/catalog/" . $arAcc["IBLOCK_CODE"] . "/" . $arAcc["IBLOCK_SECTION_ID"] . "/" . $arAcc["ID"] . "/";
                    /*-htmlspecialchars($arAcc["~DETAIL_PAGE_URL"])-*/
                }

                $XML_ID_COMPLECT = array(
                    "d4f7e3e8-ea90-11e1-9c08-14dae9a723c8",// ����� ��������
                    "53ec260f-c448-11e1-8a68-14dae9a723c8",// �������
                );

                if (!empty($arAcc["XML_ID"]) && !empty($arAcc["PROPERTY_COMPLECT_TYPE_VALUE"])) {
                    $eid = intval($arAcc["PROPERTY_COMPLECT_TYPE_ENUM_ID"]);
                    $cEnum = CIBlockPropertyEnum::GetByID($eid);

                    if (in_array($cEnum["XML_ID"], $XML_ID_COMPLECT)) {// ��������� URL ������ ��� �������
                        if ($r = CIBlockElement::GetList(array(), array("PROPERTY_COMPLECT" => $arAcc["XML_ID"]), false, false, array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_COMPLECT"))->getnext()) {
                            $arAcc['DETAIL_PAGE_URL'] = $r["DETAIL_PAGE_URL"] . "?CID=" . $arAcc["ID"];
                            $arAcc['~DETAIL_PAGE_URL'] = $r["~DETAIL_PAGE_URL"] . "?CID=" . $arAcc["ID"];
                        }
                    }
                }

                $strBid = "";
                $bid = 0;

                if (intval($arAcc["CATALOG_PRICE_27"]) > 9) {
                    $bid = intval($arAcc["CATALOG_PRICE_27"]);
                    if($dayOfWeek == "Monday" and $hour > 16 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Tuesday" and $hour > 17 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Tuesday" and $hour < 6 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Wednesday" and $hour > 19 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Wednesday" and $hour < 5 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Thursday" and $hour > 16 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Friday" and $hour > 12 and $bid > 15) $bid = round($bid / 4);
                    if($dayOfWeek == "Saturday" and $bid > 15) $minimalBids = true;
                    if($dayOfWeek == "Sunday" and $hour < 11)  $minimalBids = true;
                } else {

                    $bid = getBid($arAcc["IBLOCK_ID"], $arAcc["PROPERTY_BRAND_VALUE"]);
                    if($dayOfWeek == "Monday" and $hour > 16 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Tuesday" and $hour > 17 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Tuesday" and $hour < 6 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Wednesday" and $hour > 19 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Wednesday" and $hour < 5 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Thursday" and $hour > 16 and $bid > 20) $bid = round($bid / 3);
                    if($dayOfWeek == "Friday" and $hour > 12 and $bid > 15) $bid = round($bid / 4);
                    if($dayOfWeek == "Saturday" and $bid > 15) $minimalBids = true;
                    if($dayOfWeek == "Sunday" and $hour < 11)  $minimalBids = true;
                }


                    /**
                     * 07.08.2017 , sumato.shigoto@gmail.com
                     * ��������� ������� ������ ������ ������� ��� �������� ����������� ������ �� �����,
                     * ��������� ��������� ����� �� 1 �� 10 � ������ (����� ����������� � API ����� ������)
                     * todo ������������ ���� ������� � ������������, ����� ����� ���� ���� ���� ���-�� ���������.
                     */

                    if($mediumBids == true) {
                        if(in_array($arAcc["IBLOCK_ID"],Array(14,22,23,24,55,82))){
                            //$bid = round($bid / 3);
                            $bid = round($bid / 2);
                        } else {
                            $bid = round($bid / 4);
                            //$minimalBids = true;
                        }
                    }
                    if($minimalBids == true) {
                        $tmpPrice = pow($minPrice,0.56);
                        $bid = round(($categoryRate * $tmpPrice / 100) + rand(1,5));
						if(in_array($arAcc["IBLOCK_ID"],Array(14,22,23,24,55,82))){
						$bid = $bid + rand(1,10);
						}

                    }
                    if(in_array($arAcc["IBLOCK_ID"],$plitkaBlocks) AND in_array($arAcc["PROPERTY_APPLICATION_VALUE"],$depressedTypes)) {
                        $bid = round($bid / 3);
                }

                if($arAcc["IBLOCK_ID"] == '82' AND in_array($arAcc["PROPERTY_PRODUCT_TYPE_VALUE"],$depressedTypes))
                    $bid = round($bid / 3);

                if($extraBids == true) {
                    $bid = round($bid * 2);
                }

                $strBid .= " bid=\"" . $bid . "\"";


                $strTmpOff .= "<offer id=\"" . $arAcc["ID"] . "\"" . $str_AVAILABLE . $strBid . ">\n";


                $strTmpOff .= "<url>https://" . $arAcc['SERVER_NAME'] . htmlspecialchars($arAcc["DETAIL_PAGE_URL"]) . (strstr($arAcc['DETAIL_PAGE_URL'], '?') === false ? '?' : '&amp;') . "utm_source=Yandex.Market&amp;utm_medium=CPC&amp;utm_campaign=" . $arAcc["IBLOCK_CODE"] . $brandUtmString . "&amp;utm_term=" . $arAcc["ID"] . "-b" . $bid . "</url>\n";
                $strTmpOff .= "<price>" . round($minPrice) . "</price>\n";
                if (round($minPrice) < $basePrice) {
                    $strTmpOff .= "<oldprice>" . round($basePrice) . "</oldprice>\n";
                }
                $strTmpOff .= "<currencyId>" . $currency . "</currencyId>\n";

                $marketCatId = getYandexCategory($arAcc["IBLOCK_ID"]);
                if (!$marketCatId) {
                    $strTmpOff .= "<categoryId>" . $arAcc["IBLOCK_ID"] . "</categoryId>\n";
                } else {
                    $strTmpOff .= "<categoryId>" . $marketCatId . "</categoryId>\n";

                }

                if ($imageHash) { //2017.05.02, sumato.shigoto@gmail.com, ����� � ������ ����� ���� �������� �� ������� �������� � ��������, �� ������� ����� �������� � ��� ������������
                    $strTmpOff .= "<picture>" . $strFile . "</picture>\n";
                }

                $str = "";
                if (!empty($arAcc["PROPERTY_DELIVERY_COST_VALUE"]) && $arAcc["PROPERTY_DELIVERY_COST_VALUE"] != "&lt;&gt;" && $arAcc["PROPERTY_DELIVERY_COST_VALUE"] != "<>") {
                    if ($available) {
                        $deliveryFrom = intval($arAcc["PROPERTY_DELIVERY_DATE_VALUE"]) == 2 ? 1 : intval($arAcc["PROPERTY_DELIVERY_DATE_VALUE"]);
                    } else {
                        $deliveryFrom = 32;
                    }
                    $deliveryTill = $deliveryFrom + 2;

                    if(in_array($arAcc["IBLOCK_ID"],$plitkaBlocks) AND in_array($arAcc["PROPERTY_APPLICATION_VALUE"],$depressedTypes)) {
                        $arAcc["PROPERTY_DELIVERY_COST_VALUE"] = 0;
                        //if(($deliveryTill - $deliveryFrom) < 4) $deliveryTill = $deliveryFrom + 3;
                    }

                    $str .= "<delivery-options>\n";
                    $str .= "<option cost=\"" . preg_replace("/[^0-9.]/", "", $arAcc["PROPERTY_DELIVERY_COST_VALUE"]) . "\" days=\"" . $deliveryFrom . "-" . $deliveryTill . "\" order-before=\"" . $orderBefore . "\"/>\n";
                    $str .= "</delivery-options>\n";

                    $strTmpOff .= "<delivery>true</delivery>\n";
                    if (!in_array($arAcc["IBLOCK_ID"], $forbiddenBlocks))
                        $strTmpOff .= "<pickup>true</pickup>\n";
                }
                $strTmpOff .= $str;
                $strTmpOff .= "<vendor>" . $vendor . "</vendor>\n";
                $strTmpOff .= "<name>" . $productName . "</name>\n";

                if (!empty($arAcc["PROPERTY_COLLECTION_VALUE"]) && ($arAcc["PROPERTY_COLLECTION_VALUE"] != "&lt;&gt;")) {
                    if (!empty($arAcc["PROPERTY_NAME_COLLECTION_VALUE"]) && ($arAcc["PROPERTY_NAME_COLLECTION_VALUE"] != "&lt;&gt;")) {
                        $strTmpOff .= "<model>" . yandex_text2xml($arAcc["PROPERTY_COLLECTION_VALUE"], true) . " " . yandex_text2xml($arAcc["PROPERTY_NAME_COLLECTION_VALUE"], true) . "</model>\n";
                    } else {
                        $strTmpOff .= "<model>" . yandex_text2xml($arAcc["PROPERTY_COLLECTION_VALUE"], true) . "</model>\n";
                    }

                }




                /*  end 09.07.2012 "��������� ��������"  */





                if (strlen(TruncateText($arAcc["~PREVIEW_TEXT"])) > 4) {
                    $strTmpOff .=
                        "<description>" .
                        yandex_text2xml(TruncateText(
                            ($arAcc["PREVIEW_TEXT_TYPE"] == "html" ?
                                strip_tags(preg_replace_callback("'&[^;]*;'", "yandex_replace_special", $arAcc["~PREVIEW_TEXT"])) : preg_replace_callback("'&[^;]*;'", "yandex_replace_special", $arAcc["~PREVIEW_TEXT"])),
                            255), true) .
                        "</description>\n";
                }

                // ��������� ��������� ������
                if ($arAcc["PROPERTY_CML2_KRAT_VALUE"]) {
                    if (in_array(intval($arAcc['IBLOCK_ID']), $furnitureBlocks)) {
                        $strTmpOff .= "<sales_notes>����������� ������ �������� " . round($arAcc["PROPERTY_CML2_KRAT_VALUE"], 3) . " " . $arAcc["PROPERTY_CML2_BASE_UNIT_VALUE"] . "</sales_notes>\n";
                    } elseif(in_array(intval($arAcc['IBLOCK_ID']), $plitkaBlocks)) {
                        //$strTmpOff .= "<sales_notes>���.����� �� 9000�., �� 25000�. ������ � �������!</sales_notes>\n";
                       // $strTmpOff .= "<sales_notes>����������� ����� �� 9000 ������</sales_notes>\n";
                        $strTmpOff .= "<sales_notes>���. ����� �� 2500�. �� 3000�. - ����������.</sales_notes>\n";
                    } elseif(in_array(intval($arAcc['IBLOCK_ID']), $oboiBlocks)) {
                        $strTmpOff .= "<sales_notes>�� 9000�. �������� �� �� 500�. �� 3000� ����������</sales_notes>\n";
                    } else {
                        $strTmpOff .= "<sales_notes>�� 3000�. - ����������. ���.����� �� " . round($arAcc["PROPERTY_CML2_KRAT_VALUE"], 3) . " " . $arAcc["PROPERTY_CML2_BASE_UNIT_VALUE"] . "</sales_notes>\n";
                    }
                }



                if(!empty($arAcc["PROPERTY_GUARANTEE_VALUE"]) && $arAcc["PROPERTY_GUARANTEE_VALUE"] != '&lt;&gt;')
                    $strTmpOff .= "<manufacturer_warranty>true</manufacturer_warranty>\n";

                if (!empty($arAcc["PROPERTY_COUNTRY_VALUE"]) && ($arAcc["PROPERTY_COUNTRY_VALUE"] != "&lt;&gt;")) {
                    if($arAcc["PROPERTY_COUNTRY_VALUE"] == "�����") $arAcc["PROPERTY_COUNTRY_VALUE"] = "����� �����";
                    $strTmpOff .= "<country_of_origin>" . yandex_text2xml($arAcc["PROPERTY_COUNTRY_VALUE"]) . "</country_of_origin>\n";
                }

                if (!empty($arAcc["PROPERTY_CML2_KRAT_VALUE"])) {
                    $strTmpOff .= "<param name=\"����������� ������ ��������\">" . round($arAcc["PROPERTY_CML2_KRAT_VALUE"], 3) . " " . $arAcc["PROPERTY_CML2_BASE_UNIT_VALUE"] . "</param>\n";
                }

                if (!empty($arAcc["PROPERTY_COLOR_VALUE"])) {
                    if (!in_array($arAcc["PROPERTY_COLOR_VALUE"], $stopValues)) {
                        $strTmpOff .= "<param name=\"����\">" . strtolower(str_replace("/",",",yandex_text2xml($arAcc["PROPERTY_COLOR_VALUE"]))) . "</param>\n";
                    }
                }

                if (!empty($arAcc["PROPERTY_MATERIAL_VALUE"])) {
                    if (!is_numeric($arAcc["PROPERTY_MATERIAL_VALUE"]) && !in_array($arAcc["PROPERTY_MATERIAL_VALUE"], $stopValues)) {
                        $strTmpOff .= "<param name=\"��������\">" . strtolower(yandex_text2xml($arAcc["PROPERTY_MATERIAL_VALUE"])) . "</param>\n";
                    }
                }


                if (!empty($arAcc["PROPERTY_APPLICATION_VALUE"]) && ($arAcc["PROPERTY_APPLICATION_VALUE"] != "&lt;&gt;")) {
                    if(in_array($arAcc["IBLOCK_ID"],$plitkaBlocks) AND in_array($arAcc["PROPERTY_APPLICATION_VALUE"],$depressedTypes))
                        $additionalType = ", ������";
                    $strTmpOff .= "<param name=\"���\">" . strtolower(yandex_text2xml($arAcc["PROPERTY_APPLICATION_VALUE"])) . $additionalType . "</param>\n";
                }

                if (!empty($arAcc["PROPERTY_INPACK_VALUE"]) && ($arAcc["PROPERTY_INPACK_VALUE"] != "&lt;&gt;")) {
                    if(in_array($arAcc["IBLOCK_ID"],$plitkaBlocks)) {
                        $strTmpOff .= "<param name=\"�2 � �������\" unit=\"�2\">" . yandex_text2xml($arAcc["PROPERTY_INPACK_VALUE"]) . "</param>\n";
                    }

                }
                
                if (!empty($arAcc["PROPERTY_PACKING_VALUE"]) && ($arAcc["PROPERTY_PACKING_VALUE"] != "&lt;&gt;")) {
                    if(in_array($arAcc["IBLOCK_ID"],$plitkaBlocks)) {
                        $strTmpOff .= "<param name=\"���������� � �������\" unit=\"��\">" . yandex_text2xml($arAcc["PROPERTY_PACKING_VALUE"]) . "</param>\n";
                    }

                }

                if (!empty($arAcc["PROPERTY_SURFPROC_VALUE"]) && ($arAcc["PROPERTY_SURFPROC_VALUE"] != "&lt;&gt;")) {
                    if(in_array($arAcc["IBLOCK_ID"],$plitkaBlocks)) {
                        if(yandex_text2xml($arAcc["PROPERTY_SURFPROC_VALUE"]) == "�������������") {
                            $strTmpOff .= "<param name=\"������������� �����������\">��</param>\n";
                        } elseif(in_array(yandex_text2xml($arAcc["PROPERTY_SURFPROC_VALUE"]),$surfaceGloss)) {
                            $surface = "���������";
                        } elseif(in_array(yandex_text2xml($arAcc["PROPERTY_SURFPROC_VALUE"]),$surfaceMate)) {
                            $surface = "�������";
                        } else {
                            $surface = strtolower(yandex_text2xml($arAcc["PROPERTY_SURFPROC_VALUE"]));
                        }
                        if(!empty($surface)) {
                            $strTmpOff .= "<param name=\"������������� �����������\">���</param>\n";
                            $strTmpOff .= "<param name=\"�����������\">" . $surface . "</param>\n";
                        }
                        unset($surface);
                    }

                }

                if (!empty($arAcc["PROPERTY_DESTINATION_VALUE"]) && ($arAcc["PROPERTY_DESTINATION_VALUE"] != "&lt;&gt;")) {
                    $strTmpOff .= "<param name=\"����������\">" . strtolower(yandex_text2xml($arAcc["PROPERTY_DESTINATION_VALUE"])) . "</param>\n";
                }

                $strTmpOff .= "</offer>\n";
            }
        }
    }

    @fwrite($fp, $treeXML . "\n");
    @fwrite($fp, $deliveryOptions . "\n");

    @fwrite($fp, "<offers>\n");
    @fwrite($fp, $strTmpOff);
    @fwrite($fp, "</offers>\n");

    @fwrite($fp, "</shop>\n");
    @fwrite($fp, "</yml_catalog>\n");

    @fclose($fp);
}

if ($bTmpUserCreated) unset($USER);

$SETUP_FILE_NAME_NEW = "/yml/yandex.php";
//copy($_SERVER["DOCUMENT_ROOT"].,$_SERVER["DOCUMENT_ROOT"]."/yml/yandex-backup-".date("d.m.Y_H-i-s").".php");

$arOfferId = array();

$rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 94), false, false, array("ID", "NAME"));
while ($arElement = $rsElement->Fetch()) {
    $offerId = intval($arElement["ID"]);
    if ($offerId > 0 && !in_array($offerId, $arOfferId))
        $arOfferId[] = $offerId;

    CIBlockElement::Delete($arElement["ID"]);
}

if (!empty($arOfferId)) {
    $yandexData = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME);

    foreach ($arOfferId as $offerId) {
        while (!empty($arOfferId)) {
            $arOfferDel = array_splice($arOfferId, 0, 10); // ������� �� 10 ����
            $yandexData = preg_replace('/(<offer id=\"(' . implode('|', $arOfferDel) . ')\".+<\/offer>)\s+/sU', '', $yandexData);
        }
    }
    @file_put_contents($_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, $yandexData);
}

rename($_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, $_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME_NEW);
echo date("d.m.Y_H-i-s") . "<br>";
?>
