<?
/*-------------*/
//�������� �� ����������� �������
if (!defined("B_PROLOG_INCLUDED")) require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$deliverySchedule = Array(
    "Monday" => 24,
    "Tuesday" => 24,
    "Wednesday" => 24,
    "Thursday" => 24,
    "Friday" => 12,
    "Saturday" => 0,
    "Sunday" => 24
);

$idleDays = Array(
    "Saturday", "Sunday"
);

$dayOfWeek = date("l");
$hour = intval(date('H'));

$orderBefore = $deliverySchedule[$dayOfWeek];

$marketCats = Array(
    1 => Array("parentId" => 0, "Name" => "������� �������"),
    2 => Array("parentId" => 0, "Name" => "������������"),
    3 => Array("parentId" => 0, "Name" => "������ ��� ���� � ����"),
    4 => Array("parentId" => 1, "Name" => "������� ��� ����"),
    6 => Array("parentId" => 4, "Name" => "����������� ������� ����� ���"),
    7 => Array("parentId" => 6, "Name" => "������� ���������� ��������", "IBLOCK_ID" => 76),
    8 => Array("parentId" => 2, "Name" => "������� ������ � ������������"),
    9 => Array("parentId" => 8, "Name" => "����� � ���������", "IBLOCK_ID" => 73),
    10 => Array("parentId" => 3, "Name" => "������"),
    11 => Array("parentId" => 10, "Name" => "������� ��������� ������"),
    12 => Array("parentId" => 11, "Name" => "������� ��������� ��� ������ ������", "IBLOCK_ID" => 35),
    13 => Array("parentId" => 3, "Name" => "������ ��� ������������� � �������"),
    14 => Array("parentId" => 13, "Name" => "�����, ����, �������� �����"),
    15 => Array("parentId" => 14, "Name" => "���������� ��� ���� � ������", "IBLOCK_ID" => 64),
    16 => Array("parentId" => 15, "Name" => "�����, �������, �������", "IBLOCK_ID" => Array(57, 60, 61, 62, 63)),
    17 => Array("parentId" => 15, "Name" => "����� �������", "IBLOCK_ID" => 56),
    18 => Array("parentId" => 14, "Name" => "������� �����", "IBLOCK_ID" => Array(29, 50)),
    19 => Array("parentId" => 14, "Name" => "������������ �����", "IBLOCK_ID" => Array(12, 51)),
    20 => Array("parentId" => 13, "Name" => "���������� � �������������", "IBLOCK_ID" => 40),
    21 => Array("parentId" => 20, "Name" => "�����", "IBLOCK_ID" => 29),
    22 => Array("parentId" => 20, "Name" => "���� � ������� ������"),
    23 => Array("parentId" => 22, "Name" => "������� ������", "IBLOCK_ID" => 37),
    24 => Array("parentId" => 22, "Name" => "����, ������� ������, ���������", "IBLOCK_ID" => 38),
    25 => Array("parentId" => 20, "Name" => "��������, �����������", "IBLOCK_ID" => 33),
    26 => Array("parentId" => 20, "Name" => "����������� ����", "IBLOCK_ID" => 71),
    27 => Array("parentId" => 20, "Name" => "���������", "IBLOCK_ID" => 36),
    28 => Array("parentId" => 20, "Name" => "����� ��� ��������", "IBLOCK_ID" => 35),
    29 => Array("parentId" => 20, "Name" => "�������, ��������, ����", "IBLOCK_ID" => 34),
    30 => Array("parentId" => 13, "Name" => "������� ��������� � ����������"),
    31 => Array("parentId" => 30, "Name" => "�����������������", "IBLOCK_ID" => 39),
    32 => Array("parentId" => 30, "Name" => "������������� ������ ���", "IBLOCK_ID" => 77),
    33 => Array("parentId" => 13, "Name" => "������������ � ���������� ���������"),
    34 => Array("parentId" => 33, "Name" => "���������	 ��������"),
    35 => Array("parentId" => 34, "Name" => "�������", "IBLOCK_ID" => 15),
    36 => Array("parentId" => 34, "Name" => "��������� �����", "IBLOCK_ID" => 16),
    37 => Array("parentId" => 34, "Name" => "��������� �����", "IBLOCK_ID" => Array(17, 54)),
    38 => Array("parentId" => 34, "Name" => "��������� ���", "IBLOCK_ID" => 43),
    39 => Array("parentId" => 33, "Name" => "���������� ���������"),
    40 => Array("parentId" => 39, "Name" => "������ �� �������������", "IBLOCK_ID" => 14),
    41 => Array("parentId" => 39, "Name" => "������������ ������", "IBLOCK_ID" => 22),
    42 => Array("parentId" => 39, "Name" => "�������", "IBLOCK_ID" => 23),
    43 => Array("parentId" => 39, "Name" => "���������� ������", "IBLOCK_ID" => 24),
    44 => Array("parentId" => 39, "Name" => "����", "IBLOCK_ID" => 82),
    45 => Array("parentId" => 39, "Name" => "�������", "IBLOCK_ID" => Array(85, 86, 89, 91)),
    46 => Array("parentId" => 39, "Name" => "�������� ������", "IBLOCK_ID" => Array(87, 88, 90)),
    47 => Array("parentId" => 33, "Name" => "������������ ���������"),
    48 => Array("parentId" => 47, "Name" => "������� � ����� ������������ �����"),
    49 => Array("parentId" => 48, "Name" => "������������ ����", "IBLOCK_ID" => 83,20),
    50 => Array("parentId" => 47, "Name" => "���������� � ����������������� ���������"),
    51 => Array("parentId" => 50, "Name" => "���������", "IBLOCK_ID" => 92),
    52 => Array("parentId" => 47, "Name" => "��������� �������"),
    53 => Array("parentId" => 52, "Name" => "����������� � ��������", "IBLOCK_ID" => 93),
    54 => Array("parentId" => 52, "Name" => "��������� �����", "IBLOCK_ID" => 93),
    55 => Array("parentId" => 52, "Name" => "���������� � ������������� ��� ��������� ������", "IBLOCK_ID" => 93),
    56 => Array("parentId" => 13, "Name" => "���������"),
    57 => Array("parentId" => 56, "Name" => "������� ������ � �������������", "IBLOCK_ID" => Array(72, 74, 75)),
    58 => Array("parentId" => 15, "Name" => "����� �������", "IBLOCK_ID" => Array(55, 58, 59))
);

$mappingRules = Array();

foreach ($marketCats as $branch => $lists) {
    if (is_array($lists["IBLOCK_ID"])) {
        foreach ($lists["IBLOCK_ID"] as $item) {
            $mappingRules[$item] = $branch;
        }
    } else {
        if ($lists["IBLOCK_ID"] > 0)
            $mappingRules[$lists["IBLOCK_ID"]] = $branch;
    }
}

$treeXML = '<categories>
<category id="1">������� �������</category>
<category id="2">������������</category>
<category id="3">������ ��� ���� � ����</category>
<category id="4" parentId="1">������� ��� ����</category>
<category id="6" parentId="4">����������� ������� ����� ���</category>
<category id="7" parentId="6">������� ���������� ��������</category>
<category id="8" parentId="2">������� ������ � ������������</category>
<category id="9" parentId="8">����� � ���������</category>
<category id="10" parentId="3">������</category>
<category id="11" parentId="10">������� ��������� ������</category>
<category id="12" parentId="11">������� ��������� ��� ������ ������</category>
<category id="13" parentId="3">������ ��� ������������� � �������</category>
<category id="14" parentId="13">�����, ����, �������� �����</category>
<category id="15" parentId="14">���������� ��� ���� � ������</category>
<category id="16" parentId="15">�����, �������, �������</category>
<category id="17" parentId="15">����� �������</category>
<category id="18" parentId="14">������� �����</category>
<category id="19" parentId="14">������������ �����</category>
<category id="20" parentId="13">���������� � �������������</category>
<category id="21" parentId="20">�����</category>
<category id="22" parentId="20">���� � ������� ������</category>
<category id="23" parentId="22">������� ������</category>
<category id="24" parentId="22">����, ������� ������, ���������</category>
<category id="25" parentId="20">��������, �����������</category>
<category id="26" parentId="20">����������� ����</category>
<category id="27" parentId="20">���������</category>
<category id="28" parentId="20">����� ��� ��������</category>
<category id="29" parentId="20">�������, ��������, ����</category>
<category id="30" parentId="13">������� ��������� � ����������</category>
<category id="31" parentId="30">�����������������</category>
<category id="32" parentId="30">������������� ������ ���</category>
<category id="33" parentId="13">������������ � ���������� ���������</category>
<category id="34" parentId="33">��������� ��������</category>
<category id="35" parentId="34">�������</category>
<category id="36" parentId="34">��������� �����</category>
<category id="37" parentId="34">��������� �����</category>
<category id="38" parentId="34">��������� ���</category>
<category id="39" parentId="33">���������� ���������</category>
<category id="40" parentId="39">������ �� �������������</category>
<category id="41" parentId="39">������������ ������</category>
<category id="42" parentId="39">�������</category>
<category id="43" parentId="39">���������� ������</category>
<category id="44" parentId="39">����</category>
<category id="45" parentId="39">�������</category>
<category id="46" parentId="39">�������� ������</category>
<category id="47" parentId="33">������������ ���������</category>
<category id="48" parentId="47">������� � ����� ������������ �����</category>
<category id="49" parentId="48">������������ ����</category>
<category id="50" parentId="47">���������� � ����������������� ���������</category>
<category id="51" parentId="50">���������</category>
<category id="52" parentId="47">��������� �������</category>
<category id="53" parentId="52">����������� � ��������</category>
<category id="54" parentId="52">��������� �����</category>
<category id="55" parentId="52">���������� � ������������� ��� ��������� ������</category>
<category id="56" parentId="13">���������</category>
<category id="57" parentId="56">������� ������ � �������������</category>
<category id="58" parentId="15">����� �������</category>
</categories>';

$deliveryOptions = "<delivery-options>
<option cost=\"1400\" days=\"2-3\" order-before=\"14\"/>
</delivery-options>";



$furnitureBlocks = Array(55, 56, 57, 58, 59, 60, 61, 62, 63);


$stopValues = Array(
    '��� ������',
    '&lt;&gt;',
    ' ',
    '<>'
);

define("NOT_CHECK_PERMISSIONS", true);
CModule::IncludeModule("currency");
CModule::IncludeModule("catalog");
global $APPLICATION;

include(GetLangFileName($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/catalog/lang/", "/export_setup_templ.php"));
set_time_limit(0);
global $USER;
$bTmpUserCreated = False;
if (!is_object($USER)) {
    $bTmpUserCreated = True;
    $GLOBALS['USER'] = new CUser;
}

function yandex_replace_special($arg)
{
    if (in_array($arg[0], array("&quot;", "&amp;", "&lt;", "&gt;")))
        return $arg[0];
    else
        return " ";
}

function yandex_text2xml($text, $bHSC = false)
{
    $text = $GLOBALS['APPLICATION']->ConvertCharset($text, LANG_CHARSET, 'windows-1251');
    if ($bHSC)
        $text = htmlspecialchars($text);
    $text = preg_replace('/[\x01-\x08\x0B-\x0C\x0E-\x1F]/', "", $text);
    $text = str_replace("'", "&apos;", $text);
    $text = str_replace(array("\r", "\n"), '', $text);
    return $text;
}

function getYandexCategory($ibid)
{
    global $mappingRules;
    if ($mappingRules[$ibid] > 0) {
        return $mappingRules[$ibid];
    } else {
        return false;
    }
}

$strExportErrorMessage = "";

if (empty($SETUP_FILE_NAME))
    $SETUP_FILE_NAME = "/yml/tlock_tmp.php";
/*-------------*/
$SETUP_SERVER_NAME = trim($SETUP_SERVER_NAME);
$SETUP_FILE_NAME = Rel2Abs("/", $SETUP_FILE_NAME);


if (strlen($strExportErrorMessage) <= 0) {
    if (!$fp = @fopen($_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, "wb")) {
        $strExportErrorMessage .= str_replace('#FILE#', $_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_OPEN_WRITING')) . "\n";
    } else {
        if (!@fwrite($fp, '<?if (!isset($_GET["referer1"]) || strlen($_GET["referer1"])<=0) $_GET["referer1"] = "yandext"?>')) {
            $strExportErrorMessage .= str_replace('#FILE#', $_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_WRITE')) . "\n";
            @fclose($fp);
        }
    }
}

if (strlen($strExportErrorMessage) <= 0) {

    @fwrite($fp, '<? header("Content-Type: text/xml; charset=windows-1251");?>');
    @fwrite($fp, '<? echo "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">"?>');
    @fwrite($fp, "\n<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");
    @fwrite($fp, "<yml_catalog date=\"" . Date("Y-m-d H:i") . "\">\n");
    @fwrite($fp, "<shop>\n");
    @fwrite($fp, "<name>" . htmlspecialchars($APPLICATION->ConvertCharset(COption::GetOptionString("main", "site_name", ""), LANG_CHARSET, 'windows-1251')) . "</name>\n");
    @fwrite($fp, "<company>" . htmlspecialchars($APPLICATION->ConvertCharset(COption::GetOptionString("main", "site_name", ""), LANG_CHARSET, 'windows-1251')) . "</company>\n");
    @fwrite($fp, "<url>https://" . htmlspecialchars(strlen($SETUP_SERVER_NAME) > 0 ? $SETUP_SERVER_NAME : COption::GetOptionString("main", "server_name", "")) . "</url>\n");

    $db_acc = CCurrency::GetList(($by = "sort"), ($order = "asc"));
    $strTmp = "<currencies>\n";
    $arCurrencyAllowed = array('RUR', 'RUB', 'USD', 'EUR', 'UAH');
    while ($arAcc = $db_acc->Fetch()) {
        if (in_array($arAcc['CURRENCY'], $arCurrencyAllowed))
            $strTmp .= "<currency id=\"" . $arAcc["CURRENCY"] . "\" rate=\"" . $arAcc["AMOUNT"] . "\"/>\n";
    }
    $strTmp .= "</currencies>\n";

    @fwrite($fp, $strTmp);

    $arSelect = array(
        "ID",
        "LID",
        "IBLOCK_ID",
        "IBLOCK_CODE",
        "IBLOCK_SECTION_ID",
        "ACTIVE",
        "ACTIVE_FROM",
        "ACTIVE_TO",
        "NAME",
        "PREVIEW_PICTURE",
        "PREVIEW_TEXT",
        "PREVIEW_TEXT_TYPE",
        "DETAIL_PICTURE",
        "LANG_DIR",
        "DETAIL_PAGE_URL",
        "PROPERTY_APPLICATION",
        "PROPERTY_DESTINATION",
        "PROPERTY_BAU_CODE",
        "PROPERTY_BRAND",
        "PROPERTY_BRAND_GUID",
        "PROPERTY_COLLECTION",
        "PROPERTY_NAME_COLLECTION",
        "PROPERTY_COUNTRY",
        "PROPERTY_MATERIAL",
        "PROPERTY_COLOR",
        "PROPERTY_DELIVERY_DATE",
        "PROPERTY_DELIVERY_COST",
        "PROPERTY_COMPLECT_TYPE",
        "PROPERTY_GUARANTEE",
        "XML_ID",
        "PROPERTY_CML2_KRAT",
        "PROPERTY_CML2_BASE_UNIT"
    );

    $tOrder = Array();
    $tFilter = Array(
        "IBLOCK_ID" => 95
    );
    $tNavStartParams = Array("nTopCount" => 1);
    $tFields = Array(
        "ID", "LID", "IBLOCK_ID", "NAME", "PROPERTY_REAL_ID", "PROPERTY_URL"
    );


    $strTmpCat = "";
    $strTmpOff = "";


    $YANDEX_EXPORT = $furnitureBlocks;

    if (is_array($YANDEX_EXPORT)) {
        $arSiteServers = array();
        foreach ($YANDEX_EXPORT as $value) {
            $filter = Array("IBLOCK_ID" => IntVal($value), "ACTIVE" => "Y", "IBLOCK_ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y");
            /*-------------*/
            //�������� ������ �� ����������������� �������� �������
            //���� �������� �����, �� �� ���������
            $filter["!UF_YA"] = false;
            /*-------------*/
            $db_acc = CIBlockSection::GetList(array("left_margin" => "asc"), $filter);
            $arAvailGroups = array();
            while ($arAcc = $db_acc->Fetch()) {
                $strTmpCat .= "<category id=\"" . $arAcc["ID"] . "\"" . (IntVal($arAcc["IBLOCK_SECTION_ID"]) > 0 ? " parentId=\"" . $arAcc["IBLOCK_SECTION_ID"] . "\"" : "") . ">" . yandex_text2xml($arAcc["NAME"], true) . "</category>\n";
                $arAvailGroups[] = IntVal($arAcc["ID"]);
            }

            //*****************************************//
            /*-------------*/
            /*������ �������*/
            $filter = Array("IBLOCK_ID" => IntVal($value), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
            $productFilter[">CATALOG_PRICE_BASE"] = 20;
            $productFilter["!NAME"] = Array("%�����%", "%�����%", "!%");
            $filter["SECTION_ID"] = $arAvailGroups;
            $filter[] = array(
                "LOGIC" => "OR",
                "PROPERTY_AVAIL_DATE" => false,
                "<=PROPERTY_AVAIL_DATE" => date("Y-m-d") . ' 23:59:59'
            );

            $arNavStart = false;

            /*-------------*/
            $res = CIBlockElement::GetList(array(), $filter, false, $arNavStart, $arSelect);
            $exported = array();

            while ($arAcc = $res->GetNext()) {
                $breakCycle = false;


                if (in_array($arAcc["ID"], $exported))
                    continue;
                else
                    $exported[] = $arAcc["ID"];

                /**
                 * 15.08.2017 , sumato.shigoto@gmail.com
                 * ������ � �������� ���������� ��������, � ������� ��������� ������ BRAND_GUID,
                 * � �� ��������� �������� BRAND. ��� ����� ��������� ����� �������������� ��������,
                 * ������� �������� ��� ������������� �� ���� BRAND_GIUD .
                 * ��� ��� ��� ���� � �� ������ ����� ���� ��������� ��� ������, � ���� ��������� �������
                 * � ������������, ���������� ��������� �������������:
                 * �. ��� ��� ������� �� �������� ������ XML_ID �� 1� (�������� $checkName)
                 * �. ����� ���� ������� �� BRAND_GUID, ���������� ���������, ��� �������� ������������ ����� ����� ����,
                 * � �� ������ �����.
                 * ������, � �����, detected.
                 */
#############################
                unset($brandUtmString, $vendor);
                $checkName = preg_match("/.*-.*-.*-.*-.*/", $arAcc["PROPERTY_BRAND_VALUE"]);
                if (
                    (empty($arAcc["PROPERTY_BRAND_VALUE"]) || $arAcc["PROPERTY_BRAND_VALUE"] == "&lt;&gt;" || $checkName == 1)
                    && !empty($arAcc["PROPERTY_BRAND_GUID_VALUE"])
                    && $arAcc["PROPERTY_BRAND_GUID_VALUE"] != "&lt;&gt;"
                ) {
                    if (preg_match("/\d++/", $arAcc["PROPERTY_BRAND_GUID_VALUE"]) != 1) {
                        $arAcc["PROPERTY_BRAND_VALUE"] = $arAcc["PROPERTY_BRAND_GUID_VALUE"];
                    } else {
                        $brandQuery = CIBlockElement::GetByID($arAcc["PROPERTY_BRAND_GUID_VALUE"]);
                        if ($br_res = $brandQuery->GetNext()) {
                            $arAcc["PROPERTY_BRAND_VALUE"] = $br_res['NAME'];
                        }
                    }

                }

                if (empty($arAcc["PROPERTY_BRAND_VALUE"])) continue;
                $brandUtmString = "&amp;utm_content=" . ucfirst(rawurlencode($arAcc["PROPERTY_BRAND_VALUE"]));
                $vendor = yandex_text2xml($arAcc["PROPERTY_BRAND_VALUE"], true);
#############################


#############################

                unset($tName, $tBasePrice, $tMinPrice, $tUrl);
                if (is_numeric($arAcc["PROPERTY_BAU_CODE_VALUE"])) {
                    $tFilter["PROPERTY_REAL_ID"] = $arAcc["PROPERTY_BAU_CODE_VALUE"];
                    $tProductQuery = CIBlockElement::GetList($tOrder, $tFilter, false, $tNavStartParams, $tFields);
                    while ($tProduct = $tProductQuery->GetNext()) {
                        $tName = $tProduct["NAME"];
                        $tUrl  = $tProduct["PROPERTY_URL_VALUE"];
                        $tPriceArray = CCatalogProduct::GetOptimalPrice($tProduct["ID"], 1, $USER->GetUserGroupArray());
                        if (!empty($tPriceArray['DISCOUNT_PRICE']) && $tPriceArray['DISCOUNT_PRICE'] < $tPriceArray['PRICE']['PRICE']) {
                            $tBasePrice = round($optimalPrice['PRICE']['PRICE']);
                            $tMinPrice = round($optimalPrice['DISCOUNT_PRICE']);
                        } else {
                            $tMinPrice = round($optimalPrice['PRICE']['PRICE']);
                        }
                    }
                }
                if (empty($tName)) continue;

                $tmpPrName = yandex_text2xml($tName, true);
                $productName = $tmpPrName;
#############################


#############################

                if (strlen($SETUP_SERVER_NAME) <= 0) {
                    if (!array_key_exists($arAcc['LID'], $arSiteServers)) {
                        $rsSite = CSite::GetList(($b = "sort"), ($o = "asc"), array("LID" => $arAcc["LID"]));
                        if ($arSite = $rsSite->Fetch())
                            $arAcc["SERVER_NAME"] = $arSite["SERVER_NAME"];
                        if (strlen($arAcc["SERVER_NAME"]) <= 0 && defined("SITE_SERVER_NAME"))
                            $arAcc["SERVER_NAME"] = SITE_SERVER_NAME;
                        if (strlen($arAcc["SERVER_NAME"]) <= 0)
                            $arAcc["SERVER_NAME"] = COption::GetOptionString("main", "server_name", "");

                        $arSiteServers[$arAcc['LID']] = $arAcc['SERVER_NAME'];
                    } else {
                        $arAcc['SERVER_NAME'] = $arSiteServers[$arAcc['LID']];
                    }
                } else {
                    $arAcc['SERVER_NAME'] = $SETUP_SERVER_NAME;
                }
#############################


#############################

                if (IntVal($arAcc["DETAIL_PICTURE"]) > 0 || IntVal($arAcc["PREVIEW_PICTURE"]) > 0) {

                    $pictNo = IntVal($arAcc["DETAIL_PICTURE"]);
                    if ($pictNo <= 0) $pictNo = IntVal($arAcc["PREVIEW_PICTURE"]);
                    $arPictInfo = CFile::GetFileArray($pictNo);
                    if (is_array($arPictInfo)) {
                        if (substr($arPictInfo["SRC"], 0, 1) == "/") {
                            $strFile = "https://" . $arAcc['SERVER_NAME'] . implode("/", array_map("rawurlencode", explode("/", $arPictInfo["SRC"])));
                            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $arPictInfo['SRC'])) $breakCycle = true;
                        } elseif (preg_match("/^(http|https):\\/\\/(.*?)\\/(.*)\$/", $arPictInfo["SRC"], $match)) {
                            $strFile = "https://" . $match[2] . implode("/", array_map("rawurlencode", explode("/", $match[3])));
                        } else {
                            $strFile = $arPictInfo["SRC"];
                            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/" . $arPictInfo['SRC'])) $breakCycle = true;
                        }
                    }
                }

                if ($breakCycle) continue;
                if (empty($strFile)) continue;
#############################


#############################

                unset($basePrice, $minPrice);
                $optimalPrice = CCatalogProduct::GetOptimalPrice($arAcc["ID"], 1, $USER->GetUserGroupArray());
                if (!empty($optimalPrice['DISCOUNT_PRICE']) && $optimalPrice['DISCOUNT_PRICE'] < $optimalPrice['PRICE']['PRICE']) {
                    $basePrice = round($optimalPrice['PRICE']['PRICE']);
                    $minPrice = round($optimalPrice['DISCOUNT_PRICE']);
                } else {
                    $minPrice = round($optimalPrice['PRICE']['PRICE']);
                }

                $currency = $optimalPrice['PRICE']['CURRENCY'];
#############################


#############################

                $str_QUANTITY = DoubleVal($arAcc["CATALOG_QUANTITY"]);
                $str_QUANTITY_TRACE = $arAcc["CATALOG_QUANTITY_TRACE"];
                if (($str_QUANTITY <= 0) && ($str_QUANTITY_TRACE == "Y")) {
                    $available = false;
                    $str_AVAILABLE = ' available="false"';
                } else {
                    $available = true;
                    $str_AVAILABLE = ' available="true"';
                }
#############################


#############################

                /* begin 05.07.2012 �������� ������ "�� �����" ��� ����� �������� �� 3 � ����� ���� */

                if (!empty($arAcc["PROPERTY_DELIVERY_DATE_VALUE"]) && $arAcc["PROPERTY_DELIVERY_DATE_VALUE"] >= 3) {
                    $available = false;
                    $str_AVAILABLE = ' available="false"';
                }
#############################


#############################


                $bNoActiveGroup = True;
                $strTmpOff_tmp = "";
                $db_res1 = CIBlockElement::GetElementGroups($arAcc["ID"]);
                while ($ar_res1 = $db_res1->Fetch()) {
                    if (in_array(IntVal($ar_res1["ID"]), $arAvailGroups)) {
                        $strTmpOff_tmp .= "<categoryId>" . $ar_res1["ID"] . "</categoryId>\n";
                        $bNoActiveGroup = False;
                    }
                }

                if ($bNoActiveGroup) continue;
#############################


#############################


                if (strlen($arAcc['DETAIL_PAGE_URL']) <= 0)
                    $arAcc['DETAIL_PAGE_URL'] = '/';
                else
                    $arAcc['DETAIL_PAGE_URL'] = str_replace(' ', '%20', $arAcc['DETAIL_PAGE_URL']);
#############################


#############################

                $strTmpOff .= "<offer id=\"" . $arAcc["ID"] . "\"" . $str_AVAILABLE . ">\n";


                $strTmpOff .= "<url>https://" . $arAcc['SERVER_NAME'] . htmlspecialchars($arAcc["DETAIL_PAGE_URL"]) . (strstr($arAcc['DETAIL_PAGE_URL'], '?') === false ? '?' : '&amp;') . "utm_source=Tlock.ru&amp;utm_medium=CPC&amp;utm_campaign=" . $arAcc["IBLOCK_CODE"] . $brandUtmString . "&amp;utm_term=" . $arAcc["ID"] . "</url>\n";
                $strTmpOff .= "<price>" . round($minPrice) . "</price>\n";
                if (round($minPrice) < $basePrice) {
                    $strTmpOff .= "<oldprice>" . round($basePrice) . "</oldprice>\n";
                }
                $strTmpOff .= "<currencyId>" . $currency . "</currencyId>\n";

                $marketCatId = getYandexCategory($arAcc["IBLOCK_ID"]);
                if (!$marketCatId) {
                    $strTmpOff .= "<categoryId>" . $arAcc["IBLOCK_ID"] . "</categoryId>\n";
                } else {
                    $strTmpOff .= "<categoryId>" . $marketCatId . "</categoryId>\n";

                }

                $strTmpOff .= "<picture>" . $strFile . "</picture>\n";

                $str = "";
                if (!empty($arAcc["PROPERTY_DELIVERY_COST_VALUE"]) && $arAcc["PROPERTY_DELIVERY_COST_VALUE"] != "&lt;&gt;" && $arAcc["PROPERTY_DELIVERY_COST_VALUE"] != "<>") {
                    if ($available) {
                        $deliveryFrom = intval($arAcc["PROPERTY_DELIVERY_DATE_VALUE"]) == 2 ? 1 : intval($arAcc["PROPERTY_DELIVERY_DATE_VALUE"]);
                    } else {
                        $deliveryFrom = 32;
                    }
                    $deliveryTill = $deliveryFrom + 1;


                    $str .= "<delivery-options>\n";
                    $str .= "<option cost=\"" . preg_replace("/[^0-9.]/", "", $arAcc["PROPERTY_DELIVERY_COST_VALUE"]) . "\" days=\"" . $deliveryFrom . "-" . $deliveryTill . "\" order-before=\"" . $orderBefore . "\"/>\n";
                    $str .= "</delivery-options>\n";

                    $strTmpOff .= "<delivery>true</delivery>\n";
                    $strTmpOff .= "<pickup>true</pickup>\n";


                    $strTmpOff .= "<name>" . $productName . "</name>\n";

                    if (!empty($arAcc["PROPERTY_COLLECTION_VALUE"]) && ($arAcc["PROPERTY_COLLECTION_VALUE"] != "&lt;&gt;")) {
                        if (!empty($arAcc["PROPERTY_NAME_COLLECTION_VALUE"]) && ($arAcc["PROPERTY_NAME_COLLECTION_VALUE"] != "&lt;&gt;")) {
                            $strTmpOff .= "<model>" . yandex_text2xml($arAcc["PROPERTY_COLLECTION_VALUE"], true) . " " . yandex_text2xml($arAcc["PROPERTY_NAME_COLLECTION_VALUE"], true) . "</model>\n";
                        } else {
                            $strTmpOff .= "<model>" . yandex_text2xml($arAcc["PROPERTY_COLLECTION_VALUE"], true) . "</model>\n";
                        }

                    }


                    $strTmpOff .= "<vendor>" . $vendor . "</vendor>\n";


                    if (!empty($arAcc["PROPERTY_COUNTRY_VALUE"]) && ($arAcc["PROPERTY_COUNTRY_VALUE"] != "&lt;&gt;")) {
                        if ($arAcc["PROPERTY_COUNTRY_VALUE"] == "�����") $arAcc["PROPERTY_COUNTRY_VALUE"] = "����� �����";
                        $strTmpOff .= "<country_of_origin>" . yandex_text2xml($arAcc["PROPERTY_COUNTRY_VALUE"]) . "</country_of_origin>\n";
                    }


                    if (strlen(TruncateText($arAcc["~PREVIEW_TEXT"])) > 4) {
                        $strTmpOff .=
                            "<description>" .
                            yandex_text2xml(TruncateText(
                                ($arAcc["PREVIEW_TEXT_TYPE"] == "html" ?
                                    strip_tags(preg_replace_callback("'&[^;]*;'", "yandex_replace_special", $arAcc["~PREVIEW_TEXT"])) : preg_replace_callback("'&[^;]*;'", "yandex_replace_special", $arAcc["~PREVIEW_TEXT"])),
                                255), true) .
                            "</description>\n";
                    }

                    if ($arAcc["PROPERTY_CML2_KRAT_VALUE"]) {
                        $strTmpOff .= "<sales_notes>����������� ������ �������� " . round($arAcc["PROPERTY_CML2_KRAT_VALUE"], 3) . " " . $arAcc["PROPERTY_CML2_BASE_UNIT_VALUE"] . "</sales_notes>\n";
                    }

                    $strTmpOff .= $str;

                    if (!empty($arAcc["PROPERTY_GUARANTEE_VALUE"]) && $arAcc["PROPERTY_GUARANTEE_VALUE"] != '')
                        $strTmpOff .= "<manufacturer_warranty>true</manufacturer_warranty>\n";

                    if (!empty($arAcc["PROPERTY_CML2_KRAT_VALUE"])) {
                        $strTmpOff .= "<param name=\"����������� ������ ��������\">" . round($arAcc["PROPERTY_CML2_KRAT_VALUE"], 3) . " " . $arAcc["PROPERTY_CML2_BASE_UNIT_VALUE"] . "</param>\n";
                    }

                    if (!empty($arAcc["PROPERTY_COLOR_VALUE"])) {
                        if (!in_array($arAcc["PROPERTY_COLOR_VALUE"], $stopValues)) {
                            $strTmpOff .= "<param name=\"����\">" . strtolower(str_replace("/", ",", yandex_text2xml($arAcc["PROPERTY_COLOR_VALUE"]))) . "</param>\n";
                        }
                    }

                    if (!empty($arAcc["PROPERTY_MATERIAL_VALUE"])) {
                        if (!is_numeric($arAcc["PROPERTY_MATERIAL_VALUE"]) && !in_array($arAcc["PROPERTY_MATERIAL_VALUE"], $stopValues)) {
                            $strTmpOff .= "<param name=\"��������\">" . strtolower(yandex_text2xml($arAcc["PROPERTY_MATERIAL_VALUE"])) . "</param>\n";
                        }
                    }


                    if (!empty($arAcc["PROPERTY_APPLICATION_VALUE"]) && ($arAcc["PROPERTY_APPLICATION_VALUE"] != "&lt;&gt;")) {
                        $strTmpOff .= "<param name=\"���\">" . strtolower(yandex_text2xml($arAcc["PROPERTY_APPLICATION_VALUE"])) . "</param>\n";
                    }


                    if (!empty($arAcc["PROPERTY_DESTINATION_VALUE"]) && ($arAcc["PROPERTY_DESTINATION_VALUE"] != "&lt;&gt;")) {
                        $strTmpOff .= "<param name=\"����������\">" . strtolower(yandex_text2xml($arAcc["PROPERTY_DESTINATION_VALUE"])) . "</param>\n";
                    }

                    $strTmpOff .= "</offer>\n";
/*
                    if ($minPrice < $tMinPrice) {
                        $hrefArbist = "<a href='" . $arAcc["DETAIL_PAGE_URL"] . "' target='_arbist'>PRICE</a>";
                        $hrefTlock = "<a href='" . $tUrl . "' target='_arbist'>PRICE</a>";
                        echo "OUR ".$hrefArbist." IS " . $minPrice . ", TLOCK ".$hrefTlock." IS " . $tMinPrice . "<br>\n";
                    }
*/
                }
            }
        }

        @fwrite($fp, $treeXML . "\n");
        @fwrite($fp, $deliveryOptions . "\n");

        @fwrite($fp, "<offers>\n");
        @fwrite($fp, $strTmpOff);
        @fwrite($fp, "</offers>\n");

        @fwrite($fp, "</shop>\n");
        @fwrite($fp, "</yml_catalog>\n");

        @fclose($fp);
    }
}

if ($bTmpUserCreated) unset($USER);

$SETUP_FILE_NAME_NEW = "/yml/tl.php";
rename($_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME, $_SERVER["DOCUMENT_ROOT"] . $SETUP_FILE_NAME_NEW);
echo date("d.m.Y_H-i-s") . "<br>";
?>
