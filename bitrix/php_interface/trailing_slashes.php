<?php

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    return;
}

if ($_SERVER["REQUEST_URI"] != "/" 
    && !preg_match("#\.(php|html|htm)$#", $_SERVER["REQUEST_URI"]) 
    && preg_match("#[^/]$#", $_SERVER["REQUEST_URI"]) 
    && !preg_match("#^/bitrix#", $_SERVER["REQUEST_URI"])
    && !preg_match("#^/rpic#", $_SERVER["REQUEST_URI"])
    && !preg_match("#\?#", $_SERVER["REQUEST_URI"])
) {
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $_SERVER["REQUEST_URI"] . "/");
    exit;
}

if (preg_match('#index\.php$#',$_SERVER["REQUEST_URI"]) 
    && !preg_match("#^/bitrix#",$_SERVER["REQUEST_URI"])
    && !preg_match("#^/rpic#", $_SERVER["REQUEST_URI"])) {
    $_SERVER["REQUEST_URI"] = preg_replace("#index.php$#", "", $_SERVER["REQUEST_URI"]);
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: '.$_SERVER["REQUEST_URI"]);
    exit;
}

?>
