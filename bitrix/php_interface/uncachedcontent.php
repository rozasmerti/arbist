<?php

$GLOBALS['arUncachedContents'] = array();
$GLOBALS['arUncachedContentCallbacks'] = array();
$GLOBALS['uncachedContentCaptureKey'] = null;

function ShowUncachedContent($key, $param = null)
{
    if (empty($key)) return false;

    //$GLOBALS['arUncachedContents'][$key] = array();
    print '<!-- uncached' . md5($key) . ($param !== null ? ':' . (is_array($param) ? serialize($param) : $param) : '') . ' -->';

    return true;
}

function SetUncachedContent($key, $content)
{
    $GLOBALS['arUncachedContents'][$key][] = $content;

    return true;
}

function StartCaptureUncachedContent($key)
{
    ob_start();
    $GLOBALS['uncachedContentCaptureKey'] = $key;
    //array_unshift($GLOBALS['arUncachedContentStack'], $key);
}

function EndCaptureUncachedContent()
{
    $content = ob_get_clean();
    //$key = array_shift($GLOBALS['arUncachedContentStack']);
    SetUncachedContent($GLOBALS['uncachedContentCaptureKey'], $content);

    return true;
}

function RegisterUncachedContentCallback($key, $callback)
{
    $GLOBALS['arUncachedContentCallbacks'][$key][] = $callback;
    return true;
}

function InsertUncachedContent($content)
{
    $callbackResults = array();

    foreach ($GLOBALS['arUncachedContentCallbacks'] as $key => $callbacks) {
        preg_match_all('#<!-- uncached' . md5($key) . ':(.*) -->#siuU', $content, $matches);
		if (count($matches[1])) {
			foreach ($callbacks as $callback) {
				$result = call_user_func_array($callback, array($key, $matches[1]));

				if (is_array($result)) {
					$callbackResults[$key][] = $result;
				} else {
					trigger_error('Result of callback must by array', E_USER_WARNING);
				}
			}
		}
    }

    $replaces = array();

    foreach ($callbackResults as $key => $callbacksData) {
        foreach ($callbacksData as $callbackResult) {
            foreach ($callbackResult as $param => $value) {
                $replaces['<!-- uncached' . md5($key) . ':' . $param . ' -->'] .= $value;
            }
        }
    }

    $content = str_replace(array_keys($replaces), array_values($replaces), $content);

    foreach ($GLOBALS['arUncachedContents'] as $key => $data) {
        $str = '';

        for ($i = 0; $i < count($data); $i++) {
            $str .= $data[$i];
        }


        $content = str_replace('<!-- uncached' . md5($key) . ' -->', $str, $content);
    }


    return true;
}

AddEventHandler("main", "OnEndBufferContent", "InsertUncachedContent"); 

//RegisterUncachedContentCallback('buy-compare', 'toBasketItemCompareCallback');
//RegisterUncachedContentCallback('in-compare', 'inCompare');
//RegisterUncachedContentCallback('price-block', 'printPriceBlock');

function printPriceBlock($key, $params)
{
	$result = array();
	foreach ($params as $param)
	{
        $data = unserialize($param);
		$ELEMENT_ID = intval($data['id']);
		$price = getBestPrice($ELEMENT_ID);
		if($price["PRICE"] > 0)
		{
			$result[$param] = str_replace("#PRICE#",number_format($price["PRICE"],0,"."," "),$data["priceBlock"]);
		}
		else
		{
			$result[$param] = "";
		}
	}
	return $result;
}

function toBasketItemCompareCallback($key, $params)
{
	CModule::IncludeModule('sale');
    CModule::IncludeModule('catalog');
    $arBasketList = GetBasketListSimple();
	
	$basketComplects = array();
    $basketList = array();

    while($item = $arBasketList->fetch())
	{
		if($item["CAN_BUY"] == "Y" && $item["DELAY"] == "N")
		{
			$bPropRs = CSaleBasket::GetPropsList(array("SORT" => "ASC","NAME" => "ASC"),array("BASKET_ID" => $item["ID"]));
			while($bp = $bPropRs->fetch())
			{
				if($bp["CODE"] == "COMPLECT_CODE")
				{
					if(!in_array($bp["VALUE"],$basketComplects))$basketComplects[] = $bp["VALUE"];
				}
			}
			$basketList[$item['PRODUCT_ID']] = array(
				"QUANTITY" => $item["QUANTITY"],
			);
		}
    }

    $result = array();

	foreach ($params as $param) {
        $data = unserialize($param);
		$ELEMENT_ID = intval($data['id']);
		$product = CCatalogProduct::GetByID($ELEMENT_ID);
		$element = CIBlockElement::GetByID($ELEMENT_ID)->fetch();
		$q = intval($product["QUANTITY"]);
		
		$price = getBestPrice($ELEMENT_ID);
		if($price["PRICE"] > 0)
		{
			$data['priceBlock'] = str_replace("#PRICE_F#",$price["PRICE_F"],$data['priceBlock']);
			$data['buy'] = str_replace("#PRICE_BLOCK#",$data['priceBlock'],$data['buy']);
			$data['inBasket'] = str_replace("#PRICE_BLOCK#",$data['priceBlock'],$data['inBasket']);
			$data['notBuy'] = str_replace("#PRICE_BLOCK#",$data['priceBlock'],$data['notBuy']);
		}
		else
		{
			$data['buy'] = str_replace("#PRICE_BLOCK#","",$data['buy']);
			$data['inBasket'] = str_replace("#PRICE_BLOCK#","",$data['inBasket']);
			$data['notBuy'] = str_replace("#PRICE_BLOCK#","",$data['notBuy']);
		}
		
		if($q > 0 && is_array($element))
		{
			
			$inBasket = false;
			switch($element["IBLOCK_CODE"])
			{
				case "sewing_head":
					$engine = getEMinPrice($ELEMENT_ID, $element["IBLOCK_ID"], "ENGINE");
					$table = getEMinPrice($ELEMENT_ID, $element["IBLOCK_ID"], "TABLE");
					$complectCode = "complect_h".$ELEMENT_ID."_e".intval($engine["ELEMENT_ID"])."_t".intval($table["ELEMENT_ID"]);
					$inBasket = in_array($complectCode,$basketComplects);
					break;
				default:
					$inBasket = isset($basketList[$ELEMENT_ID]);
					break;
			}
			
			if($inBasket)$result[$param] = $data['inBasket'];
			else $result[$param] = $data['buy'];
			
		}
		else
		{
			$result[$param] = $data['notBuy'];
		}
    }

    return $result;
}

function inCompare($key, $params)
{
	$result = array();
	foreach ($params as $param)
	{
		$data = unserialize($param);
		$id = $data["id"];
		$iid = $data["iblock_id"];
		$add2compare = $data["add2compare"];
		$inCompare = $data["inCompare"];
		
		
		if(isset($_SESSION["CATALOG_COMPARE_LIST"][$iid]["ITEMS"][$id]))
		{
			$result[$param] = $data['inCompare'];
		}
		else
		{
			$result[$param] = $data['add2compare'];
		}
	}
	
	return $result;
}


function toBasketItemCallback($key, $params)
{
    CModule::IncludeModule('sale');
    CModule::IncludeModule('iblock');
    $arBasketList = GetBasketList();
    $basketList = array();

	$basketComplects = array();
	
    foreach ($arBasketList as $item) {
		$bPropRs = CSaleBasket::GetPropsList(array("SORT" => "ASC","NAME" => "ASC"),array("BASKET_ID" => $item["ID"]));
		while($bp = $bPropRs->fetch())
		{
			if($bp["CODE"] == "COMPLECT_CODE")
			{
				if(!in_array($bp["VALUE"],$basketComplects))$basketComplects[] = $bp["VALUE"];
			}
		}
        $basketList[$item['PRODUCT_ID']] = $item['QUANTITY'];
    }

    $result = array();

    foreach ($params as $param) {
        $data = unserialize($param);
		
		$ELEMENT_ID = $data['id'];
		if($product = CIBlockElement::GetByID($ELEMENT_ID)->fetch())
		{
			$inBasket = false;
			switch($product["IBLOCK_CODE"])
			{
				case "sewing_head":
					$inBasket = false;
					break;
				default:
					$inBasket = isset($basketList[$ELEMENT_ID]);
					break;
			}
		
			if($inBasket) $result[$param] = $data['inBasket'];
			else $result[$param] = $data['buy'];
		}
    }

    return $result;
}

?>
