<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockPropertyIBlock", "GetUserGroupsDescription"));

class CIBlockPropertyIBlock
{
 function GetUserGroupsDescription(){
		return array(
			"PROPERTY_TYPE"		=>"S",
			"USER_TYPE"		=>"Iblocks",
			"DESCRIPTION"		=>"�������� � ���������",
			"GetPublicViewHTML"	=>array("CIBlockPropertyIBlock","GetGroupsPublicViewHTML"),
			"GetAdminListViewHTML"	=>array("CIBlockPropertyIBlock","GetGroupsAdminListViewHTML"),
			"GetPropertyFieldHtml"	=>array("CIBlockPropertyIBlock","GetGroupsPropertyFieldHtml"),
			"ConvertToDB"		=>array("CIBlockPropertyIBlock","GroupsConvertToDB"),
			"ConvertFromDB"		=>array("CIBlockPropertyIBlock","GroupsConvertFromDB"),
		);
	}

	// ����� � ��������� ����� (DISPLAY_VALUE)
	function GetGroupsPublicViewHTML($arProperty, $value, $strHTMLControlName){
		if(is_array($value["VALUE"])){
			$rsGroups = CGroup::GetList(($by="c_sort"), ($order="asc"), Array());
   $str = '';
   while($arGroup = $rsGroups->Fetch())
   {
				if(in_array($arGroup["ID"], $value["VALUE"]))
     $str .= '['.$arGroup["ID"].'] '.$arGroup["NAME"].'<br>';
   }
   return $str;
		}
		return '';
	}

	// ����� ������ � �������
	function GetGroupsAdminListViewHTML($arProperty, $value, $strHTMLControlName){
		if(is_array($value["VALUE"])){
			$rsGroups = CGroup::GetList(($by="c_sort"), ($order="asc"), Array());
   $str = '';
   while($arGroup = $rsGroups->Fetch())
   {
				if(in_array($arGroup["ID"], $value["VALUE"]))
     $str .= '<a href="/bitrix/admin/group_edit.php?ID='.$arGroup["ID"].'&lang=ru">['.$arGroup["ID"].'] '.$arGroup["NAME"].'</a><br>';
   }
   return $str;
		}
		return '';
	}

	// �������������� � �������
	function GetGroupsPropertyFieldHtml($arProperty, $value, $strHTMLControlName){
		$strHTMLControlName["VALUE"] = htmlspecialcharsEx($strHTMLControlName["VALUE"]);
  ob_start();
  ?>
  <select name="<?=$strHTMLControlName["VALUE"]?>" >
    <option value="" <?=("" == $value['VALUE']) ? 'selected' : ''?>>�� �������</option>
   <?
   $rsIBlocks = CIBlock::GetList(array('iblock_type' => 'asc'), array('ACTIVE'=>'Y'));
   $type = '';
   while($arIBlock = $rsIBlocks->Fetch()){
    if($type != $arIBlock['IBLOCK_TYPE_ID']){
      $arIBType = CIBlockType::GetByIDLang($arIBlock['IBLOCK_TYPE_ID'], LANG);
      ?><optgroup label="<?=$arIBType['NAME']?>"><?
    }
   ?><option value="<?=$arIBlock["CODE"]?>" <?=($arIBlock["CODE"] == $value['VALUE']) ? 'selected' : ''?>><?=$arIBlock["NAME"]?></option><?
    $type = $arIBlock['IBLOCK_TYPE_ID'];
   };
   ?>
  </select>
  <?
  $return = ob_get_contents();
		ob_end_clean();
		return  $return;
	}

	// �������������� ����� �����������
	function GroupsConvertToDB($arProperty, $value){
  if(is_array($value["VALUE"])){
			$value["VALUE"] = $value["VALUE"];
		}
		return $value;
	}

	// �������������� ����� ������
	function GroupsConvertFromDB($arProperty, $value){
		$value["VALUE"] = $value["VALUE"];
		return $value;
	}
}
?>
