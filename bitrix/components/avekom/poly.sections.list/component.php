<?php

/*
 * F.A.Q.
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arNavParams = array(
	"nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
	/*"iNumPage " => 1,*/
	"bShowAll" => "N",
);
if(!isset($_REQUEST["PAGEN_1"]) && !isset($_REQUEST["PAGEN_1"]) && !isset($_REQUEST["PAGEN_3"]))
{
	$arNavParams["iNumPage"] = 1;
}
$arNavigation = CDBResult::GetNavParams($arNavParams);

if($this->StartResultCache(false, array( ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation)))
{
	CModule::IncludeModule('iblock');
	$arResult = array();

	/*
	 * InfoBlocks
	 */

	$arResult["IBLOCK_INFO"] = array();
	$res = CIBlock::GetList(array("ID"=>"ASC"),array("ID"=>$arParams["IBLOCK_ID"]));
	while($ar = $res->Fetch())
	{
		$arResult["IBLOCK_INFO"][$ar["ID"]]["NAME"]=$ar["NAME"];
		$arResult["IBLOCK_INFO"][$ar["ID"]]["CODE"]=$ar["CODE"];
	}

	/*
	 * Sections
	 */

	$filter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
	);
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"CODE",
        "PICTURE"
	);
	$arSort = array(
		"SORT"=>"ASC"
	);

	/*
	 * EXECUTE SECTIONS
	 */
	$res = CIBlockSection::GetList($arSort, $filter, false, $arSelect, $arNavParams);
	while ($ar = $res->Fetch())
	{
		if((int)$ar["ID"]>0)
		{
			//Get URL
			$arResult["SECTIONS_LIST"][$ar["ID"]] = $ar;
			$arResult["SECTIONS_LIST"][$ar["ID"]]["DETAIL_URL"] = "/catalog/".$arResult["IBLOCK_INFO"][$ar["IBLOCK_ID"]]["CODE"]."/collection/".$ar["CODE"]."/";
			$arResult["SECTIONS_LIST"][$ar["ID"]]["ELEMENT_PROPERTY"] = array();

            if(!empty($ar['PICTURE'])){
                $arResult["SECTIONS_LIST"][$ar["ID"]]['PICTURE'] = CFile::ResizeImageGet(
                    $ar['PICTURE'],
                    array("width" => 185, "height" => 152),
                    BX_RESIZE_IMAGE_EXACT,
                    true
                );
            }

			$arResult["SECTIONS_IDS"][] = $ar["ID"];
		}
	}

	$arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponentObject, "PAGER_TITLE", $arParams["PAGER_TEMPLATE"], "Y");
	$arResult["NAV_STRING_TOP"] = $res->GetPageNavStringEx($navComponentObject, "PAGER_TITLE", $arParams["PAGER_TEMPLATE_TOP"], "Y");
	$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
	$arResult["NAV_RESULT"] = $res;

	/*
	 * Elements
	 */
	$filter_elements = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arResult["SECTIONS_IDS"]
	);
	$arSort_elements = array(
		"SORT" => "ASC"
	);
	$arSelect_elements = $arParams["FIELD_ELEMENT_CODE"];
	$arSelect_elements[] = "ID";
	$arSelect_elements[] = "IBLOCK_ID";
	$arSelect_elements[] = "IBLOCK_SECTION_ID";
	$arSelect_elements[] = "PROPERTY_item_pic";
	foreach ($arParams["PROPERTY_ELEMENT_CODE"] as $prop_code)
	{
		$arSelect_elements[] = "PROPERTY_".$prop_code;
	}
	/*
	 * EXECUTE ELEMENTS
	 */
	$res = CIBlockElement::GetList($arSort_elements, $filter_elements, false, false, $arSelect_elements);
	while ($ar = $res->Fetch())
	{
		if(empty($arResult["SECTIONS_LIST"][$ar["IBLOCK_SECTION_ID"]]["ELEMENT_PROPERTY"]))
        {
            $arResult["SECTIONS_LIST"][$ar["IBLOCK_SECTION_ID"]]["ELEMENT_PROPERTY"] = $ar;
        }

        if(empty($arResult["SECTIONS_LIST"][$ar["IBLOCK_SECTION_ID"]]['PICTURE']))
        {
            if(!empty($ar["PROPERTY_ITEM_PIC_VALUE"])) {
                $file_path = $_SERVER['DOCUMENT_ROOT'].'/upload/products_foto/'.$ar["PROPERTY_ITEM_PIC_VALUE"];
                $imgPath = '/upload/resize_cache/products_foto/185_152_'.$ar["PROPERTY_ITEM_PIC_VALUE"];
                $destinationFile = $_SERVER["DOCUMENT_ROOT"] . '/upload/resize_cache/products_foto/185_152_'.$ar["PROPERTY_ITEM_PIC_VALUE"];

                if(is_file($file_path)) {
                    $resized = CFile::ResizeImageFile(
                        $file_path,
                        $destinationFile,
                        array('width'=>185, 'height'=>152),
                        BX_RESIZE_IMAGE_EXACT
                    );

                    if($resized){
                        $arResult["SECTIONS_LIST"][$ar["IBLOCK_SECTION_ID"]]['PICTURE']['src'] = $imgPath;
                    }
                }
            }
        }

	}

	$this->IncludeComponentTemplate();
}