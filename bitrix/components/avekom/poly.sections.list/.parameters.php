<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => array(
			"NAME" => "�������������� ����",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"ADDITIONAL_VALUES" => "Y",
		),
		"PAGER_TEMPLATE" => array(
			"NAME" => "������ ������������� ������� (������)",
			"TYPE" => "STRING"
		),
		"PAGER_TEMPLATE_TOP" => array(
			"NAME" => "������ ������������� ������� (�������)",
			"TYPE" => "STRING"
		),
		"PAGE_ELEMENT_COUNT" => array(
			"NAME" => "��������� �� ��������",
			"TYPE" => "INT"
		),
		"PROPERTY_ELEMENT_CODE" => array(
			"NAME" => "��� ������� ���������, ��� ������",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"ADDITIONAL_VALUES" => "Y",
		),
		"FIELDS_ELEMENT_CODE" => array(
			"NAME" => "��� ���� ���������, ��� ������",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"ADDITIONAL_VALUES" => "Y",
		),
	),
);
?>