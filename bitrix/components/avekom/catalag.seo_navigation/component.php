<?php

/*
 * F.A.Q.
 * 1) ���������� = ������� ����� � ���������
 * 2) ����� ������ ��������� ���������� ��� 1
 * 3) ����������� ������ �������� ��� ������ ������? ������ � ���������� �� ������ ���� �� ������ ����������
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule('iblock');
$arResult = array();


//������ �������� �� ��� (�.�. ���� ���������� � ������ ��� ���������� ����������?)
$arResult["POLYBLOCK"] = false;
if(is_array($arParams["IBLOCK_ID"]) && count($arParams["IBLOCK_ID"])>1)
	$arResult["POLYBLOCK"] = true;

	//���� ��������, ����� ������ �� ��������� => ���
	if($arResult["POLYBLOCK"])
	{
		$arResult["IBLOCK_INFO"] = array();
		$res = CIBlock::GetList(array("ID"=>"ASC"),array("ID"=>$arParams["IBLOCK_ID"]));
		while($ar = $res->Fetch())
		{
			$arResult["IBLOCK_INFO"][$ar["ID"]]["NAME"]=$ar["NAME"];
			$arResult["IBLOCK_INFO"][$ar["ID"]]["CODE"]=$ar["CODE"];
		}
	}
	/*echo "<pre>";
	var_dump($arResult[72]);
	echo "</pre>";*/
	if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
	{
		$arrFilter = array();
	}
	else
	{
		global $$arParams["FILTER_NAME"];
		$arrFilter = ${$arParams["FILTER_NAME"]};
		if(!is_array($arrFilter))
			$arrFilter = array();
	}

	$defaultFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y");
	$FILTER = array_merge($arrFilter, $defaultFilter);
	$SELECT = array(
		"ID",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID"
	);
	//������� �������� �� ��������
$arResult["SPR"] = array();
foreach($arParams["PROPERTY_CODE"] as $CODE)
{
	$SELECT[] = "PROPERTY_".$CODE;
	//�������� � ���� �� ����� ���� ����������
	if(defined("SPR_".strtoupper($CODE)))
	{
		//������ ��� ����������, ���� ��� ��� ����� vendor ��� ���� ����� >_<
		//�������� ���������� � $arResult'� ����� � ������� ������ ��������������
		$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>constant("SPR_".strtoupper($CODE))), false, false, array("IBLOCK_ID","ID","NAME","CODE"));
		while ($ar = $res->Fetch())
		{
			$arResult["SPR"][$CODE][$ar["ID"]] = array(
				"NAME" => $ar["NAME"],
				"CODE" => $ar["CODE"],
				"ID" => $ar["ID"]
			);
		}
	}
}

	//EXECUTE!!!!

	$res = CIBlockElement::GetList(array(), $FILTER, false, false, $SELECT);
	$Sections = array();
	$arResult["IN_IBLOCK"] = array(); //� ������ � ����������� ����, �������� ������ �� ��������� � ������� ����
	while ($ar = $res->GetNextElement())
	{
		$arFields = $ar->GetFields();
		$Sections[] = $arFields["IBLOCK_SECTION_ID"];
		$arProps = $ar->GetProperties();

		foreach($arProps as $codeProp => $arProp)
		{
			/*if($arProps[$codeProp]["VALUE"] == "���������� �������������� ������")
			{
				echo "<pre>";
				var_dump($arFields);
				echo "</pre>";
			}*/
			if(!in_array($codeProp,$arParams["PROPERTY_CODE"]) || trim($arProps[$codeProp]["VALUE"])=="" || trim($arProps[$codeProp."_text"]["VALUE"])=="&lt;&gt;" )
				continue;

			if(!in_array($arProps[$codeProp]["VALUE"],$arResult[$codeProp]["VALUES"]))
			{
				$arResult[$codeProp]["NAME"] = $arProps[$codeProp]["NAME"];
				$arResult[$codeProp]["VALUES"][] = $arProps[$codeProp]["VALUE"];
			}
			//��� ������������ ����
			if($arResult["POLYBLOCK"])
			{
				if(!in_array($arFields["IBLOCK_ID"],$arResult["IN_IBLOCK"][$codeProp][$arProps[$codeProp]["VALUE"]]))
					$arResult["IN_IBLOCK"][$codeProp][$arProps[$codeProp]["VALUE"]][] = $arFields["IBLOCK_ID"];
			}
		}
	}

	//���� ���� � ������ ������� ���������, �� ��� �������� ����� ������� ��������� ������� ���������
	//������ ��������� ������������� ���������
	if(in_array("collection",$arParams["PROPERTY_CODE"]))
	{
		$Sections = array_unique($Sections);
		$res = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ID"=>$Sections), false, array("ID","CODE","NAME"));
		while ($ar = $res->Fetch())
		{
			$arResult["SPR"]["collection"][$ar["ID"]]["NAME"] = $ar["NAME"];
			$arResult["SPR"]["collection"][$ar["ID"]]["CODE"] = $ar["CODE"];
			$arResult["SPR"]["collection"][$ar["ID"]]["ID"] = $ar["ID"];
			$arResult["collection"]["NAME"] = "���������";
			$arResult["collection"]["VALUES"][$ar["CODE"]] = $ar["ID"];
		}
	}

//�������� ������ xml_id ��������������, ������ ������� ���������� ����� �� ���� ��������������
$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>VENDORS_IBLOCK_ID, "XML_ID"=>$arResult["vendor"]["VALUES"]), false, false, array("ID", "CODE", "NAME","XML_ID"));

while ($ar = $res->Fetch())
{
	$arResult["SPR"]["vendor"][$ar["XML_ID"]]["NAME"] = $ar["NAME"];
	$arResult["SPR"]["vendor"][$ar["XML_ID"]]["CODE"] = $ar["CODE"];
	$arResult["SPR"]["vendor"][$ar["XML_ID"]]["ID"] = $ar["ID"];
	$arResult["IN_IBLOCK"]["vendor"][$ar["NAME"]] = $arResult["IN_IBLOCK"]["vendor"][$ar["XML_ID"]];
	/*$arResult["vendor"]["VALUES"][$ar["CODE"]] = $ar["NAME"];*/
}

//���� ������� ������ �� ��������, ������� ����� ��� ������
//��������!!! ���������� ������� = ������� � ��������� ����������
$arResult["SHOWED_PARAMS"] = array();
foreach ($arParams["PROPERTY_CODE"] as $code)
{
	$arResult["SHOWED_PARAMS"][$code] = $arResult[$code];

}


$navChain = array();
$dir = $GLOBALS['APPLICATION']->GetCurDir();
preg_match('#\/([^\/]+)\/([^\/]+)\/$#i', $dir, $matches);
if(!empty($matches[1])){
	$arResult['CURRENT']['PARENT_VALUE'] = $matches[1];
	$arResult['CURRENT']['VALUE'] = $matches[2];

    foreach ($arResult["SPR"] as $propCode => $valuesList) {
        foreach ($valuesList as $value) {
            if (
                $value['CODE'] == $arResult['CURRENT']['VALUE']
            ) {
                $arResult['CURRENT']['PROP'] = $propCode;
                $arResult['CURRENT']['VALUE_NAME'] = $value['NAME'];
            }

            if (
                $value['CODE'] == $arResult['CURRENT']['PARENT_VALUE']
            ) {
                $arResult['CURRENT']['PARENT_PROP'] = $propCode;
                $arResult['CURRENT']['PARENT_VALUE_NAME'] = $value['NAME'];
            }
        }
    }


    //������������ ����� � ������� ���������
    if(!empty($arResult["SHOWED_PARAMS"][$arResult['CURRENT']['PARENT_PROP']])) {
        $propName = $arResult["SHOWED_PARAMS"][$arResult['CURRENT']['PARENT_PROP']]["NAME"];

        if (($pos = strpos($propName, " (������)")) > 1) {
            $propName = substr($propName, 0, $pos);
        }

        if(strlen($propName) > 0 && strlen($arResult['CURRENT']['PARENT_VALUE_NAME']) > 0){
            $navChain[] = array(
                'NAME' => $propName . ' � ' . $arResult['CURRENT']['PARENT_VALUE_NAME'],
                'URL' => preg_replace('#[^\/]+\/$#i', '', $dir)
            );
        }
    }


	//������������� ���������
	if(!empty($arResult["SHOWED_PARAMS"][$arResult['CURRENT']['PROP']])) {
		$propName = $arResult["SHOWED_PARAMS"][$arResult['CURRENT']['PROP']]["NAME"];

		if (($pos = strpos($propName, " (������)")) > 1) {
			$propName = substr($propName, 0, $pos);
		}

		if(strlen($propName) > 0 && strlen($arResult['CURRENT']['VALUE_NAME']) > 0){
            $APPLICATION->SetPageProperty("title", $propName . ' � ' . $arResult['CURRENT']['VALUE_NAME']);
            $APPLICATION->SetPageProperty("h1", $propName . ' � ' . $arResult['CURRENT']['VALUE_NAME']);
            $navChain[] = array(
                'NAME' => $propName . ' � ' . $arResult['CURRENT']['VALUE_NAME'],
                'URL' => false
            );
		}
	}

}

foreach ($arResult["SHOWED_PARAMS"] as $code => $val){
    if(!in_array($code, $arParams['SHOW_PROPERTIES_CODE'])){
        unset($arResult["SHOWED_PARAMS"][$code]);
    }
}

if($arParams["ADD_IBLOCK_CHAIN"] == 'Y' && intval($arParams['IBLOCK_ID']) > 0)
{
    $rsIBlock = \CIBlock::GetByID(intval($arParams['IBLOCK_ID']));
    $rsIBlock = new CIBlockResult($rsIBlock);
    $arIBlock = $rsIBlock->GetNext();
    if(!empty($arIBlock)){
        $arIBlock['LIST_PAGE_URL'] = str_replace('_revizionnye', '', $arIBlock['LIST_PAGE_URL']);

        $APPLICATION->AddChainItem($arIBlock["NAME"], $arIBlock["LIST_PAGE_URL"]);
    }
}

foreach ($navChain as $item){
    $APPLICATION->AddChainItem($item["NAME"], $item["URL"]);
}

$this->IncludeComponentTemplate();
