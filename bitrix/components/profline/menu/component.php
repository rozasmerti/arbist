<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["DISPLAY_PANEL"] = $arParams["DISPLAY_PANEL"]=="Y";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default

$arResult["SECTIONS"]=array();

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, $USER->GetGroups()))
{

	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

    $IBLOCK_CODE = $_REQUEST["IBLOCK_CODE"];
    $BRAND_ID = intval($_REQUEST["BRAND_ID"]);


    $IBLOCK_TYPE   = "catalog";                     // тип инфо-блока
    $SORT          = array("LEFT_MARGIN" => "ASC"); // массив сортировки для разделов
    $CACHE_TIME    = 0;        // время кэширования 
    
    $IBLOCK_URL = "/catalog/#IBLOCK_CODE#/"; //
    $BRAND_URL = "/catalog/#IBLOCK_CODE#/#BRAND_ID#/"; //
    $ELEMENT_URL = "/catalog/#IBLOCK_CODE#/#SECTION_ID#/#ELEMENT_ID#/"; //

	
    $aMenuLinks = array();
    
    $rIBlock = CIBlock::GetList(
        array("SORT" => "ASC"), 
        array(
            "TYPE" => "catalog",
            "SITE_ID" => SITE_ID, 
            "ACTIVE" => "Y", 
        )
    );
    
	global $APPLICATION;
	$dir = $APPLICATION->GetCurDir();
	$arDirs = explode("/",$dir);
	$infoDir = "/".$arDirs[1]."/".$arDirs[2]."/info/";
	//CPL::pr($arDirs);
    

    //CPL::pr($rIBlock);
    $aMenuLinks = array();
    while ( $arIBlock = $rIBlock->GetNext() ) {
    	$arBrand = array();
		$properties = CIBlockProperty::GetPropertyEnum("BRAND", Array("sort"=>"asc", "VALUE"=>"asc"), Array("IBLOCK_ID"=>$arIBlock["ID"]));

		while($arProperty = $properties->GetNext())
		{
						if($_SERVER["REMOTE_ADDR"]=="77.73.24.175")
		{
			//echo $arIBlock["NAME"];
			//echo "<pre>"; print_r($arProperty); echo "</pre>";
		}
			$arBrand[] = array(
				"ID" => $ar_enum_list["ID"],
	            "URL" => str_replace(array("#IBLOCK_CODE#", "#BRAND_ID#"), array($arIBlock["CODE"], $arProperty["ID"]), $BRAND_URL),
				"NAME" => $arProperty["VALUE"],
	            "DEPTH_LEVEL" => 2,
	            "SELECTED" => $arProperty["ID"]==$BRAND_ID, 
			);
		}
		
		$arFilter = array("IBLOCK_ID" => 7, "PROPERTY_catalog" =>$arIBlock["ID"],"ACTIVE" => "Y");
		//CPL::pr($arFilter);
		$arSelect = array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL");
		
	
		
		
		$rsElements = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
		while($obElement = $rsElements->GetNextElement())
		{
		
			
			$arItem = $obElement->GetFields();
			//CPL::pr($arItem);
			$arBrand[] = array(
				"ID" => $arItem["ID"],
	            "URL" => $infoDir.$arItem["ID"]."/",
				"NAME" => $arItem["NAME"],
	            "DEPTH_LEVEL" => 2,
	            "SELECTED" => $arItem["ID"]==$_REQUEST["ELEMENT_ID"], 
			);
			//CPL::pr($arItem);
		}
		//CPL::pr($arIBlock);
		//CPL::pr($arIBlock["CODE"]);
		//CPL::pr($IBLOCK_CODE);
		  
        $aMenuLinks[] = array( 
            "NAME" => $arIBlock["NAME"], 
            "URL" => str_replace(array("#IBLOCK_CODE#"), array($arIBlock["CODE"]), $IBLOCK_URL), 
            "DEPTH_LEVEL" => 1,
            "SELECTED" => $arIBlock["CODE"]==$IBLOCK_CODE, 
            "FROM_IBLOCK" => true, 
            "IS_PARENT" => true,
            "BRANDS" => $arBrand
        );
   }
   $arResult["SECTIONS"] = $aMenuLinks;
   $this->IncludeComponentTemplate();

}
?>
