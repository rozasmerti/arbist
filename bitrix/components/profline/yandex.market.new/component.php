<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog"))die();


//$file = fopen($_SERVER['DOCUMENT_ROOT']. "/yandex.txt","w+");

/*************************************************************************
	Processing of received parameters
*************************************************************************/

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

if(!is_array($arParams["PROPERTY_CODE"]))
	$arParams["PROPERTY_CODE"] = array();

foreach($arParams["PROPERTY_CODE"] as $key=>$value)
{
	if($value==="")
		unset($arParams["PROPERTY_CODE"][$key]);
	else
		$arProperty[]="PROPERTY_". trim($value);
}



$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);

$arParams["COMPANY"] = trim($arParams["COMPANY"]);

if(!is_array($arParams["IBLOCK_ID_IN"]))
	$arParams["IBLOCK_ID_IN"] = array();
foreach($arParams["IBLOCK_ID_IN"] as $k=>$v)
	if($v==="")
		unset($arParams["IBLOCK_ID_IN"][$k]);


if(!is_array($arParams["IBLOCK_ID_EX"]))
	$arParams["IBLOCK_ID_EX"] = array();
foreach($arParams["IBLOCK_ID_EX"] as $k=>$v)
	if($v==="")
		unset($arParams["IBLOCK_ID_EX"][$k]);


if(strlen($arParams["ELEMENT_SORT_FIELD"])<=0)
	$arParams["ELEMENT_SORT_FIELD"]="sort";
if($arParams["ELEMENT_SORT_ORDER"]!="desc")
	 $arParams["ELEMENT_SORT_ORDER"]="asc";

if(strlen($arParams["FILTER_NAME"])<=0 || !ereg("^[A-Za-z_][A-Za-z01-9_]*$", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global $$arParams["FILTER_NAME"];
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);
$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);



if(!is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();
$arParams["USE_PRICE_COUNT"] = $arParams["USE_PRICE_COUNT"]=="Y";
$arParams["SHOW_PRICE_COUNT"] = intval($arParams["SHOW_PRICE_COUNT"]);
if($arParams["SHOW_PRICE_COUNT"]<=0)
	$arParams["SHOW_PRICE_COUNT"]=1;



$arParams["CACHE_FILTER"]=$arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;


$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";



function yandex_text2xml($text, $bHSC = false) // �������� �������, ������� �� �������� ������
{
	if ($bHSC)
		$text = htmlspecialchars($text);
	$text = ereg_replace("[\x1-\x8\xB-\xC\xE-\x1F]", "", $text);
	$text = ereg_replace("'", "&apos;", $text);
	$text = ereg_replace("\r\n", "", $text);
	$text = ereg_replace("  ", " ", $text);
	$text = ereg_replace("  ", " ", $text);
	$text = ereg_replace("  ", " ", $text);
	$text = ereg_replace("  ", " ", $text);
	return $text; 
}




$bDesignMode = $GLOBALS["APPLICATION"]->GetShowIncludeAreas() && is_object($GLOBALS["USER"]) && $GLOBALS["USER"]->IsAdmin();

if(!$bDesignMode)
{
	$APPLICATION->RestartBuffer();
//	header("Content-Type: text/xml; charset=".LANG_CHARSET);
	header("Content-Type: text/xml; charset=".SITE_CHARSET);

	header("Pragma: no-cache");
}


/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array($arrFilter, $USER->GetGroups())))
{


	$arResult["DATE"]=Date("Y-m-d H:i");
	$arResult["COMPANY"]=$arParams["COMPANY"];
	$arResult["NAME"]=htmlspecialchars(COption::GetOptionString("main", "site_name", ""));
	$arResult["URL"]='http://'. htmlspecialchars(COption::GetOptionString("main", "server_name", ""));


	// ������
	$db_acc = CCurrency::GetList(($by="sort"), ($order="asc"));
	$arCurrencyAllowed = array('RUR', 'RUB', 'USD', 'EUR', 'UAH');
	while ($arAcc = $db_acc->Fetch())
		if (in_array($arAcc['CURRENCY'], $arCurrencyAllowed))
			$arResult["CURRENCIES"][]=Array("ID" => $arAcc["CURRENCY"], "RATE" => CCurrencyRates::ConvertCurrency(1, $arAcc["CURRENCY"], "RUR"));



	//This function returns array with prices description and access rights
	//in case catalog module n/a prices get values from element properties
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);


	// list of the element fields that will be used in selection
	$arSelect = array(
		"ID",
		"NAME",
//		"CODE",
//		"DATE_CREATE",
//		"ACTIVE_FROM",
//		"CREATED_BY",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DETAIL_PICTURE",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"PREVIEW_PICTURE",
//		"PROPERTY_YA",
	);

	// ��������� �������
	if(is_array($arProperty))
		$arSelect=array_merge($arProperty, $arSelect);


	$arFilter = array(
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		"CHECK_PERMISSIONS" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
	);

//	if(isset($arParams["SECTION_ID"]))
//		$arFilter["SECTION_ID"] = $arResult["ID"];

	//PRICES
	if(!$arParams["USE_PRICE_COUNT"])
	{
		foreach($arResult["PRICES"] as $key => $value)
		{
			$arSelect[] = $value["SELECT"];
			$arFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
		}
	}
	$arSort = array(
		$arParams["ELEMENT_SORT_FIELD"] => $arParams["ELEMENT_SORT_ORDER"],
		"ID" => "DESC",
	);
//CPL::pr($arFilter);
//exit;
/*
print_r($arFilter);
print_r($arrFilter);
print_r($arSort);
print_r($arSelect);
die();
*/


//--------------------------------------
//�������� ������� ��� �������
/*
$arIblocks = array();
$resIb = CIBlock::GetList(
	Array(), 
	Array(
		'TYPE'=>'catalog', 
		'SITE_ID'=>SITE_ID, 
		'ACTIVE'=>'Y', 
		"CNT_ACTIVE"=>"Y", 
		"!ID"=>19,
	), true
);
while($arIb = $resIb->Fetch())
{
	$arIblocks
}
*/


$arFilterSecrions=array();
foreach ($arParams["IBLOCK_ID_IN"] as $IBlock) {
	$arF = Array("IBLOCK_ID"=>$IBlock, "ACTIVE"=>"Y", "IBLOCK_ACTIVE"=>"Y", "GLOBAL_ACTIVE"=>"Y", "!UF_YA" => false);
	$resS = CIBlockSection::GetList(Array("left_margin"=>"asc"), $arF, false, Array("UF_YA"));
	while($arS = $resS->GetNext())
	{
		$arFilterSecrions[]=$arS["ID"];
	}
}

$arFilter[] = array(
	"LOGIC" => "OR",
	"PROPERTY_YA_VALUE" => "Y",
	"SECTION_ID"=>$arFilterSecrions,
);
//--------------------------------------
	// �������
	$i=0;
   // CPL::pr($arParams["IBLOCK_ID_EX"]);
	//EXECUTE
	//CPL::pr($arNavParams);
	
	//CPL::pr(array_merge($arrFilter, $arFilter));
	$rsElements = GetIBlockElementListEx($arParams["IBLOCK_TYPE"], $arParams["IBLOCK_ID_IN"], $arParams["IBLOCK_ID_EX"], $arSort, $arNavParams, array_merge($arrFilter, $arFilter), $arSelect, false);
	//$rsElements = CIBlockElement::GetList(Array(), array_merge($arrFilter, $arFilter), false, false, $arSelect);
	$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
//	$rsElements->SetSectionContext($arResult);
	$arResult["OFFER"] = array();
	//exit;
	while($obElement = $rsElements->GetNextElement())
	{

		//exit;
		$arOffer = $obElement->GetFields();
		//CPL::pr($arOffer);

// !!! �� �������� -- �����!
//		$arOffer["PROPERTIES"] = $obElement->GetProperties();



//		if(isset($arParams["SECTION_ID"]))
//			$arOffer["IBLOCK_SECTION_ID"] = $arParams["SECTION_ID"];

		// �������� ������
		$arOffer["MODEL"]=yandex_text2xml($arOffer["NAME"],true);


		// ������
		$arOffer["URL"] = $arResult["URL"]. $arOffer["DETAIL_PAGE_URL"]. "&amp;r1=yandex&amp;r2=market";


		// ����������
		if($arOffer["PREVIEW_PICTURE"])
		{
			$db_file = CFile::GetByID($arOffer["PREVIEW_PICTURE"]);
			if ($ar_file = $db_file->Fetch())
				$arOffer["PREVIEW_PICTURE"]=$arResult["URL"]. "/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$ar_file["SUBDIR"]."/".urlencode($ar_file["FILE_NAME"]);
		}

		if($arOffer["DETAIL_PICTURE"])
		{
			$db_file = CFile::GetByID($arOffer["DETAIL_PICTURE"]);
			if ($ar_file = $db_file->Fetch())
				$arOffer["DETAIL_PICTURE"]=$arResult["URL"]. "/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$ar_file["SUBDIR"]."/".urlencode($ar_file["FILE_NAME"]);
		}



		//�������� ������
		if($arOffer["PREVIEW_TEXT"])
			$arOffer["PREVIEW_TEXT"]=yandex_text2xml(TruncateText(($arOffer["PREVIEW_TEXT_TYPE"]=="html"? strip_tags(preg_replace_callback("'&[^;]*;'", "replace_special", $arOffer["~PREVIEW_TEXT"])) : $arOffer["PREVIEW_TEXT"]),255), true);
		if($arOffer["DETAIL_TEXT"])
			$arOffer["DETAIL_TEXT"]=yandex_text2xml(TruncateText(($arOffer["DETAIL_TEXT_TYPE"]=="html"? strip_tags(preg_replace_callback("'&[^;]*;'", "replace_special", $arOffer["~DETAIL_TEXT"])) : $arOffer["DETAIL_TEXT"]),255), true);



		if($arParams["USE_PRICE_COUNT"])
		{
			if(CModule::IncludeModule("catalog"))
			{
				$arOffer["PRICE_MATRIX"] = CatalogGetPriceTableEx($arOffer["ID"]);
				foreach($arOffer["PRICE_MATRIX"]["COLS"] as $keyColumn=>$arColumn)
					$arOffer["PRICE_MATRIX"]["COLS"][$keyColumn]["NAME_LANG"] = htmlspecialchars($arColumn["NAME_LANG"]);
			}
			else
			{
				$arOffer["PRICE_MATRIX"] = false;
			}
			$arOffer["PRICES"] = array();
		}
		else
		{
			$arOffer["PRICE_MATRIX"] = false;
			$arOffer["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arOffer, $arParams['PRICE_VAT_INCLUDE']);
		}

		// ���� ����� ��� ���� ��� ���� <= 1, �� �������� ���
		
		foreach($arOffer["PRICES"] as $keyPr=>$arPr)
		{
			if($arPr["VALUE"]<=1)
				unset($arOffer["PRICES"][$keyPr]);
		}
		
		if(count($arOffer["PRICES"])==0) 
			continue;
$resSec = CIBlockSection::GetByID($arOffer["IBLOCK_SECTION_ID"]);
if($ar_res = $resSec->GetNext())
	$arResult["SECT"][$arOffer["IBLOCK_SECTION_ID"]] = array("ID"=>$arOffer["IBLOCK_SECTION_ID"], "NAME"=>$ar_res['NAME']);

//CPL::pr($arOffer);
//exit;
/*
		$rsPath = GetIBlockSectionPath($arOffer["IBLOCK_ID"], $arOffer["IBLOCK_SECTION_ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$arOffer["SECTION"]["PATH"][]=$arPath;
		}
*/
//	fputs($file, $arOffer["IBLOCK_ID"]. " ". $arOffer["ID"]. "\r\n");


//print_r($arOffer);
//if($i==20)
//	die();


		$arResult["OFFER"][$arOffer["ID"]]=$arOffer;

		$i++;
	}
	//CPL::pr($arResult["OFFER"]);
	//exit;
	
	
$arSections = array();	
foreach ($arParams["IBLOCK_ID_IN"] as $IBlock) {
	$filter = Array("IBLOCK_ID"=>$IBlock, "ACTIVE"=>"Y", "IBLOCK_ACTIVE"=>"Y", "GLOBAL_ACTIVE"=>"Y", "UF_YA" => 1);
	$db_acc = CIBlockSection::GetList(Array("left_margin"=>"asc"), $filter, false, Array("UF_YA"));

	for($i=0; $arAcc = $db_acc->GetNext(); $i++)
	{
		//CPL::pr($arAcc);
		if ($arAcc["UF_YA"])
			$arSections[]=$arAcc["ID"];
	}
	//CPL::pr($arSections);
}
	
	





/**/

if ($arSections) {
	unset($arFilter["PROPERTY_YA_VALUE"]);
	$arFilter = array(
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		"CHECK_PERMISSIONS" => "Y",
		"SECTION_ID" => $arSections,
	);
	// �������
	$i=0;

	//EXECUTE
	$rsElements = GetIBlockElementListEx($arParams["IBLOCK_TYPE"], $arParams["IBLOCK_ID_IN"], $arParams["IBLOCK_ID_EX"], $arSort, $arNavParams, array_merge($arrFilter, $arFilter), $arSelect, false);
	$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
//	$rsElements->SetSectionContext($arResult);
	//$arResult["OFFER"] = array();
	while($obElement = $rsElements->GetNextElement())
	{

		$arOffer = $obElement->GetFields();

// !!! �� �������� -- �����!
//		$arOffer["PROPERTIES"] = $obElement->GetProperties();



//		if(isset($arParams["SECTION_ID"]))
//			$arOffer["IBLOCK_SECTION_ID"] = $arParams["SECTION_ID"];

		// �������� ������
		$arOffer["MODEL"]=yandex_text2xml($arOffer["NAME"],true);


		// ������
		$arOffer["URL"] = $arResult["URL"]. $arOffer["DETAIL_PAGE_URL"]. "&amp;r1=yandex&amp;r2=market";


		// ����������
		if($arOffer["PREVIEW_PICTURE"])
		{
			$db_file = CFile::GetByID($arOffer["PREVIEW_PICTURE"]);
			if ($ar_file = $db_file->Fetch())
				$arOffer["PREVIEW_PICTURE"]=$arResult["URL"]. "/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$ar_file["SUBDIR"]."/".urlencode($ar_file["FILE_NAME"]);
		}

		if($arOffer["DETAIL_PICTURE"])
		{
			$db_file = CFile::GetByID($arOffer["DETAIL_PICTURE"]);
			if ($ar_file = $db_file->Fetch())
				$arOffer["DETAIL_PICTURE"]=$arResult["URL"]. "/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$ar_file["SUBDIR"]."/".urlencode($ar_file["FILE_NAME"]);
		}



		//�������� ������
		if($arOffer["PREVIEW_TEXT"])
			$arOffer["PREVIEW_TEXT"]=yandex_text2xml(TruncateText(($arOffer["PREVIEW_TEXT_TYPE"]=="html"? strip_tags(preg_replace_callback("'&[^;]*;'", "replace_special", $arOffer["~PREVIEW_TEXT"])) : $arOffer["PREVIEW_TEXT"]),255), true);
		if($arOffer["DETAIL_TEXT"])
			$arOffer["DETAIL_TEXT"]=yandex_text2xml(TruncateText(($arOffer["DETAIL_TEXT_TYPE"]=="html"? strip_tags(preg_replace_callback("'&[^;]*;'", "replace_special", $arOffer["~DETAIL_TEXT"])) : $arOffer["DETAIL_TEXT"]),255), true);



		if($arParams["USE_PRICE_COUNT"])
		{
			if(CModule::IncludeModule("catalog"))
			{
				$arOffer["PRICE_MATRIX"] = CatalogGetPriceTableEx($arOffer["ID"]);
				foreach($arOffer["PRICE_MATRIX"]["COLS"] as $keyColumn=>$arColumn)
					$arOffer["PRICE_MATRIX"]["COLS"][$keyColumn]["NAME_LANG"] = htmlspecialchars($arColumn["NAME_LANG"]);
			}
			else
			{
				$arOffer["PRICE_MATRIX"] = false;
			}
			$arOffer["PRICES"] = array();
		}
		else
		{
			$arOffer["PRICE_MATRIX"] = false;
			$arOffer["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arOffer, $arParams['PRICE_VAT_INCLUDE']);
		}

		// ���� ����� ��� ����, �� �������� ���
		if(count($arOffer["PRICES"])==0) 
			continue;



		$arResult["OFFER"][$arOffer["ID"]]=$arOffer;

		$i++;
	}
	
	
}
	
	
	
	/**/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// while($obElement = $rsElements->GetNextElement())
//fclose($file);

	$this->IncludeComponentTemplate();
}
if(!$bDesignMode)
{
	$r = $APPLICATION->EndBufferContentMan();
	echo $r;
	if(defined("HTML_PAGES_FILE") && !defined("ERROR_404")) CHTMLPagesCache::writeFile(HTML_PAGES_FILE, $r);
	die();
}
?>
