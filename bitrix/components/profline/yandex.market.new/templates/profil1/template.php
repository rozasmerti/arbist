<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
echo '<?xml version="1.0" encoding="'. LANG_CHARSET. '"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd">'."\n";
?>
<yml_catalog date="<?=$arResult["DATE"]?>">
<shop>
<name><?=$arResult["NAME"]?></name>
<company><?=$arResult["COMPANY"]?></company>
<url><?=$arResult["URL"]?></url>
<currencies>
<?foreach($arResult["CURRENCIES"] as $arCurrency):?>
	<currency id="<?=$arCurrency["ID"]?>" rate="<?=$arCurrency["RATE"]?>"/>
<?endforeach;?>
</currencies>
<categories>
<?foreach($arResult["CATEGORIES"] as $arCategory):?>
	<category id="<?=$arCategory["ID"]?>"<?
if($arCategory["PARENT"])
	echo ' parentId="'. $arCategory['PARENT']. '"';
?>><?=$arCategory["NAME"]?></category>
<?endforeach;?>
</categories>
<offers>
<?foreach($arResult["OFFER"] as $arOffer):?>
<?if (strlen($arOffer["PRICE"])<=0){continue;}?>
<offer id="<?=$arOffer["ID"]?>" available="<?=$arOffer["AVAIBLE"]?>">
<url><?=$arOffer["URL"]?></url>
<price><?=$arOffer["PRICE"]?></price>
<currencyId>RUB</currencyId>
<categoryId><?=$arOffer["CATEGORY"]?></categoryId>
<picture><?=$arOffer["PICTURE"]?></picture>
<name><?=$arOffer["MODEL"]?></name>
<description><?=$arOffer["DESCRIPTION"]?></description>
</offer>
<?endforeach;?>
</offers>
</shop>
</yml_catalog>