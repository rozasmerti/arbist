<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;
$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["BRAND_ID"] = intval($_REQUEST["BRAND_ID"]);
$arParams["IBLOCK_CODE"] = trim($_REQUEST["IBLOCK_CODE"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["DISPLAY_PANEL"] = $arParams["DISPLAY_PANEL"]=="Y";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["PAGE_ELEMENT_COUNT"] = intval($arParams["PAGE_ELEMENT_COUNT"]);
$arParams["CACHE_FILTER"]=$arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

$arResult["SECTIONS"]=array();

if ($arParams["BRAND_ID"] && !$_REQUEST["arrFilter_pf"]["BRAND"] && !isset($_REQUEST["set_filter"])) {
	$_REQUEST["arrFilter_pf"]["BRAND"] = $arParams["BRAND_ID"];
	$_REQUEST["set_filter"] = Y;
}

if(!is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();
$arParams["USE_PRICE_COUNT"] = $arParams["USE_PRICE_COUNT"]=="Y";
$arParams["SHOW_PRICE_COUNT"] = intval($arParams["SHOW_PRICE_COUNT"]);
if($arParams["SHOW_PRICE_COUNT"]<=0)
	$arParams["SHOW_PRICE_COUNT"]=1;
$arParams["FILTER_NAME"] = "arrFilter";
//CPL::pr($arParams["PRICE_CODE"]);
$APPLICATION->IncludeComponent("bitrix:catalog.filter", "plitka", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "SECTION_ID",
		2 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "COUNTRY",
		2 => "BRAND",
		3 => "SIZE",
		4 => "COLOR",
		5 => "SURFPROC",
		6 => "IMGTYPE",
		7 => "APPLICATION",
		8 => "",
	),
	"LIST_HEIGHT" => "5",
	"TEXT_WIDTH" => "20",
	"NUMBER_WIDTH" => "5",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"CACHE_GROUPS" => "Y",
	"SAVE_IN_SESSION" => "N",
	"PRICE_CODE" => array(
		0 => "Розничная",
	)
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);
if(strlen($arParams["FILTER_NAME"])<=0 || !ereg("^[A-Za-z_][A-Za-z01-9_]*$", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global $$arParams["FILTER_NAME"];
	//CPL::pr($arrFilter);
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}
	//CPL::pr($_REQUEST);
	//CPL::pr(str_replace($_SERVER["QUERY_STRING"]."&","","/catalog/".$arParams["IBLOCK_CODE"]."/".$arrFilter["PROPERTY"]["BRAND"]."/?".$_SERVER["REDIRECT_QUERY_STRING"]));
	//exit;

if (isset($arrFilter["PROPERTY"]["BRAND"]) && $arrFilter["PROPERTY"]["BRAND"]!=$arParams["BRAND_ID"]) {
	LocalRedirect(str_replace($_SERVER["QUERY_STRING"]."&","","/catalog/".$arParams["IBLOCK_CODE"]."/".$arrFilter["PROPERTY"]["BRAND"]."/?".$_SERVER["REDIRECT_QUERY_STRING"]));
}
if ($_REQUEST["set_filter"] && !isset($arrFilter["PROPERTY"]["BRAND"]) && !empty($arParams["BRAND_ID"])) {
	LocalRedirect(str_replace($_SERVER["QUERY_STRING"]."&","","/catalog/".$arParams["IBLOCK_CODE"]."/?".$_SERVER["REDIRECT_QUERY_STRING"]));
}


/*************************************************************************
			Work with cache
*************************************************************************/
//CPL::pr($this->StartResultCache(false, $USER->GetGroups()));
//exit;
if($this->StartResultCache(false, array($arrFilter, $USER->GetGroups(), $arNavigation)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$arFilter = array(
		"ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);

	$arResult["SECTION"] = false;
	if(strlen($arParams["SECTION_CODE"])>0)
	{
		$arFilter["CODE"] = $arParams["SECTION_CODE"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}
	elseif($arParams["SECTION_ID"]>0)
	{
		$arFilter["ID"] = $arParams["SECTION_ID"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}
	if ($arParams["BRAND_ID"]>0) {
		$arFilter["PROPERTY_BRAND"] = $arParams["BRAND_ID"];
	}
	if(is_array($arResult["SECTION"]))
	{
		unset($arFilter["ID"]);
		unset($arFilter["CODE"]);
		//$arFilter["LEFT_MARGIN"]=$arResult["SECTION"]["LEFT_MARGIN"]+1;
		//$arFilter["RIGHT_MARGIN"]=$arResult["SECTION"]["RIGHT_MARGIN"];
		$arFilter["<="."DEPTH_LEVEL"]=$arResult["SECTION"]["DEPTH_LEVEL"] + $arParams["TOP_DEPTH"];

		$arResult["SECTION"]["PATH"] = array();
		$rsPath = GetIBlockSectionPath($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$arResult["SECTION"]["PATH"][]=$arPath;
		}
	}
	else
	{
		$arResult["SECTION"] = array("ID"=>0, "DEPTH_LEVEL"=>0);
		$arFilter["<="."DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
	}
	$arBrandFilter = array(
		"IBLOCK_ID" => 2
		);
	$arBrandSelect = array(
		"ID",
		"NAME"
		);
	$rsBrands = CIBlockElement::GetList(Array(), $arBrandFilter, false, false, $arBrandSelect);
	$arBrands = array();
	while($ob = $rsBrands->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arBrands[$arFields["NAME"]] = $arFields["ID"];
	}
	//CPL::pr($arBrands);
	//exit;
	$res = CIBlockElement::GetList(Array(), array_merge($arrFilter,$arFilter), false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
	  $arFields = $ob->GetFields();
	  
	  if($arFields["IBLOCK_SECTION_ID"])
		$arFilter["ID"][] = $arFields["IBLOCK_SECTION_ID"];
	}
	//CPL::pr($arFilter);
	if (isset($arrFilter["SECTION_ID"]))
		$arFilter["ID"] = $arrFilter["SECTION_ID"];
	//EXECUTE
	$arNavParams = array(
		"nPageSize" => 6,
		"bDescPageNumbering" => "",
		"bShowAll" => 0,
	);
	$arSort = array("SORT"=>"ASC","NAME"=>"ASC");
	$rsSections = CIBlockSection::GetList($arSort, $arFilter, $arParams["COUNT_ELEMENTS"], $arNavParams);
	$rsSections->NavStart($arParams["PAGE_ELEMENT_COUNT"]);
	$arResult["NAV"] = $rsSections;
	$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
	
	$arSelect = array(
		"ID",
		"NAME",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"PREVIEW_PICTURE",
		"DETAIL_PICTURE",
		"PROPERTY_BRAND",
		"PROPERTY_INDEX",
		"PROPERTY_COUNTRY",
		"PROPERTY_INTERIOR"
	);
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		"CHECK_PERMISSIONS" => "Y",
		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
	);
	$arNavParams = array(
		"nPageSize" => 6,
		"bDescPageNumbering" => "",
		"bShowAll" => 0,
	);
	if (isset($_REQUEST["SECTION_ID"]) && $_REQUEST["SECTION_ID"]>0) {
		$arNavParams["nPageSize"] = 24;
	}
	$arResultPrices = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	$t = $arResultPrices;
	$price = array_pop($t);
	//CPL::pr($price);
	$arParams['PRICE_VAT_INCLUDE'] = true;
	$arSections = array();
	unset($arSelect["PROPERTY_INDEX"]);

	while($arSection = $rsSections->GetNext())
	{
		//CPL::pr($arSection);
		$arFilter["SECTION_ID"] = $arSection["ID"];
		$arFilter["CATALOG_SHOP_QUANTITY_1"] = 1;
		$arSelect[] = $price["SELECT"];
		$arSelect[]="PROPERTY_CML2_BASE_UNIT";
		$rsElements = CIBlockElement::GetList(array("SORT"=>"ASC","CATALOG_PRICE_1"=>"ASC"), array_merge($arrFilter,$arFilter), array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_INTERIOR"), false, $arSelect);
		$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
		if($arParams["BY_LINK"]!=="Y")
			$rsElements->SetSectionContext($arResult);
		$arResult["ITEMS"] = array();
		//$arResult["INTERIOR"] = array();
		while($obElement = $rsElements->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			$inter = explode(", ", $arItem["PROPERTY_INTERIOR_VALUE"]);
			//CPL::pr($arItem);
			//exit;
			if (!isset($arSections[$arItem["IBLOCK_SECTION_ID"]])) {
				$arFilterSec = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'ID'=>$arItem["IBLOCK_SECTION_ID"]);
					$arSort = array("SORT"=>"ASC","NAME"=>"ASC");
					$db_list = CIBlockSection::GetList($arSort, $arFilterSec, false);
				if ($ar_result = $db_list->GetNext(true, false)) {
					$arSections[$arItem["IBLOCK_SECTION_ID"]] = $ar_result;
					$arSections[$arItem["IBLOCK_SECTION_ID"]]["MIN_PRICE"] = 99999999999999999;
				}
			}
			$arItem["PRICES"] = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, $USER->GetUserGroupArray(), "N");
			//CPL::pr($arItem["PRICES"]);
			$arFilter1 = $arFilter;
			$arFilter1["ID"] = $arItem["ID"];
			$rsElements1 = CIBlockElement::GetList(array("SORT"=>"ASC",$price["SELECT"]=>"ASC"), $arFilter1, false, false, $arSelect);
			$obElement1 = $rsElements1->GetNextElement(false,false);
			$arItem = $obElement1->GetFields();
			$arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
			$arItem["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
			if (isset($arBrands[$arItem["PROPERTY_BRAND_VALUE"]])) {
				$arItem["PROPERTY_BRAND_VALUE"] = "<a href=\"/brands/".$arBrands[$arItem["PROPERTY_BRAND_VALUE"]]."/\">".$arItem["PROPERTY_BRAND_VALUE"]."</a>";
			}
			$db_props = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), array("ID"=>$arItem["PROPERTY_COUNTRY_ID"]));
			$ar_props = $db_props->Fetch();
			//CPL::pr($ar_props);
			//CPL::pr($arItem);
			$arItem["PROPERTY_COUNTRY_VALUE"] = $ar_props["VALUE_ENUM"];
			/*
			$sql = "SELECT price.PRICE as price FROM b_catalog_price as price LEFT JOIN b_iblock_element as el ON el.id=price.product_id LEFT JOIN b_iblock_element_property as elprop on elprop.IBLOCK_ELEMENT_ID=el.id WHERE el.id = ".$arItem["ID"]." and el.IBLOCK_ID = ".$arParams["IBLOCK_ID"]."  and price.PRICE>1 ORDER BY price DESC";
			$res = $DB->Query($sql, false, "");
			$arGroup=$res->GetNext();
			$arItem["CATALOG_PRICE_1"] = $arGroup["price"];/**/
			$arItem["PRICES"] = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, $USER->GetUserGroupArray(), "N");
			$arItem["CATALOG_PRICE_1"] = $arItem["PRICES"]["PRICE"]["PRICE"];
			$arGroup["price"] = $arItem["CATALOG_PRICE_1"];
			if ( $arGroup["price"]< $arSections[$arItem["IBLOCK_SECTION_ID"]]["MIN_PRICE"])
				$arSections[$arItem["IBLOCK_SECTION_ID"]]["MIN_PRICE"] = $arGroup["price"];
			foreach ($inter as $k=>$v) {
				$arItem["PIC"] = $v;
				$arSections[$arItem["IBLOCK_SECTION_ID"]]["INTER"][$v.$arItem["ID"]] = $arItem;
				//CPL::pr($arItem);
				//exit;
			}
			
			$arSections[$arItem["IBLOCK_SECTION_ID"]]["ITEMS"][] = $arItem;


		}
	}
	$arResult["SECTIONS"] = $arSections;
//CPL::pr($arSections);
	
	$this->IncludeComponentTemplate();
}

if(count($arResult["SECTIONS"])>0 || isset($arResult["SECTION"]))
{
	if($USER->IsAuthorized())
	{
		if($GLOBALS["APPLICATION"]->GetShowIncludeAreas() && CModule::IncludeModule("iblock"))
			$this->AddIncludeAreaIcons(CIBlock::ShowPanel($arParams["IBLOCK_ID"], 0, $arResult["SECTION"]["ID"], $arParams["IBLOCK_TYPE"], true));
		if($arParams["DISPLAY_PANEL"] && CModule::IncludeModule("iblock"))
			CIBlock::ShowPanel($arParams["IBLOCK_ID"], 0, $arResult["SECTION"]["ID"], $arParams["IBLOCK_TYPE"], false, $this->GetName());
	}

	if($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["SECTION"]) && is_array($arResult["SECTION"]["PATH"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}
}

?>
