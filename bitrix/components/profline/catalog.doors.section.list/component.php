<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;
$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["BRAND_ID"] = intval($_REQUEST["BRAND_ID"]);
$arParams["IBLOCK_CODE"] = trim($_REQUEST["IBLOCK_CODE"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["DISPLAY_PANEL"] = $arParams["DISPLAY_PANEL"]=="Y";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["PAGE_ELEMENT_COUNT"] = intval($arParams["PAGE_ELEMENT_COUNT"]);
$arParams["CACHE_FILTER"]=$arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

$arResult["SECTIONS"]=array();

if ($arParams["BRAND_ID"] && !$_REQUEST["arrFilter_pf"]["BRAND"] && !isset($_REQUEST["set_filter"])) {
	$_REQUEST["arrFilter_pf"]["BRAND"] = $arParams["BRAND_ID"];
	$_REQUEST["set_filter"] = Y;
}

$APPLICATION->IncludeComponent("bitrix:catalog.filter", "doors", Array(
	"IBLOCK_TYPE" => "catalog",	// ��� ����-�����
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],	// ����-����
	"FILTER_NAME" => "arrFilter",	// ��� ���������� ������� ��� ����������
	"FIELD_CODE" => array(	// ����
		0 => "SECTION_ID",
		1 => "",
	),
	"PROPERTY_CODE" => array(	// ��������
		0 => "BRAND",
		1 => "COLOR",
		2 => "GLAZING",
		//3 => "NAME_COLLECTION",
		3 => "MATERIAL",
		4 => "CASE",
		5 => "EDGE",
		6 => "FRAME",
		7 => "",
	),
	"LIST_HEIGHT" => "5",	// ������ ������� �������������� ������
	"TEXT_WIDTH" => "20",	// ������ ������������ ��������� ����� �����
	"NUMBER_WIDTH" => "5",	// ������ ����� ����� ��� �������� ����������
	"CACHE_TYPE" => "A",	// ��� �����������
	"CACHE_TIME" => "3600",	// ����� ����������� (���.)
	"SAVE_IN_SESSION" => "N",	// ��������� ��������� ������� � ������ ������������
	"PRICE_CODE" => $arParams["PRICE_CODE"],	// ��� ����
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);
//CPL::pr($arParams["PRICE_CODE"]);
$arParams["FILTER_NAME"] = "arrFilter";

if(strlen($arParams["FILTER_NAME"])<=0 || !ereg("^[A-Za-z_][A-Za-z01-9_]*$", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global $$arParams["FILTER_NAME"];
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}
if (isset($arrFilter["PROPERTY"]["BRAND"]) && $arrFilter["PROPERTY"]["BRAND"]!=$arParams["BRAND_ID"]) {
	LocalRedirect(str_replace($_SERVER["QUERY_STRING"]."&","","/catalog/".$arParams["IBLOCK_CODE"]."/".$arrFilter["PROPERTY"]["BRAND"]."/?".$_SERVER["REDIRECT_QUERY_STRING"]));
}
if ($_REQUEST["set_filter"] && !isset($arrFilter["PROPERTY"]["BRAND"]) && !empty($arParams["BRAND_ID"])) {
	LocalRedirect(str_replace($_SERVER["QUERY_STRING"]."&","","/catalog/".$arParams["IBLOCK_CODE"]."/?".$_SERVER["REDIRECT_QUERY_STRING"]));
}


/*************************************************************************
			Work with cache
*************************************************************************/
//CPL::pr($this->StartResultCache(false, $USER->GetGroups()));
//exit;
if($this->StartResultCache(false, array($arrFilter, $USER->GetGroups(), $arNavigation)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$arFilter = array(
		"ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	);

	$arResult["SECTION"] = false;
	if(strlen($arParams["SECTION_CODE"])>0)
	{
		$arFilter["CODE"] = $arParams["SECTION_CODE"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}
	elseif($arParams["SECTION_ID"]>0)
	{
		$arFilter["ID"] = $arParams["SECTION_ID"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}
	if ($arParams["BRAND_ID"]>0) {
		$arFilter["PROPERTY_BRAND"] = $arParams["BRAND_ID"];
	}
	if(is_array($arResult["SECTION"]))
	{
		unset($arFilter["ID"]);
		unset($arFilter["CODE"]);
		//$arFilter["LEFT_MARGIN"]=$arResult["SECTION"]["LEFT_MARGIN"]+1;
		//$arFilter["RIGHT_MARGIN"]=$arResult["SECTION"]["RIGHT_MARGIN"];
		$arFilter["<="."DEPTH_LEVEL"]=$arResult["SECTION"]["DEPTH_LEVEL"] + $arParams["TOP_DEPTH"];

		$arResult["SECTION"]["PATH"] = array();
		$rsPath = GetIBlockSectionPath($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$arResult["SECTION"]["PATH"][]=$arPath;
		}
	}
	else
	{
		$arResult["SECTION"] = array("ID"=>0, "DEPTH_LEVEL"=>0);
		//$arFilter["<="."DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
	}
	/*********************************************************************************************************************/
	$arBrandFilter = array(
		"IBLOCK_ID" => 2
		);
	/*********************************************************************************************************************/
	$arBrandSelect = array(
		"ID",
		"NAME"
		);
	$rsBrands = CIBlockElement::GetList(Array(), $arBrandFilter, false, false, $arBrandSelect);
	$arBrands = array();
	while($ob = $rsBrands->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arBrands[$arFields["NAME"]] = $arFields["ID"];
	}
	//exit;
	$arFilter["NAME"] = "�������%";
	//CPL::pr(array_merge($arrFilter,$arFilter));
	$res = CIBlockElement::GetList(Array(), array_merge($arrFilter,$arFilter), array("IBLOCK_SECTION_ID"), false, $arSelect);
	while($ob = $res->GetNextElement())
	{
	  $arFields = $ob->GetFields();
	  //CPL::pr($arFields);
	  $arFilter["ID"][] = $arFields["IBLOCK_SECTION_ID"];
	}
	unset($arFilter["NAME"]);
	if (isset($arrFilter["SECTION_ID"]))
		$arFilter["ID"] = $arrFilter["SECTION_ID"];
	//EXECUTE
	$arNavParams = array(
		"nPageSize" => 60,
		"bDescPageNumbering" => "",
		"bShowAll" => 0,
	);
	//CPL::pr($arNavParams);
	$rsSections = CIBlockSection::GetList($arSort, $arFilter, $arParams["COUNT_ELEMENTS"], $arNavParams);
	$rsSections->NavStart($arParams["PAGE_ELEMENT_COUNT"]);
	$arResult["NAV"] = $rsSections;
	//CPL::pr($arParams["PAGE_ELEMENT_COUNT"]);
	$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
	
	$arSelect = array(
		"ID",
		"NAME",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"PREVIEW_PICTURE",
		"DETAIL_PICTURE",
		"PROPERTY_BRAND",
		"PROPERTY_INDEX",
		"PROPERTY_COUNTRY",
		
	);
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		"CHECK_PERMISSIONS" => "Y",
		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
	);
	$arNavParams = array(
		"nPageSize" => 6,
		"bDescPageNumbering" => "",
		"bShowAll" => 0,
	);
	if (isset($_REQUEST["SECTION_ID"]) && $_REQUEST["SECTION_ID"]>0) {
		$arNavParams["nPageSize"] = 24;
	}
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], array("BASE"));
	$arParams['PRICE_VAT_INCLUDE'] = true;
	$arSections = array();
	unset($arSelect["PROPERTY_INDEX"]);

	while($arSection = $rsSections->GetNext())
	{
//echo 1;
		//CPL::pr($arSection);
		$arFilter["SECTION_ID"] = $arSection["ID"];
		$arFilter["NAME"] = "�������%";
		$arFilter["CATALOG_SHOP_QUANTITY_1"] = 1;
		//$arSelect[] = "CATALOG_GROUP_1";
		$rsElements = CIBlockElement::GetList(array("CATALOG_PRICE_1"=>"DESC"), array_merge($arrFilter,$arFilter), array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_INDEX", "PROPERTY_NAME_COLLECTION"), false, $arSelect);
		$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
		if($arParams["BY_LINK"]!=="Y")
			$rsElements->SetSectionContext($arResult);
		//$arResult["ITEMS"] = array();
		//CPL::pr($arFilter);
		while($obElement = $rsElements->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			//CPL::pr($arItem);
			if (!isset($arSections[$arItem["IBLOCK_SECTION_ID"]]["ITEMS"][$arItem["PROPERTY_INDEX_VALUE"].$arItem["PROPERTY_NAME_COLLECTION_VALUE"]])) {
				if (!isset($arSections[$arItem["IBLOCK_SECTION_ID"]])) {
					$arFilterSec = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'ID'=>$arItem["IBLOCK_SECTION_ID"]);
					$db_list = CIBlockSection::GetList($arSort, $arFilterSec, false);
					$db_list->SetUrlTemplates();
					if ($ar_result = $db_list->GetNext(true, false)) {
/**/						//CPL::pr($ar_result);
						//$ar_result["SECTION_PAGE_URL"] = str_replace("#CODE#", $ar_result["CODE"], $ar_result["SECTION_PAGE_URL"]);
						//CPL::pr($ar_result);
						//exit;/**/
						$arSections[$arItem["IBLOCK_SECTION_ID"]] = $ar_result;
					}
				}
				$arFilter1 = $arFilter;
				$arFilter1["ID"] = $arItem["ID"];
				$arSelect[]="CATALOG_PRICE_1";
				$arSelect[]="PROPERTY_NAME_COLLECTION";

				global $USER;
				if($USER->IsAdmin())
				{
				//echo "<pre>"; print_r($arSelect); echo "</pre>";
				
				}
				$arSelect[]="PROPERTY_CML2_BASE_UNIT";
				$rsElements1 = CIBlockElement::GetList(array("CATALOG_PRICE_4"=>"ASC"), $arFilter1, false, false, $arSelect);
				$obElement1 = $rsElements1->GetNextElement(false,false);
				$arItem = $obElement1->GetFields();
				//CPL::pr($arItem);
				//exit;
				$arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
				$arItem["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
				if (isset($arBrands[$arItem["PROPERTY_BRAND_VALUE"]])) {
					$arItem["PROPERTY_BRAND_VALUE"] = "<a href=\"/brands/".$arBrands[$arItem["PROPERTY_BRAND_VALUE"]]."/\">".$arItem["PROPERTY_BRAND_VALUE"]."</a>";
				}
				
				
				$db_props = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), array("ID"=>$arItem["PROPERTY_COUNTRY_ID"]));
				$ar_props = $db_props->Fetch();

				$arItem["PRICES"] = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, $USER->GetUserGroupArray(), "N");
				//$arItem["pidr"][$arItem["IBLOCK_SECTION_ID"]][] = $arItem["PRICES"]["PRICE"]["PRICE"];
				$arItem["CATALOG_PRICE_1"] = $arItem["PRICES"]["PRICE"]["PRICE"];
				global $USER;
				if($USER->IsAdmin())
				{
					//echo $arItem["PRICES"]["PRICE"]["PRICE"]; echo "<br>";
					if(empty($arSections[$arItem["IBLOCK_SECTION_ID"]]["PRICE"]))
						$arSections[$arItem["IBLOCK_SECTION_ID"]]["PRICE"] = $arItem["PRICES"]["PRICE"]["PRICE"];
					elseif($arItem["PRICES"]["PRICE"]["PRICE"]<$arSections[$arItem["IBLOCK_SECTION_ID"]]["PRICE"])
						$arSections[$arItem["IBLOCK_SECTION_ID"]]["PRICE"] = $arItem["PRICES"]["PRICE"]["PRICE"];
					//echo $arItem["PRICES"]["PRICE"]["PRICE"].'<HR>';
				}
				$arItem["PROPERTY_COUNTRY_VALUE"] = $ar_props["VALUE_ENUM"];
				
				if (!isset($arSections[$arItem["IBLOCK_SECTION_ID"]]["MIN_PRICE"]))
					$arSections[$arItem["IBLOCK_SECTION_ID"]]["MIN_PRICE"] = 9999999;
				if (intval($arSections[$arItem["IBLOCK_SECTION_ID"]]["MIN_PRICE"]) > intval($arItem["PRICES"]["PRICE"]["PRICE"]) && intval($arItem["PRICES"]["PRICE"]["PRICE"])>1) {
					$arSections[$arItem["IBLOCK_SECTION_ID"]]["MIN_PRICE"] = $arItem["PRICES"]["PRICE"]["PRICE"];
				}
				$arSections[$arItem["IBLOCK_SECTION_ID"]]["ITEMS"][$arItem["PROPERTY_INDEX_VALUE"].$arItem["PROPERTY_NAME_COLLECTION_VALUE"]] = $arItem;
				
			}

		}
global $USER;
if($USER->IsAdmin())
{
	//echo "---------------------------";
}
	}
//	

	$arResult["SECTIONS"] = $arSections;
//CPL::pr($arResult["SECTIONS"]);
	
	$this->IncludeComponentTemplate();
}

if(count($arResult["SECTIONS"])>0 || isset($arResult["SECTION"]))
{
	if($USER->IsAuthorized())
	{
		if($GLOBALS["APPLICATION"]->GetShowIncludeAreas() && CModule::IncludeModule("iblock"))
			$this->AddIncludeAreaIcons(CIBlock::ShowPanel($arParams["IBLOCK_ID"], 0, $arResult["SECTION"]["ID"], $arParams["IBLOCK_TYPE"], true));
		if($arParams["DISPLAY_PANEL"] && CModule::IncludeModule("iblock"))
			CIBlock::ShowPanel($arParams["IBLOCK_ID"], 0, $arResult["SECTION"]["ID"], $arParams["IBLOCK_TYPE"], false, $this->GetName());
	}

	if($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["SECTION"]) && is_array($arResult["SECTION"]["PATH"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}
}

?>
