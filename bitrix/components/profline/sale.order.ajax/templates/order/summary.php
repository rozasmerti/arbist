<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<b><?=GetMessage("SOA_TEMPL_SUM_TITLE")?></b><br />
<?//CPL::pr($arResult["BASKET_ITEMS"]);?>
<table class="sale_order_full data-table">
	<?global $USER;
	if($USER->IsAdmin()):?>
	<tr>
		<th><?=GetMessage("SOA_TEMPL_SUM_NAME")?></th>
		<th>�������<br>��.</th>
		<th>��������</th>
		<th>��������� ��������</th>
		<th>����<br>���.</th>
		<th>��. ���.</th>
		<th>����������</th>
	</tr>	
	<?else:?>
	<tr>
		<th><?=GetMessage("SOA_TEMPL_SUM_NAME")?></th>
			<th>�������<br>��.</th>
			<th>�������</th>
			<th>��. ���.</th>
			<th>���� ��������</th>
		<th><?=GetMessage("SOA_TEMPL_SUM_PRICE_TYPE")?></th>
		<th><?=GetMessage("SOA_TEMPL_SUM_DISCOUNT")?></th>
		<th><?=GetMessage("SOA_TEMPL_SUM_WEIGHT")?></th>
		<th><?=GetMessage("SOA_TEMPL_SUM_QUANTITY")?></th>
		<th><?=GetMessage("SOA_TEMPL_SUM_PRICE")?></th>
	</tr>
	<?endif;?>
	<?
	foreach($arResult["BASKET_ITEMS"] as $arBasketItems)
	{
	
	global $USER;
	if($USER->IsAdmin())
	{
	
	
	
		?>
		<tr>
			<td><?=$arBasketItems["NAME"]?></td>
				<td>
				<?=$arBasketItems["SIZE"];?>
				</td>
				<td>
				<?=$arBasketItems["PACKING"];?>
				</td>		
				<td>
				<?=$arBasketItems["CHANGE_KRAT"];?>
				</td>				
				<td align="right"><nobr><?=$arBasketItems["PRICE_FORMATED"]?></nobr></td>
				<td>
				<?=$arBasketItems["CHANGE_UNIT"];?>
				</td>
				<td><?=$arBasketItems["QUANTITY"]?></td>
			
			
		</tr>
		<?
		}
		else
		{
		?>
		<tr>
			<td><?=$arBasketItems["NAME"]?></td>
				<td>
				<?=$arBasketItems["SIZE"];?>
				</td>				
				<td>
				<?=$arBasketItems["LEAF"];?>
				</td>
				<td>
				<?=$arBasketItems["CHANGE_UNIT"];?>
				</td>
				<td>
				<?=$arBasketItems["DELIVERY_DATE"];?>
				</td>
			<td><?=$arBasketItems["NOTES"]?></td>
			<td><?=$arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
			<td><?=$arBasketItems["WEIGHT_FROMATED"]?></td>
			<td><?=$arBasketItems["QUANTITY"]?></td>
			<td align="right"><nobr><?=$arBasketItems["PRICE_FORMATED"]?></nobr></td>
		</tr>
		<?		
		}
	}
	?>
	<tr>
		<td align="right" colspan="6"><b><?=GetMessage("SOA_TEMPL_SUM_WEIGHT_SUM")?></b></td>
		<td align="right" colspan="1"><nobr><?=$arResult["ORDER_WEIGHT_FORMATED"]?></nobr></td>
	</tr>
	<tr>
		<td align="right" colspan="6"><b><?=GetMessage("SOA_TEMPL_SUM_SUMMARY")?></b></td>
		<td align="right" colspan="1"><nobr><?=$arResult["ORDER_PRICE_FORMATED"]?></nobr></td>
	</tr>
	<?
	if (doubleval($arResult["DISCOUNT_PRICE"]) > 0)
	{
		?>
		<tr>
			<td align="right" colspan="6"><b><?=GetMessage("SOA_TEMPL_SUM_DISCOUNT")?><?if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"])>0):?> (<?echo $arResult["DISCOUNT_PERCENT_FORMATED"];?>)<?endif;?>:</b></td>
			<td align="right" colspan="1"><nobr><?echo $arResult["DISCOUNT_PRICE_FORMATED"]?></nobr>
			</td>
		</tr>
		<?
	}
	/*
	if (doubleval($arResult["VAT_SUM_FORMATED"]) > 0)
	{
		?>
		<tr>
			<td align="right">
				<b><?=GetMessage("SOA_TEMPL_SUM_VAT")?></b>
			</td>
			<td align="right" colspan="6"><?=$arResult["VAT_SUM_FORMATED"]?></td>
		</tr>
		<?
	}
	*/
	if(!empty($arResult["arTaxList"]))
	{
		foreach($arResult["arTaxList"] as $val)
		{
			?>
			<tr>
				<td align="right" colspan="6"><?=$val["NAME"]?> <?=$val["VALUE_FORMATED"]?>:</td>
				<td align="right" colspan="1"><nobr><?=$val["VALUE_MONEY_FORMATED"]?></nobr></td>
			</tr>
			<?
		}
	}
	if (doubleval($arResult["DELIVERY_PRICE"]) > 0)
	{
		?>
		<tr>
			<td align="right" colspan="6">
				<b><?=GetMessage("SOA_TEMPL_SUM_DELIVERY")?></b>
			</td>
			<td align="right" colspan="1"><nobr><?=$arResult["DELIVERY_PRICE_FORMATED"]?></nobr></td>
		</tr>
		<?
	}
	?>
	<tr>
		<td align="right" colspan="6"><b><?=GetMessage("SOA_TEMPL_SUM_IT")?></b></td>
		<td align="right" colspan="1"><b><nobr><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></nobr></b>
		</td>
	</tr>
	<?
	if (strlen($arResult["PAYED_FROM_ACCOUNT_FORMATED"]) > 0)
	{
		?>
		<tr>
			<td align="right" colspan="6"><b><?=GetMessage("SOA_TEMPL_SUM_PAYED")?></b></td>
			<td align="right" colspan="1"><?=$arResult["PAYED_FROM_ACCOUNT_FORMATED"]?></td>
		</tr>
		<?
	}
	?>
</table>

<br /><br />
<b><?=GetMessage("SOA_TEMPL_SUM_ADIT_INFO")?></b><br /><br />

<table class="sale_order_full_table">
	<tr>
		<td width="50%" align="left" valign="top"><?=GetMessage("SOA_TEMPL_SUM_COMMENTS")?><br />
			<textarea rows="4" cols="40" name="ORDER_DESCRIPTION"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea>
		</td>
	</tr>
</table>
