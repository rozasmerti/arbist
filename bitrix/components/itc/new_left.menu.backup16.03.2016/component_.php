<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
global $USER;
$arResult = array();

$vendorPattern = "#^/catalog/[a-zA-Z0-9-_]+/vendor/([a-zA-Z0-9-_]+)/#";
$countryPattern = "#^/catalog/[a-zA-Z0-9-_]+/country/([a-zA-Z0-9-_']+)/#";

$interiorDoorsPageUrl = INTERIOR_DOORS_PAGE_PROP_URL;
$inInteriorDoorsPages = (strpos($_SERVER["REQUEST_URI"], $interiorDoorsPageUrl) === 0);
if($inInteriorDoorsPages) {
    $arParams["SECTION_ID"] = "dveri";
    $arParams["IBLOCK_CODE"] = "doors";
    
    $matches = array();
    if(preg_match("#^/interior_doors/([a-zA-Z0-9-_]+)/#Us", $_SERVER["REQUEST_URI"], $matches)){
        if($matches[ 1 ]){
            $arParams["INTERIOR_DOORS_SELECTED_SUBSECTION"] = $matches[ 1 ];
        }
    
    }
    
// /catalog/doors/vendor/mariyskiy_mebelnyy_kombinat/        
}elseif(preg_match($vendorPattern, $_SERVER["REQUEST_URI"], $matches)) {
    if($matches[ 1 ]){
        $arParams["SELECTED_VENDOR"] = $matches[ 1 ];
    }
    
// /catalog/plitka_premium/country/Italiya-Kitaj/
}elseif(preg_match($countryPattern, $_SERVER["REQUEST_URI"], $matches)){
    if($matches[ 1 ]){
        $arParams["SELECTED_COUNTRY"] = $matches[ 1 ];
    }
}

//�� ������� �������� "������" ���������� "���� ������"
$curPage = $APPLICATION -> GetCurPage();
if($curPage == '/catalog/doors/'){
    $inInteriorDoorsPages = true;
}

if ($this->StartResultCache(false, array($_COOKIE["ITC"], $USER->GetGroups()))) {
    CModule::IncludeModule('iblock');

    // ���������� ��� ����������
	//��������� ��� ��������� �������� 3��� ������
	//ID ������� ��� ������������� �������
	$arResult["BRAND_ID"] = $arParams["BRAND_ID"];
	//ID �������� � ������� �����
	$arResult["FILTER_REQUEST"]["COUNTRY"] = explode(",", $arParams["FILTER_REQUEST"]["COUNTRY"]);
	//ID ��������� � ������� ��������������
	$arResult["FILTER_REQUEST"]["BRAND_GUID"] = explode(",", $arParams["FILTER_REQUEST"]["BRAND_GUID"]);
	//������������ ID
	$arResult["REQUEST_ID"] = $arParams["SECTION_ID"];
    
	$arResult["REQUEST_IBLOCK_CODE"] = $arParams["IBLOCK_CODE"];

	$arResult["SECTIONS"] = array();
	//�������
    $arResult["REQUEST_ID_FLAG"] = true;
    global $USER;

	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"GLOBAL_ACTIVE" => "Y"
	);  
	$res = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter);  
	while($ar_res = $res->fetch()) {

		$arResult["SECTIONS"][$ar_res["CODE"]]["NAME"] = $ar_res["NAME"];
        $sectionCodes[$ar_res["ID"]] = $ar_res["CODE"];
		if(empty($arResult["REQUEST_ID"]) && $ar_res["XML_ID"] == "main") {
			////$arResult["MAIN"]["ID"] = $ar_res["ID"];
			////$arResult["MAIN"]["NAME"] = $ar_res["NAME"];
            $arResult["REQUEST_ID_FLAG"] = false;
			$arResult["REQUEST_ID"] = $ar_res["CODE"];

		}
			
	}
	//��������
	$arFilter = Array(   
		"IBLOCK_ID" => $arParams["IBLOCK_ID"], 
		"ACTIVE" => "Y",    
	);
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_IBLOCK_ID", "PROPERTY_SUBSECTION"));
	while($ar_fields = $res->fetch()) {
		$tempItems[$ar_fields["ID"]] = $ar_fields;
		$tempIblockIds[] = $ar_fields["PROPERTY_IBLOCK_ID_VALUE"];
	}
	//���������
	$arFilter = Array(
		"IBLOCK_TYPE" => "catalog", 
		//"SITE_ID" => SITE_ID, 
		"ACTIVE" => "Y", 
		"ID" => $tempIblockIds
	);
	$res = CIBlock::GetList(Array("SORT" => "ASC"),	$arFilter, false);
	while($ar_res = $res->Fetch()) {
		$tempIblock[$ar_res["ID"]] = $ar_res;
	}

	/******************************************************/
	//�.�. ������������� ������ ����, �� ��� ������ ������ ������� ������ ���
	/*$arBrandGuidFilter = Array(
		"IBLOCK_CODE" => "brand_guid",
	);
	$resBrandGuid = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
	while($ar_fields = $resBrandGuid->fetch()) {
		$brandGuid[$ar_fields["ID"]] = $ar_fields["NAME"];
	}
	*/
	/******************************************************/
		
	//�������� ������ � ����� � ������� ������ � ����������� ����
	foreach($tempItems as $idItem => $item) {

		//������� ��������
		//������
		if($item["PROPERTY_SUBSECTION_ENUM_ID"] == 15499 && $tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["CODE"] == $arResult["REQUEST_IBLOCK_CODE"]) { //
			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_CODE"] = "COUNTRY";
			$db_enum_list = CIBlockProperty::GetPropertyEnum("COUNTRY", Array("sort"=>"asc"), Array("IBLOCK_ID" => $item["PROPERTY_IBLOCK_ID_VALUE"]));
			while($ar_enum_list = $db_enum_list->fetch()) {  
				$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_VALUES"][$ar_enum_list["ID"]] = $ar_enum_list["VALUE"];
			}
		} 
		//�������������
		elseif($item["PROPERTY_SUBSECTION_ENUM_ID"] == 15500 && $tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["CODE"] == $arResult["REQUEST_IBLOCK_CODE"]) { //

			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_CODE"] = "BRAND_GUID";

			$arFilter = Array(
			    "IBLOCK_ID" => $item["PROPERTY_IBLOCK_ID_VALUE"],
			    "ACTIVE" => "Y",
			    "SECTION_GLOBAL_ACTIVE" => "Y",
			    "!CATALOG_PRICE_10" => false,
				//"IBLOCK_LID" => "s2",
				//"IBLOCK_ACTIVE" => "Y",
				//"ACTIVE_DATE" => "Y",
				//"CHECK_PERMISSIONS" => "Y",
				"INCLUDE_SUBSECTIONS" => "Y",
				"CATALOG_SHOP_QUANTITY_10" => 1,
				
			);
			
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("PROPERTY_BRAND_GUID","PROPERTY_BRAND_GUID_VALUE","ID"));
			$brandsArray = Array();
			while($ar_fields = $res->fetch())
				if (!in_array($ar_fields["PROPERTY_BRAND_GUID_VALUE"], $brandsArray))
					$brandsArray[] =  $ar_fields["PROPERTY_BRAND_GUID_VALUE"];
			
			$brandGuid = false;
			if(count($brandsArray) > 0)
			{
				$arBrandGuidFilter = Array(   
					"IBLOCK_CODE" => "brand_guid",
					"ACTIVE" => "Y",
					"ID" => $brandsArray,
					"!NAME" => "<>",
				);
				$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME", "CODE"));
				while($ar_fields = $res->fetch()) {
                        $brandGuid[$ar_fields["ID"]]["NAME"] = $ar_fields["NAME"]; /**/
                        $brandGuid[$ar_fields["ID"]]["CODE"] = $ar_fields["CODE"]; /**/
				}	
			}
			
			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_VALUES"] = $brandGuid;
		}
		//�������		
		elseif($item["PROPERTY_SUBSECTION_ENUM_ID"] == 15593 && $tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["CODE"] == $arResult["REQUEST_IBLOCK_CODE"]) {
			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_CODE"] = "SECTIONS";
			$arSectionFilter = Array(
				"IBLOCK_ID" => $item["PROPERTY_IBLOCK_ID_VALUE"], 
				"GLOBAL_ACTIVE" => "Y",
				"ACTIVE" => "Y"
			);  
			$db_list = CIBlockSection::GetList(Array("NAME" => "ASC"), $arSectionFilter);  
			while($ar_result = $db_list->fetch()) {
				$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_VALUES"][$ar_result["ID"]] = $ar_result["NAME"];
			}  
		}

		$item["IBLOCK_INFO"] = $tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]];

		//��� ���������
		if($item["IBLOCK_INFO"]["CODE"] == $arResult["REQUEST_IBLOCK_CODE"]) {
			$arResult["REQUEST_ID"] = $sectionCodes[$item["IBLOCK_SECTION_ID"]];
        }
		$arResult["SECTIONS"][$sectionCodes[$item["IBLOCK_SECTION_ID"]]]["ITEMS"][$idItem] = $item;

		if($item["IBLOCK_INFO"]["CODE"] == $arResult["REQUEST_IBLOCK_CODE"])
			$arResult["SECTIONS"][$sectionCodes[$item["IBLOCK_SECTION_ID"]]]["SELECTED"] = "Y";
		elseif(empty($arResult["SECTIONS"][$sectionCodes[$item["IBLOCK_SECTION_ID"]]]["SELECTED"]))
			$arResult["SECTIONS"][$sectionCodes[$item["IBLOCK_SECTION_ID"]]]["SELECTED"] = "N";
	}

	$arResult["SEO_NAME"] = $arResult["SECTIONS"][$arResult["REQUEST_ID"]]["NAME"];
    
    // if( $USER -> IsAdmin() ) 
    {
        $arInteriorFake = array();
        
        $interiorFakeName = '���� ������';
        $interiorDoorsMenuId = 91587;
        
        $arInteriorFake["NAME"] = $interiorFakeName;
        $arInteriorFake["DETAIL_PAGE_URL"] = $interiorDoorsPageUrl;
        
        //�������� ��������    
        $arInteriorFake["INTERIOR_FAKE_PAGES"] = itc\getInteriorDoorsPageProps();
        
        if($inInteriorDoorsPages){
            $arResult["REQUEST_IBLOCK_CODE"] = "doors";
            $arResult["SECTIONS"]["dveri"]["SELECTED"] = "Y";
            $arInteriorFake["SELECTED"] = $inInteriorDoorsPages;
            
            $selectedSubSection = $arParams["INTERIOR_DOORS_SELECTED_SUBSECTION"];
            if($selectedSubSection){
                $arInteriorFake["INTERIOR_FAKE_PAGES"][ $selectedSubSection ]["SELECTED"] = true;
            }
        }
        
        $arResult["SECTIONS"]["dveri"]["ITEMS"][$interiorDoorsMenuId]["IBLOCK_INFO"]["PROPERTY_VALUES"]["INTERIOR_FAKE"] = $arInteriorFake;
    }
    
    $this->IncludeComponentTemplate();
}


// ���, ������������� ��� ����������� �� ����

?>
