<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default

$arResult["SECTIONS"]=array();

if(!empty($arParams['SHOW_COUNT']) && intval($arParams['SHOW_COUNT']) > 0){
	$arParams['SHOW_COUNT'] = intval($arParams['SHOW_COUNT']);
} else {
	$arParams['SHOW_COUNT'] = 15;
}
/*************************************************************************
			Work with cache
*************************************************************************/
$arNavParams = array(
	"nPageSize" => $arParams['SHOW_COUNT'], // ��� ����
	"bDescPageNumbering" => "N",
	"bShowAll" => "N",
);

$arNavigation = CDBResult::GetNavParams($arNavParams);

if($this->StartResultCache(false, array($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups(), $arNavigation)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CNT_ACTIVE" => "Y",
	);

	$arSelect = array();
	if(isset($arParams["SECTION_FIELDS"]) && is_array($arParams["SECTION_FIELDS"]))
	{
		foreach($arParams["SECTION_FIELDS"] as $field)
			if(is_string($field) && !empty($field))
				$arSelect[] = $field;
	}

	if(!empty($arSelect))
	{
		$arSelect[] = "ID";
		$arSelect[] = "NAME";
		$arSelect[] = "LEFT_MARGIN";
		$arSelect[] = "RIGHT_MARGIN";
		$arSelect[] = "DEPTH_LEVEL";
		$arSelect[] = "IBLOCK_ID";
		$arSelect[] = "IBLOCK_SECTION_ID";
		$arSelect[] = "LIST_PAGE_URL";
		$arSelect[] = "SECTION_PAGE_URL";
	}

	if(isset($arParams["SECTION_USER_FIELDS"]) && is_array($arParams["SECTION_USER_FIELDS"]))
	{
		foreach($arParams["SECTION_USER_FIELDS"] as $field)
			if(is_string($field) && preg_match("/^UF_/", $field))
				$arSelect[] = $field;
	}

	$arResult["SECTION"] = false;
	/*if(strlen($arParams["SECTION_CODE"])>0)
	{
		$arFilter["CODE"] = $arParams["SECTION_CODE"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true, $arSelect);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}
	elseif($arParams["SECTION_ID"]>0)
	{
		$arFilter["ID"] = $arParams["SECTION_ID"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true, $arSelect);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}

	if(is_array($arResult["SECTION"]))
	{
		unset($arFilter["ID"]);
		unset($arFilter["CODE"]);
		$arFilter["LEFT_MARGIN"]=$arResult["SECTION"]["LEFT_MARGIN"]+1;
		$arFilter["RIGHT_MARGIN"]=$arResult["SECTION"]["RIGHT_MARGIN"];
		$arFilter["<="."DEPTH_LEVEL"]=$arResult["SECTION"]["DEPTH_LEVEL"] + $arParams["TOP_DEPTH"];

		$arResult["SECTION"]["PATH"] = array();
		$rsPath = CIBlockSection::GetNavChain($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$arResult["SECTION"]["PATH"][]=$arPath;
		}
	}
	else
	{
		$arResult["SECTION"] = array("ID"=>0, "DEPTH_LEVEL"=>0);
		$arFilter["<="."DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
	}*/
	
	//�������� ��� ������
	$arBrandGuidFilter = Array(
		"IBLOCK_CODE" => "brand_guid"
	);
	$resBrandGuid = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME", "CODE"));
	while($ar_fields = $resBrandGuid->GetNext()) {
		$allBrands[$ar_fields["NAME"]] = $ar_fields["CODE"];
	}
	
	//ORDER BY
	if(empty($arParams['SORT_BY']) || empty($arParams['SORT_ORDER'])) {
		$arSort = array(
			//"left_margin"=>"asc",
			"NAME" => "ASC",
		);
	} else {
		$arSort = array(
			$arParams['SORT_BY'] => $arParams['SORT_ORDER'],
		);
	}
	//EXECUTE
    
    // ��� � ��� ���������
    /*$iblockFilter = Array(
        "SITE_ID" => SITE_ID,
        "ACTIVE" => "Y",
        "CODE" => $arParams["FILTER_IBLOCK_CODE"]
    );
    
    $resIB = CIBlock::GetList(Array(), $iblockFilter, false);
    if($arResIB = $resIB->Fetch()) {
        $arFilter["IBLOCK_ID"] = $arResIB["ID"];
        $arFilter["IBLOCK_TYPE"] = $arResIB["IBLOCK_TYPE_ID"];
    }*/
    $arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
    $arFilter["IBLOCK_TYPE"] = $arParams["IBLOCK_TYPE"];
    if(!empty($arFilter["IBLOCK_ID"])) {
    
        $arFilterElements = Array(
            "IBLOCK_ID" =>  $arFilter["IBLOCK_ID"],
            "ACTIVE" => "Y",
            "PROPERTY_" . $arParams["FILTER_PROP_ID"] => $arParams["FILTER_PROP_VALUE"],
        );
		if (!empty($arParams["OTHER_CODE"]) && !empty($arParams["OTHER_VALUE"])) {
			$arFilterElements["PROPERTY_" . $arParams["OTHER_CODE"]] = $arParams["OTHER_VALUE"];
		}
		if($arParams["IBLOCK_ID"] != 50 && $arParams["IBLOCK_ID"] != 51) $arFilterElements["!CATALOG_PRICE_10"] = false;
		
        $res = CIBlockElement::GetList(Array(), $arFilterElements, Array("IBLOCK_SECTION_ID"));
        while($arFields = $res->GetNext()) {  
           $arFilter["ID"][] = $arFields["IBLOCK_SECTION_ID"];
        }
        if(!empty($arFilter["ID"])) {
            $rsSections = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
			
			// ����������� �������� � ������ ��� �������� (������ ��� ��������������)
			if ($arParams['FILTER_PROP_TYPE'] == 'vendor')
			{
				$arResult['NAV_BUTTONS_SECTIONS'] = array();
				while ($arSection = $rsSections->GetNext())
					$arResult['NAV_BUTTONS_SECTIONS'][] = array(
						'NAME' => $arSection['NAME'],
						'SECTION_PAGE_URL' => $arSection['SECTION_PAGE_URL']
					);
			}
			
            //�����������
            $rsSections->NavStart($arParams['SHOW_COUNT']); // ���� ���
            $arResult["NAV_TOP"] = $rsSections->GetPageNavStringEx($navComponentObject, "��������:", "nav_top"); //
            $arResult["NAV_BOT"] = $rsSections->GetPageNavStringEx($navComponentObject, "��������:", "nav_bot"); //
            //
			
			// ��������� � ������� ��������� �������
			$PROPERTY_ACTION_NEW_SALE = array();
			$res = CIBlockPropertyEnum::GetList(
				array(),
				array(
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"XML_ID" => array('987617dd-090b-11e0-87a1-00166f1a5311', 'd536afec-2269-11e0-9088-0018f34aa38f', 'a379a7ca-9a4a-11e0-b2a0-0018f34aa38f')
				)
			);
			while ($ar_res = $res->GetNext())
				$PROPERTY_ACTION_NEW_SALE[] = $ar_res['ID'];
			
            $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
            while($arSection = $rsSections->GetNext())
            {	
                /******����� ������ �������� �� ������ ������� �������� (���������� �� �����)******/
                $arSelect = Array("ID", "NAME", "PROPERTY_INTERIOR", "PROPERTY_BRAND");
                $arFilterPic = Array(
                    "IBLOCK_ID" => $arFilter["IBLOCK_ID"], 
                    "SECTION_ID" => $arSection["ID"],
                    "!PROPERTY_INTERIOR" => false,
                    "ACTIVE" => "Y",
                );
                $res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilterPic, false, Array("nTopCount" => 1), $arSelect);
                if($ar_res = $res->GetNext()) {
                    //$interiorArray = explode(",", $ar_res["PROPERTY_INTERIOR_VALUE"]);
					// �� ��� ����� ���� ����������� � ;
					$interiorArray = preg_split( "/[,;]+/", $ar_res["PROPERTY_INTERIOR_VALUE"]);
                    $arSection["PICTURE_PATH"] = trim($interiorArray[0]);
                    $arSection["BRAND"] = $ar_res["PROPERTY_BRAND_VALUE"];
					$arSection["BRAND_CODE"] = $allBrands[$arSection["BRAND"]];
                }

				if(empty($arSection["PICTURE_PATH"])) {
					/******����� ������ �������� �� ������ ������� �������� (���������� �� �����)******/
					$arSelect = Array("ID", "NAME", "PROPERTY_ITEM_PIC", "PROPERTY_BRAND");
					$arFilterPic = Array(
						"IBLOCK_ID" => $arFilter["IBLOCK_ID"],
						"SECTION_ID" => $arSection["ID"],
						"!PROPERTY_ITEM_PIC" => false,
						"ACTIVE" => "Y",
					);
					$res = CIBlockElement::GetList(Array(), $arFilterPic, false, Array("nTopCount" => 1), $arSelect);
					if ($ar_res = $res->GetNext()) {
						//$interiorArray = explode(",", $ar_res["PROPERTY_INTERIOR_VALUE"]);
						// �� ��� ����� ���� ����������� � ;
						$arSection["PICTURE_PATH"] = $ar_res["PROPERTY_ITEM_PIC_VALUE"];
						$arSection["BRAND"] = $ar_res["PROPERTY_BRAND_VALUE"];
						$arSection["BRAND_CODE"] = $allBrands[$arSection["BRAND"]];
					}
				}

				if(strlen($arSection["PICTURE_PATH"]) > 3){

					$arSection["PICTURE_PATH"] = trim($arSection["PICTURE_PATH"]);

					$file_path = '';
					if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $arSection["PICTURE_PATH"])) {
						$file_path = $_SERVER["DOCUMENT_ROOT"] . '/images/catalog/' . $arSection["PICTURE_PATH"];
					} elseif (file_exists($_SERVER["DOCUMENT_ROOT"] . '/upload/products_foto/' . $arSection["PICTURE_PATH"])) {
						$file_path = $_SERVER["DOCUMENT_ROOT"] . '/upload/products_foto/' . $arSection["PICTURE_PATH"];
					}

					//���� ���� ������ �� �����
					if (strlen($file_path) > 0) {
						
						//������
						$destinationFile = $_SERVER["DOCUMENT_ROOT"] . '/upload/resize_cache/section/185_152_' . $arSection["PICTURE_PATH"];
						$imgPath = '/upload/resize_cache/section/185_152_' . $arSection["PICTURE_PATH"];
						$resized = \CFile::ResizeImageFile(
							$file_path,
							$destinationFile,
							array('width' => 185, 'height' => 152),
							BX_RESIZE_IMAGE_EXACT
						);

						if ($resized) {
							$arSection["PRODUCT_IMAGE"] = $imgPath;
						}

					}

				}
				
				// ���������� ����� ��� ����� �� ������� ������ �������
				$arSection['LABELS'] = array();
				if (!empty($PROPERTY_ACTION_NEW_SALE))
				{
					unset($arFilterPic['!PROPERTY_INTERIOR']);
					$arFilterPic['PROPERTY_ACTION_NEW_SALE'] = $PROPERTY_ACTION_NEW_SALE;
					$res = CIBlockElement::GetList(array(), $arFilterPic, false, false, array('PROPERTY_ACTION_NEW_SALE'));
					while ($ar_res = $res->GetNext())
						if (!in_array($ar_res['PROPERTY_ACTION_NEW_SALE_VALUE'], $arSection['LABELS']))
							$arSection['LABELS'][] = $ar_res['PROPERTY_ACTION_NEW_SALE_VALUE'];
				}
				
                $arSelect = Array("ID", "IBLOCK_ID","NAME", "CATALOG_GROUP_10", "CATALOG_PRICE_10", "PROPERTY_CHANGE_UNIT");
                $arFilter = Array(
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"], 
                    "SECTION_ID" => $arSection["ID"],
                    //"PROPERTY_CHANGE_UNIT_VALUE" => "�2",
                    "!PROPERTY_CHANGE_UNIT_VALUE" => false,
                    "ACTIVE" => "Y",
                    ">CATALOG_PRICE_10" => 20
                );
                $res = CIBlockElement::GetList(Array("CATALOG_PRICE_10" => "ASC"), $arFilter, false, Array("nTopCount" => 1), $arSelect);
                if($ar_res = $res->GetNext()) {
                    $arSection["MIN_PRICE"] =  $ar_res["CATALOG_PRICE_10"];
                    $arSection["PROPERTY_CHANGE_UNIT"] = $ar_res["PROPERTY_CHANGE_UNIT_VALUE"];

                }

                /**********************************************************************************/
                if(isset($arSection["PICTURE"]))
                    $arSection["PICTURE"] = CFile::GetFileArray($arSection["PICTURE"]);

                $arButtons = CIBlock::GetPanelButtons(
                    $arSection["IBLOCK_ID"],
                    0,
                    $arSection["ID"],
                    array("SESSID"=>false)
                );
                $arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
                $arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];

                $arResult["SECTIONS"][]=$arSection;
            }
        }
    }
	$arResult["SECTIONS_COUNT"] = count($arResult["SECTIONS"]);
	
	$this->SetResultCacheKeys(array(
		"SECTIONS_COUNT",
		"SECTION",
		"IBLOCK_NAME",
		"ALL_SECTIONS_COUNT" /**/
	));

	$this->IncludeComponentTemplate();
}
$APPLICATION->AddViewContent("ALL_SECTIONS_COUNT", $arResult["ALL_SECTIONS_COUNT"], $pos); /**/
if($arResult["SECTIONS_COUNT"] > 0 || isset($arResult["SECTION"]))
{
	if(
		$USER->IsAuthorized()
		&& $APPLICATION->GetShowIncludeAreas()
		&& CModule::IncludeModule("iblock")
	)
	{
		$UrlDeleteSectionButton = "";
		if(isset($arResult["SECTION"]) && $arResult["SECTION"]['IBLOCK_SECTION_ID'] > 0)
		{
			$rsSection = CIBlockSection::GetList(
				array(),
				array("=ID" => $arResult["SECTION"]['IBLOCK_SECTION_ID']),
				false,
				array("SECTION_PAGE_URL")
			);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arSection = $rsSection->GetNext();
			$UrlDeleteSectionButton = $arSection["SECTION_PAGE_URL"];
		}

		if(empty($UrlDeleteSectionButton))
		{
			$url_template = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "LIST_PAGE_URL");
			$arIBlock = CIBlock::GetArrayByID($arParams["IBLOCK_ID"]);
			$arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
			$UrlDeleteSectionButton = CIBlock::ReplaceDetailURL($url_template, $arIBlock, true, false);
		}

		$arReturnUrl = array(
			"add_section" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"add_element" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"delete_section" => $UrlDeleteSectionButton,
		);
		$arButtons = CIBlock::GetPanelButtons(
			$arParams["IBLOCK_ID"],
			0,
			$arResult["SECTION"]["ID"],
			array("RETURN_URL" =>  $arReturnUrl)
		);

		$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
	}

	if($arParams["ADD_IBLOCK_CHAIN"] == 'Y' && intval($arParams['IBLOCK_ID']) > 0)
    {
        $rsIBlock = \CIBlock::GetByID(intval($arParams['IBLOCK_ID']));
        $rsIBlock = new CIBlockResult($rsIBlock);
        $arIBlock = $rsIBlock->GetNext();
        if(!empty($arIBlock)){
            $arIBlock['LIST_PAGE_URL'] = str_replace('_revizionnye', '', $arIBlock['LIST_PAGE_URL']);

            $APPLICATION->AddChainItem($arIBlock["NAME"], $arIBlock["LIST_PAGE_URL"]);
        }
    }

	if($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["SECTION"]) && is_array($arResult["SECTION"]["PATH"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}

}
?>
