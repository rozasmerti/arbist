<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["TYPE"] = htmlentities(addslashes($arParams["TYPE"]));    
if(empty($arParams["TYPE"])){
    $arParams["TYPE"] = "pol";
}
$TYPE = $arParams["TYPE"];
$VENDOR_CODE = $arParams["VENDOR_CODE"];


$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default

$arResult["SECTIONS"]=array();

/*************************************************************************
			Work with cache
*************************************************************************/
$arNavParams = array(
	"nPageSize" => 16, // Äëÿ êåøà
	"bDescPageNumbering" => "N",
	"bShowAll" => "N",
);
$arNavigation = CDBResult::GetNavParams($arNavParams);

if($this->StartResultCache(false, array($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups(), $arNavigation)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}    
    
    $arIblockIds = array();
    
    global $DB;
    $query = "SELECT 
                 b_iblock_element.IBLOCK_ID as IBLOCK_ID 
              FROM 
                 seo_association 
              INNER JOIN 
                 b_iblock_element 
              ON 
                 seo_association.element_id = b_iblock_element.ID 
              WHERE 
                 seo_association.type='" . $TYPE . "'
              GROUP BY IBLOCK_ID;";
    
    $res = $DB->Query($query);
    while($arRes = $res->Fetch()) {
        $arIblockIds[] = $arRes["IBLOCK_ID"];
    }
    
    $arParams["IBLOCK_ID"] = $arIblockIds;
        
	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CNT_ACTIVE" => "Y",
	);

	$arSelect = array();
	if(isset($arParams["SECTION_FIELDS"]) && is_array($arParams["SECTION_FIELDS"]))
	{
		foreach($arParams["SECTION_FIELDS"] as $field)
			if(is_string($field) && !empty($field))
				$arSelect[] = $field;
	}

	if(!empty($arSelect))
	{
		$arSelect[] = "ID";
		$arSelect[] = "NAME";
		$arSelect[] = "LEFT_MARGIN";
		$arSelect[] = "RIGHT_MARGIN";
		$arSelect[] = "DEPTH_LEVEL";
		$arSelect[] = "IBLOCK_ID";
		$arSelect[] = "IBLOCK_SECTION_ID";
		$arSelect[] = "LIST_PAGE_URL";
		$arSelect[] = "SECTION_PAGE_URL";
	}

	if(isset($arParams["SECTION_USER_FIELDS"]) && is_array($arParams["SECTION_USER_FIELDS"]))
	{
		foreach($arParams["SECTION_USER_FIELDS"] as $field)
			if(is_string($field) && preg_match("/^UF_/", $field))
				$arSelect[] = $field;
	}

	$arResult["SECTION"] = false;


	//ORDER BY
	$arSort = array(
		//"left_margin"=>"asc",
        "NAME" => "ASC",
	);
	//EXECUTE
    $arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
    $arFilter["IBLOCK_TYPE"] = $arParams["IBLOCK_TYPE"];
    if(!empty($arFilter["IBLOCK_ID"])) {

        $arFilterVendors = Array(
            "IBLOCK_ID" => VENDORS_IBLOCK_ID,
            "ACTIVE" => "Y",
             array(
                 "LOGIC" => "OR",
                 array("NAME" => $arParams["VENDOR_CODE"]),
                 array("CODE" => $arParams["VENDOR_CODE"]),
             )
        );
        
        $vendors = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilterVendors, false, false, Array("ID", "NAME", "CODE"));
        
        $VENDOR_ID_VALUE = 0;
        if($arVendor = $vendors->GetNext()) {
            $VENDOR_ID_VALUE = $arVendor["ID"];
            $VENDOR_NAME = $arVendor["NAME"];
        }
        
        /*title*/
        $BREADCRUMBS_TITLE = '';
        switch($TYPE){
            case 'pol':
                $BREADCRUMBS_TITLE = 'Ïðîèçâîäèòåëü ';
                break;
           case 'plitka':
                $BREADCRUMBS_TITLE = 'Ôàáðèêà ';
                break;
           default:
                $BREADCRUMBS_TITLE = 'Ïðîèçâîäèòåëü ';
                break;
        }
        
        $BREADCRUMBS_TITLE .= $VENDOR_NAME; 
        $arResult['BREADCRUMBS_TITLE'] = $BREADCRUMBS_TITLE;
        
		
		
		if($TYPE=='plitka'){
			$arResult['PAGE_TITLE'] = 'Ïëèòêà '.$VENDOR_NAME.': ïðîäàæà, öåíû íà ïëèòêó '.$VENDOR_NAME.' â Ìîñêâå';
			$arResult['H1'] = 'Ïëèòêà '.$VENDOR_NAME;
			$arResult['DESCRIPTION'] = 'Êóïèòü ïëèòêó '.$VENDOR_NAME.' îíëàéí â èíòåðíåò-ìàãàçèíå Àðáèñò. Äîñòàâêà ïëèòêè ïðîèçâîäñòâà '.$VENDOR_NAME.' ïî Ìîñêâå è Ðîññèè. Ãàðàíòèÿ êà÷åñòâà.';
			$arResult['UPPER_TEXT'] = 'Â ïðîäàæå ïëèòêà îò ïðîèçâîäèòåëÿ '.$VENDOR_NAME.' ïî íèçêîé öåíå ñ äîñòàâêîé ïî âñå ðåãèîíû.';
			$arResult['LOWER_TEXT'] = 'Ïëèòêà '.$VENDOR_NAME.' ñ ïîäðîáíûìè õàðàêòåðèñòèêàìè, ôîòî è öåíàìè.';
		}elseif($TYPE=='pol'){
			$arResult['PAGE_TITLE'] = 'Íàïîëüíûå ïîêðûòèÿ '.$VENDOR_NAME.': ïðîäàæà, öåíû íà íàïîëüíûå ïîêðûòèÿ '.$VENDOR_NAME.' â Ìîñêâå';
			$arResult['H1'] = 'Íàïîëüíûå ïîêðûòèÿ '.$VENDOR_NAME;
			$arResult['DESCRIPTION'] = 'Êóïèòü íàïîëüíîå ïîêðûòèå '.$VENDOR_NAME.' îíëàéí â èíòåðíåò-ìàãàçèíå Àðáèñò. Äîñòàâêà íàïîëüíûõ ïîêðûòèé ïðîèçâîäñòâà '.$VENDOR_NAME.' ïî Ìîñêâå è Ðîññèè. Ãàðàíòèÿ êà÷åñòâà.';
			$arResult['UPPER_TEXT'] = 'Â ïðîäàæå íàïîëüíûå ïîêðûòèÿ îò ïðîèçâîäèòåëÿ '.$VENDOR_NAME.' ïî íèçêîé öåíå ñ äîñòàâêîé ïî âñå ðåãèîíû.';
			$arResult['LOWER_TEXT'] = 'Íàïîëüíûå ïîêðûòèÿ '.$VENDOR_NAME.' ñ ïîäðîáíûìè õàðàêòåðèñòèêàìè, ôîòî è öåíàìè.';
		}else{
			$arResult['PAGE_TITLE'] = $VENDOR_NAME . '  — ïðîäàæà ïðîäóêöèè ' . $VENDOR_NAME . ' â Ìîñêâå';
			$arResult['H1'] = $VENDOR_NAME;
			$arResult['UPPER_TEXT'] = 'Â ïðîäàæå ñòðîèòåëüíûå òîâàðû ïðîèçâîäèòåëÿ ' . $VENDOR_NAME . ' ïî íèçêîé öåíå ñ äîñòàâêîé.';
			$arResult['LOWER_TEXT'] = 'Îòäåëî÷íûå ìàòåðèàëû îò '. $VENDOR_NAME . ' ñ ïîäðîáíûìè õàðàêòåðèñòèêàìè, ôîòî è öåíàìè.';
		}

		
        /*ïîëó÷àþ âñå id äëÿ ñâîéñòâà-ïðèâÿçêè "ïðîèçâîäèòåëü" äëÿ ðàçíûõ ÈÁ ýòîé ãðóïïû*/
        $rsProps = CIBlockProperty::GetList(
            array(),
            array(
                "ACTIVE"    => "Y",
                "PROPERTY_TYPE" => "E",
                "NAME"          => "Ïðîèçâîäèòåëü"
            )     
        );

        $arVendorPropIds = array();
        while($arProp = $rsProps -> GetNext()){
            if(in_array($arProp["IBLOCK_ID"], $arIblockIds)){
                $arVendorPropIds[] = $arProp["ID"];
            }
        }
        
        /*ñîáèðàåì ôèëüòð äëÿ ðàçíûõ ñâîéñòâ "ïðîèçâîäèòåëü"*/
        $arVendorFilter = array(
            "LOGIC" => "OR"
        );

        foreach($arVendorPropIds as $vendorPropId){ 
            $arVendorFilter[] = array(
                "PROPERTY_" . $vendorPropId => $VENDOR_ID_VALUE
            );
        }
        /**/
        
        $arFilterElements = Array(
            "IBLOCK_ID" =>  $arFilter["IBLOCK_ID"],
            "ACTIVE" => "Y",
        );
        
        if(count($arVendorFilter) > 1){
            $arFilterElements[] = $arVendorFilter;
        }
        
        
		// if($arParams["IBLOCK_ID"] 50 && $arParams["IBLOCK_ID"] != 51) $arFilterElements["!CATALOG_PRICE_10"] = false;
        if(!in_array(51, $arFilterElements["IBLOCK_ID"]) && !in_array(50, $arFilterElements["IBLOCK_ID"])){
            $arFilterElements["!CATALOG_PRICE_10"] = false;
        }    
		
        /*ïîëó÷àåì id-èíôîáëîêîâ, â êîòîðûõ åñòü àêòèâíûå òîâàðû äàííîãî ïðîèçâîäèòåëÿ (äëÿ êíîïîê)*/
        $arButtonsIblockIds = array();
        $rsElements = CIBlockElement::GetList(array(),$arFilterElements, array("IBLOCK_ID"));
        while( $arElement = $rsElements->GetNext() ) {
            if(intval($arElement["CNT"])){
                $arButtonsIblockIds[] = $arElement["IBLOCK_ID"];
            }     
        }
        
        
        
        $res = CIBlockElement::GetList(Array(), $arFilterElements, Array("IBLOCK_SECTION_ID"));
        while($arFields = $res->GetNext()) {  
           $arFilter["ID"][] = $arFields["IBLOCK_SECTION_ID"];
        }

        if(!empty($arFilter["ID"])) {
            $rsSections = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
            //Ïîñòðàíè÷êà
            $rsSections->NavStart(16); // Âûøå êåø
            $arResult["NAV_TOP"] = $rsSections->GetPageNavStringEx($navComponentObject, "Ñòðàíèöû:", "nav_top"); //
            $arResult["NAV_BOT"] = $rsSections->GetPageNavStringEx($navComponentObject, "Ñòðàíèöû:", "nav_bot"); //
            //
            $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
            while($arSection = $rsSections->GetNext())
            {	
                /******Áåðåì ïåðâóþ êàðòèíêó èç ñàìîãî ïåðâîãî ýëåìåíòà (ñîðòèðîâêà ïî èìåíè)******/
                $arSelect = Array("ID", "NAME", "PROPERTY_INTERIOR", "PROPERTY_BRAND");
                $arFilterPic = Array(
                    "IBLOCK_ID" => $arFilter["IBLOCK_ID"], 
                    "SECTION_ID" => $arSection["ID"],
                    "!PROPERTY_INTERIOR" => false,
                    "ACTIVE" => "Y",
                );
                $res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilterPic, false, Array("nTopCount" => 1), $arSelect);
                if($ar_res = $res->GetNext()) {
                    $interiorArray = explode(",", $ar_res["PROPERTY_INTERIOR_VALUE"]);
                    $arSection["PICTURE_PATH"] = trim($interiorArray[0]);
                    $arSection["BRAND"] = $ar_res["PROPERTY_BRAND_VALUE"];
                }

                $arSelect = Array("ID", "IBLOCK_ID","NAME", "CATALOG_GROUP_10", "CATALOG_PRICE_10", "PROPERTY_CHANGE_UNIT");
                $arFilter = Array(
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"], 
                    "SECTION_ID" => $arSection["ID"],
                    "PROPERTY_CHANGE_UNIT_VALUE" => "êâ.ì.",
                    "ACTIVE" => "Y"
                );
                $res = CIBlockElement::GetList(Array("CATALOG_PRICE_10" => "ASC"), $arFilter, false, Array("nTopCount" => 1), $arSelect);
                if($ar_res = $res->GetNext()) {
                    $arSection["MIN_PRICE"] =  $ar_res["CATALOG_PRICE_10"];
                    $arSection["PROPERTY_CHANGE_UNIT"] = $ar_res["PROPERTY_CHANGE_UNIT_VALUE"];

                }

                /**********************************************************************************/
                if(isset($arSection["PICTURE"]))
                    $arSection["PICTURE"] = CFile::GetFileArray($arSection["PICTURE"]);

                $arButtons = CIBlock::GetPanelButtons(
                    $arSection["IBLOCK_ID"],
                    0,
                    $arSection["ID"],
                    array("SESSID"=>false)
                );
                $arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
                $arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];

                $arResult["SECTIONS"][]=$arSection;
            }
        }
    }
	$arResult["SECTIONS_COUNT"] = count($arResult["SECTIONS"]);
	$arResult["VENDOR_ID_VALUE"] = $VENDOR_ID_VALUE;
    $arResult["TYPE"] = $TYPE;

	$this->SetResultCacheKeys(array(
		"SECTIONS_COUNT",
		"SECTION",
		"IBLOCK_NAME",
        "PAGE_TITLE",
        "VENDOR_ID_VALUE",
        "TYPE",
        "BREADCRUMBS_TITLE",
        "PAGE_TITLE",
        "H1",
		"DESCRIPTION",
		"ALL_SECTIONS_COUNT" /**/
	));
    
    
    /*äëÿ "êíîïîê"*/
    if($VENDOR_ID_VALUE && $TYPE && !empty($arButtonsIblockIds)){
        $arIblockInfo = array();
        $rsIblock = CIBlock::GetList(
            array(
                "SORT" => "ASC"
            ),
            array(
                "ID" => $arButtonsIblockIds
            )    
        );
        
        while($arIblock = $rsIblock -> GetNext()){
            $arIblockInfo[] = $arIblock;
        }

        
        foreach($arIblockInfo as $arIblock){
            $arResult['NAV_BUTTONS'][] = array(
                'PATH' => '/catalog/'.$arIblock['CODE'].'/vendor/'.$VENDOR_CODE.'/',
                'NAME' => $arIblock['NAME'] . ' ' . $VENDOR_NAME
            );	
        }
        /**/
        
        /*ïîëó÷èòü SEO-òåêñò*/
        $arFilter = array(
            "IBLOCK_ID"             => SEO_COMMON_VENDOR_TEXT_IBLOCK_ID,
            "ACTIVE"                => "Y",
            "ACTIVE_DATE"           => "Y",
            "PROPERTY_TYPE_VALUE"   => $TYPE,
            "PROPERTY_VENDOR"       => $VENDOR_ID_VALUE,
        );
        
        $rsElements = CIBlockElement::GetList(
            array(),
            $arFilter
        );
        
        $arResult["SEO_TEXT"] = '';
        while( $arElement = $rsElements->GetNext() ) {
            $arResult["SEO_TEXT"] = $arElement["DETAIL_TEXT"];
            $arResult["UPPER_SEO_TEXT"] = $arElement["PREVIEW_TEXT"];
        }
    }
	$this->IncludeComponentTemplate();
}
$APPLICATION->AddViewContent("ALL_SECTIONS_COUNT", $arResult["ALL_SECTIONS_COUNT"], $pos); /**/
if($arResult["SECTIONS_COUNT"] > 0 || isset($arResult["SECTION"]))
{
	if(
		$USER->IsAuthorized()
		&& $APPLICATION->GetShowIncludeAreas()
		&& CModule::IncludeModule("iblock")
	)
	{
		$UrlDeleteSectionButton = "";
		if(isset($arResult["SECTION"]) && $arResult["SECTION"]['IBLOCK_SECTION_ID'] > 0)
		{
			$rsSection = CIBlockSection::GetList(
				array(),
				array("=ID" => $arResult["SECTION"]['IBLOCK_SECTION_ID']),
				false,
				array("SECTION_PAGE_URL")
			);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arSection = $rsSection->GetNext();
			$UrlDeleteSectionButton = $arSection["SECTION_PAGE_URL"];
		}

		if(empty($UrlDeleteSectionButton))
		{
			$url_template = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "LIST_PAGE_URL");
			$arIBlock = CIBlock::GetArrayByID($arParams["IBLOCK_ID"]);
			$arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
			$UrlDeleteSectionButton = CIBlock::ReplaceDetailURL($url_template, $arIBlock, true, false);
		}

		$arReturnUrl = array(
			"add_section" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"add_element" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"delete_section" => $UrlDeleteSectionButton,
		);
		$arButtons = CIBlock::GetPanelButtons(
			$arParams["IBLOCK_ID"],
			0,
			$arResult["SECTION"]["ID"],
			array("RETURN_URL" =>  $arReturnUrl)
		);

		$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
	}

	/*if($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["SECTION"]) && is_array($arResult["SECTION"]["PATH"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}*/
}

if(!$arResult["TYPE"] || !$arResult["VENDOR_ID_VALUE"]){
    LocalRedirect('/404.php');
}

global $APPLICATION;
    
if($arResult["PAGE_TITLE"]){
    $APPLICATION -> SetTitle($arResult["PAGE_TITLE"]);
}    
if($arResult["BREADCRUMBS_TITLE"]){
    $APPLICATION->AddChainItem($arResult["BREADCRUMBS_TITLE"], '');
}
if($arResult["H1"]){
    $APPLICATION->SetPageProperty('h1',$arResult["H1"]);
} 
if($arResult["DESCRIPTION"]){
    $APPLICATION->SetPageProperty('description',$arResult["DESCRIPTION"]);
}    

?>
