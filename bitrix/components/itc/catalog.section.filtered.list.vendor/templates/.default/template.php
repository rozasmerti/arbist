<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$i = 0;
//echo "<pre>"; print_r($arResult["SECTIONS"][0]); echo "</pre>";
foreach($arResult["SECTIONS"] as $arSection){
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
?>
	<?if($i == 0) {?> 
		<div class="gap1 row"> 
	<?}?>
			<div class="item1 item" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
				<div class="image1 image">
					<div class="labels">

					</div>
					<a href="<?=$arSection["SECTION_PAGE_URL"]?>">
					<?if(!empty($arSection["PICTURE"])):?>
						<img src="/rpic/c/<?=$arSection["PICTURE"]?>_h93_w136_crop.jpg" alt=""/>
					<?else:?>
						<img src="/img/noimg0.jpg" alt="" width="136" height="93"/>
					<?endif;?>
					</a> 
				</div>
				<div class="desc">
					<h4 class="link0">��������� <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></h4>
					<div class="fontReduced hl0"><?=$arSection["DESCRIPTION"]?></div>
				</div>
			</div>
	<?if($i == 3) {
		$i = 0;
	?>
		</div> 
	<?
	} else {
		$i++;
	}
	?>
<?
}
if($i != 0) {
?>
	</div>
<?
}
?>
