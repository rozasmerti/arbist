<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? global $USER; ?>

<?if(!empty($arResult["SEO_TEXT"])){?>
    <div id="vendor-seo-text" class="vendor-texts">
        <?echo $arResult["SEO_TEXT"];?>
    </div>
<?}?>
<?if(!empty($arResult["UPPER_TEXT"])){?>
    <div class="vendor-texts">
        <?echo $arResult["UPPER_TEXT"];?>
    </div>
<?}?>
<?if(!empty($arResult["UPPER_SEO_TEXT"])){?>
    <div class="vendor-texts">
        <?echo $arResult["UPPER_SEO_TEXT"];?>
    </div>    
<?}?>

<?if(!empty($arResult['NAV_BUTTONS'])) {
	?>
	<div style="width: 100%; text-align: center; margin: 50px 0px;">
		<?
		foreach($arResult['NAV_BUTTONS'] as $button) {
			?>
			<div class="new_button full_width<?/* width50*/?>">
				<?/*<button onclick="window.location.href='<?=$button['PATH']?>'" type="button"><?=$button['NAME']?></button>*/?>
				<a class="a-button" href="<?=$button['PATH']?>" type="button"><?=$button['NAME']?></a>
				
				<? if (isset($arResult['NAV_BUTTONS_SECTIONS']) && isset($arResult['NAV_BUTTONS_SECTIONS'][$button['ID']])) : ?>
					<table style="width: 100%; vertical-align: top; text-align: left;">
						<tr>
							<td style="width: 33%; padding-left: 20px; vertical-align: top;">
					<?
						$i = 0;
						$threed = ceil(count($arResult['NAV_BUTTONS_SECTIONS'][$button['ID']]) / 3);
					?>
					<? foreach ($arResult['NAV_BUTTONS_SECTIONS'][$button['ID']] as $section) : ?>
					<?
						if ($i > 0 && ($i % $threed) == 0)
							echo '</td><td style="width: 33%; padding-left: 20px; vertical-align: top;">';
						$i++;
					?>
					<a class="mainlink" style="text-decoration: none;" href="<?= $section['SECTION_PAGE_URL'] ?>"><?= $section['NAME'] ?></a><br/>
					<? endforeach; ?>
							</td>
						</tr>
					</table>
				<? endif; ?>
			</div>
			<?
		}
		?>
		<div style="clear: both;"></div>
	</div>
	<?
}	
?>
<?
if(!empty($arResult["NAV_TOP"]))
	echo $arResult["NAV_TOP"];
$i = 0;

foreach($arResult["SECTIONS"] as $arSection){
    if($USER->IsAdmin()) {

    //    echo "<pre>". print_r($arSection).  "</pre>";
    }
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
?>
	<?if($i == 0) {?> 
		<div class="gap1 row"> 
	<?}?>
			<div class="item1 item b-item3-on-row" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
				<div class="image1 image b-light-border ceramic-img">
					<? if (!empty($arSection['LABELS'])) : ?>
					<div class="labels">
						<div class="lab"><span><?= implode(', ', $arSection['LABELS']) ?></span></div>
					</div>
					<? endif; ?>
					
					<a href="<?=$arSection["SECTION_PAGE_URL"]?>">
					<?if(!empty($arSection["PICTURE_PATH"]) && fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$arSection["PICTURE_PATH"],'r')):?> <?/*PICTURE*/?>
						<?$pic = split("\.", $arSection["PICTURE_PATH"]);?>
						<img src="/rpic_path/c/<?=$pic[0]?>_h152_w185_auto.<?=$pic[1]?>" alt=""/>
					<?elseif (!empty($arSection["COLLECTION_PICTURE"])):?>
						<img src="<?=$arSection["COLLECTION_PICTURE"]?>" alt="" style="height: 100%; width: 100%; object-fit: contain"/>
					<?else:?>
						<img src="/img/noimg0.jpg" alt="" width="136" height="93"/>
					<?endif;?>
					</a> 
				</div>
				<div class="desc">

					<div><a  class="light-red-link" href="<?=$arSection["SECTION_PAGE_URL"]?>"><span class="small-seo-text"><?=$APPLICATION->getProperty("h1");?></span><br/><?=$arSection["NAME"]?></a></div>
                    <? if (!empty($arSection["MIN_PRICE"]) && $arSection['PROPERTY_CHANGE_UNIT'] == '�2') { ?> <div class="minimal-category-price minimal-category-price_bg hl0">�� <?=round($arSection["MIN_PRICE"]);?> <span class="b-rub">�</span> �� <?=$arSection['PROPERTY_CHANGE_UNIT'];?></div> <?}?>
				</div>
			</div>
	<?if($i == 2) {
		$i = 0;
	?>
		</div> 
	<?
	} else {
		$i++;
	}
	?>
<?
}
if($i != 0) {
?>
	</div>
<?
}
if(!empty($arResult["NAV_BOT"]))
	echo $arResult["NAV_BOT"];
?>
<?if(!empty($arResult["LOWER_TEXT"])){?>
    <div class="vendor-texts">
        <?echo $arResult["LOWER_TEXT"];?>
    </div>
<?}?>
