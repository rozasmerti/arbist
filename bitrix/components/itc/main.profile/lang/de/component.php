<?
$MESS["USER_DONT_KNOW"] = "(unbekannt)";
$MESS["PROFILE_DEFAULT_TITLE"] = "Nutzerprofil";
$MESS["main_profile_sess_expired"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["main_profile_decode_err"] = "Fehler der Passwortentschlüsselung (#ERRCODE#).";
?>