<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}

//D'L ��� ������ ������ ��������� � ������� ��� ����� ����
$arResult["ITEMS_ADD_INFO"]["IDS"] = array();
foreach($arResult["ITEMS"] as $sid => $sElements) { //"PAGE_SECTIONS"
	//echo "<pre>"; print_r($sElements[0]); echo "</pre>";
	$arResult["ITEMS_ADD_INFO"]["IDS"][] = $sElements["ID"];
	$arResult["SECTIONS_COUNTRY"][$sid] = $sElements[0]["PROPERTIES"]["COUNTRY"]["VALUE"];
	$arResult["SECTIONS_BRAND_GUID"][$sid] = $sElements[0]["PROPERTIES"]["BRAND_GUID"]["VALUE"];
}
$arBrandGuidFilter = Array(   
	"IBLOCK_CODE" => "brand_guid",
	"ACTIVE" => "Y",
	"ID" => $arResult["SECTIONS_BRAND_GUID"]
);
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
while($ar_fields = $res->GetNext()) {
	$brands[$ar_fields["ID"]] = $ar_fields["NAME"]; /**/
}
foreach($arResult["SECTIONS_BRAND_GUID"] as $id => $value) {
	$arResult["SECTIONS_BRAND_GUID"][$id] = $brands[$value];
}


CModule::IncludeModule("catalog");
global $USER;
$userGroups = $USER->GetUserGroupArray();


$discountFlag = 0;
foreach($GLOBALS["GROUP_STATUS"] as $groupId => $val) {
	if(in_array($groupId, $userGroups)) {
		$discountFlag = 1;
		break;
	}
}


if($discountFlag == 1) {
	foreach($arResult["ITEMS"] as $sid=>$sElements)
	{
		foreach($sElements as $cell=>$arElement)
		{	
			$arResult["ITEMS"][$sid][$cell]["BASE_PRICES"] = $arResult["ITEMS"][$sid][$cell]["PRICES"]; //
			$optimalPrice = CCatalogProduct::GetOptimalPrice($arElement["ID"],1,$userGroups);
			$arResult["ITEMS"][$sid][$cell]["PRICES"] = array(
				array(
					"VALUE" => $optimalPrice["PRICE"]["PRICE"],
					"DISCOUNT_VALUE"=> $optimalPrice["DISCOUNT_PRICE"],
					"CURRENCY"=> $optimalPrice["PRICE"]["CURRENCY"],
				),
			);
		}
	}
}


if($arParams['FILTER_PROP_TYPE'] == 'vendor' && $arParams['FILTER_PROP_VALUE']) {
	
	CModule::IncludeModule('iblock');
	$arVendor = array();
	$res = CIBlockElement::GetByID($arParams['FILTER_PROP_VALUE']);
	if($ar_res = $res->GetNext()) 
		$arVendor = $ar_res;
		
	
	if($arVendor) {
		// ���� ��� ��������� ���� �������
		$arIblocks = array();
		$res = CIBlock::GetList(	
			Array(), 	
			Array(		
				'TYPE'=>'catalog', 		
				'SITE_ID'=>SITE_ID, 		
				'ACTIVE'=>'Y',
				'!ID' => array(12,29, $arParams['IBLOCK_ID']) // 12,29 - �������� ����������� ������
			), 
			false
		);
		while($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;
		
		//��� ������� ��������� ���� �� ��� ������ ����� ������
		foreach($arIblocks as $iblock) {
			
			$res = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock['ID'], "CODE" => 'BRAND_GUID'));
			if($res->fetch()) {
				$res = CIBlockElement::GetList( 
					Array("SORT"=>"ASC"), 
					Array('IBLOCK_ID' => $iblock['ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_GUID' => $arVendor['ID']), 
					false, 
					false, 
					Array('ID')
				);	
				if($res->SelectedRowsCount()) {
					$arResult['NAV_BUTTONS'][] = array(
						'PATH' => '/catalog/'.$iblock['CODE'].'/vendor/'.$arVendor['CODE'].'/',
						//'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblock['NAME'])
						'NAME' => $iblock['NAME'].' '.$arVendor['NAME']
					);			
				}
			}
		}
	}
	
}

//D'L � ��� � ������� �������� ���, ����������, � ������ �� ������������ ���������?
//������ ��� ��� (������� ��������) ������������� � ������ ������ ���� ������� � ������� ���� ������ � �� ���������

$res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ID"=>$arResult["ITEMS_ADD_INFO"]["IDS"]), false, false, array("ID","IBLOCK_ID","PROPERTY_".PROPERTY_CODE));
while ($ar = $res->Fetch())
{
	if(strlen($ar["PROPERTY_".PROPERTY_CODE."_VALUE"])>2)
	{
		$arResult["ITEMS_ADD_INFO"]["ART"][$ar["ID"]]=$ar["PROPERTY_".PROPERTY_CODE."_VALUE"];
	}
}



?>