<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<h2 id="complect">������������� ������:</h2>
<p>
    ���� �� ���������� ���� ����� �� ����� ������� �������������, ���� �� �������� ��������� �������� ���������
    ���������, ������� ����� �� �����, ������� � ������� ��������� ��������� � ��������� ���� ������.
</p>

<? CModule::IncludeModule('itconstruct.resizer'); ?>
<? if (!empty($arResult["SECTIONS"])) { ?> <!--$arResult["PAGE_SECTIONS"]-->
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING_TOP"] ?><br/>
    <? endif; ?>

    <? $sectionsCount = count($arResult["SECTIONS"]) ?> <!--$arResult["PAGE_SECTIONS"]-->
    <? foreach ($arResult["ITEMS"] as $sid => $sElements): ?> <!--$arResult["PAGE_SECTIONS"]-->
        <div class="gap1" style="position: relative;">
            <h3 class="link0 alt0">
                <?= $arResult["IBLOCKS"][$sid]['NAME'] ?>
            </h3>
        </div>

        <!---->
        <div class="gap4 b-gap4">
        <? foreach ($sElements as $cell => $arElement): ?>
            <?
            $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arElement["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arElement["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
            ?>
            <? //echo "<pre>"; print_r($arElement["PROPERTIES"]["ACTION_NEW_SALE"]); echo "</pre>"?>
            <? if ($cell % $arParams["LINE_ELEMENT_COUNT"] == 0): ?>
                <div class="gap1 row">
            <? endif; ?>
            <div class="item1 item b-item3-on-row" id="<?= $this->GetEditAreaId($arElement['ID']); ?>">
                <? if (isset($arResult["ITEMS_ADD_INFO"]["ART"][$arElement['ID']]) && strlen($arResult["ITEMS_ADD_INFO"]["ART"][$arElement['ID']]) > 2): ?>
                    ���: <?= $arResult["ITEMS_ADD_INFO"]["ART"][$arElement['ID']] ?>
                <? endif; ?>
                <a name="product<?= $arElement["ID"] ?>"></a>
                <? if (is_array($arElement["DETAIL_PICTURE"])): ?>
                <a id="photo_<?= $arElement["ID"] ?>" class="nodecor" href="<?= $arElement["DETAIL_PICTURE"]["SRC"] ?>"
                   rel="lightbox<?= $arElement["ID"] ?>">
                    <? elseif (is_array($arElement["PREVIEW_PICTURE"])): ?>
                    <a id="photo_<?= $arElement["ID"] ?>" class="nodecor"
                       href="<?= $arElement["PREVIEW_PICTURE"]["SRC"] ?>" rel="lightbox<?= $arElement["ID"] ?>">
                        <? else: ?>
                        <a id="photo_<?= $arElement["ID"] ?>" class="nodecor"
                           href="<?= $arElement["DETAIL_PAGE_URL"] ?>" rel="lightbox<?= $arElement["ID"] ?>">
                            <? endif; ?>
                            <div id="photo" class="image1 image b-light-border ceramic-img">
                                <? //foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                <? $arPrice = array_shift($arElement["PRICES"]) ?>
                                <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                                    <div class="labels">
                                        <div class="lab"><span>������</span></div>
                                    </div>
                                <? elseif ($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "987617dd-090b-11e0-87a1-00166f1a5311" ||
                                    $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "d536afec-2269-11e0-9088-0018f34aa38f" ||
                                    $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "a379a7ca-9a4a-11e0-b2a0-0018f34aa38f"
                                ): ?>
                                    <div class="labels">
                                        <div class="lab">
                                            <span><?= $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE"] ?></span>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if (is_array($arElement["PREVIEW_PICTURE"])): ?>
                                    <? if ($arElement["PREVIEW_PICTURE"]["HEIGHT"] > 93 || $arElement["PREVIEW_PICTURE"]["WIDTH"] > 136): ?>
                                    <? if (($arElement["PREVIEW_PICTURE"]["HEIGHT"] - 93) > ($arElement["PREVIEW_PICTURE"]["WIDTH"] - 136)): ?>
                                <img
                                    src="<?= itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'height', null, 152) ?>"
                                    alt="<?= $arElement["NAME"]; ?>"/>
                                <? else: ?>
                                <img src="<?= itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'width', 185) ?>"
                                     alt="<?= $arElement["NAME"]; ?>"/>
                                <? endif; ?>
                                <? else: ?>
                                <img
                                    src="<?= itc\Resizer::get($arElement["PREVIEW_PICTURE"]["ID"], 'auto', 185, 152) ?>"
                                    alt=""/>
                                <? endif; ?>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#photo_<?=$arElement["ID"]?>').lightBox();
                                        });
                                    </script>
                                <? elseif (is_array($arElement["DETAIL_PICTURE"])): ?>
                                <? if ($arElement["DETAIL_PICTURE"]["HEIGHT"] > 93 || $arElement["DETAIL_PICTURE"]["WIDTH"] > 136): ?>
                                <? if (($arElement["DETAIL_PICTURE"]["HEIGHT"] - 93) > ($arElement["DETAIL_PICTURE"]["WIDTH"] - 136)): ?>
                                <img
                                    src="<?= itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'height', null, 152) ?>"
                                    alt="<?= $arElement["NAME"]; ?>"/>
                                <? else: ?>
                                <img src="<?= itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'width', 185) ?>"
                                     alt="<?= $arElement["NAME"]; ?>"/>
                                <? endif; ?>
                                <? else: ?>
                                <img src="<?= itc\Resizer::get($arElement["DETAIL_PICTURE"]["ID"], 'auto', 185, 152) ?>"
                                     alt="<?= $arElement["NAME"]; ?>"/>
                                <? endif; ?>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#photo_<?=$arElement["ID"]?>').lightBox();
                                        });
                                    </script>
                                <? else: ?>
                                <? /*<a id="photo_<?=$arElement["ID"]?>" href="<?=$arElement["DETAIL_PAGE_URL"]?>" rel="lightbox<?=$arElement["ID"]?>">*/ ?>
                                <img src="/img/noimg0.jpg" alt="" width="136" height="93"/><? /*</a>*/ ?>
                                <? endif ?>
                            </div>
                        </a>
                        <h5 class="desc0 desc link3"><a
                                href="<?= $arElement["DETAIL_PAGE_URL"] ?>"><?= $arElement["NAME"] ?></a></h5>
                        <div class="bot">
                            <div class="aligned row-b">
                                <? if (canBuyElement($arElement) && (intVal($arPrice["VALUE"]) > 0)) { ?>
                                    <div class="item"><?
                                        if ($arPrice["VALUE"] > 5) {
                                            ?>
                                            <div class="price0 price minimal-category-price_bg">
                                            <span class="val">
											<? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
                                                $val = round($arPrice["DISCOUNT_VALUE"]);
                                                if (!empty($arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"]))
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]) . "&nbsp;��&nbsp;" . $arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"];
                                                else
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]);
                                                echo str_replace("���", '<span class="b-rub">�</span>', $priceVal);;
                                            } else {
                                                $val = round($arPrice["VALUE"]);
                                                if (!empty($arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"]))
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]) . "&nbsp;��&nbsp;" . $arElement["PROPERTIES"]["CHANGE_UNIT"]["VALUE"];
                                                else
                                                    $priceVal = str_replace("#", $val, $arResult["CURRENCY_ARRAY"][$arPrice["CURRENCY"]]);
                                                echo str_replace("���", '<span class="b-rub">�</span>', $priceVal);;
                                            }
                                            ?>
										</span>
                                            </div><?
                                        }
                                        ?></div>
                                    <div class="item">
                                        <? if ($arElement["CAN_BUY"]): ?>
                                            <form action="#">
                                                <fieldset>
                                                    <div id="buy<?= $arElement["ID"] ?>" class="button4 button">
                                                        <i class="helper"></i>
                                                        <?
                                                        $arElement["ADD_URL"] .= "#product" . $arElement["ID"];
                                                        ?>
                                                        <button type="button"
                                                                onclick="location.href='<?= $arElement["ADD_URL"] ?>'">
                                                            <i></i></button>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        <? endif; ?>
                                    </div>
                                <? } else { ?>
                                    <div class="item">
                                        <div class="new_button get_price">
                                            <a class="a-button" href="/price-request/?productId=<?= $arElement['ID'] ?>"
                                               type="button" target="_blank">�������� ������� � ����</a>
                                        </div>
                                    </div>
                                <? } ?>
                                <span class="under"></span>
                            </div>
                            <? if (($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "987617dd-090b-11e0-87a1-00166f1a5311" ||
                                    $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "d536afec-2269-11e0-9088-0018f34aa38f") && ($arElement["PROPERTIES"]["STRIP_COUNT"]["VALUE"] != '')
                            ): ?>
                                <div class="old">
                                    <span class="price1 price"><span
                                            class="val"><? echo trim($arElement["PROPERTIES"]["STRIP_COUNT"]["VALUE"]); ?></span></span>
                                    <span class="b-rub b-old-ruble">�</span>&nbsp;&mdash; ������ ����
                                </div>
                            <? endif; ?>

                            <? //endforeach;?>
                        </div>
            </div>

            <? $cell++;
            if ($cell % $arParams["LINE_ELEMENT_COUNT"] == 0):?>
                </div>
            <? endif ?>

        <? endforeach; // foreach($arResult["ITEMS"] as $arElement):?>

        <? if ($cell % $arParams["LINE_ELEMENT_COUNT"] != 0): ?>
            </div>
        <? endif ?>

        </div>
        <? //=$arResult["DESCRIPTIONS"][$sid]?>
    <? endforeach; ?>

    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING_BOT"] ?>
    <? endif; ?>

    <?
} ?>