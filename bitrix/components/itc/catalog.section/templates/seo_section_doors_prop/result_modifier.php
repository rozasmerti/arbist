<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}

foreach($arResult["ITEMS"] as $sid => $sElements) { //"PAGE_SECTIONS"
	//echo "<pre>"; print_r($sElements[0]); echo "</pre>";
	$arResult["SECTIONS_COUNTRY"][$sid] = $sElements[0]["PROPERTIES"]["COUNTRY"]["VALUE"];
	$arResult["SECTIONS_BRAND_GUID"][$sid] = $sElements[0]["PROPERTIES"]["BRAND_GUID"]["VALUE"];
}
$arBrandGuidFilter = Array(   
	"IBLOCK_CODE" => "brand_guid",
	"ACTIVE" => "Y",
	"ID" => $arResult["SECTIONS_BRAND_GUID"]
);
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME", "CODE"));
while($ar_fields = $res->GetNext()) {
	$brands[$ar_fields["ID"]] = $ar_fields; /**/
}
foreach($arResult["SECTIONS_BRAND_GUID"] as $id => $value) {
	$arResult["SECTIONS_BRAND_GUID"][$id] = $brands[$value];
}


CModule::IncludeModule("catalog");
global $USER;
$userGroups = $USER->GetUserGroupArray();


$discountFlag = 0;
foreach($GLOBALS["GROUP_STATUS"] as $groupId => $val) {
	if(in_array($groupId, $userGroups)) {
		$discountFlag = 1;
		break;
	}
}


if($discountFlag == 1) {
	foreach($arResult["ITEMS"] as $sid=>$sElements)
	{
		foreach($sElements as $cell=>$arElement)
		{	
			$arResult["ITEMS"][$sid][$cell]["BASE_PRICES"] = $arResult["ITEMS"][$sid][$cell]["PRICES"]; //
			$optimalPrice = CCatalogProduct::GetOptimalPrice($arElement["ID"],1,$userGroups);
			$arResult["ITEMS"][$sid][$cell]["PRICES"] = array(
				array(
					"VALUE" => $optimalPrice["PRICE"]["PRICE"],
					"DISCOUNT_VALUE"=> $optimalPrice["DISCOUNT_PRICE"],
					"CURRENCY"=> $optimalPrice["PRICE"]["CURRENCY"],
				),
			);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////// �������� ��������� ����
CModule::IncludeModule("catalog");
$COMPLECT_IBLOCK_ID = intval($arParams["COMPLECT_IBLOCK_ID"]);



$polotnoXmlIds = $arParams["XML_ID_COMPLECT"];

$complectTypePolotno = CIBlockPropertyEnum::GetList(array(),array("IBLOCK_ID"=>$COMPLECT_IBLOCK_ID,"CODE"=>"COMPLECT_TYPE","XML_ID"=>$polotnoXmlIds))->fetch();// �������

$basePriceType = CCatalogGroup::GetBaseGroup();
$basePriceTypeID = intval($basePriceType["ID"]);
if($basePriceTypeID > 0)
{
	foreach($arResult["ITEMS"] as $sid=>$sElements)
	{
        global $USER;
		foreach($sElements as $cell=>$arElement)
		{	
			$xmlIds = $arElement["PROPERTIES"]["COMPLECT"]["VALUE"];
			if(is_array($xmlIds) && count($xmlIds) > 0)
			{
				$filter = array(
					"IBLOCK_ID"=>$COMPLECT_IBLOCK_ID,
					"ACTIVE"=>"Y",
					"XML_ID"=>$xmlIds,
					"PROPERTY_COMPLECT_TYPE"=>$complectTypePolotno["ID"],
				);
				
				//PR($xmlIds);
				
				
				if($minPriceComplect = CIBlockElement::GetList(array("CATALOG_PRICE_".$basePriceTypeID=>"ASC"),$filter,false,array("nTopCount"=>1),array("ID","IBLOCK_ID","XML_ID","PROPERTY_CHANGE_UNIT","CATALOG_GROUP_".$basePriceTypeID))->fetch())
				{
					/*
					PR(array(
						"XML_ID"=>$minPriceComplect["XML_ID"],
						"PRICE"=>$minPriceComplect["CATALOG_PRICE_".$basePriceTypeID],
					));
					*/
					
					$price = floatval($minPriceComplect["CATALOG_PRICE_10"]);
					$price_f = number_format($price, 0,"."," ");
					$arResult["ITEMS"][$sid][$cell]["PRICE_FROM"] = $price;
					$arResult["ITEMS"][$sid][$cell]["PRICE_FROM_F"] = $price_f;
					$arResult["ITEMS"][$sid][$cell]["CHANGE_UNIT"] = $minPriceComplect["PROPERTY_CHANGE_UNIT_VALUE"];
				}
			}
			
			
			
			
		}
	}
}


if($arParams['FILTER_PROP_TYPE'] == 'vendor' && $arParams['FILTER_PROP_VALUE']) {
	
	CModule::IncludeModule('iblock');
	$arVendor = array();
	$res = CIBlockElement::GetByID($arParams['FILTER_PROP_VALUE']);
	if($ar_res = $res->GetNext()) 
		$arVendor = $ar_res;
		
	
	if($arVendor) {
		// ���� ��� ��������� ���� �������
		$arIblocks = array();
		$res = CIBlock::GetList(	
			Array(), 	
			Array(		
				'TYPE'=>'catalog', 		
				'SITE_ID'=>SITE_ID, 		
				'ACTIVE'=>'Y',
				'!ID' => array(12,29, $arParams['IBLOCK_ID']) // 12,29 - �������� ����������� ������
			), 
			false
		);
		while($ar_res = $res->Fetch()) $arIblocks[] = $ar_res;
		
		//��� ������� ��������� ���� �� ��� ������ ����� ������
		foreach($arIblocks as $iblock) {
			
			$res = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock['ID'], "CODE" => 'BRAND_GUID'));
			if($res->fetch()) {
				$res = CIBlockElement::GetList( 
					Array("SORT"=>"ASC"), 
					Array('IBLOCK_ID' => $iblock['ID'], 'ACTIVE' => 'Y', 'PROPERTY_BRAND_GUID' => $arVendor['ID']), 
					false, 
					false, 
					Array('ID')
				);	
				if($res->SelectedRowsCount()) {
					$arResult['NAV_BUTTONS'][] = array(
						'PATH' => '/catalog/'.$iblock['CODE'].'/vendor/'.$arVendor['NAME'].'/',
						'NAME' => '��� ��������� '.$arVendor['NAME'].' '.mb_strtolower($iblock['NAME'])
					);			
				}
			}
		}
	}
	
}


?>
