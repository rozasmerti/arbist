<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "PROP_NAME" => Array(
        "PARENT" => "BASE",
        "NAME" => "Название свойства",
        "TYPE" => "STRING",
        "DEFAULT" => "",
    )
);
?>