<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING_TOP"]?><br />
	<?endif;?>
<?//echo "<pre>"; print_r($arResult); echo "</pre>";?>
<?$sectionsCount = count($arResult["ITEMS"])?>
<?foreach($arResult["ITEMS"] as $sid=>$sElements):?>
	<?if($sectionsCount == 1):?>
	<h3 class="catalog"><?=$arResult["SECTION_NAME"][$sid]?></h3>
	<?else:?>
	<div class="gap1">
		<h3 class="link0 alt0">��������� <a href="<?=$arResult["SECTION_PATH"][$sid]?>"><?=$arResult["SECTION_NAME"][$sid]?></a></h3>
		<div class="fontReduced hl0"></div> <!--���������, �������������-->
	</div>
	<?endif;?>
	<!--���� �����������-->

	<?if(!empty($arResult["GALLERY"][$sid])):?>
	<div class="gap4 gal">
		<ul id="gallery">
		<?foreach($arResult["GALLERY"][$sid] as $pictureId):?>
			<li>
				<!--68 x 50-->
				<img src="/rpic/c/<?=$pictureId?>_h50_w68_crop.jpg" alt=""/>

				<div class="panel-content">
					<!--400 x 296-->
					<img src="/rpic/c/<?=$pictureId?>_h296_w400_crop.jpg" alt=""/>
				</div>
			</li>
		<?endforeach;?>
		</ul>
	</div>
	<?endif;?>
	<!---->
	<div class="gap4">
			<?foreach($sElements as $cell=>$arElement):?>
			<?
			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
			?>
			<?if($cell%$arParams["LINE_ELEMENT_COUNT"] == 0):?>
			<div class="gap1 row">
			<?endif;?>

			<div class="item1 item" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
				<div class="image1 image">
			<?//foreach($arElement["PRICES"] as $code=>$arPrice):?>
			<?$arPrice = array_shift($arElement["PRICES"])?>
				<?if($arPrice["PRINT_DISCOUNT_VALUE"] < $arPrice["PRINT_VALUE"]):?>
					<div class="labels">
						<div class="lab"><span>������</span></div>
					</div>
				<?elseif($arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "987617dd-090b-11e0-87a1-00166f1a5311" || 
						 $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "d536afec-2269-11e0-9088-0018f34aa38f" ||  
						 $arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE_XML_ID"] == "a379a7ca-9a4a-11e0-b2a0-0018f34aa38f"):?>
                    <div class="labels">
						<div class="lab"><span><?=$arElement["PROPERTIES"]["ACTION_NEW_SALE"]["VALUE"]?></span></div>
					</div>
				<?endif;?>
					<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="/rpic/c/<?=$arElement["PREVIEW_PICTURE"]["ID"]?>_h93_w136_crop.jpg" alt=""/></a>
					<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="/rpic/c/<?=$arEl