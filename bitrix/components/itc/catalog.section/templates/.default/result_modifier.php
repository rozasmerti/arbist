<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$currencyArray = array("RUB", "USD", "EUR");
foreach($currencyArray as $val) {
	$result = CCurrencyLang::GetByID($val, "ru");
	$arResult["CURRENCY_ARRAY"][$val] = $result["FORMAT_STRING"];
}

/*global $USER;
if($USER->IsAdmin()) {*/
	//echo "<pre>"; print_r($arResult); echo "</pre>";
	if(count($arResult["IBLOCK_ID"]) > 1)
		$iblockID = $arResult["IBLOCK_ID"][0];
	else
		$iblockID = $arResult["IBLOCK_ID"];
	$arSelect = Array("IBLOCK_SECTION_ID");
	$arFilter = Array(
		"IBLOCK_ID" => 47, 
		"ACTIVE" => "Y",
		"PROPERTY_IBLOCK_ID" => $iblockID
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1), $arSelect);
	if($arRes = $res->GetNext()) {
		
		$arFilter = Array(
			"IBLOCK_ID" => 47,
			"GLOBAL_ACTIVE" => "Y",
			"ACTIVE" => "Y",
			"ID" => $arRes["IBLOCK_SECTION_ID"],
		);
		$db_list = CIBlockSection::GetList(Array(), $arFilter, false);
		if($ar_result = $db_list->GetNext()) {
			$arResult["SECTION_XML_ID"] = $ar_result["XML_ID"];
		}
	}
/*}*/
foreach($arResult["ITEMS"] as $sid => $sElements) { //"PAGE_SECTIONS"
	//echo "<pre>"; print_r($sElements[0]); echo "</pre>";
	$arResult["SECTIONS_COUNTRY"][$sid] = $sElements[0]["PROPERTIES"]["COUNTRY"]["VALUE"];
	$arResult["SECTIONS_BRAND_GUID"][$sid] = $sElements[0]["PROPERTIES"]["BRAND_GUID"]["VALUE"];
}
$arBrandGuidFilter = Array(   
	"IBLOCK_CODE" => "brand_guid",
	"ACTIVE" => "Y",
	"ID" => $arResult["SECTIONS_BRAND_GUID"]
);
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
while($ar_fields = $res->GetNext()) {
	$brands[$ar_fields["ID"]] = $ar_fields["NAME"]; /**/
}
foreach($arResult["SECTIONS_BRAND_GUID"] as $id => $value) {
	$arResult["SECTIONS_BRAND_GUID"][$id] = $brands[$value];
}
?>