<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
//Узнаю число коллекций
/*foreach($arResult["arrProp"] as $prop) {
    if($prop["CODE"] == "COLLECTION") {
        $arResult["COLLECTION_COUNT"] = count($prop["VALUE_LIST"]);
        break;
    }
}*/
//$APPLICATION->ShowViewContent("ALL_SECTIONS_COUNT");
$arResult["COLLECTION_COUNT"] = count($arResult["arrSection"]);
switch($arResult["COLLECTION_COUNT"]%10) {
    case 1: $arResult["COLLECTION_TITLE"] = "коллекция"; break;
    case 2: case 3: case 4: $arResult["COLLECTION_TITLE"] = "коллекции"; break;
    default:  $arResult["COLLECTION_TITLE"] = "коллекций";
}
//Раскидываю свойства по 3-ем колонкам 
$i = 1;
$columns = array();
//global $USER;


/*
function cmp($a, $b) 
{
    if(!empty($a["NAME"]) && empty($b["NAME"]))return -1;
    if(empty($a["NAME"]) && !empty($b["NAME"]))return 1;
    if(empty($a["NAME"]) && empty($b["NAME"]))return 0;
    
    if ($a["NAME"] == $b["NAME"])
    {
        return 0;
    }
    
    return (strcmp($a,$b) > 0) ? -1 : 1;
}
usort($arResult["ITEMS"], "cmp");
*/


foreach($arResult["ITEMS"] as $arItem)
{
    //if($USER->IsAdmin()) {echo "<pre>"; print_r($arItem); echo "</pre>";}
    if(!array_key_exists("HIDDEN", $arItem)) {
        $columnsValues = array();
        $j = 1;
        $n = 0;
        //3 колонки для критерия "Вид товара"
        //if($arItem["CODE"] == "APPLICATION") 
            $colums = 3;
        /*else
            $colums = 4;*/
        // Удаляю <> из массива 
        $delKeys = array_keys($arItem["VALUES"], "&lt;&gt;");
        foreach($delKeys as $key)
            unset($arItem["VALUES"][$key]);
        
        $columnCount = ceil(count($arItem["VALUES"])/$colums);
        asort($arItem["VALUES"]);
        foreach($arItem["VALUES"] as $key => $val) {
            /*$columnsValues[$j][$key] = $val;
            if($j == 4)
                $j = 1;
            else
                $j++;*/
            
            $columnsValues[$j][$key] = $val;
            $n++;
            if($n >= $columnCount) {
                $j++; 
                $n = 0;
            }
        }
        $arItem["COLUMNS"] = $columnsValues;
        $arItem["COLUMNS_COUNT"] = count($arItem["VALUES"]);
        $columns[$i][] = $arItem;
        if($i == 3)
            $i = 1;
        else
            $i++;
    }
}
$arResult["COLUMNS"] = $columns;
//echo "<pre>"; print_r($arResult["COLUMNS"]); echo "</pre>";
//global $USER;
//Мин и Макс цены раздела
$GLOBALS["MIN_PRICE_VALUE"] = NULL;
$GLOBALS["MAX_PRICE_VALUE"] = NULL;

// $prices = array(array("ID" => $arResult["FILTER_PRICE_ID"]));

$price = CCatalogGroup::GetList(array(),array("NAME"=>$arParams["PRICE_CODE"]))->fetch();
$priceID = intval($price["ID"]);

$COMPLECT_IBLOCK_ID = intval($arParams["COMPLECT_IBLOCK_ID"]);

if($COMPLECT_IBLOCK_ID > 0)
{
    $arFilter = Array(
        "IBLOCK_ID" => $COMPLECT_IBLOCK_ID, 
        "ACTIVE" => "Y"
    );
    
    $polotnoXmlIds = $arParams["XML_ID_COMPLECT"];
    $complectTypePolotno = CIBlockPropertyEnum::GetList(array(),array("IBLOCK_ID"=>$COMPLECT_IBLOCK_ID,"CODE"=>"COMPLECT_TYPE","XML_ID"=>$polotnoXmlIds))->fetch();// полотно
    $arFilter["PROPERTY_COMPLECT_TYPE"] = $complectTypePolotno["ID"];
    $arFilter[">CATALOG_PRICE_" . $price["ID"]] = 0;
    
    //Min
    $res = CIBlockElement::GetList(Array("CATALOG_PRICE_" . $price["ID"] => "ASC"), $arFilter, false, Array("nTopCount" => 1));
    if($ar_res = $res->GetNext())
    {
        $GLOBALS["MIN_PRICE_VALUE"] = floor($ar_res["CATALOG_PRICE_" . $price["ID"]]);
    }
    
    //Max
    $res = CIBlockElement::GetList(Array("CATALOG_PRICE_" . $price["ID"] => "DESC"), $arFilter, false, Array("nTopCount" => 1));
    if($ar_res = $res->GetNext())
    {
        $GLOBALS["MAX_PRICE_VALUE"] = ceil($ar_res["CATALOG_PRICE_" . $price["ID"]]);
        if(floatval($GLOBALS["MAX_PRICE_VALUE"]) > 0)
        {
            $userGroups = $USER->GetUserGroupArray();
            $optimalPrice = CCatalogProduct::GetOptimalPrice($ar_res ["ID"],1,$userGroups);
            $GLOBALS["MAX_PRICE_VALUE"] = $optimalPrice["PRICE"]["PRICE"];
            $GLOBALS["MAX_PRICE_VALUE"] = floor($GLOBALS["MAX_PRICE_VALUE"]/50)*50 + 50;
        }
    }
}



//echo "<pre>"; print_r($prices); echo "</pre>";
?>

