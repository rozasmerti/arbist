<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?//echo "<pre>"; print_r($arResult["SECTIONS"]); echo "</pre>";?>

<div class="gap1">
	<div class="block3">
		<div class="r-border-shape">
			<div class="tb">
				<div class="cn l"></div>
				<div class="cn r"></div>
			</div>
			<div class="d dl"><i></i></div>
			<div class="d dr"><i></i></div>
			<div class="d dc"><i></i></div>
			<div class="cnt">
				<div class="menu-a">
					<ul class="list">
					<?$i = 0;?>
					<?foreach($arResult["SECTIONS"] as $key => $section):?>
						<?if($i == 0):?>
							<li class="current">
						<?else:?>
							<li>
						<?endif?>
								<a href="/catalog/<?=$arResult["IBLOCKS_INFO"][$section["IBLOCK_INFO"]]["CODE"]?>/"><span><?=$section["NAME"]?></span></a>
								<div class="image">
									<?if(!empty($section["PICTURE_ID"])):?>
										<?/*<img src="/rpic/c/<?=$section["PICTURE_ID"]?>_h153_w211_crop.jpg" alt=""/>*/?>
										<img src="<?=itc\Resizer::get($section["PICTURE_ID"], 'crop', 211, 153)?>" alt=""/>
									<?else:?>
										<img src="/img/noimg0.jpg" alt=""/>
									<?endif;?>
								</div>
							</li>
					<?$i++;?>
					<?endforeach;?>
					</ul>
				</div>
			</div>

			<div class="bb">
				<div class="cn l"></div>
				<div class="cn r"></div>
			</div>
		</div>
	</div>
</div>
<div id="mainRightBlock" class="gap1">

    <?if(!empty($arResult["COUNTRY_LIST"])):?>
	<?ksort($arResult["COUNTRY_LIST"]);?>
    <h3 class="shift0">������</h3>
	<div class="shift0">
		<div class="row">
                <?
                    $i = 0;
                    $count = count($arResult["COUNTRY_LIST"]);
                    $max = round($count/4);
                    $maxStep = $max;
                    $step = 0;
                ?>
				<?foreach($arResult["COUNTRY_LIST"] as $name => $iblocks):?>
                    <?if($i == 0 || ($i == $max && $step != 4)):
                        $max = $maxStep + $i;
                        $step++;
                    ?>
                        <div class="item0 item">
                            <ul class="link1 list0 list">
                    <?endif;?>
                                <li><span class="mainlink" key="_c<?=$i?>"><?=$name?></span></li>  <?/*/catalog/<?=$arResult["CUR_IBLOCK_CODE"]?>/?arrFilter_pf[BRAND_GUID]=<?=$key?>&set_filter=Y*/?>
                                <div id="block_c<?=$i?>" class="pop block-brand blockHide">
                                    <div class="block0">
                                        <div class="r-border-shape">
                                            <div class="tb">
                                                <div class="cn l"></div>
                                                <div class="cn r"></div>
                                            </div>
                                            <div class="d dl"><i></i></div>
                                            <div class="d dr"><i></i></div>
                                            <div class="d dc"><i></i></div>
                                            <div class="cnt cnt-min-height">
                                                <div class="gap0">
                                                    �������� ������������ ��� ������
                                                    <ul class="ul-brand">
                                                    <?foreach($iblocks as $key => $iblockId):?>
                                                        <li><a href="/catalog/<?=$arResult["IBLOCKS_INFO"][$iblockId]["CODE"]?>/country/<?=TranslateIt($name);?>/"><b><u><?=$arResult["IBLOCKS_INFO"][$iblockId]["NAME"]?></u></b></a></li>
                                                    <?endforeach;?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="bb">
                                                <div class="cn l"></div>
                                                <div class="cn r"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?$i++?>
                    <?if(($i == $max && $step != 4) || $i == $count):?>
                            </ul>
                        </div>
                    <?endif;?>
				<?endforeach;?>
		</div>
	</div>
    <?endif;?>
    
    <?if(!empty($arResult["PURPOSE_LIST"])):?>
	<?ksort($arResult["PURPOSE_LIST"]);?>
    <h3 class="shift0">����������</h3>
	<div class="shift0">
		<div class="row">
                <?
                    $i = 0;
                    $count = count($arResult["PURPOSE_LIST"]);
                    $max = round($count/4);
                    $maxStep = $max;
                    $step = 0;
                ?>
				<?foreach($arResult["PURPOSE_LIST"] as $name => $iblocks):?>
                    <?if($i == 0 || ($i == $max && $step != 4)):
                        $max = $maxStep + $i;
                        $step++;
                        
                        $purposeVal = $name;
                        $purposeVal = strtolower(str_replace(' ', '-', TranslateIt($name, 'ru_en')));
                    ?>
                        <div class="item0 item">
                            <ul class="link1 list0 list">
                    <?endif;?>
                                <li><span class="mainlink" key="_p<?=$i?>"><?=$name?></span></li>  <?/*/catalog/<?=$arResult["CUR_IBLOCK_CODE"]?>/?arrFilter_pf[BRAND_GUID]=<?=$key?>&set_filter=Y*/?>
                                <div id="block_p<?=$i?>" class="pop block-brand blockHide">
                                    <div class="block0">
                                        <div class="r-border-shape">
                                            <div class="tb">
                                                <div class="cn l"></div>
                                                <div class="cn r"></div>
                                            </div>
                                            <div class="d dl"><i></i></div>
                                            <div class="d dr"><i></i></div>
                                            <div class="d dc"><i></i></div>
                                            <div class="cnt cnt-min-height">
                                                <div class="gap0">
                                                    �������� ������������ ��� ������
                                                    <ul class="ul-brand">
                                                    <?foreach($iblocks as $key => $iblockId):?>
                                                        <li><a href="/catalog/<?=$arResult["IBLOCKS_INFO"][$iblockId]["CODE"]?>/purpose/<?=$purposeVal?>/"><b><u><?=$arResult["IBLOCKS_INFO"][$iblockId]["NAME"]?></u></b></a></li>
                                                    <?endforeach;?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="bb">
                                                <div class="cn l"></div>
                                                <div class="cn r"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?$i++?>
                    <?if(($i == $max && $step != 4) || $i == $count):?>
                            </ul>
                        </div>
                    <?endif;?>
				<?endforeach;?>
		</div>
	</div>
    <?endif;?>

    <?if(!empty($arResult["VENDOR_LIST"])):?>
	<?ksort($arResult["VENDOR_LIST"]);?>

	<h3 class="shift0">�������������</h3>
	<div class="shift0">
		<div class="row">
                <?
                    $i = 0;
                    $count = count($arResult["VENDOR_LIST"]);
                    $max = round($count/4);
                    $maxStep = $max;
                    $step = 0;
                ?>
				<?foreach($arResult["VENDOR_LIST"] as $name => $iblocks):?>
                    <?if($i == 0 || ($i == $max && $step != 4)):
                        $max = $maxStep + $i;
                        $step++;
                    ?>
                        <div class="item0 item">
                            <ul class="link1 list0 list">
                    <?endif;?>
                                <li><span class="mainlink" key="_v<?=$i?>"><?=$name?></span></li>  <?/*/catalog/<?=$arResult["CUR_IBLOCK_CODE"]?>/?arrFilter_pf[BRAND_GUID]=<?=$key?>&set_filter=Y*/?>
                                <div id="block_v<?=$i?>" class="pop block-brand blockHide">
                                    <div class="block0">
                                        <div class="r-border-shape">
                                            <div class="tb">
                                                <div class="cn l"></div>
                                                <div class="cn r"></div>
                                            </div>
                                            <div class="d dl"><i></i></div>
                                            <div class="d dr"><i></i></div>
                                            <div class="d dc"><i></i></div>
                                            <div class="cnt cnt-min-height">
                                                <div class="gap0">
                                                    �������� ������������ ��� ������
                                                    <ul class="ul-brand">
                                                    <?foreach($iblocks as $key => $iblockId):?>
                                                        <li><a href="/catalog/<?=$arResult["IBLOCKS_INFO"][$iblockId]["CODE"]?>/vendor/<?=$arResult["ALL_BRANDS"][$name];?>/"><b><u><?=$arResult["IBLOCKS_INFO"][$iblockId]["NAME"]?></u></b></a></li>
                                                    <?endforeach;?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="bb">
                                                <div class="cn l"></div>
                                                <div class="cn r"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?$i++?>
                    <?if(($i == $max && $step != 4) || $i == $count):?>
                            </ul>
                        </div>
                    <?endif;?>
				<?endforeach;?>
		</div>
	</div>
    <?endif;?>

</div>
<div style="letter-spacing: 0; word-spacing: 0; cursor:pointer; margin-left: 510px;" onclick="$(document).scrollTop(0); return false;">
	������� ������
</div>
<?if(!empty($arResult["DESCRIPTION"])):?>
	<h3 class="shift0"><?=$arResult["NAME"]?></h3>

	<div class="shift1">
		<p><?=$arResult["DESCRIPTION"]?></p>
	</div>
<?else:?>
<br>
<?endif;?>
