<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = array();

if ($this->StartResultCache(false, array($_COOKIE["ITC"], $USER->GetGroups()))) {
	CModule::IncludeModule('iblock');

	// ���������� ��� ����������
	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE"    => "Y",
	);
	if (empty($arParams["SECTION_ID"])) {
		$arFilter["XML_ID"] = "main";
		$selectArray        = Array("ID", "CODE", "NAME");
	} else {
		$arFilter["CODE"] = $arParams["SECTION_ID"];
		$selectArray      = Array("ID", "CODE", "DESCRIPTION", "NAME");
	}
	$res = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false, $selectArray);
	if ($ar_fields = $res->GetNext()) {
		$arParams["SECTION_ID"]  = $ar_fields["ID"];
		$arResult["CODE"]        = $ar_fields["CODE"];
		$arResult["DESCRIPTION"] = $ar_fields["DESCRIPTION"];
		$arResult["NAME"]        = $ar_fields["NAME"];
	} else {
		LocalRedirect("/404.php");
	}

	//������� "�������" ��������,
	$arFilter = Array(
		"IBLOCK_ID"  => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arParams["SECTION_ID"],
		"ACTIVE"     => "Y",
	);
	$res      = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array("ID", "NAME", "PROPERTY_IBLOCK_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE"));
	while ($ar_res = $res->GetNext()) {
		$arResult["SECTIONS"][$ar_res["ID"]]["NAME"]        = $ar_res["NAME"];
		$arResult["SECTIONS"][$ar_res["ID"]]["IBLOCK_INFO"] = $ar_res["PROPERTY_IBLOCK_ID_VALUE"];
		if (!empty($ar_res["DETAIL_PICTURE"]))
			$arResult["SECTIONS"][$ar_res["ID"]]["PICTURE_ID"] = $ar_res["DETAIL_PICTURE"];
		else
			$arResult["SECTIONS"][$ar_res["ID"]]["PICTURE_ID"] = $ar_res["PREVIEW_PICTURE"];
		$tempIblockIds[] = $ar_res["PROPERTY_IBLOCK_ID_VALUE"];
	}

	//������ � ����������
	$arFilter = Array(
		"IBLOCK_TYPE" => "catalog",
		"SITE_ID"     => SITE_ID,
		"ACTIVE"      => "Y",
		"ID"          => $tempIblockIds
	);
	$res      = CIBlock::GetList(Array("SORT" => "ASC"), $arFilter, false);
	while ($ar_res = $res->Fetch()) {
		$arResult["IBLOCKS_INFO"][$ar_res["ID"]] = $ar_res;
	}
	global $DB;

	$query = "SELECT
                 seo_association.element_id as EID, 
                 seo_association.vendor_name as VN, 
                 seo_association.country_name as CN, 
                 seo_association.purpose_name as PN, 
                 b_iblock_element.IBLOCK_ID as IBLOCK_ID 
              FROM 
                 seo_association 
              INNER JOIN 
                 b_iblock_element 
              ON 
                 seo_association.element_id = b_iblock_element.ID 
              WHERE 
                 seo_association.type='" . $arResult["CODE"] . "'
                 AND b_iblock_element.ACTIVE = 'Y'";


	$res = $DB->Query($query);
	while ($arRes = $res->Fetch()) {
		/*if($USER->IsAdmin())
		{
			echo "<pre>"; print_r($arRes); echo "</pre>";
		}*/

		// �������������
		if (!empty($arRes["VN"]) && strpos($arRes["VN"], "&lt;&gt;") === false) {
			if (!in_array($arRes["IBLOCK_ID"], $arResult["VENDOR_LIST"][$arRes["VN"]])) {
				$arResult["VENDOR_LIST"][$arRes["VN"]][] = $arRes["IBLOCK_ID"];
			}
		}
		if (!empty($arRes["CN"]) && strpos($arRes["CN"], "&lt;&gt;") === false) {
			if (!in_array($arRes["IBLOCK_ID"], $arResult["COUNTRY_LIST"][$arRes["CN"]]))
				$arResult["COUNTRY_LIST"][$arRes["CN"]][] = $arRes["IBLOCK_ID"];
		}
		if (!empty($arRes["PN"]) && strpos($arRes["PN"], "&lt;&gt;") === false) {
			if (!in_array($arRes["IBLOCK_ID"], $arResult["PURPOSE_LIST"][$arRes["PN"]]))
				$arResult["PURPOSE_LIST"][$arRes["PN"]][] = $arRes["IBLOCK_ID"];
		}
	}
	
	// ��� ��� ������ ����, ����� seo_association ���� �� ��������� ��� �������������� ������
	// ��������� �������� �� ����� ��������������� ���� �������������
	if ($arResult['CODE'] == 'dveri')
	{
		$arResult["VENDOR_LIST"] = array();
		
		$brandsArray = array();
		$res = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => $tempIblockIds, 'ACTIVE' => 'Y'), Array('IBLOCK_ID', 'PROPERTY_BRAND_GUID'), false);
		while ($ar_fields = $res->GetNext())
			$brandsArray[$ar_fields['PROPERTY_BRAND_GUID_VALUE']] =  $ar_fields['IBLOCK_ID'];
	}
	
	$arBrandGuidFilter = Array(
		"IBLOCK_CODE" => "brand_guid",
		"ACTIVE"      => "Y",
		"!NAME"       => "<>",
	);

	$allBrands = Array();
	$res       = CIBlockElement::GetList(Array("NAME" => "ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME", "CODE"));
	while ($ar_fields = $res->GetNext())
	{
		// ������ �����
		if ($arResult['CODE'] == 'dveri')
			if (key_exists($ar_fields['ID'], $brandsArray))
				$arResult['VENDOR_LIST'][$ar_fields['NAME']][] = $brandsArray[$ar_fields['ID']];
		
		$allBrands[$ar_fields["NAME"]] = $ar_fields["CODE"];
	}

	$arResult["ALL_BRANDS"] = $allBrands;
	$this->IncludeComponentTemplate();
}
// ���, ������������� ��� ����������� �� ����

?>
