<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

if ($this->StartResultCache(false, false)) {
    CModule::IncludeModule('iblock');
    
    $arPageProperties = array();
    
    $interiorDoorsUrl = '/interior_doors/';
    
     $rsProperties = CIBlockProperty::GetList(
        array(
            "SORT" => "ASC"
        ),
        array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"]
        )
    );
    
    while($arProperty = $rsProperties -> GetNext()){
        if(strpos($arProperty["CODE"], 'PAGE_') === 0)
        {
            $propertyName = $arProperty["NAME"];
            
            $propertyName = substr($propertyName, strpos($propertyName, ':'), strlen($propertyName) );
            $propertyName = ltrim($propertyName, ':');
            $propertyName = rtrim($propertyName, '.');
            $propertyName = trim($propertyName);
            
            $propCode = $arProperty["CODE"];
            $propCode = str_replace('PAGE_', '', $propCode);
            $propCode = strtolower($propCode);
            
            $arProperty["NAME"] = $propertyName;
            $arProperty["DETAIL_PAGE_URL"] = $interiorDoorsUrl . $propCode . '/'; 
            $arPageProperties[] = $arProperty;
        }    
    }
    
    $arResult["PROPERTIES"] = $arPageProperties;
    $this->IncludeComponentTemplate();
}

// Код, выполняющийся вне зависимости от кэша

?>
