<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<h3 class="shift0"><?=$arParams["TITLE"]?></h3>
<div class="shift0">
    <div class="row">
            <?
                $i = 0;
                $count = count($arResult["PROPERTIES"]);
                $max = round($count/4);
                $maxStep = $max;
                $step = 0;
            ?>
            <?foreach($arResult["PROPERTIES"] as $arProperty):?>
                <?if($i == 0 || ($i == $max && $step != 4)):
                    $max = $maxStep + $i;
                    $step++;
                ?>
                    <div class="item0 item">
                        <ul class="link1 list0 list">
                <?endif;?>
                            <?
                            /*������������� "������" ������ ������ ��������� ������ ����*/
                            ?>
                            <li><span class="mainlink"><a href="<?=$arProperty["DETAIL_PAGE_URL"]?>"><?=$arProperty["NAME"]?></a></span></li>
                    <?$i++?>
                <?if(($i == $max && $step != 4) || $i == $count):?>
                        </ul>
                    </div>
                <?endif;?>
            <?endforeach;?>
    </div>
</div>        