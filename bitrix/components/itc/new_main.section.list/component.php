<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if ($this->StartResultCache(false, false)) {
    CModule::IncludeModule('iblock');

    // ���������� ��� ����������
	$arFilter = Array(   
		"IBLOCK_ID" => $arParams["IBLOCK_ID"], 
		"ACTIVE" => "Y",    
	);
	if(empty($arParams["SECTION_ID"])) {
		$arFilter["XML_ID"] = "main";
	} else {
		$arFilter["CODE"] = $arParams["SECTION_ID"];
	}
	$res = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter);
	if($ar_fields = $res->GetNext()) {
		$arParams["SECTION_ID"] = $ar_fields["ID"];
		$arResult["DESCRIPTION"] = $ar_fields["DESCRIPTION"];
		$arResult["NAME"] = $ar_fields["NAME"];
	}
	
	//������� "�������" ��������,
	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arParams["SECTION_ID"],
        "ACTIVE" => "Y",
	);  
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID", "NAME", "PROPERTY_IBLOCK_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE"));  
	while($ar_res = $res->GetNext()) {
		$arResult["SECTIONS"][$ar_res["ID"]]["NAME"] = $ar_res["NAME"];
		$arResult["SECTIONS"][$ar_res["ID"]]["IBLOCK_INFO"] = $ar_res["PROPERTY_IBLOCK_ID_VALUE"];
		if(!empty($ar_res["DETAIL_PICTURE"]))
			$arResult["SECTIONS"][$ar_res["ID"]]["PICTURE_ID"] = $ar_res["DETAIL_PICTURE"];
		else
			$arResult["SECTIONS"][$ar_res["ID"]]["PICTURE_ID"] = $ar_res["PREVIEW_PICTURE"];
		$tempIblockIds[] = $ar_res["PROPERTY_IBLOCK_ID_VALUE"];
	}
	//������ � ����������
	$arFilter = Array(
		"IBLOCK_TYPE" => "catalog", 
		"SITE_ID" => SITE_ID, 
		"ACTIVE" => "Y", 
		"ID" => $tempIblockIds
	);
	$res = CIBlock::GetList(Array("SORT" => "ASC"),	$arFilter, false);
	while($ar_res = $res->Fetch()) {
		$arResult["IBLOCKS_INFO"][$ar_res["ID"]] = $ar_res;
	}
	//����������� ������ � "��������" � ������� � ����������
	foreach($arResult["SECTIONS"] as $key => $section) {
		$arResult["SECTIONS"][$key]["IBLOCK_INFO"] = $arResult["IBLOCKS_INFO"][$section["IBLOCK_INFO"]];
	}
	//������ ��������������
	/*$arBrandGuidFilter = Array(
		"IBLOCK_CODE" => "brand_guid",
	);
	$i = 0;
	$resBrandGuid = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));*/
	
	$arFilter = Array(
	   "IBLOCK_ID" => $tempIblockIds,    
	   "ACTIVE" => "Y"
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, Array("PROPERTY_BRAND_GUID", "IBLOCK_CODE", "IBLOCK_ID"), false);
	$brandsArray = Array();
	while($ar_fields = $res->GetNext()) {
		if (!in_array($ar_fields["PROPERTY_BRAND_GUID_VALUE"], $brandsArray))
			$brandsArray[] =  $ar_fields["PROPERTY_BRAND_GUID_VALUE"];
		$arResult["BRAND_IBLOCK_CODES"][$ar_fields["PROPERTY_BRAND_GUID_VALUE"]][$ar_fields["IBLOCK_ID"]] = $ar_fields["IBLOCK_CODE"];
	}
	//echo "<pre>"; print_r($brandsArray); echo "</pre>";
	//echo "<pre>"; print_r($arResult["BRAND_IBLOCK_CODES"]); echo "</pre>";
	$arBrandGuidFilter = Array(   
		"IBLOCK_CODE" => "brand_guid",
		"ACTIVE" => "Y",
		"ID" => $brandsArray,
		"!NAME" => "<>",
	);
	//
	$i = 0;
	$resBrandGuid = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
	$countItems = $resBrandGuid->SelectedRowsCount();
	$countColumnItems = ceil($countItems/4);
	$n = 0;
    
    $arBrandNames = array();
    
	while($ar_fields = $resBrandGuid->GetNext()) {
		$arResult["BRAND_GUID"][$i][$ar_fields["ID"]] = $ar_fields["NAME"];
        $arBrandNames[] = $ar_fields["NAME"];
		$n++;
		if($n >= $countColumnItems) {
			$n = 0;
			$i++;
		}
	}
  
    $brandDB = CIBlockElement::GetList( 
			Array("SORT"=>"ASC"), 
			Array(
                'IBLOCK_ID' => 46,
                'NAME' => array_map('htmlspecialchars_decode', $arBrandNames), 
                'ACTIVE'=>'Y'
            ), 
			false, 
			false, 
			Array('ID', 'CODE', 'NAME')
    );
    
    $arBrandsInfo = array();
    while($arBrand = $brandDB -> GetNext()){
        $arBrandsInfo[ htmlspecialchars($arBrand["NAME"]) ] = $arBrand;
    }
    
    $arResult["BRANDS_INFO"] = $arBrandsInfo;
	$arResult["CUR_IBLOCK_CODE"] = $arResult["SECTIONS"][$arParams["SECTION_ID"]]["IBLOCK_INFO"]["CODE"];
    $this->IncludeComponentTemplate();
}

// ���, ������������� ��� ����������� �� ����

?>
