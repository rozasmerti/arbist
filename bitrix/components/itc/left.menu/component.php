<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if ($this->StartResultCache(false, false)) {
    CModule::IncludeModule('iblock');

    // ���������� ��� ����������
	//���� �� ������ ������� ������, �� ������������ ��������� (���������� ��� main)
	if(empty($arParams["SECTION_ID"])) {
		$arFilter = Array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"CODE" => "main",
		);
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID"));
		if($ar_fields = $res->GetNext()) {
			$arResult["REQUEST_ID"] = $ar_fields["ID"];
		}
	} else {
		$arResult["REQUEST_ID"] = $arParams["SECTION_ID"];
	}
	$arResult["SECTIONS"] = array();
	//�������
	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"GLOBAL_ACTIVE" => 'Y'
	);
	$res = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter);
	while($ar_res = $res->GetNext()) {
		$arResult["SECTIONS"][$ar_res["ID"]]["NAME"] = $ar_res["NAME"];
	}
	//��������
	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		//"SECTION_ID" => "0",
		//"INCLUDE_SUBSECTIONS" => "Y"
	);
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID", "NAME", "IBLOCK_SECTION_ID","PROPERTY_IBLOCK_ID", "PROPERTY_SUBSECTION"));
	while($ar_fields = $res->GetNext()) {
		//$arResult["SECTIONS"][$ar_fields["IBLOCK_SECTION_ID"]]["ITEMS"][$ar_fields["ID"]] = $ar_fields;
		$tempItems[$ar_fields["ID"]] = $ar_fields;
		$tempIblockIds[] = $ar_fields["PROPERTY_IBLOCK_ID_VALUE"];
	}
	//���������
	$arFilter = Array(
		"TYPE" => "catalog",
		"SITE_ID" => SITE_ID,
		"ACTIVE" => "Y",
		"ID" => $tempIblockIds
	);
	$res = CIBlock::GetList(Array("SORT" => "ASC"),	$arFilter, false);
	while($ar_res = $res->Fetch()) {
		$tempIblock[$ar_res["ID"]] = $ar_res;
	}
	//������� ��������
	/*foreach($tempIblock as $iblockId => $iblockInfo ) {
		$db_enum_list = CIBlockProperty::GetPropertyEnum("COUNTRY", Array(), Array("IBLOCK_ID"=>$iblockId));
		while($ar_enum_list = $db_enum_list->GetNext()) {  
			$tempIblock[$iblockId]["PROPERTY_CODE"] = "COUNTRY";
			$tempIblock[$iblockId]["PROPERTY_VALUES"][$ar_enum_list["ID"]] = $ar_enum_list["VALUE"];
		}
	}*/
	/******************************************************/
	//�.�. ������������� ������ ����, �� ��� ������ ������ ������� ������ ���
	$arBrandGuidFilter = Array(
		"IBLOCK_CODE" => "brand_guid",
	);
	$resBrandGuid = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
	while($ar_fields = $resBrandGuid->GetNext()) {
		$brandGuid[$ar_fields["ID"]] = $ar_fields["NAME"];
	}

	/******************************************************/

	//�������� ������ � ����� � ������� ������ � ����������� ����
	$arResult["MENU"] = "Y"; //������
	foreach($tempItems as $idItem => $item) {
		//������� ��������
		if($item["PROPERTY_SUBSECTION_ENUM_ID"] == 15499 && $idItem == $arResult["REQUEST_ID"]) { //
			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_CODE"] = "COUNTRY";
			$db_enum_list = CIBlockProperty::GetPropertyEnum("COUNTRY", Array(), Array("IBLOCK_ID" => $item["PROPERTY_IBLOCK_ID_VALUE"]));
			while($ar_enum_list = $db_enum_list->GetNext()) {
				$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_VALUES"][$ar_enum_list["ID"]] = $ar_enum_list["VALUE"];
			}
		} elseif($item["PROPERTY_SUBSECTION_ENUM_ID"] == 15500 && $idItem == $arResult["REQUEST_ID"]) { //
			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_CODE"] = "BRAND_GUID";
			//�.�. ������������� ������ ����, �� ��� ������ ������ ������� ������ ���
			//$arBrandGuidFilter = Array(
			//	"IBLOCK_CODE" => "brand_guid",
			//);
			//$resBrandGuid = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
			//while($ar_fields = $resBrandGuid->GetNext()) {
			//	$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_VALUES"][$ar_fields["ID"]] = $ar_fields["NAME"];
			//}
			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_VALUES"] = $brandGuid;
		} elseif($item["PROPERTY_SUBSECTION_ENUM_ID"] == 15593 && $idItem == $arResult["REQUEST_ID"]) {
			$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_CODE"] = "SECTIONS";
			$arSectionFilter = Array(
				"IBLOCK_ID" => $item["PROPERTY_IBLOCK_ID_VALUE"],
				"GLOBAL_ACTIVE" => "Y",
				"ACTIVE" => "Y"
			);
			$db_list = CIBlockSection::GetList(Array("NAME" => "ASC"), $arSectionFilter);
			while($ar_result = $db_list->GetNext()) {
				$tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]]["PROPERTY_VALUES"][$ar_result["ID"]] = $ar_result["NAME"];
			}
		}
		$item["IBLOCK_INFO"] = $tempIblock[$item["PROPERTY_IBLOCK_ID_VALUE"]];
		$arResult["SECTIONS"][$item["IBLOCK_SECTION_ID"]]["ITEMS"][$idItem] = $item;
		if($idItem == $arResult["REQUEST_ID"]) {
			$arResult["SECTIONS"][$item["IBLOCK_SECTION_ID"]]["CUR"] = "Y";
			//$arResult["MENU"] = "N"; // �� ������
		}
		elseif($arResult["SECTIONS"][$item["IBLOCK_SECTION_ID"]]["CUR"] != "Y") {
			$arResult["SECTIONS"][$item["IBLOCK_SECTION_ID"]]["CUR"] = "N";
		}
	}

    $this->IncludeComponentTemplate();
}

// ���, ������������� ��� ����������� �� ����

?>
