<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if ($this->StartResultCache(false, false)) {
    CModule::IncludeModule('iblock');

    // ���������� ��� ����������
	// ��������
	$sectionId = NULL;
	$arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID");
	$arFilter = Array(
		"IBLOCK_ID" => intval($arParams["IBLOCK_ID"]), 
		"PROPERTY_IBLOCK_ID" => intval($arParams["PROP_IBLOCK_ID"]),
		//"ACTIVE" => "Y",
		
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ar_res = $res->GetNext()) {
		$arResult["sectionId"] = $ar_res["IBLOCK_SECTION_ID"];
		$arResult["iblockName"] = $ar_res["NAME"];
	}
	// ������
	if(!empty($arResult["sectionId"])) {
		$arFilter = Array(
			"IBLOCK_ID" => intval($arParams["IBLOCK_ID"]), 
			"GLOBAL_ACTIVE" => "Y", 
			"ID" => $arResult["sectionId"],
		);  
		$db_list = CIBlockSection::GetList(Array(), $arFilter, false, Array("ID", "NAME", "CODE"));  
		if($ar_result = $db_list->GetNext()) {
			$arResult["sectionName"] = $ar_result["NAME"];
			$arResult["sectionId"] = $ar_result["ID"];
			$arResult["sectionCode"] = $ar_result["CODE"];
		}
	}
    // ���������
	if(!empty($arParams["SECTION_ID"])) {
        $arFilter = Array(
			"IBLOCK_ID" => intval($arParams["PROP_IBLOCK_ID"]), 
			//"GLOBAL_ACTIVE" => "Y", 
			"ID" => $arParams["SECTION_ID"],
		);  
		$db_list = CIBlockSection::GetList(Array(), $arFilter, false, Array("ID", "NAME"));  
		if($ar_result = $db_list->GetNext()) {
			$arResult["collectionName"] = $ar_result["NAME"];
			$arResult["collectionId"] = $ar_result["ID"];
		}
    }
    // �����
    if(!empty($arParams["ELEMENT_ID"])) {
        $arSelect = Array("ID", "NAME");
        $arFilter = Array(
            "IBLOCK_ID" => intval($arParams["PROP_IBLOCK_ID"]), 
           // "ACTIVE" => "Y",
            "ID" => $arParams["ELEMENT_ID"],
            
        );
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ar_res = $res->GetNext()) {
            $arResult["elementName"] = $ar_res["NAME"];
        }
    }
		
	$this->SetResultCacheKeys(array(
        "elementName",
        "collectionName",
        "collectionId",
		"sectionName",
		"sectionCode",
		"iblockName",
		$arParams["DISPLAY_IBLOCK_CHAIN"],
		$arParams["PROP_IBLOCK_CODE"],
	));
	
    $this->IncludeComponentTemplate();
}
if(!empty($arResult["sectionCode"])) {
	$APPLICATION->AddChainItem($arResult["sectionName"], "/" . $arResult["sectionCode"] . "/");
}
if(!empty($arResult["iblockName"]) && $arParams["DISPLAY_IBLOCK_CHAIN"] == "Y") {
	$APPLICATION->AddChainItem($arResult["iblockName"], "/catalog/" . $arParams["PROP_IBLOCK_CODE"] . "/");
}
elseif(!empty($arResult["iblockName"]) && $arParams["DISPLAY_IBLOCK_CHAIN"] == "N") {
    $APPLICATION->AddChainItem($arResult["iblockName"], "");
}
if(!empty($arParams["SECTION_ID"]) && !empty($arParams["ELEMENT_ID"])) {
    $APPLICATION->AddChainItem($arResult["collectionName"], "/catalog/" . $arParams["PROP_IBLOCK_CODE"] . "/" . $arResult["collectionId"] . "/");
    $APPLICATION->AddChainItem($arResult["elementName"], "");
}
elseif(!empty($arParams["SECTION_ID"]) && empty($arParams["ELEMENT_ID"])) {
    $APPLICATION->AddChainItem($arResult["collectionName"], "");
}

// ���, ������������� ��� ����������� �� ����

?>
