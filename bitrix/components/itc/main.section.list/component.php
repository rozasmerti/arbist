<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if ($this->StartResultCache(false, false)) {
    CModule::IncludeModule('iblock');

    // ���������� ��� ����������
	if(empty($arParams["SECTION_ID"])) {
		$arFilter = Array(   
			"IBLOCK_ID" => $arParams["IBLOCK_ID"], 
			"ACTIVE" => "Y",    
			"CODE" => "main",
		);
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID"));
		if($ar_fields = $res->GetNext()) {
			$arParams["SECTION_ID"] = $ar_fields["ID"];
		}
	}
	//��������� ID �������
	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ID" => $arParams["SECTION_ID"],
	);  
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("IBLOCK_SECTION_ID", "PREVIEW_TEXT", "NAME"));  
	if($ar_res = $res->GetNext()) {
		$sectionId = $ar_res["IBLOCK_SECTION_ID"];
		$arResult["DESCRIPTION"] = $ar_res["PREVIEW_TEXT"];
		$arResult["NAME"] = $ar_res["NAME"];
	}
	//������� "�������" ��������,
	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $sectionId,
	);  
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID", "NAME", "PROPERTY_IBLOCK_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE"));  
	while($ar_res = $res->GetNext()) {
		$arResult["SECTIONS"][$ar_res["ID"]]["NAME"] = $ar_res["NAME"];
		$arResult["SECTIONS"][$ar_res["ID"]]["IBLOCK_INFO"] = $ar_res["PROPERTY_IBLOCK_ID_VALUE"];
		if(!empty($ar_res["DETAIL_PICTURE"]))
			$arResult["SECTIONS"][$ar_res["ID"]]["PICTURE_ID"] = $ar_res["DETAIL_PICTURE"];
		else
			$arResult["SECTIONS"][$ar_res["ID"]]["PICTURE_ID"] = $ar_res["PREVIEW_PICTURE"];
		$tempIblockIds[] = $ar_res["PROPERTY_IBLOCK_ID_VALUE"];
	}
	//������ � ����������
	$arFilter = Array(
		"TYPE" => "catalog", 
		"SITE_ID" => SITE_ID, 
		"ACTIVE" => "Y", 
		"ID" => $tempIblockIds
	);
	$res = CIBlock::GetList(Array("SORT" => "ASC"),	$arFilter, false);
	while($ar_res = $res->Fetch()) {
		$tempIblock[$ar_res["ID"]] = $ar_res;
	}
	//����������� ������ � "��������" � ������� � ����������
	foreach($arResult["SECTIONS"] as $key => $section) {
		$arResult["SECTIONS"][$key]["IBLOCK_INFO"] = $tempIblock[$section["IBLOCK_INFO"]];
	}
	//������ ��������������
	$arBrandGuidFilter = Array(
		"IBLOCK_CODE" => "brand_guid",
	);
	$i = 0;
	$resBrandGuid = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
	while($ar_fields = $resBrandGuid->GetNext()) {
		if($i == 4) 
			$i = 0;
		$arResult["BRAND_GUID"][$i][$ar_fields["ID"]] = $ar_fields["NAME"];
		$i++;
	}
	
	$arResult["CUR_IBLOCK_CODE"] = $arResult["SECTIONS"][$arParams["SECTION_ID"]]["IBLOCK_INFO"]["CODE"];
    $this->IncludeComponentTemplate();
}

// ���, ������������� ��� ����������� �� ����

?>
