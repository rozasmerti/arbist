<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();
if ($this->StartResultCache(false, false)) {
    CModule::IncludeModule('iblock');
	//echo "<pre>"; print_r($arParams); echo "</pre>";
    // ���������� ��� ����������
	/********************/
	//���������� ���������� ID "������� 1��� ������"
	$arResult["SECTION_CODE"] = $arParams["SECTION_ID"];
    if(!empty($arResult["SECTION_CODE"])) {
        $arFilterSCode = Array(
            "IBLOCK_ID" => 47, 
            "GLOBAL_ACTIVE" => "Y",
            "ACTIVE" => "Y",
            "CODE" => $arResult["SECTION_CODE"],
        );
        $dbListSCode = CIBlockSection::GetList(Array(), $arFilterSCode, false, Array("ID"));  
        if($arResSCode = $dbListSCode->GetNext()) {
            $arResult["SECTION_ID"] =  $arResSCode["ID"];
        }
    }
	//���������� ���������� ��� ��������� ("������ 2��� ������"), 
	//����� ��� ����������� ������� 1��� ������, ���� �� ��������� � ������ �������
	$arResult["IBLOCK_CODE"] = $arParams["IBLOCK_CODE"];
	
	//���� ��� �����, �� ������������ ������ ��� ������ 1��� ������
	if(empty($arResult["SECTION_ID"]) && empty($arResult["IBLOCK_CODE"])) {
		$arFilter = Array(
			"IBLOCK_ID" => 47, 
			"GLOBAL_ACTIVE" => "Y",
			"ACTIVE" => "Y",
			"XML_ID" => "main",
		);
		$db_list = CIBlockSection::GetList(Array(), $arFilter, false);  
		if($ar_result = $db_list->GetNext()) {
			$arResult["SECTION_ID"] =  $ar_result["ID"];
		}
	}
	//�����, ���� ������ 1��� ������ �� �����, 
	//�� ���� ������ � ������� 2��� ������ (�.�. ��������� � ��������)
	elseif(empty($arResult["SECTION_ID"]) && !empty($arResult["IBLOCK_CODE"])) {
		//ID ���������
		$arFilter =  Array(
			"TYPE" => "catalog",
			"SITE_ID" => SITE_ID,
			"ACTIVE" => "Y",
			"CODE" => $arResult["IBLOCK_CODE"]
		);
		$res = CIBlock::GetList(Array(), $arFilter, false);
		if($ar_res = $res->Fetch()) {
			//$catalogIblockId = $ar_res["ID"];
		//ID ������� 1��� ������
			$arFilter = Array(
				"IBLOCK_ID" => 47, 
				"ACTIVE" => "Y",
				"PROPERTY_IBLOCK_ID" => $ar_res["ID"],
			);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("IBLOCK_SECTION_ID"));  
			if($ar_result = $res->GetNext()) {
				$arResult["SECTION_ID"] =  $ar_result["IBLOCK_SECTION_ID"];
			}
		}
	}
	$allListId = array();
	$arFilter = Array(
		"IBLOCK_ID" => 47,
		"SECTION_ID" => $arResult["SECTION_ID"],
		"ACTIVE" => "Y",
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("PROPERTY_IBLOCK_ID"));
	while($ar_fields = $res->GetNext()) {
		$allListId[] = $ar_fields["PROPERTY_IBLOCK_ID_VALUE"];
	}
	$arResult["ALL_VIEW_LIST_ID"] = implode(",", $allListId);
	/********************/
	$arFilter = Array(   
		"IBLOCK_ID" => $allListId,//$arPaparms["ALL_LIST_IB"],
		"PROPERTY_ACTION_NEW_SALE_VALUE" => $arParams["NAME_VALUE"] . "%", 
		"ACTIVE"=>"Y",    
	);
	$arElemSelect = array("ID", "IBLOCK_ID", "IBLOCK_CODE", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_COUNTRY", "PROPERTY_BRAND_GUID");
	//��������
	$sections = array();
	$brand_guids = array();
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arElemSelect); 
	while($ar_fields = $res->GetNext()) {
		//��������
		if(!empty($ar_fields["PREVIEW_PICTURE"]))
			$ar_fields["PICTURE_ID"] = $ar_fields["PREVIEW_PICTURE"];
		else
			$ar_fields["PICTURE_ID"] = $ar_fields["DETAIL_PICTURE"];
		//������ ��� ��������	
		if(in_array($ar_fields["IBLOCK_ID"], $arParams["SECTION_LIST_IB"]))
			$ar_fields["DETAIL_PAGE_URL"] = "/" .  $arParams["IBLOCK_TYPE"] . "/" . $ar_fields["IBLOCK_CODE"] . "/" . $ar_fields["IBLOCK_SECTION_ID"] . "/";
		else
			$ar_fields["DETAIL_PAGE_URL"] = "/" .  $arParams["IBLOCK_TYPE"] . "/" . $ar_fields["IBLOCK_CODE"] . "/" . $ar_fields["IBLOCK_SECTION_ID"] . "/" . $ar_fields["ID"] . "/";
		$arResult["ITEMS"][] = $ar_fields;
		if(!in_array($ar_fields["IBLOCK_SECTION_ID"], $sections))
			$sections[] = $ar_fields["IBLOCK_SECTION_ID"];
		if(!in_array($ar_fields["PROPERTY_BRAND_GUID_VALUE"], $brand_guid))
			$brand_guids[] = $ar_fields["PROPERTY_BRAND_GUID_VALUE"];
		//echo "<pre>"; print_r($ar_fields); echo "</pre>";
	}
	if(!empty($brand_guids) && !empty($sections)) {
		//�������������
		$arBrandGuidFilter = Array(
			"IBLOCK_CODE" => "brand_guid",
			"ID" => $brand_guids,
		);
		$resBrandGuid = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
		while($ar_fields = $resBrandGuid->GetNext()) {
			$tempBrandGuids[$ar_fields["ID"]] = $ar_fields["NAME"];
		}
		//������� ID �������������� �� �� ��������
		foreach($arResult["ITEMS"] as $key => $item) {
			$arResult["ITEMS"][$key]["PROPERTY_BRAND_GUID_VALUE"] = $tempBrandGuids[$item["PROPERTY_BRAND_GUID_VALUE"]];
		}
		//�������
		$arSectionFilter = Array(
			"ID" => $sections,
		);
		$resSecion = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arSectionFilter);
		while($ar_fields = $resSecion->GetNext()) {
			$ar_fields["URL"] = "/" . $ar_fields["IBLOCK_TYPE_ID"] . "/" . $ar_fields["IBLOCK_CODE"] . "/" . $ar_fields["ID"] . "/";
			$arResult["SECTIONS"][$ar_fields["ID"]] = $ar_fields;
		}
		//echo "<pre>"; print_r($arResult["SECTIONS"]); echo "</pre>";
	}
    $this->IncludeComponentTemplate();
}

// ���, ������������� ��� ����������� �� ����

?>
