<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();
if ($this->StartResultCache(false, false)) {
    CModule::IncludeModule('iblock');
	//echo "<pre>"; print_r($arParams); echo "</pre>";
    // ���������� ��� ����������
	/********************/
	//���������� ���������� ID "������� 1��� ������"
	$arResult["SECTION_CODE"] = $arParams["SECTION_ID"];
	$arResult["IBLOCK_CODE"] = $arParams["IBLOCK_CODE"];
	$arFilter = array();
	
	if (($arResult['IBLOCK_CODE'] == 'doors') && (!$arResult['SECTION_CODE'])) {
		$arResult['SECTION_CODE'] = 'dveri';
	}

    if(!empty($arResult["SECTION_CODE"])) {
        $arFilterSCode = Array(
            "IBLOCK_ID" => 47, 
            "GLOBAL_ACTIVE" => "Y",
            "ACTIVE" => "Y",
            "CODE" => $arResult["SECTION_CODE"],
        );
        $dbListSCode = CIBlockSection::GetList(Array(), $arFilterSCode, false, Array("ID"));  
        if($arResSCode = $dbListSCode->GetNext()) {
            $arResult["SECTION_ID"] =  $arResSCode["ID"];
        }
    }
	//���������� ���������� ��� ��������� ("������ 2��� ������"), 
	//����� ��� ����������� ������� 1��� ������, ���� �� ��������� � ������ �������
	
	//���� ��� �����, �� ������������ ������ ��� ������ 1��� ������
	if(empty($arResult["SECTION_ID"]) && empty($arResult["IBLOCK_CODE"])) {
		$arFilter = Array(
			"IBLOCK_ID" => 47, 
			"GLOBAL_ACTIVE" => "Y",
			"ACTIVE" => "Y",
			"XML_ID" => "main",
		);
		$db_list = CIBlockSection::GetList(Array(), $arFilter, false);  
		if($ar_result = $db_list->GetNext()) {
			$arResult["SECTION_ID"] =  $ar_result["ID"];
		}
	}
	//�����, ���� ������ 1��� ������ �� �����, 
	//�� ���� ������ � ������� 2��� ������ (�.�. ��������� � ��������)
	elseif(empty($arResult["SECTION_ID"]) && !empty($arResult["IBLOCK_CODE"])) {
		//ID ���������
		$arFilter =  Array(
			"TYPE" => "catalog",
			"SITE_ID" => SITE_ID,
			"ACTIVE" => "Y",
			"CODE" => $arResult["IBLOCK_CODE"]
		);
		$res = CIBlock::GetList(Array(), $arFilter, false);
		if($ar_res = $res->Fetch()) {
			//$catalogIblockId = $ar_res["ID"];
		//ID ������� 1��� ������
			$arFilter = Array(
				"IBLOCK_ID" => 47, 
				"ACTIVE" => "Y",
				"PROPERTY_IBLOCK_ID" => $ar_res["ID"],
			);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("IBLOCK_SECTION_ID"));  
			if($ar_result = $res->GetNext()) {
				$arResult["SECTION_ID"] =  $ar_result["IBLOCK_SECTION_ID"];
			}
		}
	}
	
	$allListId = array();
	// if(!empty($_REQUEST["IBLOCK_CODE"])) {
	// 	if($_SERVER["REMOTE_ADDR"]=="212.164.239.249") {
	// 		$arFilter = Array(
	// 			"IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"],
	// 			// "SECTION_ID" => $arResult["SECTION_ID"],
	// 			"ACTIVE" => "Y",
	// 		);
	// 	}
	// } else
	$arFilter = Array(
		"IBLOCK_ID" => 47,
		"SECTION_ID" => $arResult["SECTION_ID"],
		"ACTIVE" => "Y",
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("PROPERTY_IBLOCK_ID"));
	while($ar_fields = $res->GetNext()) {
		$allListId[] = $ar_fields["PROPERTY_IBLOCK_ID_VALUE"];
	}
	// if(!empty($_REQUEST["IBLOCK_CODE"])) {
	// 	if($_SERVER["REMOTE_ADDR"]=="212.164.239.249") {
	// 		// echo "<pre>"; print_r($allListId); echo "</pre>";
	// 		// $arFilter = array("IBLOCK_CODE"=>$_REQUEST["IBLOCK_CODE"]);
	// 	}
	// }
	$arResult["ALL_VIEW_LIST_ID"] = implode(",", $allListId);
	/********************/
	if(!empty($_REQUEST["IBLOCK_CODE"]) && $APPLICATION->GetCurPage()!="/interior_doors/veneered_doors/6934/") {
	
	$arFilter = Array(   
		"IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"],
		// "IBLOCK_ID" => $allListId,//$arPaparms["ALL_LIST_IB"],
		"PROPERTY_ACTION_NEW_SALE_VALUE" => $arParams["NAME_VALUE"] . "%", 
		"ACTIVE"=>"Y",    
	);

	}

	$arFilter = Array(   
		// "IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"],
		"IBLOCK_ID" => $allListId,//$arPaparms["ALL_LIST_IB"],
		"PROPERTY_ACTION_NEW_SALE_VALUE" => $arParams["NAME_VALUE"] . "%", 
		"ACTIVE"=>"Y",    
	);

	$arElemSelect = array("ID", "IBLOCK_ID", "IBLOCK_CODE", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_COUNTRY", "PROPERTY_BRAND_GUID");

    // ���� ��� "������", �� ������� ��� �������� � �� ������� ���������� INTERIOR (�������� ���������) � ������� ��� �������� � �������
    if($arResult["SECTION_ID"] == 4365) {
        //$arFilter["!PROPERTY_INTERIOR"] = false;
        $arFilter["!PROPERTY_INTERIOR"] = "";
        $arElemSelect[] = "PROPERTY_INTERIOR";
    }

	//��������
	$sections = array();
	$brand_guids = array();
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arElemSelect);

    $interiorArray = array();
	$interiorPic = "";
    $pictureWas = array();

	while($ar_fields = $res->GetNext()) {
		//��������
        

        $interiorArray = explode(",", $ar_fields["PROPERTY_INTERIOR_VALUE"]);
		$interiorPic = trim($interiorArray[0]);
        
        if(in_array($interiorPic, $pictureWas)) continue;
        
        if(fopen($_SERVER["DOCUMENT_ROOT"] . "/images/catalog/" . $interiorPic, "r") && strlen($interiorPic) != 0) {
							
            $pictureWas[] = $interiorPic;
            $pic = split("\.", $interiorPic);
            $ar_fields["PICTURE"] = "<img src=\"/rpic_path/c/" . $pic[0] . "_h138_w200_crop." . $pic[1] . "\" alt=\"\"/>";
            
            
        } elseif(!empty($ar_fields["PREVIEW_PICTURE"])) {
			$ar_fields["PICTURE"] = "<img src=\"/rpic/c/" . $ar_fields["PREVIEW_PICTURE"] . "_h138_w200_auto.jpg\" alt=\"\"/>";
		} elseif(!empty($ar_fields["DETAIL_PICTURE"])) {
			$ar_fields["PICTURE"] = "<img src=\"/rpic/c/" . $ar_fields["DETAIL_PICTURE"] . "_h138_w200_auto.jpg\" alt=\"\"/>";
        } else {
            $ar_fields["PICTURE"] = "<img src=\"/img/noimg0.jpg\" alt=\"\"/>";
        }
		//������ ��� ��������	
		if(in_array($ar_fields["IBLOCK_ID"], $arParams["SECTION_LIST_IB"]))
			$ar_fields["DETAIL_PAGE_URL"] = "/" .  $arParams["IBLOCK_TYPE"] . "/" . $ar_fields["IBLOCK_CODE"] . "/" . $ar_fields["IBLOCK_SECTION_ID"] . "/";
		else
			$ar_fields["DETAIL_PAGE_URL"] = "/" .  $arParams["IBLOCK_TYPE"] . "/" . $ar_fields["IBLOCK_CODE"] . "/" . $ar_fields["IBLOCK_SECTION_ID"] . "/" . $ar_fields["ID"] . "/";
		$arResult["ITEMS"][] = $ar_fields;
		if(!in_array($ar_fields["IBLOCK_SECTION_ID"], $sections))
			$sections[] = $ar_fields["IBLOCK_SECTION_ID"];
		if(!in_array($ar_fields["PROPERTY_BRAND_GUID_VALUE"], $brand_guid))
			$brand_guids[] = $ar_fields["PROPERTY_BRAND_GUID_VALUE"];
		//echo "<pre>"; print_r($ar_fields); echo "</pre>";
	}
	if(!empty($brand_guids) && !empty($sections)) {
		//�������������
		$arBrandGuidFilter = Array(
			"IBLOCK_CODE" => "brand_guid",
			"ID" => $brand_guids,
		);
		$resBrandGuid = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arBrandGuidFilter, false, false, Array("ID", "NAME"));
		while($ar_fields = $resBrandGuid->GetNext()) {
			$tempBrandGuids[$ar_fields["ID"]] = $ar_fields["NAME"];
		}
		//������� ID �������������� �� �� ��������
		foreach($arResult["ITEMS"] as $key => $item) {
			$arResult["ITEMS"][$key]["PROPERTY_BRAND_GUID_VALUE"] = $tempBrandGuids[$item["PROPERTY_BRAND_GUID_VALUE"]];
		}
		//�������
		$arSectionFilter = Array(
			"ID" => $sections,
		);
		$resSecion = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arSectionFilter);
		while($ar_fields = $resSecion->GetNext()) {
			$ar_fields["URL"] = "/" . $ar_fields["IBLOCK_TYPE_ID"] . "/" . $ar_fields["IBLOCK_CODE"] . "/" . $ar_fields["ID"] . "/";
			$arResult["SECTIONS"][$ar_fields["ID"]] = $ar_fields;
		}

		//echo "<pre>"; print_r($arResult["SECTIONS"]); echo "</pre>";
	}
    $this->IncludeComponentTemplate();
}

// ���, ������������� ��� ����������� �� ����

?>
