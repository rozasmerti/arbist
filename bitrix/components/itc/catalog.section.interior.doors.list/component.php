<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams["SECTION_ID"] = intval($arParams["~SECTION_ID"]);

// if($arParams["SECTION_ID"] > 0 && $arParams["SECTION_ID"]."" != $arParams["~SECTION_ID"])
// {
	// ShowError(GetMessage("CATALOG_SECTION_NOT_FOUND"));
	// @define("ERROR_404", "Y");

	// if($arParams["SET_STATUS_404"]==="Y") {
        // LocalRedirect("/404.php");
		// //CHTTP::SetStatus("404 Not Found");
    // }
	// return;
// }

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global $$arParams["FILTER_NAME"];
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}

$arParams["SHOW_SEO_TEXT"] = empty($_REQUEST["PAGEN_1"]) || ($_REQUEST["PAGEN_1"] == 1) ? true : false;
/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array($arrFilter, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()))))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
    $arCollectionsByBrand = array();

    $arFilter = array(
        "IBLOCK_ID" => INTERIOR_DOORS_IBLOCK_ID,
        "ACTIVE"    => "Y"
    );
    
    $arFilter = array_merge((array)$arrFilter, (array)$arFilter);
    
    /*���������� ������������� ��������� �� �������*/
    $rsElements = CIBlockElement::GetList(
        array(),
        $arFilter,
        array("PROPERTY_BRAND_GUID","IBLOCK_SECTION_ID")
    );

    $arCollectionIds = array();
    $arBrandIds = array();
    $arCollectionId2BrandId = array();

    $arCollections = array();
    $arBrands = array();

    //���������� �� ����������
    while($arElement = $rsElements->GetNext()) {
        $collectionId = $arElement["IBLOCK_SECTION_ID"];
        $brandId = $arElement["PROPERTY_BRAND_GUID_VALUE"];
        
        $arCollectionIds[] = $collectionId;
        $arBrandIds[] = $brandId;
        
        $arCollectionId2BrandId[ $collectionId ] = $brandId;
        
        $arCollectionsByBrand[ $brandId ]["COLLECTIONS"][] = $collectionId;
    }

    $arFilter = array(
        "IBLOCK_ID" => INTERIOR_DOORS_IBLOCK_ID,
        "ACTIVE"    => "Y",
        "ID"        => $arCollectionIds
    );
    $rsSections = CIBlockSection::GetList(
        array(),
        $arFilter
    );
    while($arSection = $rsSections->GetNext()) {
        $arSection["SECTION_PAGE_URL"] = "/interior_doors/" . $arParams["PROP_CODE"] . "/" . $arSection["ID"] . "/";
        $arCollections[ $arSection["ID"] ] = $arSection;
    }

    //���������� �� �������
    CModule::IncludeModule('iblock');
    $arFilter = array(
        "IBLOCK_ID" => BRANDS_IBLOCK_ID,
        "ACTIVE"    => "Y",
        "ID"        => $arBrandIds
    );
    $rsElements = CIBlockElement::GetList(
        array(),
        $arFilter
    );
    while($arElement = $rsElements->GetNext()) {
        $arBrands[ $arElement["ID"] ] = $arElement;
    }
    //get country of brand
	$arFilter = array(
        "IBLOCK_ID" => INTERIOR_DOORS_IBLOCK_ID,
        "ACTIVE"    => "Y",
        "IBLOCK_SECTION_ID"        => $arBrands[ $arCollectionId2BrandId[ $arParams["SECTION_ID"] ] ]["ID"],
		"!PROPERTY_COUNTRY" => false,
    );
    $rsElements = CIBlockElement::GetList(
        array(),
        $arFilter,
		false,
		array("nTopCount"=>"1"),
		array("PROPERTY_COUNTRY")
    );
    while($arElement = $rsElements->GetNext()) {
       $COUNTRY=$arElement["PROPERTY_COUNTRY_VALUE"];
		
    }
    //�������� SEO-������
    CModule::IncludeModule('iblock');
    $arFilter = array(
        "IBLOCK_ID" => SEO_INTERIOR_DOORS_IBLOCK_ID,
        "ACTIVE"    => "Y",
        "FILTER_PROP_CODE" => $arParams["PROP_CODE"]
    );
    
    if($arParams["SECTION_ID"]) {
        $arFilter["PROPERTY_COLLECTION"] = $arParams["SECTION_ID"];
    } else {
        $arFilter["PROPERTY_COLLECTION"] = false;
    }
    
    $rsElements = CIBlockElement::GetList(
        array(),
        $arFilter
    );
    while($arElement = $rsElements->GetNext()) {
        $arResult["UPPER_SEO_TEXT"] = $arElement["PREVIEW_TEXT"];
        $arResult["LOWER_SEO_TEXT"] = $arElement["DETAIL_TEXT"];
    }
    
    $arResult["BRANDS"] = $arBrands;
    $arResult["COLLECTIONS"] = $arCollections;
    $arResult["COLLECTIONS_BY_BRAND"] = $arCollectionsByBrand;
    
    $arFilterMerge = array_merge((array)$arrFilter, (array)$arFilter);
    $NAME1=$arParams["PROP_NAME"]; 
    $NAMEH1=$arParams["PROP_NAME"]; 
	 switch($arParams["PROP_CODE"]){
            case 'painted_doors':
                $NAME2 = '���������� �����';
				$NAME23= '������������ ���������� �����';
                $NAME3 = '�������� ������';
                $NAME4 = '�������� ������';
                $NAME5 = '���������� �����';
                break; 
			case 'white_doors':
                $NAME2 = '����� �����';
				$NAME23= '������������ ����� �����';
                $NAME3 = '����� ������';
                $NAME4 = '������ ������ �����';
                $NAME5 = '����� �����';
                break;
			case 'veneered_doors':
                $NAME2 = '������ �� �����';
				$NAME23= '������������ ������������� �����';
                $NAME3 = '������ �� �����';
                $NAME4 = '������ �� �����';
                $NAME5 = '������������� �����';
                break;
			case 'with_glass_doors':
                $NAME2 = '����� �� �������';
				$NAME23= '������������ ����� �� �������';
                $NAME3 = '������ �� �������� �� ������';
                $NAME4 = '������ �� �������';
                $NAME5 = '����� ������������ �� �������';
                break;
			case 'glossy_doors':
                $NAME2 = '����� ������';
				$NAME23= '������������ ��������� �����';
                $NAME3 = '������ ������';
                $NAME4 = '������ ������';
                $NAME5 = '��������� �����';
                break;
			case 'ecospun_doors':
                $NAME2 = '����� �� ��������';
				$NAME23= '������������ ����� �������';
                $NAME3 = '������ �� ��������';
                $NAME4 = '������ �� ��������';
                $NAME5 = '����� �������';
                break;
           
			case 'glass_doors':
                $NAME1 = '���������� �����';
				$NAME2 = '���������� �����';
				$NAME23= '������������ ����� �� ������';
                $NAME3 = '������ ����������';
                $NAME4 = '���������� ������';
                $NAME5 = '����� ����������';
				$NAMEH1 = '���������� ������������ �����';
                break;
           
           
			case 'enamel_doors':
                $NAME1 = '�������� �����';
				$NAME2 = '������������� �����';
				$NAME23= '������������ ������������� �����';
                $NAME3 = '������ ��������';
                $NAME4 = '������������� ������';
                $NAME5 = '�������� �����';
				$NAMEH1 = '����� ����� (�������������)';
                break;
           
           
			case 'array_doors':
                $NAME2 = '����� �� �������';
				$NAME23= '������������ ����� �������';
                $NAME3 = '������ � �������� �������� ������';
                $NAME4 = '������ �� �������';
                $NAME5 = '������ �� �������';
				$NAMEH1 = '����� � �������� �������� ������';
                break;
           
			case 'deaf_doors':
                $NAME2 = '������ �����';
				$NAME23= '������������ ������ �����';
                $NAME3 = '������ ������';
                $NAME4 = '������ ������';
                $NAME5 = '������ �����';
				$NAMEH1 = '������ ������������ �����';
                break;
           
			
           
           default:
                $NAME2 = $arParams["PROP_NAME"];
                $NAME23 = $arParams["PROP_NAME"];
                $NAME3 = $arParams["PROP_NAME"];
                $NAME4 = $arParams["PROP_NAME"];
                $NAME5 = $arParams["PROP_NAME"];
                break;
        }
    $arResult["PAGE_TITLE"] = $NAME1.' - �������, ���� �� ������������ '.$NAME2.' � ������';
    if($arParams["SECTION_ID"]){
        $arResult["PAGE_TITLE"] = $arParams["PROP_NAME"].' '.$arCollections[ $arParams["SECTION_ID"] ]["NAME"] . ", " . $arBrands[ $arCollectionId2BrandId[ $arParams["SECTION_ID"] ] ]["NAME"]." - ������ �� ������ ���� � ������";
    }
    $arResult["H1"] = $NAMEH1;
    if($arParams["SECTION_ID"]){
        $arResult["H1"] = $arParams["PROP_NAME"]." ". $arCollections[ $arParams["SECTION_ID"] ]["NAME"] . ", " . $arBrands[ $arCollectionId2BrandId[ $arParams["SECTION_ID"] ] ]["NAME"];
    } 
	$arResult["DESCRIPTION"] = $NAME23.' - ������� ������ � ��������-�������� Arbist, ������� �������� '.$NAME3.', ������� �������� � ��������.';
    if($arParams["SECTION_ID"]){
        $arResult["DESCRIPTION"] = '� ������� '.mb_strtolower($arParams["PROP_NAME"]).' ������������ '.$COUNTRY.' ��������� '.$arCollections[ $arParams["SECTION_ID"] ]["NAME"] . ", " . $arBrands[ $arCollectionId2BrandId[ $arParams["SECTION_ID"] ] ]["NAME"].' �� �������� ����� � ��������� ��������� � ����������������.';
		
    }
	$arResult["UPPER_SEO_TEXT"] = '������� '.$NAME4.' �� ������� �������������� � ��������� ��������� � ����������������.';
	$arResult["LOWER_SEO_TEXT"] = '��������-������� ������ ���������� ������� � ������ ������������ '.$NAME5.' �� ���������� ���� �� �������� ��������.';
   
	
    $arResult["BREADCRUMBS_TITLE"] = $arResult["H1"];
    
	$this->SetResultCacheKeys(array(
            "PAGE_TITLE",
            "BREADCRUMBS_TITLE",
            "H1",
			"DESCRIPTION"
        )
    );
	
	$this->IncludeComponentTemplate();
}


global $APPLICATION;
    
if($arResult["PAGE_TITLE"]){
    $APPLICATION -> SetPageProperty('title', $arResult["PAGE_TITLE"]);
}    
if($arResult["BREADCRUMBS_TITLE"]){
    if($arParams["SECTION_ID"]){
        $APPLICATION->AddChainItem($arParams["PROP_NAME"], '/interior_doors/' . $arParams["PROP_CODE"] . "/");
    }
    $APPLICATION->AddChainItem($arResult["BREADCRUMBS_TITLE"], '');
}
if($arResult["H1"]){
    $APPLICATION->SetPageProperty('h1',$arResult["H1"]);
}  
if($arResult["DESCRIPTION"]){
    $APPLICATION->SetPageProperty('description',$arResult["DESCRIPTION"]);
}  
?>
