<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!empty($arResult["NAV_TOP"]))
	echo $arResult["NAV_TOP"];
$i = 0;
//echo "<pre>"; print_r($arResult["SECTIONS"][0]); echo "</pre>";
foreach($arResult["SECTIONS"] as $arSection){
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
?>
	<?if($i == 0) {?> 
		<div class="gap1 row"> 
	<?}?>
			<div class="item1 item b-item3-on-row" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
				<div class="image1 image b-light-border ceramic-img">
					<div class="labels">

					</div>
					<a href="<?=$arSection["SECTION_PAGE_URL"]?>">
					<?if(!empty($arSection["PICTURE_PATH"]) && fopen($_SERVER["DOCUMENT_ROOT"] . '/images/catalog/'.$arSection["PICTURE_PATH"],'r')):?> <?/*PICTURE*/?>
						<?$pic = split("\.", $arSection["PICTURE_PATH"]);?>
						<img src="/rpic_path/c/<?=$pic[0]?>_h152_w185_crop.<?=$pic[1]?>" alt=""/>
					<?else:?>
						<img src="/img/noimg0.jpg" alt="" width="136" height="93"/>
					<?endif;?>
					</a> 
				</div>
				<div class="desc">

					<div><a  class="light-red-link" href="<?=$arSection["SECTION_PAGE_URL"]?>"><span class="small-seo-text"><?=$APPLICATION->getProperty("h1");?></span><br/><?=$arSection["NAME"]?></a></div>
                    <? if (!empty($arSection["MIN_PRICE"]) && $arSection['PROPERTY_CHANGE_UNIT'] == '��.�.') { ?> <div class="minimal-category-price minimal-category-price_bg hl0">�� <?=round($arSection["MIN_PRICE"]);?> <span class="b-rub">�</span> �� <?=$arSection['PROPERTY_CHANGE_UNIT'];?></div> <?}?>
				</div>
			</div>
	<?if($i == 2) {
		$i = 0;
	?>
		</div> 
	<?
	} else {
		$i++;
	}
	?>
<?
}
if($i != 0) {
?>
	</div>
<?
}
if(!empty($arResult["NAV_BOT"]))
	echo $arResult["NAV_BOT"];
?>

<?
if(!empty($arResult['NAV_BUTTONS'])) {
	?>
	<div style="width: 100%; text-align: center; margin-top: 30px;">
		<?
		foreach($arResult['NAV_BUTTONS'] as $button) {
			?>
			<div class="new_button<?/* width50*/?>">
				<?/*<button onclick="window.location.href='<?=$button['PATH']?>'" type="button"><?=$button['NAME']?></button>*/?>
                <a class="a-button" href="<?=$button['PATH']?>" type="button"><?=$button['NAME']?></a>
			</div>
			<?
		}
		?>
		<div style="clear: both;"></div>
	</div>
	<?
}	
?>
