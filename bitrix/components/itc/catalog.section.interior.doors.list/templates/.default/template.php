<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?


if($arParams["SHOW_SEO_TEXT"] && $arResult["UPPER_SEO_TEXT"]){?>
    <div class="vendor-texts">
        <?=$arResult["UPPER_SEO_TEXT"]?>
    </div>
<?}
foreach($arResult["COLLECTIONS_BY_BRAND"] as $brandId => $arBrand){
    $brandName = $arResult["BRANDS"][ $brandId ]["NAME"];?>
    <h3 class="h3-brand-name"><?=$brandName?></h3>
    <?foreach($arBrand["COLLECTIONS"] as $collectionId){
        $arCollection = $arResult["COLLECTIONS"][ $collectionId ];
        $collectionName = $arCollection["NAME"];
        $collectionUrl = $arCollection["SECTION_PAGE_URL"];?>
        <div style="width:50%; float:left">		
            <div style="position: relative; margin-bottom: 5px !important;" class="gap1">
                <h3 class="link0 alt0 fs14">
                    <a href="<?=$collectionUrl?>#<?=$collectionId?>">
                        <?=$collectionName?>
                    </a>
                </h3>
            </div>
        </div>
    <?
    }
    ?>
    <div class="collection-block-end"></div>
<?
}
if($arParams["SHOW_SEO_TEXT"] && $arResult["LOWER_SEO_TEXT"]){?>
    <?itc\CUncachedArea::startCapture();?>
        <div class="vendor-texts">
            <?=$arResult["LOWER_SEO_TEXT"]?>
        </div>
    <?$lowerSeoText = itc\CUncachedArea::endCapture();
    itc\CUncachedArea::setContent('lowerSeoText', $lowerSeoText);
    ?>   
<?}
?>