<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 14.10.2016
 * Time: 11:28
 * Используется на странице продукта, чтобы получить линию из 5 категорий для Яндекс Метрики и Google Analytics
 * Так как можно передать только 5 категорий, выбираются первые 5.
 */
$strReturn = '';
$i=0;
for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{

    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    if($i < 5) {
        $strReturn .= $title.'/';
    }
    $i++;
}
return $strReturn;