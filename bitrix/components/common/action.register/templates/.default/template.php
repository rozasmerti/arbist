<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arResult["ERRORS"]): ?>
    <div class="action-messages">
        <? foreach ($arResult["ERRORS"] as $error): ?>
            <div><? print $error; ?></div>
        <? endforeach; ?>
        <div class="clear"></div>
    </div>
<? else: ?>
    <div style="height:20px;">&nbsp;</div>
<? endif; ?>
<div class="action register">
    <div class="action action-top">
        <div class="left_column">
            <form action="" method="post">
                <input type="hidden" name="sent" value="Y">
                <ul class="action form-header">
                    <li>�����������</li>
                </ul>
                <div class="action form-content">
                    <ul>
                        <? foreach ($arResult['FIELDS'] as $arField): ?>
                            <li>
                                <span class="label"
                                      for="<?= strtolower($arField['CODE']) ?>"><?= $arField['NAME'] ?></span>
                                <input type="text" name="<?= strtolower($arField['CODE']) ?>"
                                       id="<?= strtolower($arField['CODE']) ?>" value="<?= $arField['VALUE'] ?>">
                            </li>
                        <? endforeach; ?>
                    </ul>
                    <input type="submit" value="������������������" class="submit">
                    <div class="iagree">������� ������ "��������", � ��� �������� �� <a href="javascript:void(0);">�������������
                            �����</a> �� ��������� ������������ ������.
                    </div>
            </form>
        </div>
    </div>
    <div class="right_column">
        <div class="action text">
            <h2>������ ��� ��������</h2>
            <img src="/images/a/zima_banner_small.png" align="right">
            ������ ����! ������ ���� �����, ������ � ��� ����������� ���� ��� ������������������ �����������! �������
            ������ ��� ��������!
        </div>
        <!--<div class="pull-right"></div>-->
        <div class="clearfix" style="height:18px;"></div>
        <div class=""><img class="pagelet" src="/images/a/oboi.jpg"></div>

    </div>
</div>
<div class="clearfix"></div>
<div class="action middle">
    <ul>
        <li><img class="pagelet" src="/images/a/plitka.jpg"></li>
        <li><img class="pagelet right" src="/images/a/laminat.jpg"></li>
    </ul>
</div>
<div class="action footer"></div>
</div>


<div id="agreementForm" style="display: none;">

    ���������, �, (����� � ����), ��� ���� ��������
    <div class="companyProfile"><span class="companyName">�� "��������� ������� ����������"</span>, <span
            class="addressLabel">����������� �����:</span> <span class="address">127411 �. ������, ����������� �., �.157, ���.12/2, ��.122-208,</span>
        <span class="OgrnLabel">����</span> <span class="OGRN">311774617100733</span></div>
    (����� � ��������) �� ��������� ����� ������������ ������, ��������� ��� ����������� �� ����� ��������.<br>
    <p class="agreeText">��������� (����, ��������������, ����������, ��������, ���������, �������������, �������������,
        ��������, ������������, �����������) ������������ ������ ���� �������������� ��� ����� ����������� ���� �� �����
        �������� � ����� ������� ���� �� ����, ��� ��������� ������������ � ���� ����������, ������� ���� �����
        ����������� � ������ ������������ ���������������� �� � ����������� ������������ ��������.</p>

    <p class="agreeText">��������� ������������ ������ ���� ����� �������������� � ������� ������� ������������� �/���
        ��� ������������� ������� ������������� � ������������ � ����������� ����������������� �� � �����������
        ��������. ��������� ���� ����������� �� �������� ����� ������������ ������ ������� ����� ��� �� ��������� �
        ������������ � ������, ���������������� ��������� ���������, �� ��������� ���������, ����������� ��������� �
        ����� ������. ��������� �������� ���� �� ��������� ���/�� ������������ ������, ��������� ��� ����������� ���� ��
        ����� ��������, ������������ (�����������) � �������������� ���������� �����, ��������� � ������� �����������
        ���� �� ����� �������� �� ������� ��� ������. �������� �� ��������� ������������ ������, ��������� ���
        ����������� ���� �� ����� ��������, ������������ (�����������) � �������������� ���������� �����, ����� ����
        �������� ����� ��� ������ ����������� ��������� (������) � ��������. ��������� ������������ ������ ����
        ������������ ���������� � ������� ��������� ��������� ����������� ��������� (������) ���� �/��� � ������
        ���������� ���� ��������� � ������������ � ���� � �� ��������, ������������� �������, ���� �� �������������
        ����.</p>
    <span class="agree">��������� ���� ��� ����������� �� ����� �������� ������������ ������������� ��������� ����������.</span>

</div>
</div>