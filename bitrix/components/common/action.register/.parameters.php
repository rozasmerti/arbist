<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypes = CIBlockParameters::GetIBlockTypes();

/* IBLOCKS */
$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

 /* LINKED PROPERTY */   
$arProperty_E = array();
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"]));
while ($arr=$rsProp->Fetch())
{
    $arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
    if (in_array($arr["PROPERTY_TYPE"], array("E")))
    {
        $arProperty_L[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
    }
}


$arComponentParameters = array(
	"GROUPS" => array(
        "BASE" => array(
            "NAME" => GetMessage("GROUP_BASE"),
        ),
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypes,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
        "DISPLAY_PROPERTIES" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DISPLAY_PROPERTIES"),
            "TYPE" => "LIST",
            "VALUES" => $arProperty,
            "ADDITIONAL_VALUES" => "Y",
            "MULTIPLE" => "Y",
            "SIZE" => 5,
        ),
        "ORDER_PAGE" => array(
            "PARENT" => "BASE",
            "NAME" => "Ссылка на отдельную страницу заказа",
            "TYPE" => "TEXT",
            "DEFAULT" => "/personal/order/make/",
        ),
	),
);
?>