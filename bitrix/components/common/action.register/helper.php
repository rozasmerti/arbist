<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CActionRegisterHelper
{
    /**
     * @var array
     */
    public $errors = array();

    public function validateData($arFieldsList = array(), $sent = false)
    {
        global $USER, $DB;
        $format = $DB->DateFormatToPHP(FORMAT_DATETIME);

        $result = array(
            'ERRORS' => array(),
            'FIELDS' => array()
        );

        foreach ($arFieldsList as $key => $arField) {

            $saveField = true;
            switch ($arField['TYPE']) {
                case 'PHONE':
                    if (isset($arField['VALUE']) && strlen($arField['VALUE']) > 0) {
                        $arField['VALUE'] = strval($arField['VALUE']);
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    $arField['VALID'] = Local\Lib\Helpers\Validation::validatePhone($arField['VALUE']);
                    break;

                case 'LOGIN':
                    $arField['~DEFAULT_VALUE'] = $_COOKIE[COption::GetOptionString("main", "cookie_name", "BITRIX_SM") . "_LOGIN"];
                    $arField['DEFAULT_VALUE'] = htmlspecialchars($arField['~VALUE']);

                    if (isset($arField['VALUE']) && strlen($arField['VALUE']) > 0) {
                        $arField['VALUE'] = strval($arField['VALUE']);
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    break;

                case 'TEXT':
                    if (isset($arField['VALUE'])) {
                        if (is_array($arField['VALUE'])) {
                            foreach ($arField['VALUE'] as &$value) {
                                $value = strval($value);
                            }
                        } elseif (strlen($arField['VALUE']) > 0) {
                            $arField['VALUE'] = strval($arField['VALUE']);
                        }
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    $arField['VALID'] = true;
                    break;

                case 'EMAIL':
                    if (isset($arField['VALUE']) && strlen($arField['VALUE']) > 0) {
                        $arField['VALUE'] = strval($arField['VALUE']);
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    $arField['VALID'] = Local\Lib\Helpers\Validation::validateEmail($arField['VALUE']);
                    break;

                default:
                    $saveField = false;
            }

            if (is_array($arField['VALUE'])) {
                foreach ($arField['VALUE'] as &$value) {
                    $value = htmlspecialchars($value);
                }
            } else {
                $arField['VALUE'] = htmlspecialchars($arField['VALUE']);
            }

            //setting errors
            $arField['ERROR'] = '';
            if ($sent) {
                if ($arField['REQUIRED'] && empty($arField['VALUE'])) {
                    $errorText = '���� ' . $arField['NAME'] . ' ������������';
                    $arField['ERROR'] = $errorText;
                    $result['ERRORS'][] = $errorText;
                } elseif ($arField['REQUIRED'] || !empty($arField['VALUE'])) {
                    if (!$arField['VALID']) {
                        $errorText = '��������� ���� "' . $arField['NAME'] . '"';
                        $arField['ERROR'] = $errorText;
                        $result['ERRORS'][] = $errorText;
                    }
                }
            } else {
                if (empty($arField['VALUE']) && !empty($arField['DEFAULT_VALUE'])) {
                    $arField['VALUE'] = $arField['DEFAULT_VALUE'];
                }
            }

            if ($saveField)
                $result['FIELDS'][$key] = $arField;
        }

        return $result;
    }

    public function sendEmail($arResult)
    {
        global $DB, $USER;

        $event = new CEvent;

        $arFields = Array(
            "EMAIL" => $arResult['FIELDS']['EMAIL']['VALUE'],
            "NAME" => $arResult['FIELDS']['NAME']['VALUE'],
            "LOGIN" => $arResult['FIELDS']['EMAIL']['VALUE'],
            "PASSWORD" => $arResult['PASSWORD'],
        );

        $event->Send("ACTION_REGISTER", SITE_ID, $arFields, 'Y');
    }
}