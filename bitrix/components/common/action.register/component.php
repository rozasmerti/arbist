<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('catalog')) {
    die('NO CATALOG MODULE');
}

if (!CModule::IncludeModule('iblock')) {
    die('NO IBLOCK MODULE');
}

if (!CModule::IncludeModule('sale')) {
    die('NO SALE MODULE');
}

if (!CModule::IncludeModule('local.lib')) {
    die('NO lical.lib MODULE');
}

global $USER;

$arResult = array(
    'ERRORS' => array()
);

require_once __DIR__ . '/helper.php';

$helper = new CActionRegisterHelper();


$dataSent = false;
if ($_REQUEST['sent'] == 'Y') {
    $dataSent = true;
}
$result = $helper->validateData($arParams['FIELDS'], $dataSent);
$arResult['FIELDS'] = $result['FIELDS'];
$arResult['ERRORS'] = array_merge($arResult['ERRORS'], $result['ERRORS']);

if ($dataSent) {
    if (!in_array(strtolower($arResult['FIELDS']['ACTION']['VALUE']), $arParams['ACTION_CODES'])) {
        $arResult['ERRORS'][] = '������� ������ ��� �����';
    }

    if (empty($arResult['ERRORS'])) {
        $arResult['PASSWORD'] = randString(7);

        $added = $USER->Add(array(
            'LOGIN' => $arResult['FIELDS']['EMAIL']['VALUE'],
            'PASSWORD' => $arResult['PASSWORD'],
            'NAME' => $arResult['FIELDS']['NAME']['VALUE'],
            'EMAIL' => $arResult['FIELDS']['EMAIL']['VALUE'],
            "GROUP_ID" => $arParams['GROUP_ID'],
        ));

        if (!$added) {
            $arResult['ERRORS'][] = $USER->LAST_ERROR;
        }

        if (empty($arResult['ERRORS'])) {
            $helper->sendEmail($arResult);

            if(isset($arParams['REDIRECT_URL']) && strlen($arParams['REDIRECT_URL']) > 0){
                LocalRedirect($arParams['REDIRECT_URL']);
            }
        }
    }
}

$this->IncludeComponentTemplate();