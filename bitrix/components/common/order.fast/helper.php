<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CFastOrderHelper
{
    /**
     * @var \CSaleBasket
     */
    public $CSaleBasket;

    /**
     * @var CIBlockElement
     */
    public $CIBlockElement;

    /**
     * @var \CSaleOrder
     */
    public $CSaleOrder;

    /**
     * @var array
     */
    public $errors = array();

    function __construct()
    {
        $this->CSaleBasket = new CSaleBasket();
        $this->CIBlockElement = new CIBlockElement();
        $this->CSaleOrder = new CSaleOrder();
    }

    /**
     * @return array
     */
    public function getCurrentBasket()
    {
        $result = array(
            'basket' => array(),
            'product_ids' => array(),
            'total' => 0
        );

        //getting current basket
        $dbBasketItems = $this->CSaleBasket->GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
            array(
                "FUSER_ID" => $this->CSaleBasket->GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL",
                "DELAY" => 'N',
                'CAN_BUY' => 'Y'
            ),
            false,
            false,
            array(
                "ID", "NAME", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "PRODUCT_PRICE_ID", "QUANTITY", "DELAY", "CAN_BUY",
                "PRICE", "WEIGHT", "DETAIL_PAGE_URL", "NOTES", "CURRENCY", "VAT_RATE", "CATALOG_XML_ID",
                "PRODUCT_XML_ID", "SUBSCRIBE", "DISCOUNT_PRICE", "PRODUCT_PROVIDER_CLASS", "TYPE", "SET_PARENT_ID"
            )
        );

        $elementIDs = array();
        while ($arItem = $dbBasketItems->Fetch()) {
            if (strlen($arItem["CALLBACK_FUNC"]) > 0) {
                $this->CSaleBasket->UpdatePrice($arItem["ID"],
                    $arItem["CALLBACK_FUNC"],
                    $arItem["MODULE"],
                    $arItem["PRODUCT_ID"],
                    $arItem["QUANTITY"]);
                $arItem = $this->CSaleBasket->GetByID($arItem["ID"]);
            }

            $arItem["PRICE"] = round($arItem["PRICE"]);
            
            $arItem['PRICE_FORMATED'] = SaleFormatCurrency($arItem['PRICE'], $arItem['CURRENCY']);

            $result['total'] += floatval($arItem['PRICE'] * $arItem['QUANTITY']);

            $result['product_ids'][$arItem['PRODUCT_ID']] = $arItem['PRODUCT_ID'];
            $result['basket'][$arItem['ID']] = $arItem;
        }

        sort($result['product_ids']);

        return $result;
    }

    /**
     * @param array $elementIDs
     *
     * @return array
     */
    public function getProducts($elementIDs = array())
    {
        $productsList = array();

        if (!empty($elementIDs)) {
            $arFilter = array(
                'ACTIVE' => 'Y',
                'ID' => $elementIDs
            );

            $arSort = array(
                'SORT' => 'ASC',
                'NAME' => 'ASC'
            );

            $rsElements = $this->CIBlockElement->GetList($arSort, $arFilter);

            while ($obElement = $rsElements->GetNextElement()) {
                $arElement = $obElement->GetFields();
                $arElement['PROPERTIES'] = $obElement->GetProperties();

                $image = new \Local\Lib\Parts\Image(array(
                    'width' => 75,
                    'height' => 75
                ));
                $image->addImage($arElement['PROPERTIES']['PHOTO_NAME']['VALUE']);
                $image->addImage(intval($arElement['DETAIL_PICTURE']));
                $image->addImage(intval($arElement['PREVIEW_PICTURE']));
                $arElement['IMAGE'] = $image->getFirstResized();

                $productsList[$arElement['ID']] = $arElement;
            }
        }

        return $productsList;
    }

    public function getIBlocks()
    {
        $arIBlockList = array();

        $rsIBlocks = \CIBlock::GetList(array(), array('TYPE' => 'catalog'));

        while ($arIBlock = $rsIBlocks->Fetch()) {
            $arIBlockList[$arIBlock['ID']] = $arIBlock;
        }

        return $arIBlockList;
    }

    public function getMenu()
    {
        $arSectionList = array();

        $rsSections = \CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => 47
            )
        );

        while ($arSection = $rsSections->Fetch()) {
            $arSection['ELEMENTS'] = array();
            $arSectionList[$arSection['ID']] = $arSection;
        }

        $arFilter = array(
            'IBLOCK_ID' => 47,
            'ACTIVE' => 'Y'
        );

        $arSort = array(
            'SORT' => 'ASC',
            'NAME' => 'ASC'
        );

        $rsElements = \CIBlockElement::GetList($arSort, $arFilter, false, false, array(
            'ID',
            'IBLOCK_ID',
            'IBLOCK_SECTION_ID',
            'NAME',
            'PROPERTY_IBLOCK_ID'
        ));

        while ($arElement = $rsElements->Fetch()) {
            if (isset($arSectionList[$arElement['IBLOCK_SECTION_ID']])) {
                $arSectionList[$arElement['IBLOCK_SECTION_ID']]['ELEMENTS'][$arElement['ID']] = $arElement;
            }
        }

        return $arSectionList;
    }

    public function validateData($arFieldsList = array(), $sent = false)
    {
        global $USER, $DB;
        $format = $DB->DateFormatToPHP(FORMAT_DATETIME);

        $result = array(
            'ERRORS' => array(),
            'FIELDS' => array()
        );

        foreach ($arFieldsList as $key => $arField) {

            $saveField = true;
            switch ($arField['TYPE']) {
                case 'PHONE':
                    if (isset($arField['VALUE']) && strlen($arField['VALUE']) > 0) {
                        $arField['VALUE'] = strval($arField['VALUE']);
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    $arField['VALID'] = Local\Lib\Helpers\Validation::validatePhone($arField['VALUE']);
                    break;

                case 'LOGIN':
                    $arField['~DEFAULT_VALUE'] = $_COOKIE[COption::GetOptionString("main", "cookie_name", "BITRIX_SM") . "_LOGIN"];
                    $arField['DEFAULT_VALUE'] = htmlspecialchars($arField['~VALUE']);

                    if (isset($arField['VALUE']) && strlen($arField['VALUE']) > 0) {
                        $arField['VALUE'] = strval($arField['VALUE']);
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    break;

                case 'TEXT':
                    if (isset($arField['VALUE'])) {
                        if (is_array($arField['VALUE'])) {
                            foreach ($arField['VALUE'] as &$value) {
                                $value = strval($value);
                            }
                        } elseif (strlen($arField['VALUE']) > 0) {
                            $arField['VALUE'] = strval($arField['VALUE']);
                        }
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    $arField['VALID'] = true;
                    break;

                case 'EMAIL':
                    if (isset($arField['VALUE']) && strlen($arField['VALUE']) > 0) {
                        $arField['VALUE'] = strval($arField['VALUE']);
                    } else {
                        $code = strtolower($arField['CODE']);
                        if (isset($_REQUEST[$code]) && strlen($_REQUEST[$code]) > 0) {
                            $arField['VALUE'] = strval($_REQUEST[$code]);
                        } else {
                            $arField['VALUE'] = '';
                        }
                    }
                    $arField['VALID'] = Local\Lib\Helpers\Validation::validateEmail($arField['VALUE']);
                    break;

                default:
                    $saveField = false;
            }

            if (is_array($arField['VALUE'])) {
                foreach ($arField['VALUE'] as &$value) {
                    $value = htmlspecialchars($value);
                }
            } else {
                $arField['VALUE'] = htmlspecialchars($arField['VALUE']);
            }

            //setting errors
            $arField['ERROR'] = '';
            if ($sent) {
                if ($arField['REQUIRED'] && empty($arField['VALUE'])) {
                    $errorText = '���� ' . $arField['NAME'] . ' ������������';
                    $arField['ERROR'] = $errorText;
                    $result['ERRORS'][] = $errorText;
                } elseif ($arField['REQUIRED'] || !empty($arField['VALUE'])) {
                    if (!$arField['VALID']) {
                        $errorText = '��������� ���� "' . $arField['NAME'] . '"';
                        $arField['ERROR'] = $errorText;
                        $result['ERRORS'][] = $errorText;
                    }
                }
            } else {
                if (empty($arField['VALUE']) && !empty($arField['DEFAULT_VALUE'])) {
                    $arField['VALUE'] = $arField['DEFAULT_VALUE'];
                }
            }

            if ($saveField)
                $result['FIELDS'][$key] = $arField;
        }

        return $result;
    }

    protected function getOrderProps()
    {
        $rsProps = CSaleOrderProps::GetList(
            array("SORT" => "ASC"),
            array(
                'PERSON_TYPE_ID' => 1
            ),
            false,
            false,
            array()
        );

        $arPropsList = array();
        while ($arProp = $rsProps->GetNext()) {
            $arPropsList[$arProp["ID"]] = $arProp;
        }

        return $arPropsList;
    }

    public function addOrder($price, $arSentData)
    {
        global $USER, $APPLICATION;

        $result = array();

        $user_id = 15220;
        if ($USER->IsAuthorized()) {
            $user_id = $USER->GetID();
        }

        $arFields = array(
            "LID" => SITE_ID,
            "PERSON_TYPE_ID" => 1,
            "PAYED" => "N",
            "CANCELED" => "N",
            "STATUS_ID" => "N",
            "PRICE" => $price,
            "CURRENCY" => 'RUB',
            "USER_ID" => $user_id,
            "PAY_SYSTEM_ID" => 7,
            "PRICE_DELIVERY" => 0,
            "DELIVERY_ID" => 3,
            "DISCOUNT_VALUE" => 0,
            "TAX_VALUE" => 0,
            "COMMENTS" => "�������� �����."
        );

        // add Guest ID
        if (CModule::IncludeModule("statistic"))
            $arFields["STAT_GID"] = \CStatistic::GetEventParam();

        $affiliateID = \CSaleAffiliate::GetAffiliate();
        if ($affiliateID > 0)
            $arFields["AFFILIATE_ID"] = $affiliateID;
        else
            $arFields["AFFILIATE_ID"] = false;

        $result["ORDER_ID"] = $this->CSaleOrder->Add($arFields);
        $result["ORDER_ID"] = intval($result["ORDER_ID"]);

        if ($result["ORDER_ID"] <= 0) {
            if ($ex = $APPLICATION->GetException())
                $result["ERRORS"][] = $ex->GetString();
            else
                $result["ERRORS"][] = "������, ���������� �������� � ��� ������������� ��������";
        }

        if (empty($result["ERRORS"])) {
            $arPropsList = $this->getOrderProps();
            foreach ($arPropsList as $arProp) {
                if (!empty($arSentData[$arProp['CODE']]['VALUE'])) {
                    $arFields = array(
                        "ORDER_ID" => $result["ORDER_ID"],
                        "ORDER_PROPS_ID" => $arProp["ID"],
                        "NAME" => $arProp["NAME"],
                        "CODE" => $arProp["CODE"],
                        "VALUE" => $arSentData[$arProp['CODE']]['VALUE'],
                    );

                    CSaleOrderPropsValue::Add($arFields);
                }
            }

            $this->CSaleBasket->OrderBasket($result["ORDER_ID"], $this->CSaleBasket->GetBasketUserID(), SITE_ID);
        }

        return $result;
    }

    public function sendEmail($arResult)
    {
        global $DB, $USER;

        $event = new CEvent;

        $strOrderList = "";
        foreach ($arResult["BASKET"] as $arItem) {
            $strOrderList .= $arItem["NAME"];
            $strOrderList .= "\n";
        }

        $email = 'express@test.ru';
        if($USER->IsAuthorized()) {
            $email = strlen($arResult['ORDER_FIELDS']["EMAIL"]['VALUE']) > 0 ? $arResult['ORDER_FIELDS']["EMAIL"]['VALUE'] : $USER->GetEmail();
        }
        $arFields = Array(
            "ORDER_ID" => $arResult["ORDER_ID"],
            "ORDER_DATE" => date($DB->DateFormatToPHP(\CLang::GetDateFormat("SHORT", SITE_ID))),
            "ORDER_USER" => ((strlen($arResult['ORDER_FIELDS']['CONTACT_PERSON']['VALUE']) > 0) ? $arResult['ORDER_FIELDS']['CONTACT_PERSON']['VALUE'] : $USER->GetFullName()),
            "PRICE" => $arResult['BASKET_TOTAL'] . ' �.',
            "BCC" => COption::GetOptionString("sale", "order_email", "order@" . $SERVER_NAME),
            "EMAIL" => $email,
            "ORDER_LIST" => $strOrderList,
            "SALE_EMAIL" => COption::GetOptionString("sale", "order_email", "order@" . $SERVER_NAME)
        );

        $event->Send("SALE_NEW_ORDER", SITE_ID, $arFields, 'Y');

        if (!$USER->IsAuthorized() && strlen($arResult['ORDER_FIELDS']["EMAIL"]['VALUE']) > 0) {
            $event->Send("ORDER_UNREGISTERED", SITE_ID, $arFields, 'Y');
        }
    }

    public function addLead($arResult)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_URL, 'https://tavrost.bitrix24.ru/crm/configs/import/lead.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, true);

        $strOrderList = "";
        foreach ($arResult["BASKET"] as $arItem) {
            $strOrderList .= $arItem["NAME"];
            $strOrderList .= " <a href='http://www.arbist.ru/catalog/plitka_premium/8074/147586/'>";
            $strOrderList .= "������";
            $strOrderList .= "</a>";
            $strOrderList .= "<br>";
        }

        $commentText = '����� �'.$arResult["ORDER_ID"].'<br>';
        $commentText .= '<br>';
        $commentText .= '������:';
        $commentText .= '<br>';
        $commentText .= $strOrderList;

        $commentText = iconv('windows-1251', 'UTF-8', $commentText);

        $fio = explode(' ', iconv('windows-1251', 'UTF-8', $arResult['ORDER_FIELDS']['CONTACT_PERSON']['VALUE']), 2);

        $arData = array(
            'LOGIN' => 'robot@tavrost.ru',
            'PASSWORD' => '3Yy2x96',
            'TITLE' => iconv('windows-1251', 'UTF-8', '����� �'.$arResult["ORDER_ID"].' arbist.ru'),
            'OPPORTUNITY' => $arResult['BASKET_TOTAL'],
            'CURRENCY' => 'RUB',
            'SOURCE_ID' => 'Arbist',
            'PHONE_MOBILE' => iconv('windows-1251', 'UTF-8', $arResult['ORDER_FIELDS']['PHONE']['VALUE']),
            'STATUS_ID' => 'NEW',
            'EMAIL_HOME' => iconv('windows-1251', 'UTF-8', $arResult['ORDER_FIELDS']['EMAIL']['VALUE']),
            'NAME' => $fio[1],
            'LAST_NAME' => $fio[0],
            'COMMENTS' => $commentText,
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, $arData);
        curl_exec($ch);
    }

}