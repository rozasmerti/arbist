<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("NAME"),
    "DESCRIPTION" => GetMessage("DESCRIPTION"),
    "ICON" => "/images/news_detail.gif",
    "SORT" => 20,
//    "SCREENSHOT" => array(
//        "/images/post-77-1108567822.jpg",
//        "/images/post-1169930140.jpg",
//    ),
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "clothes",
        "CHILD" => array(
            "ID" => "ajax.auth",
            "NAME" => GetMessage("NAME"),
            "SORT" => 10,
        ),
    ),
);

?>