<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('catalog')) {
    die('NO CATALOG MODULE');
}

if (!CModule::IncludeModule('iblock')) {
    die('NO IBLOCK MODULE');
}

if (!CModule::IncludeModule('sale')) {
    die('NO SALE MODULE');
}

if (!CModule::IncludeModule('local.lib')) {
    die('NO lical.lib MODULE');
}

global $USER;

$arResult = array(
    'ERRORS' => array()
);

require_once __DIR__ . '/helper.php';

$isAjax = $_REQUEST["ajax"] == "y";

switch ($_REQUEST['action'])
{
    case 'delete':
        if (intval($_REQUEST["id"])>0)
        {
            $basket = new CSaleBasket();
            $basket->Delete($_REQUEST["id"]);
        }
        break;
    case "quantity":
        if (intval($_REQUEST["id"])>0)
        {
            $basket = new CSaleBasket();
            $basket->Update($_REQUEST["id"], array("QUANTITY"=>$_REQUEST["quantity"]));
        }
        break;
}

$helper = new CFastOrderHelper();

$data = $helper->getCurrentBasket();
$arResult['BASKET'] = $data['basket'];
$arResult['PRODUCTS'] = $helper->getProducts($data['product_ids']);
$arResult['BASKET_TOTAL'] = $data['total'];

$arResult['MIN_ORDER_PRICE'] = 2500;
$arResult['ALLOW_ORDER'] = true;

$checkMinPrice = false;

$arIBlockCheck = array(14, 22, 23, 24);

foreach ($arResult['PRODUCTS'] as $arItem)
{
	if (in_array($arItem['IBLOCK_ID'], $arIBlockCheck))
	{
		$checkMinPrice = true;
		break;
	}
}

if ($checkMinPrice && $arResult['BASKET_TOTAL']<2500)
	$arResult['ALLOW_ORDER'] = false;

unset($data);

$arResult['MENU'] = $helper->getMenu();

foreach ($arResult['BASKET'] as $key => $arItem) {
    if (isset($arResult['PRODUCTS'][$arItem['PRODUCT_ID']])) {
        $arResult['BASKET'][$key]['PRODUCT'] =& $arResult['PRODUCTS'][$arItem['PRODUCT_ID']];

        $iblock_id = $arResult['PRODUCTS'][$arItem['PRODUCT_ID']]['IBLOCK_ID'];
        foreach ($arResult['MENU'] as $sectionId => $arSection)
        {
            foreach ($arSection['ELEMENTS'] as $id => $arElement)
            {
                if($iblock_id == $arElement['PROPERTY_IBLOCK_ID_VALUE']){
                    $arResult['BASKET'][$key]['MENU'] = array();
                    $arResult['BASKET'][$key]['MENU']['SECTION'] =& $arResult['MENU'][$sectionId];
                    $arResult['BASKET'][$key]['MENU']['ELEMENT'] =& $arResult['MENU'][$sectionId]['ELEMENTS'][$id];
                }
            }
        }
    }
}

$authSent = false;
$orderSent = false;

$arResult['FORM_TYPE'] = 'order';
if ($_REQUEST['sent'] == 'Y') {
    if ($_REQUEST['action'] == 'auth') {
        $authSent = true;
        $arResult['FORM_TYPE'] = 'auth';
    } else {
        $orderSent = true;
    }
}

$arFields = array(
    'LOGIN' => array(
        'TYPE' => 'TEXT',
        'CODE' => 'LOGIN',
        'REQUIRED' => true,
        'NAME' => '�����'
    ),
    'PASSWORD' => array(
        'TYPE' => 'TEXT',
        'CODE' => 'PASSWORD',
        'REQUIRED' => true,
        'NAME' => '������'
    )
);
$authData = $helper->validateData($arFields, $authSent);
$arResult['AUTH_FIELDS'] = $authData['FIELDS'];

if ($authSent && empty($authData['ERRORS'])) {
    $result = $USER->Login($arResult['AUTH_FIELDS']['LOGIN']['VALUE'], $_REQUEST['password'], "Y", 'Y');
    if ($result == true) {
        $arResult['FORM_TYPE'] = 'order';
    }
}


$arUserResult = Array(
    "PERSON_TYPE_ID" => false,
    "PAY_SYSTEM_ID" => false,
    "DELIVERY_ID" => false,
    "ORDER_PROP" => false,
    "DELIVERY_LOCATION" => false,
    "TAX_LOCATION" => false,
    "PAYER_NAME" => false,
    "USER_EMAIL" => false,
    "PROFILE_NAME" => false,
    "PAY_CURRENT_ACCOUNT" => false,
    "CONFIRM_ORDER" => false,
    "FINAL_STEP" => false,
    "ORDER_DESCRIPTION" => false,
    "PROFILE_ID" => false,
    "PROFILE_CHANGE" => false,
    "DELIVERY_LOCATION_ZIP" => false,
);

$arFields = array(
    'CONTACT_PERSON' => array(
        'TYPE' => 'TEXT',
        'CODE' => 'CONTACT_PERSON',
        'REQUIRED' => true,
        'NAME' => '���� ���'
    ),
    'PHONE' => array(
        'TYPE' => 'PHONE',
        'CODE' => 'PHONE',
        'REQUIRED' => true,
        'NAME' => '�������'
    ),
    'EMAIL' => array(
        'TYPE' => 'EMAIL',
        'CODE' => 'EMAIL',
        'REQUIRED' => false,
        'NAME' => 'E-Mail'
    ),
);

if ($USER->IsAuthorized()) {
    $arResult['USER'] = $USER->GetByID($USER->GetID())
        ->Fetch();

    $arFields['CONTACT_PERSON']['DEFAULT_VALUE'] = $arResult['USER']['LAST_NAME'] . ' ' . $arResult['USER']['NAME'] . ' ' . $arResult['USER']['SECOND_NAME'];
    $arFields['CONTACT_PERSON']['DEFAULT_VALUE'] = trim($arFields['CONTACT_PERSON']['DEFAULT_VALUE']);
    $arFields['PHONE']['DEFAULT_VALUE'] = $arResult['USER']['PERSONAL_PHONE'];
    $arFields['EMAIL']['DEFAULT_VALUE'] = $arResult['USER']['EMAIL'];
}

$orderFieldsData = $helper->validateData($arFields, $orderSent);
$arResult['ORDER_FIELDS'] = $orderFieldsData['FIELDS'];

if ($orderSent && empty($orderFieldsData['ERRORS'])) {
    $result = $helper->addOrder($arResult['BASKET_TOTAL'], $arResult['ORDER_FIELDS']);

    if (!empty($result['ERRORS'])) {
        $arResult['ERRORS'] = array_merge($arResult['ERRORS'], $result['ERRORS']);
    } else {
        $arResult['ORDER_ID'] = $result['ORDER_ID'];
        $arResult['REDIRECT_URL'] = $APPLICATION->GetCurPageParam('ORDER_ID=' . $arResult['ORDER_ID']);

        if ($USER->IsAuthorized()) {
            $arUserFields = array(
                'PERSONAL_PHONE' => $arResult['ORDER_FIELDS']['PHONE']['VALUE'],
                'NAME' => $arResult['ORDER_FIELDS']['CONTACT_PERSON']['VALUE'],
                'LAST_NAME' => '',
                'SECOND_NAME' => '',
            );

            $USER->Update($USER->GetID(), $arUserFields);
        } else {

        }

        $_SESSION['ORDER_DATA'] = $arResult;

        $helper->sendEmail($arResult);
    }
} else {
    $arResult['ERRORS'] = array_merge($arResult['ERRORS'], $orderFieldsData['ERRORS']);
}

$arResult['MESSAGE'] = '';
if (!empty($arParams['WORK_TIME'])) {
    $weekDay = date('N');
    if (isset($arParams['WORK_TIME'][$weekDay])) {
        $hour = intval(date('H'));
        foreach ($arParams['WORK_TIME'][$weekDay] as $period) {
            if (isset($period['FROM']) && isset($period['TO']) && !empty($period['MESSAGE'])) {
                if ($hour > $period['FROM'] && $hour < $period['TO']) {
                    $arResult['MESSAGE'] = $period['MESSAGE'];
                    break;
                }
            }
        }

        if (empty($arResult['MESSAGE']) && !empty($arParams['WORK_TIME'][$weekDay]['DEFAULT_MESSAGE'])) {
            $arResult['MESSAGE'] = $arParams['WORK_TIME'][$weekDay]['DEFAULT_MESSAGE'];
        }
    }
}

if (empty($arResult['MESSAGE'])) {
    $arResult['MESSAGE'] = $arParams['DEFAULT_MESSAGE'];
}

if ($isAjax) {
    $this->IncludeComponentTemplate("ajax");
}else {
    $this->IncludeComponentTemplate();
}

if(isset($_GET['ORDER_ID']) && intval($_GET['ORDER_ID']) > 0 && !empty($_SESSION['ORDER_DATA'])){
    $helper->addLead($_SESSION['ORDER_DATA']);
    unset($_SESSION['ORDER_DATA']);
}

//echo '<pre>'; print_r($arResult); echo '</pre>';