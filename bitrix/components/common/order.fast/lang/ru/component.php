<?
$MESS ['ALL_CHARACTERISTICS'] = "Всего предложений";
$MESS ['ALL_CHARACTERISTICS_INACTIVE'] = "Неактивных предложений";
$MESS ['ALL_GOODS'] = "Всего товаров";
$MESS ['ALL_GOODS_INACTIVE'] = "Неактивных товаров";  
$MESS ['ALL_SECTIONS'] = "Всего разделов";
$MESS ['NO_BAR_CODE'] = "Без штрих кода";
$MESS ['PARENT'] = "Родительские";
$MESS ['NO_SMALL_IMAGE'] = "Без маленькой картинки";
$MESS ['NO_MEDIUM_IMAGE'] = "Без средней картинки";
$MESS ['NO_BIG_IMAGE'] = "Без большой картинки";
$MESS ['NO_DESCRIPTION'] = "Без описания";
$MESS ['NO_SYMBOL_CODE'] = "Без символьного кода";
$MESS ['WRONG_SYMBOL_CODE'] = "Ошибки в символьном коде";
$MESS ['NO_SECTION_SYMBOL_CODE'] = "Раздел без символьного кода";
$MESS ['NO_GROUP_PROPERTY'] = "Не установлена группа кресла";
$MESS ['NO_WEIGHT_PROPERTY'] = "Не задан вес ребенка";
$MESS ['NO_AGE_PROPERTY'] = "Не задан возраст ребенка";
?>