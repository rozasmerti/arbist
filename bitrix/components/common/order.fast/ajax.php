<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.
header('Content-Type: application/json');

$arAnswer = array();

if (isset($_REQUEST['id']) && intval($_REQUEST['id']) > 0) {
    if (CModule::IncludeModule('sale')) {
        $basket = new CSaleBasket();
        $arAnswer['deleted'] = $basket->Delete(intval($_REQUEST['id']));
    }
}

echo json_encode($arAnswer);

