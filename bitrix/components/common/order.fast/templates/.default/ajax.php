<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->RestartBuffer();
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.
header('Content-Type: application/json');

unset($arResult["AUTH_FIELDS"]);
unset($arResult["MENU"]);
unset($arResult["USER"]);

echo json_encode($arResult);
die();
?>