<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="thanks-block">
<div class="thankyou"><h3>�������!</h3></div>
    <div class="order-details"">��� ����� ������� ��������! <br/>����� ������ ������ <span class="order-id"><?=intval($_GET['ORDER_ID'])?></span>, � <span class="accent" id="promiseCall"><?=$arResult['MESSAGE']?></span> ��� �������� �������� � ���� �� ��������, ����� �������� ������ �� ���������� ������, �������� � ������ ������.<br> <div class="notice">P.S. ������ ������� ��� �� ���� ( ���� �� ��������� ������� ���� ����� ��������) - �� ����������� � ���� ��������.</div></div>
    <div class="order-message">
        ����� �� �� ��������,<br>
        ������, ����� ��� ������,<br>
        ��� �������� ������ ������ -<br>
        ����� �������� �����!!<br>
    </div>
</div>
<script>
    var sd = <?=date('N');?>;
    var sh = <?=intval(date('H'));?>;


<?
    if($_SESSION['ORDER_DATA']['ORDER_ID'] > 0)   {
        echo "var products = [\n";

    $cnt = 1;
    $orderID = $_SESSION['ORDER_DATA']['ORDER_ID'];
    $orderRevenue = round($_SESSION['ORDER_DATA']['BASKET_TOTAL']);
    $productsQtty = sizeof($_SESSION['ORDER_DATA']['BASKET']);
    foreach ($_SESSION['ORDER_DATA']['BASKET'] as $k=>$v) {

       echo "{\n";
        echo "\"id\":".$v['PRODUCT_ID'].",\n";
        echo "\"name\":\"".$v['NAME']."\",\n";
        echo "\"vendor\":\"".$v['PRODUCT']['PROPERTIES']['BRAND']['VALUE']."\",\n";
        echo "\"price\":".round($v['PRICE']).",\n";
        echo "\"quantity\":1,\n";
        echo "\"category\":\"".$v['MENU']['SECTION']['NAME']."/".$v['PRODUCT']['PROPERTIES']['DEPARTMENT']['VALUE']."\",\n";
        echo "\"pcat\":\"".$v['PRODUCT']['PROPERTIES']['DEPARTMENT']['VALUE']."\",\n";

      if($cnt < $productsQtty) {
          echo "},\n";
      } else {
          echo "}\n";
      }
        $cnt++;
    }
    echo "];\n";
?>
        var productsQtty = <?=$productsQtty ?>;
        var prods = [];
        var pnames  = [];
        var prices  = [];
        var cats  = [];
        var total =0;

        for(var i=0;i<productsQtty;i++) {
            var product = products[i];
            prods.push(product.id);
            pnames.push(product.name);
            prices.push(''+product.price+'');
            cats.push(product.pcat);
            total += Number(product.price);
            delete products[i].pcat;
        }

        var google_tag_params = {
            'ecomm_pagetype': 'purchase',
            'ecomm_prodid': prods,
            'ecomm_pname': pnames,
            'ecomm_pcat': cats,
            'ecomm_pvalue': prices,
            'ecomm_totalvalue':''+total+''
        };

        window.dataLayer = window.dataLayer || [];

        window.dataLayer.push ({
            'event':'remarketingTriggered',
            'google_tag_params': google_tag_params,
            "ecommerce": {
                "purchase": {
                    "actionField": {
                        "id": "<?=$orderID?>",
                        "revenue": "<?=$orderRevenue?>",
                        "list": "purchase",
                        "goal_id" : "24112832"
                    },
                    "products": products
                }
            }
        });

<?
    }
?>
    </script>

<?
/*
global $USER;
if($USER->IsAdmin()){

    //print_r($_SESSION['ORDER_DATA']);
    foreach ($_SESSION['ORDER_DATA']['BASKET'] as $k=>$v) {
      //  echo $v['PRODUCT_ID']." - ".$v['NAME']." - ".round($v['PRICE'])." - ".$v['PRODUCT']['PROPERTIES']['BRAND']['VALUE']." - ".$v['MENU']['SECTION']['NAME']." - ".$v['PRODUCT']['PROPERTIES']['DEPARTMENT']['VALUE']." <br>";
        //echo "<pre> Product Menu ".print_r($v['PRODUCT']['MENU'])."</pre>";
    }

}
*/
?>