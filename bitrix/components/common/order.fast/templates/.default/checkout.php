<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (empty($arResult['BASKET'])): ?>

    <div class="cart-empty">
        <h3>���� ������� �����</h3>
        <div class="cart-sorry">
            ��� �����, ����� ����.....
        </div>
    </div>

<? else: ?>
    <? if($USER->IsAdmin()) {
        echo "\n<!--\n";
        print_r($arResult);
        echo "\n-->\n";
    }
    ?>

    <div class="basket_block">
        <div class="message-hours" style="display:none;">
            <div class="pull-right">
                <button class="close">x</button>
            </div>
            <div class="announce-order">���������� ������ ������ ����� ���������!</div>
            ������� ���� ��� � ���������� �������, � ������� ������ "��������"!
            <br>���� ������ ����� ����������������, � <span class="accent" id="promiseCall">�����</span> ��� �������� �������� Arbist.RU, ����� ����������� ������ �� ���������� � ��������
            <div class="clear"></div>
        </div>
        <div class="left_column">
            <ul class="tabs_basket">

                <?if(!$USER->IsAuthorized()):?>
                    <li data-area="order" class="active">��� �����������</li>
                <?endif;?>

                <li data-area="auth" class="<?=$USER->IsAuthorized() ? 'active' : ''?>">��� ��������</li>
            </ul>
            <div class="tabs_basket_content">

                <? if ($arResult["ERRORS"]): ?>
                    <div class="messages">
                        <? foreach ($arResult["ERRORS"] as $error): ?>
                            <div><? print $error; ?></div>
                        <? endforeach; ?>
                        <div class="clear"></div>
                    </div>
                <? endif; ?>



                <div id="order" class="tabs_item <?= $arResult['FORM_TYPE'] == 'order' ? 'active' : '' ?>">
                    <form action="<?= POST_FORM_ACTION_URI ?>" method="post" name="ORDERFORM" class="buy_for_click">
                        <input type="hidden" name="PERSON_TYPE" value="1">
                        <input type="hidden" name="PAY_SYSTEM_ID" value="7">
                        <input type="hidden" name="DELIVERY_ID" value="1">
                        <input type="hidden" name="action" value="order">
                        <input type="hidden" name="sent" value="Y">

                        <label for="name">��� � ��� ���������� ?</label>
                        <input type="text" id="name" name="contact_person" placeholder="���� ���"
                               value="<?= $arResult['ORDER_FIELDS']['CONTACT_PERSON']['VALUE'] ?>" required class="required"/>
                        <label for="phone">���������� �������</label>
                        <input type="text" id="phone" name="phone" placeholder="�������:  XXX XXXXX"
                               value="<?= $arResult['ORDER_FIELDS']['PHONE']['VALUE'] ?>" required class="required"/>
                        <label for="email">���� ����� <span>(�� �����������)</span></label>
                        <input type="text" id="email" name="email"
                               value="<?= $arResult['ORDER_FIELDS']['EMAIL']['VALUE'] ?>" placeholder="E-mail"/>

			<div class="submit_order_block"<?if (!$arResult["ALLOW_ORDER"]):?> style="display:none;"<?endif?>>
	                        <input type="submit" value="��������">
        	                <div class="iagree">������� ������ "��������", � ��� �������� �� <a href="javascript:void(0);">������������� �����</a> �� ��������� ������������ ������.</div>
			</div>
			<div class="minprice_order_block"<?if ($arResult["ALLOW_ORDER"]):?> style="display:none;"<?endif?>>
				��������� ����������! <br>����������� ����� ��� ������ ������ � ����� �������� - <?= $arResult['MIN_ORDER_PRICE'] ?> ������. ������ � ��� �� ������� <span class="order_need_amount"><?=$arResult['MIN_ORDER_PRICE'] - $arResult['BASKET_TOTAL']?> �.</span> �� �������������� ������.
			</div>
                    </form>
                </div>
                <div id="auth" class="tabs_item <?= $arResult['FORM_TYPE'] == 'auth' ? 'active' : '' ?>">
                    <form action="" method="post" class="buy_for_click">
                        <input type="hidden" name="action" value="auth">
                        <input type="hidden" name="sent" value="Y">

                        <label for="login">�����</label>
                        <input type="text" id="login" name="login" placeholder="�����"
                               value="<?= $arResult['AUTH_FIELDS']['LOGIN']['VALUE'] ?>" required/>
                        <label for="phone">������</label>
                        <input type="password" id="password" name="password" placeholder="������" value="" required/>

                        <input type="submit" value="�����">
                        <div class="forgotten">���� �� ������ ���� ������, ��������� �� <a href="http://www.arbist.ru/auth/">��� ��������</a>, ����� ������������ ���</div>
                        <? if(!$USER->IsAuthorized()) { ?>
                        <div class="invite"><a href="javascript:void(0);">����� ����� �����������?</a></div>
                        <? } ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="right_column">
            <form action="/" class="order_products_form">
                <table class="order_tabel">
                    <thead>
                    <tr>
                        <td>����</td>
                        <td>��������</td>
                        <td>����</td>
			<td>����</td>
                        <td>�������</td>
                    </tr>
                    </thead>
                    <tbody>
                    <? $cartTotal = 0; ?>
                    <? foreach ($arResult['BASKET'] as $arItem): ?>


                        <? if($USER->IsAdmin()) {
                            echo "\n<!--\n";
                            print_r($arItem);
                            echo "\n-->\n";
                        }
                        ?>

                    <?    $cartTotal = $cartTotal + round($arItem['PRICE'] * $arItem["QUANTITY"]); ?>
                        <tr data-basket-id="<?=$arItem["ID"]?>" data-basket-name="<?=$arItem["NAME"]?>">
                            <td class="photo_product">
                                <? if (!empty($arItem['PRODUCT']['IMAGE']['SRC'])): ?>
                                    <img src="<?= $arItem['PRODUCT']['IMAGE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                                <? endif; ?>
                            </td>
                            <td class="name_product">
                                <?= $arItem['NAME'] ?>
                                <?if (doubleval($arItem["PRODUCT"]["PROPERTIES"]["CML2_KRAT"]["VALUE"]) != 1):?>
                                <span class="product_hint">����������� ���������� :<?=$arItem["PRODUCT"]["PROPERTIES"]["CML2_KRAT"]["VALUE"]?> <?=$arItem["PRODUCT"]["PROPERTIES"]["CML2_BASE_UNIT"]["VALUE"]?></span>
                                <?endif?>
                            </td>
                            <td class="price_product"><?= $arItem['PRICE_FORMATED'] ?></td>
			    <td class="quantity_product">
				<input type='button' value='-' class='qtyminus' field='quantity' data-field='quantity_<?=$arItem["ID"]?>'/>
 				<input type='text' name='quantity_<?=$arItem["ID"]?>' value='<?=ceil($arItem["QUANTITY"])?>' class='qty' data-basket-id='<?=$arItem["ID"]?>' />
    				<input type='button' value='+' class='qtyplus' field='quantity' data-field='quantity_<?=$arItem["ID"]?>'/>
			    </td>
                            <td class="del_product">
                                <button data-id="<?=$arItem['ID']?>"  data-name="<?= $arItem['NAME'] ?>" data-category="<?= $arItem['NAME'] ?>" data-price="<?= round($arItem['PRICE']) ?>" class="delpos">x</button>
                            </td>
                        </tr>
                    <? endforeach; ?>
                        <tr>
                            <td colspan="2" class="cart-total-text">�����:</td>
                            <td colspan="2" class="cart-total-sum"><span id="total-price"><?=$cartTotal ?></span> <span class="b-rub">�</span></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="cart-notice">* ���������� ��� ������ ������������ ������� �� ������� �������� � ���������� ��� �������� ������</td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <div class="banner_phone">
                <div class="t1">��� �������</div>
                <div class="t2"><span>(495) </span>139-60-90</div>
            </div>
        </div>
    </div>

    <div id="dialog-confirm" title="������������� ��������">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>������� ��� ������� �� ������� ?</p>
    </div>

    <script type="text/javascript">
var products = [
            <?
            $cnt = 0;
            $productsQtty = sizeof($arResult['BASKET']);
            foreach ($arResult['BASKET'] as $arItem) {
                $cnt++;
            ?>
{"id":"<?=$arItem['ID']?>","name":"<?= $arItem['NAME'] ?>","price":"<?= round($arItem['PRICE']) ?>","vendor":"<?=$arItem['PRODUCT']['PROPERTIES']['BRAND']['VALUE']?>","quantity":1,"category":"<?= $arItem['MENU']['SECTION']['NAME'] ?>/<?=$arItem['PRODUCT']['PROPERTIES']['DEPARTMENT']['VALUE']?>","pcat":"<?=$arItem['PRODUCT']['PROPERTIES']['DEPARTMENT']['VALUE']?>"}<? if($cnt < $productsQtty) echo "," ?>
 <? } ?>
];
<? if($productsQtty > 0) {
echo  "var productsQtty = ".$productsQtty.";\n";
?>
    var prods = [];
    var pnames  = [];
    var  prices  = [];
    var cats  = [];
    var total =0;

    for(var i=0;i<productsQtty;i++) {
        var product = products[i];
        prods.push(product.id);
        pnames.push(product.name);
        prices.push(''+product.price+'');
        cats.push(product.pcat);
        total += Number(product.price);
        delete products[i].pcat;
}

var google_tag_params = {
    'ecomm_pagetype': 'cart',
    'ecomm_prodid': prods,
    'ecomm_pname': pnames,
    'ecomm_pcat': cats,
    'ecomm_pvalue': prices,
    'ecomm_totalvalue':''+total+''
};

window.dataLayer = window.dataLayer || [];
window.dataLayer.push ({
    'event':'remarketingTriggered',
    'google_tag_params': window.google_tag_params
});
window.dataLayer.push([{
    "ecommerce": {
        "cart": {
            "actionField": {"list": "Cart"},
            "products" : products
        }
    }
}]);
<?
    }
?>
    </script>
    <div id="agreementForm" style="display: none;">

        ���������, �, (����� � ����), ��� ���� �������� <div class="companyProfile"><span class="companyName">�� "��������� ������� ����������"</span>, <span class="addressLabel">����������� �����:</span> <span class="address">127411 �. ������, ����������� �., �.157, ���.12/2, ��.122-208,</span> <span class="OgrnLabel">����</span> <span class="OGRN">311774617100733</span></div> (����� � ��������) �� ��������� ����� ������������ ������, ��������� ��� ����������� �� ����� ��������.<br>
        <p class="agreeText">��������� (����, ��������������, ����������, ��������, ���������, �������������, �������������, ��������, ������������, �����������) ������������ ������ ���� �������������� ��� ����� ����������� ���� �� ����� �������� � ����� ������� ���� �� ����, ��� ��������� ������������ � ���� ����������, ������� ���� ����� ����������� � ������ ������������ ���������������� �� � ����������� ������������ ��������.</p>

        <p class="agreeText">��������� ������������ ������ ���� ����� �������������� � ������� ������� ������������� �/��� ��� ������������� ������� ������������� � ������������ � ����������� ����������������� �� � ����������� ��������. ��������� ���� ����������� �� �������� ����� ������������ ������ ������� ����� ��� �� ��������� � ������������ � ������, ���������������� ��������� ���������, �� ��������� ���������, ����������� ��������� � ����� ������. ��������� �������� ���� �� ��������� ���/�� ������������ ������, ��������� ��� ����������� ���� �� ����� ��������, ������������ (�����������) � �������������� ���������� �����, ��������� � ������� ����������� ���� �� ����� �������� �� ������� ��� ������. �������� �� ��������� ������������ ������, ��������� ��� ����������� ���� �� ����� ��������, ������������ (�����������) � �������������� ���������� �����, ����� ���� �������� ����� ��� ������ ����������� ��������� (������) � ��������. ��������� ������������ ������ ���� ������������ ���������� � ������� ��������� ��������� ����������� ��������� (������) ���� �/��� � ������ ���������� ���� ��������� � ������������ � ���� � �� ��������, ������������� �������, ���� �� ������������� ����.</p>
        <span class="agree">��������� ���� ��� ����������� �� ����� �������� ������������ ������������� ��������� ����������.</span>

    </div>
    <div id="regReasons" style="display: none;">
        <div class="regTitle">5 ������ ��� ����������� �� Arbist.RU</div>
        <div class="regText">
            <ul class="regList">
                <li><span class="regReasonBullet">�������� ��������� ������</span><br>����� ������ ������� � ��� �� ����� �� ������ ����������� ��������� ������ ������. ����� �� ������ ����������� ��� ��� ���������� ������� ��� ������ �������� �����. </li>
                <li><span class="regReasonBullet">����������� ����������� ������ �������</span> <br>���� � �������� ���������� ����� �������� ������ � ������� ���������� ����������� ������, � ����� �������� ����������� ������ ������������� ���������, �� ������� ��������������� ������������ �������� �����. ������������������ ������������ ����� �������� ����� � ������� � �����������, � ��������� ������ ������ - ����� ��� ��� ����� ����� ��� � �������!</li>
                <li><span class="regReasonBullet">������ � ������ � ����������� �����</span><br>����� �� ������� � ��� ������ ���������� �����, � ����� �������� ����������. ������ � ����������� ����� �������� ������ �������������� ������������!!! </li>
                <li><span class="regReasonBullet">����������� ��������� ������������ ������</span><br>�� ���� ���������� ������� �������, �� ������ ����� �������� ������������ ������ �� ���������� ������.</li>
                <li><span class="regReasonBullet">������ � ������ � ������ ���������</span><br>���� �� �������������� �� ����� � ��������� ����������, ��������, � �������� ����� � �������, �� ������ ����� �������� ������ � ������� �� ������ ���������, ���������� ����������, �������� ��� ��������. </li>
            </ul></div>
        <div class="regFooter">��� ��� �������, �����-�� � <a href="http://www.arbist.ru/auth/" target="_blank">���������������</a></div>
    </div>

    <script>
        var sd = <?=date('N');?>;
        var sh = <?=intval(date('H'));?>;
    </script>
    <?php
    //print_r($arResult['BASKET']);
    ?>
<? endif; ?>