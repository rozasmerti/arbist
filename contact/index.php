<?
if (!defined("B_PROLOG_INCLUDED")) require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
?>
<style type="text/css">
    .rightCol .contacts {color:black; font-size:medium; text-align: left;}
    .rightCol .contacts .title {font-size: x-large; font-weight: bold;}
    .rightCol .contacts .subtitle {font-size: medium; font-weight: bold; font-family: Arial,serif;
        padding:6px 0px;}
    .rightCol .contacts .text { margin-top: 16px; font-size: 14px; }
    .rightCol .contacts .text-red { margin-top: 16px; font-size: 14px; color:#ee1d24;}
    .rightCol .contacts .text span.phone, span.email {
        margin-top: 16px;
        margin-right:55px;
        font-size: 16px;
        font-weight: bold;
        padding: 4px 16px;
        border-radius: 3px;
        background-color: coral;
    }

    .rightCol .contacts .text a.pass-request { color: #2f3192;
        font-weight:bold; }
    .rightCol .contacts table {
        font-size: 14px;
    }
    .rightCol .contacts table .subtitle {
        font-size: 14px;
    }
    .rightCol .contacts .text ul {
        list-style-type: none;
        padding: 0px;
    }
    .rightCol .contacts .text ul span { font-weight: bold; padding-left: 5px;}

</style>
    <div itemscope itemtype="http://schema.org/LocalBusiness" style="display:none;">
        <span itemprop="name">Arbist.ru</span>
        <a href="https://www.arbist.ru" itemprop="url">Arbist.ru</a>
        <span itemprop="image">https://www.arbist.ru/img/logo.png</span>
        <span itemprop="description">�������� ������� ���������� ���������� Arbist.ru, ������� ����� ����������� ������, �������������, �������, �����, ������� �����, ������, � ����� ������ ������� ���������</span>
        <span itemprop="openingHours" datetime="Mo,Tu,We,Th,Fr 09:00-20:00">��.- ��. � 9.00 �� 20.00</span>
        <span itemprop="openingHours" datetime="Sa 09:00-18:00">�� � 09:00 �� 18:00</span>
        <span itemprop="openingHours" datetime="Su 10:00-18:00">���. � 10:00 �� 18:00</span>
        <span itemprop="address">127411 �. ������,  ����������� ����� �. 157, ���.12/2, ���� 2, ���� 122-208, ������-����� "������"</span>
        <span itemprop="telephone">+74951396090</span>
    </div>
<div class="contacts">
    <p class="title"><strong>������</strong></p>
    <p class="subtitle">����� ����� ��������:</p>
    <p class="text">127411 �. ������,������������ ����� �. 157, ���.12/2, ���� 2, ���� 122-208. ������ ����� ������.</p>
    <p class="text">����� �������� ���� ��������, ���������� ��������������
        <noindex>
            <a href="https://www.arbist.ru/pass_request/" target="_parent" rel="nofollow" class="pass-request">�������� �������</a>
        </noindex>���� ������� �� ���������� ������ ������.
        ������� �� ���������� ������, ��� ����� � ���� ��������� �������� ���������� ������� (����������� ������� ��� ������������ �������������).
        � �������� ����� ��������� ����������, ��� ������ � ��� � ����.
        (����� � ���������, ����� ����� � ������� �6 ������������� �������� ���������, ��������� �� ���������� � ������ �� �������� 5 ������ �� �������� ������������� �����, ��������� �� ��� �� 2 ����.
        �� 2 ����� ��������� ������ � ������ �� ����� ��������, ����� ��������� ������� - ������ ����� ������ ����� ��� ���� 122-208.)
    </p>
    <p class="subtitle"><a name="pickup"></a>����� ������ ����������:</p>
    <p class="text">127591 �. ������, ��.�����������, �. 83� ���.19</p>
    <p class="text">
        ��� ������� �� ����� ����������, ��������� ������� �� ���������.
        ������� �� ���������� ������, �� ������ ���������� ������� �������� ����� ��������.
        ����� �������� �� ������� ����������� �������� ��������� ������ � ����������� ������ �� ����� ����������.
        �������� ����� ����� ��������������� �� ����� ������ ����������.
    </p>
    <p class="subtitle">���� ������:</p>
    <table border="0" cellpadding="1" cellspacing="1" width="98%" style="border-collapse: collapse;">
        <tbody>
        <tr>
            <td class="subtitle" align="left" valign="middle">����:</td>
            <td class="text" align="right" valign="middle">
                <ul>
                    <li>��.- ��.<span> � 9.00 �� 20.00</span></li>
                    <li>��.<span> � 09.00 �� 18.00</span></li>
                    <li>��.<span> � 10.00 �� 18.00</span></li>
                </ul>
            </td>
            <td width="20%">&nbsp;</td>
            <td class="subtitle" align="left" valign="middle">����� ����������:</td>
            <td class="text" align="right" valign="middle">
                <ul>
                    <li>��.- ��.<span>� 9.00 �� 19.00</span></li>
                    <li>��.<span>�� 9.00 �� 18.00</span></li>
                </ul>
            </td>
        </tr>
        </tbody>
    </table>
    <p class="text-red">
    <b>*</b> ����� ������ � ��������� ��������:���.- ��.: � 9.00 �� 18.00<br />
        <span class="small"><b>**</b> ����� � �������� ������ � ����������� �������� ������ �� ���������������� ������������</span>
    </p>
    <p class="text"><span class="phone">���.:�(495) 139-60-90</span> <span class="email">E-mail:<a href="mailto:info@arbist.ru" >info@arbist.ru</a></span></p>
<hr width="100%"/>
    <div style="margin-bottom: 16px;margin-top:10px; width: 672px; height: 240px; min-width: 672px; min-height: 240px;" id="ymap">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7af6f9ce370dc79a58ddb0c7e3ea14a0283cb486236d94773b8761d57eada1da&amp;width=670&amp;height=240&amp;lang=ru_RU&amp;scroll=true"></script>
    </div>
<p><b>��� ������ � ���� ��������:</b></p>
 
<p><b>�� ����� &quot;����������&quot;</b>, ������� �774 � 
  <br />
 <b>�� ����� &quot;���������&quot;</b>, ��������: �284; 774; 92; 428; 352; 763; 763� � 
  <br />
 <i>�� ��������� &quot;����������� �����, 155&quot;. ������� �� ���������, ����� 150 ������ ����� ��������������� ����� �� ��������� � ������ ����� ������. </i> </p>
 
<p><b>�� ����� &quot;���������-�����������&quot;</b>, �������� �763�; 63; 994; 149 (�� ������������� �������), ���������� �78 (�� ������������ �����)� 
  <br />
 <b>�� ����� &quot;������ ������&quot;</b>, ������� �284 � 
  <br />
 <i>�� ��������� &quot;����������� �����, 155&quot;. ������� �� ���������, ��������� �� ���������� �������� �� ������ ������� �����, ����� 150 ������ ����� ��������������� ����� �� ��������� � ������ ����� ������. </i> </p>
 
<p>�� ��������� � ���� ���������, ��������� �������������� ���������� �������. (����������� ������� ��� ������������ �������������).</p>
 
<p><b>��� ������ �� ������ ����������:</b></p>
 
<p><b>�� ����� &quot;����������&quot;</b>, ������� �352; 774� 
  <br />
 <b>�� ����� &quot;���������&quot;</b>, ��������: �273; 284; 302; 352; 456; 459; 519; 559; 571; 592; 644; 685; 774; 836; 867; 92; 928; 98; 994</p>
 
<p><i>�� ��������� &quot;��������� ���������&quot;. ��������� �� ������ ������� ������ � ���� ����� �������� ������� � �������� ������� �� �������� ����� ��������������� ����. ��������� ����� ���� � ������� �� ������. ������������ ������� � ��������� ����� ������ �� �������� 200 ������ �� ����������� �������� �� ����������. ��������� �� ������ ������� � ���� ������ �� �������� ����� ��������, ����� 400 ������ ������� �� �������� �����, ��� � ����� ������� ����� ����� �������� �����. ������� �� ����� ������ � ������������ ������ � �������. ����� 100 ������ ����� ����� � ������ � ������ �������. ����� � ������ - ��� ����� ����� �� ����� ������.</i></p>
 
<p><b>�� ����� &quot;���������-�����������&quot;</b>, ������� �677 (�� ������������� �������)�</p>
 
<p><i>�� ��������� &quot;�������� �����&quot;. ��������� �� ������ ������� ������ � ���� �� &quot;�������������� ������� �5265&quot;. ����� 500 ������ ����� ������� ������� ������������ ������� � �������. ����� 100 ������ ����� ����� � ������ � ������ �������. ����� � ������ - ��� ����� ����� �� ����� ������.</i></p>
 
<p><b>�� ����� &quot;������ ������&quot;</b>, ������� �284�</p>
 
<p><i>�� ��������� &quot;��������� ���������&quot;. ���� ����� �������� ������� � �������� ������� �� �������� ����� ��������������� ����. ��������� ����� ���� � ������� �� ������. ������������ ������� � ��������� ����� ������ �� �������� 200 ������ �� ����������� �������� �� ����������. ��������� �� ������ ������� � ���� ������ �� �������� ����� ��������, ����� 400 ������ ������� �� �������� �����, ��� � ����� ������� ����� ����� �������� �����. ������� �� ����� ������ � ������������ ������ � �������. ����� 100 ������ ����� ����� � ������ � ������ �������. ����� � ������ - ��� ����� ����� �� ����� ������.</i></p>
 
<p><hr/></p>
 
<p class="subtitle">����� ��� ������ (��������) ������:</p>
 
<p style="font-family: 'Times New Roman';">127411 �. ������,<span class="Apple-converted-space">�</span>��.�����������, �. 83� ���.19. ����� ������ ��� ������ (��������): ��. - ��. � 10*00 �� 18*00. ��������� &quot;������� �������&quot; �����<span class="Apple-converted-space">�</span><a id="bxid_468600" title="&quot;������� �������&quot; - ��������� � ����� ����." target="_blank" href="http://www.arbist.ru/vacancy/" >�����</a><span class="Apple-converted-space">�</span>.</p>
 
<p> <b><font color="#ff0000" size="4">��������!</font></b><span class="Apple-converted-space">�</span> 
  <br />
 ��������� ����������, ������������� �������� �������� �������������, ����� ������� �� ����� �������� ����� ���������� �� ��������� �����. � ������ ���� ��� ���������� ����� ��������� �����-���� ����,<span class="Apple-converted-space">�</span><b>����������� �������� � ��� ������� � �������� ���������������� ���,</b><span class="Apple-converted-space">�</span>���������� ���� ����� � ���������� ��������. �����������<b> ��������� � ��������� ������� ���������� ������ � ���������������� ����</b>, ��� ��� ���������� ���� ����������� ������ � ����, �� ��������������� ���������.</p>

    </div>
 <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>