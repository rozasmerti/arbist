<?
    // ������ ��� �������� ������������ ��������
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
    
    // �������� ������������ ����� �� ������ �������
    if(CModule::IncludeModule('iblock'))
    {
        $res = CIBlockProperty::GetByID(584, false, 'plitka_premium');
        if ($ar_res = $res->GetNext())
        {
            $countries = array();
            
            // �������� ������������
            $enum = CIBlockPropertyEnum::GetList(array('SORT' => 'ASC', 'VALUE' => 'ASC'), array('PROPERTY_ID' => $ar_res['ID']));
            while ($propVariant = $enum->fetch())
                $countries[] = $propVariant['VALUE'];
            
            // ������� �� ���-������� ��������������
            global $DB;
            $query = '
            DELETE
                s.*
            FROM
                seo_association as s
            INNER JOIN 
                b_iblock_element as b
            ON 
                s.element_id = b.ID 
            WHERE
                s.type = "plitka"
                AND b.IBLOCK_ID = 22
                AND s.country_name not in ("'.implode('","', $countries).'")
                AND b.ACTIVE = "Y"
            ';
            $res = $DB->Query($query);
            
            echo 'ok';
        }
    }
?>