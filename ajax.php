<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

class CYandexYmlUtils {

    var $yandexFile;
    var $yandexTmpFile;
    var $yandexDelIblockId;

    function __construct()
    {
        $this->yandexFile = $_SERVER["DOCUMENT_ROOT"] . '/yml/yandex.php';
        $this->yandexTmpFile = $_SERVER["DOCUMENT_ROOT"] . '/yml/yandex_tmp.php';
        $this->yandexDelIblockId = 94;
    }

    function  deleteFromYandexYml($arOfferId)
    {
        if (!empty($arOfferId))
        {
            $el = new CIBlockElement();

            if (!file_exists($this->yandexTmpFile)) {
                $yandexData = @file_get_contents($this->yandexFile);
                while (!empty($arOfferId))
                {
                    $arOfferDel = array_splice($arOfferId, 0, 10); // ������� �� 10 ����
                    $yandexData = preg_replace('/(<offer id=\"(' . implode('|',$arOfferDel) . ')\".+<\/offer>)\s+/sU', '', $yandexData);
                }
                @file_put_contents($this->yandexFile, $yandexData);
            } else {

                foreach ($arOfferId as $offerId)
                {
                    $el->Add(array("IBLOCK_ID" => $this->yandexDelIblockId, "NAME" => $offerId));
                }
            }
        }
    }

    function updateItemsByNameAndHash($arName, $arHash, $availDate, $availNote, $availWho)
    {
        if (!is_array($arName))
            $arName = array($arName);

        if (!is_array($arHash))
            $arHash = array($arHash);

        $arName = array_unique($arName);
        $arHash = array_unique($arHash);

        $arName = array_filter($arName, function($value) {return strlen(trim($value))>0; });
        $arHash = array_filter($arHash, function($value) {return strlen(trim($value))>0; });

        if (empty($arName) && empty($arHash))
            return;

        $arFilter = array(
            "=IBLOCK_TYPE" => "catalog",
        );

        $arSubFilter = array(
            "LOGIC" => "OR",
        );

        if (!empty($arName))
            $arSubFilter[] = array("=NAME"=>$arName);

        if (!empty($arHash))
            $arSubFilter[] = array("=PROPERTY_PICTURE_HASH"=>$arHash);

        $arFilter[] = $arSubFilter;

        $arOfferId = array();

        $rsElement = CIBlockElement::GetList(array(),$arFilter,false,false,array("IBLOCK_ID","ID"));
        while ($arElement = $rsElement->Fetch())
        {
            $arOfferId[] = $arElement["ID"];
            CIBlockElement::SetPropertyValuesEx(
                $arElement["ID"],
                $arElement["IBLOCK_ID"],
                array(
                    'AVAIL_DATE' => $availDate,
                    'AVAIL_NOTE' => $availNote,
                    'AVAIL_WHO' => $availWho,
                )
            );
        }

        if (!empty($availDate) && !empty($arOfferId)) {
            $this->deleteFromYandexYml($arOfferId);
        }
    }
}

function getNotes() {
    $note  = empty($_POST['wrong_price']) ? "" : $_POST['wrong_price'] . ",";
    $note .= empty($_POST['supply_awaiting']) ? "" : $_POST['supply_awaiting'] . ",";
    $note .= empty($_POST['obsolete']) ? "" : $_POST['obsolete'];
    rtrim($note,',');
    return $note;
}

if (in_array('5', CUser::GetUserGroup($USER->GetID())) || in_array('1', CUser::GetUserGroup($USER->GetID()))) {

    define("PRICE_TYPE_ID", 10);

    $note = getNotes();
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/ajax_log.txt',$_POST);
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/ajax_log_note.txt',$note);

    switch ($_POST['type']) {
        case 'avail_element':
            if (CModule::IncludeModule("iblock")) {
                if ($arElement = CIBlockElement::GetList(array(),array("ID"=>$_POST['id']),false,false,array("IBLOCK_ID","ID","NAME","PROPERTY_PICTURE_HASH"))->Fetch())
                {
                    $utils = new CYandexYmlUtils();
                    $utils->updateItemsByNameAndHash(array($arElement["NAME"]),array($arElement["PROPERTY_PICTURE_HASH_VALUE"]), $_POST['avail_date'], iconv('utf8', 'cp1251', $note),$USER->GetID());
                }

                echo BXClearCache(true, ''); // /s2/itc/catalog.element/
            }
            //echo BXClearCache(true, ''); // /s2/itc/catalog.section/

            /*
            $arElement = CIBlockElement::GetByID($_POST['id'])
                ->Fetch();

            if($arElement){
                global $CACHE_MANAGER;
                $CACHE_MANAGER->ClearByTag("iblock_id_".$arElement['IBLOCK_ID']);
            }
            */
            //ob_start();
            //include($_SERVER["DOCUMENT_ROOT"].'/yandex_custom_run.php');
            //ob_end_clean();

            echo true;
            break;
        case 'avail_section':
            if (CModule::IncludeModule("iblock")) {
                $arOfferId = array();

                $arName = array();
                $arHash = array();

                $res = CIBlockElement::GetList(array(), array('SECTION_ID' => $_POST['id']), false, false, array("IBLOCK_ID","ID","NAME","PROPERTY_PICTURE_HASH"));
                while ($arElement = $res->Fetch())
                {
                    $arName[] = $arElement["NAME"];
                    $arHash[] = $arElement["PROPERTY_PICTURE_HASH_VALUE"];
                }

                $utils = new CYandexYmlUtils();
                $utils->updateItemsByNameAndHash($arName,$arHash, $_POST['avail_date'], iconv('utf8', 'cp1251', $note),$USER->GetID());
            }
            echo BXClearCache(true, ''); // /s2/itc/catalog.element/
            //echo BXClearCache(true, ''); // /s2/itc/catalog.section/

            //ob_start();
            //include($_SERVER["DOCUMENT_ROOT"].'/yandex_custom_run.php');
            //ob_end_clean();

            echo true;
            break;

        case 'price_element':
            if (CModule::IncludeModule("iblock"))
                CIBlockElement::SetPropertyValuesEx(
                    $_POST['id'],
                    false,
                    array(
                        'PRICE_DATE' => $_POST['price_date'],
                        'AVAIL_WHO' => $USER->GetID(),
                    )
                );
            if (CModule::IncludeModule("catalog")) {
                $arFields = Array(
                    "PRODUCT_ID" => $_POST['id'],
                    "CATALOG_GROUP_ID" => PRICE_TYPE_ID,
                    "PRICE" => floatval($_POST['price']),
                    "CURRENCY" => "RUB"
                );

                $resp = CPrice::GetList(
                    array(),
                    array(
                        "PRODUCT_ID" => $_POST['id'],
                        "CATALOG_GROUP_ID" => PRICE_TYPE_ID
                    )
                );
                if ($ar_fieldsp = $resp->GetNext())
                    CPrice::Update($ar_fieldsp["ID"], $arFields);
                else
                    CPrice::Add($arFields);
            }
            echo BXClearCache(true, ''); // /s2/itc/catalog.element/
            //echo BXClearCache(true, ''); // /s2/itc/catalog.section/

            //ob_start();
            //include($_SERVER["DOCUMENT_ROOT"].'/yandex_custom_run.php');
            //ob_end_clean();

            echo true;
            break;
        case 'price_section':
            if (CModule::IncludeModule("iblock")) {
                $arFields = Array(
                    "CATALOG_GROUP_ID" => PRICE_TYPE_ID,
                    "PRICE" => floatval($_POST['price']),
                    "CURRENCY" => "RUB"
                );
                $res = CIBlockElement::GetList(array(), array('SECTION_ID' => $_POST['id']), false, false, array('ID'));
                while ($ar_fields = $res->GetNext()) {
                    CIBlockElement::SetPropertyValuesEx(
                        $ar_fields['ID'],
                        false,
                        array(
                            'PRICE_DATE' => $_POST['price_date'],
                            'AVAIL_WHO' => $USER->GetID(),
                        )
                    );

                    if (CModule::IncludeModule("catalog")) {
                        $arFields['PRODUCT_ID'] = $ar_fields['ID'];
                        $resp = CPrice::GetList(
                            array(),
                            array(
                                "PRODUCT_ID" => $ar_fields['ID'],
                                "CATALOG_GROUP_ID" => PRICE_TYPE_ID
                            )
                        );
                        if ($ar_fieldsp = $resp->GetNext())
                            CPrice::Update($ar_fieldsp["ID"], $arFields);
                        else
                            CPrice::Add($arFields);
                    }
                }
            }
            echo BXClearCache(true, ''); // /s2/itc/catalog.element/
            //echo BXClearCache(true, ''); // /s2/itc/catalog.section/

            //ob_start();
            //include($_SERVER["DOCUMENT_ROOT"].'/yandex_custom_run.php');
            //ob_end_clean();

            echo true;
            break;
    }
}
?>