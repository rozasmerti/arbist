#!/usr/bin/php -q
<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CAT_CRON", true);
define('NO_AGENT_CHECK', true);
define('SITE_ID', 's2'); // your site ID - need for language ID

if(!$_SERVER['DOCUMENT_ROOT']){
    $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . "/../../");
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

set_time_limit(1800);
//ini_set('display_errors', true);

CModule::IncludeModule('excel.import');
$filePath = $_SERVER['DOCUMENT_ROOT'] . '/upload/prices/prices.xlsx';
$import = new Excel\Import\Catalog\Main();
$import->import(
    $filePath,
    array(
        'prices' => array(
            'Розничная',
            'Акционная',
            'Цена по акции',
            'BId - простой аукцион',
            'FEE - цена комиссии'
        )
    )
);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");