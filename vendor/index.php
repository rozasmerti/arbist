<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Производитель");
?><?CPageOption::SetOptionString("main", "nav_page_in_session", "N");?>
<h1><?$APPLICATION->ShowProperty("h1");?></h1>
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "new_arbist_breadcrumb", Array(),false);?> 
<?
$APPLICATION->IncludeComponent(
    "itc:catalog.section.filtered.list.vendor",
    "seo",
    Array(
        "TYPE" => $_REQUEST["SECTION_MENU_ID"],
        "VENDOR_CODE" => $_REQUEST["VENDOR_CODE"],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "360",
        "CACHE_GROUPS" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y"
    ),
    false,
    Array(
        'HIDE_ICONS' => 'N'
    )
);?> 
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>