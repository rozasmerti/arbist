 
<style>
<!--
 p.MsoNormal
	{mso-style-parent:"";
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Arial","sans-serif";
	color:black; margin-left:0cm; margin-right:0cm; margin-top:0cm}
-->
</style>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� �������� ��� ������ �������, ��� ����� ��������� ��������. ��� ��������, ������������ �� ���� ���������� ������, ������ ��������� �������� � ����� ���� ��������. ��� ��������� ������� ��� �������� �� ������ ������� ��������� ���������, �� � ������� ���������.</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ���������� ���� ����� ����� ����������� �� ��������� ���������� ���������� ����������.</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ���-������, ������ �� ��� �������� ����������� ����������, ��� �������� ����� ��� ������ ���������� �������� � ������� �������, � ����� ������ ����� ���������, ����� ��� �������� � �������� �������. ������ - ��� ������������ ������ � ��������������� ��������, ��� ��� ��� �� ����������� ����, �, �������������, �������� ����������� ������� ��� �����, ���������� �������������� �������������. </p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ���-������, ��������� �������� �������� �������������. ���� ������ ���������� ���� �� ���������, �� �� ������ ���� ��������� � ����� ��� ������������ �������, ����, ���������� ������� � �������. �� ���� ������ ��� �������� �������� ��� ���� � ���� ����� �������� ��������� � ������ ������.</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ������� �����, ������ �������� ����� �������� ����� �������� ���������, ��� ��������� �������, � ��� �������� ����� ��� ������� �������. </p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ������� �������� ����������, ��� ������ ����������� �� ��������� ������������ ��� ������, ��� ��� ��� ��� ����� ��������� �������� �� ���� � ���� ������-������������ �������. </p>
 
<p class="MsoNormal" style="border: medium none; ">� ���� ��������� ������ ������ � ��������, ��������� �������� �������� �������� - ��� �������� ���������� ���������� ����.</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ��� ����� �����, ��� ��������� �������� �������� ��� �������� ������ � �������� ���������. ��� �������� ����������� ��������, ��� ������� �������� ����� � �������� � �������. ��������� ��� - ��� �� ������ �������, �� � ������.</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� � � ����� ��������-�������� ����������� ������� ����� ��������� ���������� �������� �� ����� ����. �� ������ ������� ��������� ��������, ������� ����� �������� ���� ����� �����������, � �� �������� ��� ����� � ������ ����.</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ����������� ���������, �� ������� � ������� &ldquo;�������&rdquo;.</p>
 
<p class="MsoNormal" style="border: medium none; ">&nbsp;&nbsp;� ������ ���� ������ ��� � ����� ����� ���������� ��������. ���� ����� �������!</p>
 