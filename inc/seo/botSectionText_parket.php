
<p><span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">
    <br />
  </span></p>

<p><span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ��������� �����&nbsp;�������� ����� �� �������������� ����������� ���������� ��������, ������������ ������� ���������� ���������� ����� ���������� ������. </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> 
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� �������� �������� ��������� ����� �� �������� ������� - ��� �������� ������ ������.</span><span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; ">
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ������ ��������� ����� ����� �� ������ ��������: ���������� ������ ���������� ������ �� �������, �������������, ��� �������������� � ������ ��������� �������������� ������, �� ��������� �����, �������� ������������ � �� ������� � ������. �����, ������� ������ �������� ��� ����, ��� ��������� ����� ������ � ������������ � ������� ��� ������ �������������, ����� ��� �������� � ��������� ����. </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> 
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ��������� ����� ����� �������������� ��� ������� �� ������ ���������������, �� � ��������� � ���� ����, ��� ��� ������ �������� �������� � ���� �������� ������� � �������������, � ������� ������� ����������. </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> 
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ������ ����� ������� ������� ���, ������� ��������� ��� ������� ��� � ����� ��������� � �������������. </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> 
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ������������� ��������� ����� ���������� ��������� �������� �� ����� ����, ��� ��� ���� �������� ������ �������������� ���� �� ����� ���������� ����������. </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> 
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� �� ���������� ��� ��������� ������ ��������� �����<b> </b>�� �������� ��������������, ����� ��� Upofloor, </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> <a style="color: blue; text-decoration: underline; text-underline: single" href="http://www.arbist.ru/catalog/parket/?arrFilter_pf%5bBRAND_GUID%5d=91491&amp;set_filter=Y" > <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; text-decoration: none; "> Polarwood</span></a></span><span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">� � </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> <a style="color: blue; text-decoration: underline; text-underline: single" href="http://www.arbist.ru/catalog/parket/?arrFilter_pf%5bBRAND_GUID%5d=95503&amp;set_filter=Y" > <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; text-decoration: none; "> BOEN</span></a></span><span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">.</span><span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; ">
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ��������� ����� - ��� ����������� ������� ��� ������� ��������, �������� � ������� ������. ��� ������ � ������������, �� ����� � ������ ����, � �� ����� �������, ��� ��� ������������ �������, ������� ��� ���������� �������, ��� �������� ���� ����� � ��������.</span><span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; ">
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ������ ��� ����������� ��������� ������� ��� ������ �������� ��������������� ����������, ���� ��������� �������� ��������� �����, ����������� ���� ������ (�� �������� ����, �� �����-��������� ����), �������� � ����� �������� � ������� ��� ���������� � ����������. </span> <span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; "> 
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� ����������� ���������, �������������� �������� ����� ���������� ����������, ��� ������� � ������� &ldquo;�������&rdquo;.</span><span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; ">
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� �������� �������������� �� ���� �������� ������.</span><span style="font-size: 13.5pt; font-family: 'Times New Roman', serif; color: black; ">
    <br />
   </span> <span style="font-size: 11.5pt; font-family: Arial, sans-serif; color: black; ">&nbsp;&nbsp;� �� ����� ���� ������ ��� � ����� ����� ���������� ��������!</span></p>
