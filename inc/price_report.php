<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 06.08.2017
 * Time: 23:18
 */

if (!defined("B_PROLOG_INCLUDED")) require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


define("NOT_CHECK_PERMISSIONS", true);
CModule::IncludeModule("main");
CModule::IncludeModule("currency");
CModule::IncludeModule("catalog");
global $APPLICATION;

global $USER;

$server = "http://www.arbist.ru";
$adminUsers = array();
$titles = array("PID","ACTIVE","NAME","RAZDEL","FABRIC","COLLECTION","SUPPLIER","DATE","WHO","COMMENT","PRICE","DETAILS","CATEGORY");
$data = array();




function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: text/csv; charset=utf-8");
    header("Content-Disposition: attachment; filename={$filename}");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
   // header("Content-Disposition: attachment;filename={$filename}");
    //header("Content-Transfer-Encoding: binary");
}

function array2csv(array &$array, $titles) {
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, $titles, ';');
    foreach ($array as $row) {
        fputcsv($df, $row, ';');
    }
    fclose($df);
    return ob_get_clean();
}

    $arSelect = array(
        "ID",
        "LID",
        "IBLOCK_ID",
        "IBLOCK_NAME",
        "IBLOCK_SECTION_ID",
        "ACTIVE",
        "ACTIVE_FROM",
        "ACTIVE_TO",
        "NAME",
        "DETAIL_PAGE_URL",
        "LIST_PAGE_URL",
        "PROPERTY_BRAND_GUID",
        "PROPERTY_SUPPLIER",
        "PROPERTY_COLLECTION",
        "PROPERTY_AVAIL_DATE",
        "PROPERTY_AVAIL_NOTE",
        "PROPERTY_AVAIL_WHO"
    );

        if (empty($EXPORT_BLOCKS)) {
            $iblockFilter = Array(
                "ACTIVE" => "Y"
            );
            $res = CCatalog::GetList(array(), $iblockFilter);
            while ($ar_res = $res->Fetch()) {
                $EXPORT_BLOCKS[] = $ar_res["IBLOCK_ID"];
            }
        }
    if (is_array($EXPORT_BLOCKS)) {
        $arSiteServers = array();
        foreach ($EXPORT_BLOCKS as $ykey => $yvalue) {
            $filter = Array("IBLOCK_ID" => IntVal($yvalue), "ACTIVE" => "Y", "IBLOCK_ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y");

            $filter["!UF_YA"] = false;

            $db_acc = CIBlockSection::GetList(array("left_margin" => "asc"), $filter);


            $arAvailGroups = array();
            while ($arAcc = $db_acc->Fetch()) {
                $arAvailGroups[] = IntVal($arAcc["ID"]);
            }

            $filter[] = IntVal($yvalue);
            $filter["SECTION_ID"] = $arAvailGroups;
            $filter[">=PROPERTY_AVAIL_DATE"] = date("Y-m-d") . ' 00:00:00';

            $arNavStart = false;
            $res = CIBlockElement::GetList(array(), $filter, false, $arNavStart, $arSelect);


            while ($arAcc = $res->GetNext()) {
                if(empty($arAcc["PROPERTY_AVAIL_DATE_VALUE"])) continue;
                $row = Array();

                if(!empty($arAcc["PROPERTY_AVAIL_WHO_VALUE"]) > 0 && !in_array($arAcc["PROPERTY_AVAIL_WHO_VALUE"],$adminUsers)) {
                $rsUser = CUser::GetByID($arAcc["PROPERTY_AVAIL_WHO_VALUE"]);
                $arUser = $rsUser->Fetch();
                $adminUsers[$arAcc["PROPERTY_AVAIL_WHO_VALUE"]] = $arUser;
                }


                array_push($row,$arAcc["ID"]);
                array_push($row,$arAcc["ACTIVE"]);
                array_push($row,$arAcc["NAME"]);

                if(!empty($arAcc["IBLOCK_NAME"])) {
                    array_push($row,$arAcc["IBLOCK_NAME"]);
                } else {
                    array_push($row," ");
                }

                $br = CIBlockElement::GetByID($arAcc["PROPERTY_BRAND_GUID_VALUE"]);
                if ($br_res = $br->GetNext()) {
                    array_push($row,$br_res['NAME']);
                } else {
                    array_push($row," ");
                }

                if(!empty($arAcc["PROPERTY_COLLECTION_VALUE"])) {
                    array_push($row,$arAcc["PROPERTY_COLLECTION_VALUE"]);
                } else {
                    array_push($row," ");
                }

                if(!empty($arAcc["PROPERTY_SUPPLIER_VALUE"])) {
                    array_push($row,$arAcc["PROPERTY_SUPPLIER_VALUE"]);
                } else {
                    array_push($row," ");
                }


                if(!empty($arAcc["PROPERTY_AVAIL_DATE_VALUE"])) {
                    array_push($row,$arAcc["PROPERTY_AVAIL_DATE_VALUE"]);
                } else {
                    array_push($row," ");
                }

                if(!empty($arAcc["PROPERTY_AVAIL_WHO_VALUE"])) {
                    $userName = $adminUsers[$arAcc["PROPERTY_AVAIL_WHO_VALUE"]]["NAME"];
                    if(!empty($adminUsers[$arAcc["PROPERTY_AVAIL_WHO_VALUE"]]["LAST_NAME"])) $userName .= $adminUsers[$arAcc["PROPERTY_AVAIL_WHO_VALUE"]]["LAST_NAME"];
                    array_push($row,$userName);
                    unset($userName);
                } else {
                    array_push($row," ");
                }


                if(!empty($arAcc["PROPERTY_AVAIL_NOTE_VALUE"])) {
                    array_push($row,$arAcc["PROPERTY_AVAIL_NOTE_VALUE"]);
                } else {
                    array_push($row," ");
                }

                $optPrice = CCatalogProduct::GetOptimalPrice($arAcc["ID"],1,2);
                if($optPrice['PRICE']['PRICE'] > 0) {
                    array_push($row,round($optPrice['PRICE']['PRICE']));
                } else {
                    array_push($row,0);
                }

                if(!empty($arAcc["DETAIL_PAGE_URL"])) {
                    array_push($row,$server . $arAcc["DETAIL_PAGE_URL"]);
                } else {
                    array_push($row," ");
                }

                if(!empty($arAcc["LIST_PAGE_URL"])) {
                    array_push($row,$server . $arAcc["LIST_PAGE_URL"]);
                } else {
                    array_push($row," ");
                }
                array_push($data,$row);


            }
        }
    }
download_send_headers("price-report.csv");
echo array2csv($data,$titles);
die();