<? global $USER; ?>
<?
$availDate = date('d-m-Y', strtotime("+3 days"));
$obsoleteDate = date('d-m-Y', strtotime("+30 days"));
?>
<link rel="stylesheet" href="../css/font-awesome.min.css">
<div style="margin-right: 10px; float: left;" class="avail-controls">
	<script>

        function avail_form_submit(event) {
            event.preventDefault();
            if (!$("[id^=option]").is(':checked')) {
                console.log("no");
                $('#options-warning').show();
                $('.warning').hide();
            } else {
                console.log("yes");
                $('#form-avail').hide();
                $('.avail-controls').css({'float':'left','margin-right':'10px','width':''});
                $('#avail-link').show();
                $.post(
                    'ajax.php',
                    $(event.target).serialize(),
                    function(data)
                    {
                        window.location.reload();
                    }
                );
            }
        }

        $(document).ready(function() {
            $('#close').click(function () {
                if ($('#form-avail').is(':visible')) {
                    $('#form-avail').hide();
                    $('.avail-controls').css({'float':'left','margin-right':'10px','width':''});
                    $('#avail-link').show();
                } else {
                    $('.avail-controls').css({'float':'left','margin-right':'10px','width':''});
                    $('#form-avail').show();
                    $('#avail-link').hide();
                }
            });
            $('#option3').click(function(){
                if($('#option3').is(':checked')) {
                    $('#val3').val('<?=$obsoleteDate ?>');
                    $('#view-date').text('<?=$obsoleteDate ?>');
                } else {
                    $('#val3').val('<?=$availDate ?>');
                    $('#view-date').text('<?=$availDate ?>');
                }

            });
            $("[id^=option]").click(function(){
                if($('#options-warning').is(':visible')) {
                    $('#options-warning').hide();
                    $('.warning').show();
                }
            });
        });
	</script>

	
	<? if ($avail_type == 'section') : ?>
    <a href="#" style="color:green;"  id="avail-link" onclick="event.preventDefault(); $(event.target).parent().css({'margin-right': '', 'float': ''}); $(event.target).hide(); $(event.target).next('form').show();">�����������</a>
    <? else : ?>
    <? if (checkAvailDate($element)) : ?>
    <a href="#" style="color:green;" id="avail-link" onclick="event.preventDefault(); $(event.target).parent().css({'margin-right': '', 'float': ''}); $(event.target).hide(); $(event.target).next('form').show(); ">�����������</a>
	<? else : ?>
	<a href="#" style="color:red;"  id="avail-link" onclick="event.preventDefault(); $(event.target).parent().css({'margin-right': '', 'float': ''});$(event.target).hide(); $(event.target).next('form').show();">�����������</a>
	<? endif; ?>
    <? endif; ?>
    
	<form style="display: none;" onsubmit="avail_form_submit(event);" id="form-avail">
		<input type="hidden" name="type" value="avail_<?= $avail_type ?>" />
		<input type="hidden" name="id" value="<?= $id ?>" />
        <input type="hidden" name="avail_date" value="<?= $availDate ?>" id="val3">
        <div class="head">
            <div class="warning">����������� ����� ����� �� <span id="view-date"><?=$availDate ?></span></div>
                <div id="options-warning">���� ������� ������� ���� ������� ������!</div>
            <div class="collapse" id="close"><i class="fa fa-times" aria-hidden="true"></i></div>
        </div>
        <div class="checkbox checkbox-success checkbox-inline">
            <input type="checkbox" class="styled" id="option1" name="wrong_price" value="������������ ����">
            <label for="option1">������������ ����</label>
            <input type="checkbox" class="styled" id="option2" name="supply_awaiting" value="��������� �����������">
            <label for="option2">��������� �����������</label>
            <input type="checkbox" class="styled" id="option3" name="obsolete" value="�� �����">
            <label for="option3">�� �����</label>
        </div>
        <div class="save-button"><input class="btn btn-warning" type="submit" value="���������"></div>
	</form>
</div>