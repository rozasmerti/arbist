<?

if (preg_match("|^(.*)(/{2,})(.*)$|i", $_SERVER['REQUEST_URI'], $match)) {
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://www.arbist.ru" . $match[1] . "/" . $match[3]);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("title", "��������������: ������ ���������� � ������������ ��������� �� �������� ����� � ��������-�������� ������");
//$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
//$APPLICATION->SetTitle("������� ��������");
//$APPLICATION->SetTitle("���������� � ������������ ��������� | ����, ����, ������� � ��������-������� Arbist.ru");
//$APPLICATION->SetPageProperty("description", "��������-������� ���������� � ������������ ���������� Arbist � � ������� ���������� ��������� ������� �������������� �� ������ �����.");
?>
	<h1><?$APPLICATION->ShowProperty("h1");?></h1>
<?if($_REQUEST["SECTION_MENU_ID"] != 'ljuki' && $_REQUEST["SECTION_MENU_ID"] != 'heating'):?>
	<?$APPLICATION->ShowProperty("topText");?>
<?endif;?>
<?
$template = 'new';
if($_REQUEST["SECTION_MENU_ID"] == 'dveri') {
	//��� ������ ���� ������
	$template = 'doors';
}elseif($_REQUEST["SECTION_MENU_ID"] == 'ljuki' || $_REQUEST["SECTION_MENU_ID"] == 'heating')
{
	$arPropsMap = array(
		"ljuki" => array( //���������� ��������� ������� = ���������� ������ �������
			"vendor",
			"view",
			"schema",
			"uses",
			"material"
		),
		"heating" => array(
			"country",
			"vendor",
			"material"
		)
	);
	$arIBlockIDMap = array(
		"ljuki" => array(
			IBLOCK_LJUKI_ID
		),
		"heating" => array(
			IBLOCK_OBOGREV_KROVLI_I_PLOSHADOK_ID,
			IBLOCK_OBOGREV_TRUB_VODOPROVODA_ID,
			IBLOCK_SPECIALNIE_NAGREVATELNIE_KABELI_ID,
			IBLOCK_TERMOREGULYATORI,
			IBLOCK_JELEKTRICHESKIE_TEPLYE_POLY_ID,
		)
	);

	$arTemplateMap = array(
		"ljuki" =>"",
		"heating" => "polyBlock"
	);

	$APPLICATION->IncludeComponent(
		"avekom:catalag.seo_navigation",
		$arTemplateMap[$_REQUEST["SECTION_MENU_ID"]],
		Array(
			"IBLOCK_ID" => $arIBlockIDMap[$_REQUEST["SECTION_MENU_ID"]],
			"PROPERTY_CODE"=>$arPropsMap[$_REQUEST["SECTION_MENU_ID"]],
			"SHOW_PROPERTIES_CODE"=>$arPropsMap[$_REQUEST["SECTION_MENU_ID"]],
		)
	);

}
?>
<?if($_REQUEST["SECTION_MENU_ID"] != "ljuki" && $_REQUEST["SECTION_MENU_ID"] != 'heating'):?>
	<?$APPLICATION->IncludeComponent(
		"itc:seo.main.section.list",
		$template,
		Array(
			"SECTION_ID" => $_REQUEST["SECTION_MENU_ID"],
			"IBLOCK_TYPE" => "dynamic_content",
			"IBLOCK_ID" => "47",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600000"
		)
	);?>
<?endif;?>

<?if($_REQUEST["SECTION_MENU_ID"] == "ljuki"):?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"new_arbist_breadcrumb",
		Array(
		)
	);?>
	<?$APPLICATION->IncludeComponent(
		"itc:catalog.section.filtered.list",
		"seo_ljuki",
		Array(
			"IBLOCK_TYPE" => "",
			"IBLOCK_ID" => IBLOCK_LJUKI_ID,
			"SECTION_ID" => "",
			"SECTION_CODE" => "",
			"SECTION_URL" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "2",
			"SECTION_FIELDS" => "",
			"SECTION_USER_FIELDS" => "",
			"ADD_IBLOCK_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			"SHOW_COUNT" => 30
			/*"FILTER_PROP_ID" => $map[$iblockCode][$property],
			"FILTER_PROP_VALUE" => $value*/
		)
	);?>

<?endif;?>

<?
/*
 * ��� ������� ������ ��������, ����� �������������� ����� �������� ����� ���������� ����������
 */
if($_REQUEST["SECTION_MENU_ID"] == 'heating'):
	$APPLICATION->IncludeComponent(
		"avekom:poly.sections.list",
		"",
		Array(
			"IBLOCK_ID" => $arIBlockIDMap[$_REQUEST["SECTION_MENU_ID"]],
			"PAGER_TEMPLATE" => "nav_bot",
			"PAGER_TEMPLATE_TOP" => "nav_top", //����������� ���������� ����������� ��������� ������� ������ ������� ��� ������ ��������� � ������. �������� ������� ���
			"PROPERTY_ELEMENT_CODE"=>array("material_text"),
			"FIELD_ELEMENT_CODE"=>array("DETAIL_PICTURE"),
			"PAGE_ELEMENT_COUNT"=>40
		),
		false
	);
endif;
?>

<?
//��� ����� � ��� ����� ����� ������� � ������������� ����� ������
if($_REQUEST["SECTION_MENU_ID"] == "ljuki" || $_REQUEST["SECTION_MENU_ID"] == 'heating'):
	?>
	<div class="dl-seo-text">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/bitrix/templates/arbist/include/index/".$_REQUEST["SECTION_MENU_ID"].".php",
				"EDIT_TEMPLATE" => ""
			),
			false
		);?>
	</div>
<?else:?>
	<?$APPLICATION->ShowProperty("botText");?>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>