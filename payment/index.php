<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������");
?> 
<h3>������� ������</h3>
 
<p>� ��������� ����� ������ �� ������� ����� �������������:</p>
 
<ul class="gap1 list2 list"> 	 
  <li>��������������� � �������� ��� ������ ���������� ��������;</li>
 	 
  <li>��������� ��������-����������� ��� ��������;</li>
 	 
  <li>���������� ���������.</li>
 </ul>
 
<p>������ ������ ������������ ������ � ���������� ������, ������ � ������������ � �����, ��������� � �������� ��� �������� ����.</p>
 
<p>��� ���������� ������ ��� ����������� - ���������� ������� ��������� ���, � ��� ����������� ��� ������������ ����������� � �� ���������.�</p>
 
<p>���� ����������� �������� � ���, ����������� ������� ���, �� �������� ���� � ������ ���, ��������� ������ ��� ���� ��������� �������.�</p>
 
<p>��� �������� ��� ��� ��������� ������ �� ������ ����������, �� ���������� ������ ����� ������������� ����������.</p>
 
<p><i>(�� �������� ��� ������������� ����������� ������������ �� ��������� ����������� ������).</i></p>
 
<h3>���� ���������</h3>
 
<div class="gap4"> 	 
  <table class="table1 table"> 		 
    <tbody> 
      <tr> 			<td> 				 �������� ��������:</td> 			<td> 				 �� ��������� ������� ����������</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">����������� �����: </div>
         			</td> 			<td> 				 
          <div class="hl3"><span itemprop="addressLocality" style="font-family: Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">127411 �.������</span><span style="font-family: Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">,�</span><span itemprop="streetAddress" style="font-family: Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">����������� �., �.157, ���.12/2, ��.122-208</span></div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">�������������:</div>
         			</td> 			<td> 				 
          <div class="hl3"><span style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">311774617100733</span></div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">����</div>
         			</td> 			<td> 				 
          <div class="hl3"><span style="font-family: &quot;Times New Roman&quot;, serif; font-size: 16px;">311774617100733</span></div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">���</div>
         			</td> 			<td> 				 
          <div class="hl3"><span style="font-family: &quot;Times New Roman&quot;, serif; font-size: 16px;">211400085330</span></div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">�����</div>
         			</td> 			<td> 				 
          <div class="hl3"><span style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">45000000000</span></div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">�./�. � </div>
         			</td> 			<td> 				 
          <div class="hl3"><span style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">40802810938000018508</span></div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">���� </div>
         			</td> 			<td> 				 
          <div class="hl3">��� &quot;��������&quot; �. ������</div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">�./�. � </div>
         			</td> 			<td> 				 
          <div class="hl3"><span style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">30101810400000000225</span></div>
         			</td> 		</tr>
     		 
      <tr> 			<td> 				 
          <div class="hl0">��� </div>
         			</td> 			<td> 				 
          <div class="hl3">044525225</div>
         			</td> 		</tr>
     	</tbody>
   </table>
 </div>
 
<h3 class="alt1">��������!</h3>
 
<p>��������� ����������, ������������� �������� �������������, ��� ����� ������� �� ����� �������� ����� ���������� �� ��������� �����. � ������ ���� ��� ���������� ����� ��������� �����-���� ����, ����������� �������� � ��� ������� � �������� ���������������� ���, ���������� ���� ����� � ���������� ��������. ����������� ��������� � ��������� ������� ���������� ������ � ���������������� ����, ��� ��� ���������� ���� ����������� ������ � ����, �� ��������������� ���������.</p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>