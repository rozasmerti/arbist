<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?
if(isset($_REQUEST["ELEMENT_ID"]))
{
	$arFilter = Array(
		"IBLOCK_ID" => 76,
		"ACTIVE" => "Y",
		"XML_ID" => "main"
	);
	$db_list = CIBlockSection::GetList(Array(), $arFilter, false);
	if($ar_result = $db_list->GetNext()) {

		$arFilter = Array(
			"IBLOCK_ID" => 76,
			"ACTIVE" => "Y",
			"SECTION_ID" => $ar_result["ID"],
		);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("PROPERTY_IBLOCK_ID"));
		while($ar_fields = $res->GetNext()) {
			$plitkaSections[] = $ar_fields["PROPERTY_IBLOCK_ID_VALUE"];
		}
	}

	?>
	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "new_arbist_breadcrumb", array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
	),
	false
);?>
	<h1><?$APPLICATION->ShowProperty("h1")?></h1>
	<?
	/****************************************************/
	/* � $arParams bitrix:catalog.element ������ ����   */
	/* �������� "PLITKA_SECTIONS" => $plitkaSections,   */
	/****************************************************/
	?>



	<?$ElementID=$APPLICATION->IncludeComponent(
	"itc:catalog.element",
	"",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "76",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"ELEMENT_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DISPLAY_PANEL" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"FIELD_CODE" => array("DETAIL_PICTURE"),
		"PROPERTY_CODE" => Array(
			"vendor",
			"country_text", //������
			"vendor", //�������������
			"view_text", //��� ������
			"uses_text", //����������
			"timing", //���� ��������
			"material_text", //��������
			"ed_izm", //�������� ���������
			"otgruz", //��������� ��������
			"garanty", //��������� ��������

			"tolsh", //�������
			"power", //��������

		),
		"PRICE_CODE" => Array("���������"),
		"USE_PRICE_COUNT" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	)
);?>
	<?
	if(intval($arResult["VARIABLES"]["SECTION_ID"]) == 0 && intval($arResult["VARIABLES"]["ELEMENT_ID"]) > 0) {
		$resDB  = CIBlockElement::GetList(
			Array("SORT"=>"ASC"),
			Array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ID' => intval($arResult["VARIABLES"]["ELEMENT_ID"])),
			false,
			false,
			Array('ID','IBLOCK_SECTION_ID')
		);
		if($result = $resDB->fetch()){
			$arResult["VARIABLES"]["SECTION_ID"] = $result['IBLOCK_SECTION_ID'];
		}
	}
	?>
	<?$APPLICATION->IncludeComponent("itc:catalog.addchain", ".default", array(
		"DISPLAY_IBLOCK_CHAIN" => "Y",
		"PROP_IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"PROP_IBLOCK_CODE" => $_REQUEST["IBLOCK_CODE"],
		"SECTION_ID" => intval($arResult["VARIABLES"]["SECTION_ID"]),
		"ELEMENT_ID" => $ElementID,
		"IBLOCK_TYPE" => "dynamic_content",
		"IBLOCK_ID" => "47",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
	),
	$component
);?>

	<?if($arParams["USE_REVIEW"]=="Y" && IsModuleInstalled("forum") && $ElementID):?>
	<br />
	<?$APPLICATION->IncludeComponent(
		"bitrix:forum.topic.reviews",
		"",
		Array(
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
			"USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
			"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
			"FORUM_ID" => $arParams["FORUM_ID"],
			"URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
			"SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
			"ELEMENT_ID" => $ElementID,
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
			"POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
			"URL_TEMPLATES_DETAIL" => $arParams["POST_FIRST_MESSAGE"]==="Y"? $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"] :"",
		),
		$component
	);?>
<?endif?>
	<?if($arParams["USE_ALSO_BUY"] == "Y" && IsModuleInstalled("sale") && $ElementID):?>

	<?$APPLICATION->IncludeComponent("bitrix:sale.recommended.products", ".default", array(
			"ID" => $ElementID,
			"MIN_BUYES" => $arParams["ALSO_BUY_MIN_BUYES"],
			"ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
			"LINE_ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
			"DETAIL_URL" => $arParams["DETAIL_URL"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		),
		$component
	);

	?>
<?endif?>
<?
	}else{
// ����� ��� �����, ������� ��������
CModule::IncludeModule('iblock');

$res = CIBlock::GetList(array("ID"=>"ASC"),array("CODE"=>$_REQUEST["IBLOCK_CODE"]));
$_NAME_BLOCK ="";
if ($ar = $res->Fetch())
{
	$_ID_BLOCK = $ar["ID"];
	$_NAME_BLOCK = $ar["NAME"];
}
$APPLICATION->SetTitle($_NAME_BLOCK);
?><h1><?=$_NAME_BLOCK?></h1><?

$APPLICATION->IncludeComponent(
	"avekom:catalag.seo_navigation",
	"",
	Array(
		"IBLOCK_ID" => $_ID_BLOCK,
		"PROPERTY_CODE" => array(
			"country",
			"vendor",
			"material"
		)
	)
);

	$APPLICATION->IncludeComponent(
		"itc:catalog.section.filtered.list",
		"seo_obogrev",
		Array(
			"IBLOCK_TYPE" => "",
			"IBLOCK_ID" => $_ID_BLOCK,
			"SECTION_ID" => "",
			"SECTION_CODE" => "",
			"SECTION_URL" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "2",
			"SECTION_FIELDS" => "",
			"SECTION_USER_FIELDS" => "",
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			/*"FILTER_PROP_ID" => $map[$iblockCode][$property],
			"FILTER_PROP_VALUE" => $value*/
			"SORT_BY" => 'sort',
			"SORT_ORDER" => 'asc'
		)
	);
}
?>
<div class="dl-seo-text">
	<?
	$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/bitrix/templates/arbist/include/catalog/".$_REQUEST["IBLOCK_CODE"].".php",
		"EDIT_TEMPLATE" => ""
	),
	false
	);
	?>
	</div>
<?

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>