<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������� ������� ������� � ��������");
?>
<?php
$map = array(
    "plitka_premium" => array(
        "vendor" => 2529,
        "purpose" => 580,
        "country" => 584,
    ),
    "plitka_mosaico" => array(
        "vendor" => 610,
        "purpose" => 609,
        "country" => 613,
    ),
    "plitka_clinker" => array(
        "vendor" => 2550,
        "purpose" => 1472,
        "country" => 639,
    ),
    "keramogranit" => array(
        "vendor" => 2627,
        "purpose" => 172,
        "country" => 176,
    ),
    "acs_plitka" => array(
        "vendor" => 2628,
        "purpose" => 1568, // �����
        "country" => 530,
    ),
    "laminat" => array(
        "vendor" => 2629, 
        "purpose" => 1607, // �����
        "country" => 207,
    ),
    "parket" => array(
        "vendor" => 2630,
        "purpose" => 1648, // �����
        "country" => 254,
    ),
    "massiveboard" => array(
        "vendor" => 2631,
        "purpose" => 1736, // �����
        "country" => 227,
    ),
    "probca" => array(
        "vendor" => 2554,
        "purpose" => 2325, // �����
        "country" => 2331,
    ),
    "acs_pol" => array(
        "vendor" => 2632,
        "purpose" => 1787, // �����
        "country" => 547,
    ),
    "doors_vxod" => array(
        "vendor" => 2634,
        "purpose" => 788, // �����
        "country" => 792,
    ),
    "doors" => array(
        "vendor" => 2531,
        "purpose" => 1063, // �����
        "country" => 136,
    ),
    "santexnica_vanna" => array(
        "vendor" => 2534,
        "purpose" => 1187, // �����
        "country" => 1193,
    ),
    "santexnica_mebel" => array(
        "vendor" => 2540,
        "purpose" => 1412, // �����
        "country" => 1418,
    ),
    "santexnica_bude" => array(
        "vendor" => 2538,
        "purpose" => 1337, // �����
        "country" => 1343,
    ),
    "santexnica_moiki" => array(
        "vendor" => 2536,
        "purpose" => 1262, // �����
        "country" => 1268,
    ),
    "santexnica_mixers" => array(
        "vendor" => 2542,
        "purpose" => 1908,
        "country" => 1914,
    ),
    "santexnica_showers" => array(
        "vendor" => 2544,
        "purpose" => 1986, // �����
        "country" => 1992,
    ),
    "santexnica_showersys" => array(
        "vendor" => 2546,
        "purpose" => 2064,
        "country" => 2070,
    ),
    "santexnica_towel" => array(
        "vendor" => 2548,
        "purpose" => 2142, // �����
        "country" => 2148,
    ),
    "santexnica_acs" => array(
        "vendor" => 2552,
        "purpose" => 2220, //�����
        "country" => 2226,
    ),
);

CModule::IncludeModule('iblock');

print '<h2 style="border-bottom: 1px solid #CC6600; color: #CC6600;">�������������</h2>';
print '<table cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse;" width="500">';

$resVendors = CIBlockElement::GetList(array('NAME' => 'ASC'), array('IBLOCK_ID' => '46', 'ACTIVE' => 'Y'));
$i = 0;

while ($arItem = $resVendors->Fetch()) {
    if ($i % 2 == 0) {
        print '<tr>';
    }
    print '<td width="200">' . $arItem['NAME'] . '</td>';
    print '<td>' . $arItem['ID'] . '</td>';

    if ($i % 2 != 0) {
        print '</tr>';
    }

    $i++;
}

print '</table>';
print '<br/>';
print '<br/>';

foreach ($map as $iblockCode => $props) {
    $arIblock = CIBlock::GetList(array(), array('CODE' => $iblockCode))->Fetch();
    print '<h2 style="border-bottom: 1px solid #CC6600; color: #CC6600;">' . $arIblock['NAME'] . '</h2>';

    foreach ($props as $propPseudoname => $propId) {
        if ($propPseudoname == 'vendor') {
            continue;
        }

        $arProp = CIBlockProperty::GetByID($propId)->Fetch();
        print '<b>' . $arProp['NAME'] . '</b><br/>';
        print '<table cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse;" width="400">';

        $values = array();

        $resEnum = CIBlockPropertyEnum::GetList(array('value' => 'asc'), array('IBLOCK_ID' => $arProp['IBLOCK_ID'], 'PROPERTY_ID' => $arProp['ID']));

        while ($arEnum = $resEnum->Fetch()) {
            $values[] = array(
                'id' => $arEnum['ID'],
                'value' => $arEnum['VALUE'],
            );
        }

        if (count($values) == 0) {
            print '<tr>';
            print '<td>�������� �����������</td>';
            print '</tr>';
        } else {
            foreach ($values as $value) {
                print '<tr>';
                print '<td width="300">' . $value['value'] . '</td>';
                print '<td>' . $value['id'] . '</td>';
                print '</tr>';
            }
        }

        print '</table>';
        print '<br/>';
    }

    print '<br/>';
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
