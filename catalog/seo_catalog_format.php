<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CPageOption::SetOptionString("main", "nav_page_in_session", "N");
function sklon($slovo, $padej) {
	$mas = array( 'IP', 'RP', 'DP', 'VP', 'TP', 'PP');
	$val = array_search($padej, $mas);
	$xml = simplexml_load_file("http://export.yandex.ru/inflect.xml?name=$slovo");
	return (string) $xml->inflection[$val];
}
?><h1><?$APPLICATION->ShowProperty("h1");?></h1>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"new_arbist_breadcrumb",
	Array()
);?><?
if($APPLICATION->GetCurPage()=="/catalog/keramogranit/purpose/plitka-dlya-kuxni/") 
{
	LocalRedirect('/404.php');
}
/*
if($_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/��� �����/' || $_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/�����/')
    //LocalRedirect('/catalog/plitka_premium/purpose/������ ��� �����/');
if($_SERVER["REQUEST_URI"] == '/catalog/plitka_mosaico/purpose/��� �����/' || $_SERVER["REQUEST_URI"] == '/catalog/plitka_mosaico/purpose/�����/') 
    //LocalRedirect('/catalog/plitka_mosaico/purpose/������ ��� �����/');
if($_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/��� �����/' || $_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/�����/') 
    //LocalRedirect('/catalog/plitka_premium/purpose/������ ��� �����/');
if($_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/��� ����/' || $_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/����/')
    //LocalRedirect('/catalog/plitka_premium/purpose/������ ��� ����/');
if($_SERVER["REQUEST_URI"] == '/catalog/keramogranit/purpose/��� ����/' || $_SERVER["REQUEST_URI"] == '/catalog/keramogranit/purpose/����/')
    //LocalRedirect('/catalog/keramogranit/purpose/������ ��� ����/');
if($_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/��� ����/' || $_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/����/')
    //LocalRedirect('/catalog/plitka_premium/purpose/������ ��� ����/');
if($_SERVER["REQUEST_URI"] == '/catalog/keramogranit/purpose/��� �����/' || $_SERVER["REQUEST_URI"] == '/catalog/keramogranit/purpose/�����/')
    //LocalRedirect('/catalog/keramogranit/purpose/������ ��� �����/');

if($_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/�����/' ||
   $_SERVER["REQUEST_URI"] == '/catalog/plitka_mosaico/purpose/�����/' ||
   $_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/�����/' ||
   $_SERVER["REQUEST_URI"] == '/catalog/plitka_premium/purpose/����/' ||
   $_SERVER["REQUEST_URI"] == '/catalog/keramogranit/purpose/����/'
  ) //LocalRedirect("/404.php");

*/

CModule::IncludeModule("iblock");
$iblockCode = $_REQUEST["IBLOCK_CODE"];
$property = $_REQUEST["PROPERTY"];
$value = $_REQUEST["VALUE"];
$other = $_REQUEST["OTHER"];

//������������ �������� ������ � ��������, �.�. � url ��� CEO ���� ���������� ��������, �� ��� ��� ��� ��������� ��� �������� 
//� ���� ��� �� 301 ��������, ���� ����� ������ �� ������� ������ � ������������� ������� � �������� ������jj
if ($property == "country") 
{
	$temp_value = TranslateIt($value, 'en_ru');
			
	$countryManufacturerPattern = "#\(.+\)#Us";
	$countryManufacturerReplacePattern = "#\s+\(.+\)#Us";
	$matches = array();

	//��������� �������� "������� (BerryAlloc)"
	if(preg_match($countryManufacturerPattern, $value, $matches)){
		$partToTranslate = preg_replace($countryManufacturerReplacePattern, '', $value);
		$manufacturerPart = $matches[ 0 ];
		
		//$value = TranslateIt($partToTranslate, 'en_ru') . ' ' . $manufacturerPart;
	} else {
					
		if ($temp_value == $value) {
			//LocalRedirect(str_replace($value, TranslateIt($value), $_SERVER["REQUEST_URI"]), false, 301);
		} else {
			$value = $temp_value;
		}
	}    
}
    
//if($GLOBALS['USER']->GetID() == 549)
//������� ����������� ��������� ��� �������� "����������". �������� �������� �� �������� � ��� ������ ������� ������� 301� �������� -- Vanes 2014-03-19 issue #7537 punkt 1
// {
	if ($property == "purpose") 
	{            
		$temp_value = strtolower(str_replace(' ', '-', TranslateIt($value, 'ru_en')));
		if ($temp_value != $value) 
		{
			//LocalRedirect(str_replace($value, $temp_value, $_SERVER["REQUEST_URI"]), false, 301);
		} 
		else 
		{
			//���������� ����� �������������, �.�. ����� ����� �������� ������� �������� ��������� "����������" ���� ������, �� �������� ��������, ������� ����������� ������ �� �������
			$value = array_search($temp_value, $GLOBALS['arPurposeCyr']);
		}     
	}
// }
    
global $USER;


// BRAND_GUID - ������������� (vendor)
// DESTINATION (�� � ����) - ���������� (purpose)
// COUNTRY - ������ (country)

$map = array(
	"plitka_premium" => array(
		"vendor" => 2529,
		"purpose" => 580,
		"country" => 584,
	),
	"plitka_mosaico" => array(
		"vendor" => 2530,
		"purpose" => 609,
		"country" => 613,
	),
	"plitka_clinker" => array(
		"vendor" => 2550,
		"purpose" => 1472,
		"country" => 639,
	),
	"keramogranit" => array(
		"vendor" => 2627,
		"purpose" => 172,
		"country" => 176,
	),
	"acs_plitka" => array(
		"vendor" => 2628,
		"purpose" => 1568, // �����
		"country" => 530,
	),
	"laminat" => array(
		"vendor" => 2629, 
		"purpose" => 1607, // �����
		"country" => 207,
	),
	"parket" => array(
		"vendor" => 2630,
		"purpose" => 1648, // �����
		"country" => 254,
	),
	"massiveboard" => array(
		"vendor" => 2631,
		"purpose" => 1736, // �����
		"country" => 227,
	),
	"probca" => array(
		"vendor" => 2554,
		"purpose" => 2325, // �����
		"country" => 2331,
	),
	"acs_pol" => array(
		"vendor" => 2632,
		"purpose" => 1787, // �����
		"country" => 547,
	),
	"doors_vxod" => array(
		"vendor" => 2862,
		//"purpose" => 788, // �����
		"country" => 2836,
	),
	"doors" => array(
		"vendor" => 2948,
		//"purpose" => 1063, // �����
		"country" => 2922,
	),
	"santexnica_vanna" => array(
		"vendor" => 2534,
		"purpose" => 1187, // �����
		"country" => 1193,
	),
	"santexnica_mebel" => array(
		"vendor" => 2540,
		"purpose" => 1412, // �����
		"country" => 1418,
	),
	"santexnica_bude" => array(
		"vendor" => 2538,
		"purpose" => 1337, // �����
		"country" => 1343,
	),
	"santexnica_moiki" => array(
		"vendor" => 2536,
		"purpose" => 1262, // �����
		"country" => 1268,
	),
	"santexnica_mixers" => array(
		"vendor" => 2542,
		"purpose" => 1908,
		"country" => 1914,
	),
	"santexnica_showers" => array(
		"vendor" => 2544,
		"purpose" => 1986, // �����
		"country" => 1992,
	),
	"santexnica_showersys" => array(
		"vendor" => 2546,
		"purpose" => 2064,
		"country" => 2070,
	),
	"santexnica_towel" => array(
		"vendor" => 2548,
		"purpose" => 2142, // �����
		"country" => 2148,
	),
	"santexnica_acs" => array(
		"vendor" => 2552,
		"purpose" => 2220, //�����
		"country" => 2226,
	),
	"board" => array(
		"vendor" => 3211,
		"purpose" => 3179, //�����
		"country" => 3185,
	),
	
	
	"door_handles" => array(
		"vendor" => 3297,
		"purpose" => 3265, //�����
		"country" => 3271,
	),
	"hinges" => array(
		"vendor" => 3383,
		"purpose" => 3351, //�����
		"country" => 3357,
	),
	"door_locks" => array(
		"vendor" => 3469,
		"purpose" => 3437, //�����
		"country" => 3443,
	),
	"wrapping" => array(
		"vendor" => 3555,
		"purpose" => 3523, //�����
		"country" => 3529,
	),
	"door_linings" => array(
		"vendor" => 3641,
		"purpose" => 3609, //�����
		"country" => 3615,
	),
	"cylinder_mechanism" => array(
		"vendor" => 3727,
		"purpose" => 3695, //�����
		"country" => 3701,
	),
	"door_stops" => array(
		"vendor" => 3813,
		"purpose" => 3781, //�����
		"country" => 3787,
	),
	"crossbars" => array(
		"vendor" => 3899,
		"purpose" => 3867, //�����
		"country" => 3873,
	),
	"sliding_systems" => array(
		"vendor" => 3985,
		"purpose" => 3953, //�����
		"country" => 3959,
	),
	"avtoporogi" => array(
		"vendor" => 4071,
		"purpose" => 4039, //�����
		"country" => 4045,
	),
);
    
$textValue = $value;
$res = CIBlockProperty::GetByID($map[$iblockCode][$property], false, $iblockCode);
if($ar_res = $res->GetNext())
{
	if($ar_res['PROPERTY_TYPE'] == 'L')
	{
		$enum = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC", "VALUE"=>"ASC"), Array('PROPERTY_ID' => $ar_res['ID'], 'VALUE' => $value));
		if($propVariant = $enum->fetch()) 
			$value = $propVariant['ID'];
	}
	else 
	{
		if($property == 'vendor')
		{
			$arFilter = Array(
				"IBLOCK_ID" => 46,
				"ACTIVE" => "Y",
				 array(
					 "LOGIC" => "OR",
					 array("NAME" => $value),
					 array("CODE" => $value),
				 )
			);
			$vendors = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array("ID", "NAME", "CODE"));
			if($arFields = $vendors->GetNext()) 
			{
				if ($value == $arFields["NAME"]) 
				{
					//LocalRedirect(str_replace($value, $arFields["CODE"], $_SERVER["REQUEST_URI"]), false, 301);
				}
				$value = $arFields["ID"];
				$textValue = $arFields["NAME"];
			}
		}
	}
} 
	
	
	
// ������� ��� � ��� ���������
$iblockFilter = Array(
	"SITE_ID" => SITE_ID,
	"ACTIVE" => "Y",
	"CODE" => $iblockCode
);

$resIB = CIBlock::GetList(Array(), $iblockFilter, false);
if($arResIB = $resIB->Fetch()) 
{
	$iblockId = $arResIB["ID"];
	$iblockType = $arResIB["IBLOCK_TYPE_ID"];
	$iblockName = $arResIB["NAME"];
}

$doNext = false;
if(empty($other) || preg_match("/\?(.*)/", $other)) 
{
	$doNext = true;
}

if(intval($value) > 0 && $doNext) 
{
	switch($property) 
	{
		case "vendor": 
		{
	  
			// $APPLICATION->SetPageProperty("title", $textValue . " - � ������� " . $iblockName . " " . $textValue . " � ������ �� ������ ����� | ������� ��������-�������� Arbist.ru");
			//$v_p = mb_strtolower(iconv( "utf-8","windows-1251", sklon($iblockName, 'VP')));
			$v_p = mb_strtolower(iconv( "utf-8","windows-1251", sklon($iblockName, 'IP')));
			
			$APPLICATION->SetPageProperty("title", $iblockName . ' ' . $textValue . ': ������ '  . (($v_p == '')?$iblockName:$v_p) . ' �� ������������� ' . $textValue . ' �� ������ ����� � ������ - ������� ��������-�������� Arbist');
			$APPLICATION->SetPageProperty("description", $textValue . " �� ������ �����, " . $iblockName . " " . $textValue . " � ������ &ndash; ��������-������� Arbist");
			$APPLICATION->SetPageProperty("h1", $iblockName . " " . $textValue);
			echo '� ������� ������ �� ������� &laquo;'.$iblockName.' '.$textValue.'&raquo; �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';
			break;
		}
		case "country": 
		{
			if($iblockName != '������������')
			{
				$v_p = mb_strtolower(iconv( "utf-8","windows-1251", sklon($iblockName, 'VP')));                    
				$adjectiveCountryVP = $GLOBALS['arAdjectiveCountry'][$textValue]['female']['vp'];
				$adjectiveCountryTP = $GLOBALS['arAdjectiveCountry'][$textValue]['female']['tp'];
			}
			else
			{
				$v_p = mb_strtolower(iconv( "utf-8","windows-1251", sklon($iblockName, 'IP')));                    
				$adjectiveCountryVP = $GLOBALS['arAdjectiveCountry'][$textValue]['male']['vp'];
				$adjectiveCountryTP = $GLOBALS['arAdjectiveCountry'][$textValue]['male']['tp'];                    
			}
			$ibNameRP = mb_strtolower(iconv( "utf-8","windows-1251", sklon($iblockName, 'RP')));
			$textValueRP = iconv( "utf-8","windows-1251", sklon($textValue, 'RP'));
			$ibAltNameRP = $ibNameRP;
			if($iblockName == '�������')
			{
				//$iblockName = '���������� ������';
				$v_p = '���������� ������';
				$ibAltNameRP = '���������� ������';
			}                
			
			$APPLICATION->SetPageProperty("title", $iblockName . " " . $textValue . ": ����, ������ " . $adjectiveCountryVP . ' ' . $v_p /*. " " . $textValue*/ . " � ������ � ��������� - ������� ��������-�������� Arbist");
			$APPLICATION->SetPageProperty("description", '������� ����� � � ������� ' . $ibNameRP . " �� " . $textValueRP . ". ������� " . $adjectiveCountryTP . ' ' . $ibAltNameRP . " � ��������-�������� Arbist");
			//$APPLICATION->SetPageProperty("description", "� ������� " . $iblockName . " " . $textValue . " �� ������ �����, ������ " . $iblockName . " " . $textValue . " � ������ - ��������-������� Arbist.ru");
			$APPLICATION->SetPageProperty("h1", $iblockName . " " . $textValue);
			if(!$_GET["PAGEN_1"]) 
				echo '� ������� ������ �� ������� &laquo;'.$iblockName.' '.$textValue.'&raquo; �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';
			
			if (preg_match('|/catalog/((.+?)/country/(.+?))/|i', $_SERVER['REQUEST_URI'], $url)) 
			{
				$arFilter = Array('ID'=>66);
				$arSelect = Array("PROPERTY_4076");
				$ar_result = CIBlockElement::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>66, "PROPERTY_4075"=>$url[1],false, false, $arSelect ));
						//echo '<!--123';
						//var_dump($url[1]);
						//echo '-->';
				while($ob = $ar_result->GetNextElement())
				{
					$arFields = $ob->GetFields();
					$arProps = $ob->GetProperties();
					//	$APPLICATION->SetTitle($arProps['4076']['VALUE']);
					$APPLICATION->SetPageProperty("title", $arProps['4076']['VALUE']);
				}
			}
			break;
		}
			
		case "purpose": 
		{
			if($textValue == "������ ��� �����" && $iblockCode == "plitka_premium") 
			{
				$APPLICATION->SetPageProperty("title", "������ ��� ������: ������ ������������ ������ ��� ������ ������� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������ ������ ��� ������ �� ������ �����, ������ ������ ��� ������ ������� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ������ ��� ������");
				if(!isset($_GET['PAGEN_1']))
					echo '� ������� ������������ ������ ��� ������ ������� �� ������ �����. ��������� �������� � ��������������.<br/><br/>';
				
			} 
			elseif($textValue == "������ ��� �����" && $iblockCode != "keramogranit") 
			{	
				$APPLICATION->SetPageProperty("title", "������ ��� �����: ������ ������������ ������ ��� ����� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������ ������ ��� ����� �� ������ �����, ������ ������ ��� ����� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ������ ��� �����");
				if(!isset($_GET['PAGEN_1']))
                    echo '� ������� ������������ ������ ��� ����� �� ������ �����. ��������� �������� � ��������������.<br/><br/>';
                    
			} 
			elseif($textValue == "������ ��� ����" && $iblockCode == "plitka_premium") 
			{
				$APPLICATION->SetPageProperty("title", "������ ��� ����: ������ ������������ ������ ��� ���� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������ ������ ��� ���� �� ������ �����, ������ ������ ��� ���� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ������ ��� ����");
				if(!isset($_GET['PAGEN_1']))
					echo '� ������� ������������ ������ ��� ���� �� ������ �����. ��������� �������� � ��������������.<br/><br/>';
			} 
			elseif($textValue == "������ ��� ����") 
			{
				if (preg_match('|/catalog/keramogranit/.*|i', $_SERVER['REQUEST_URI'])) 
				{
					$APPLICATION->SetPageProperty("title", "������������ ��� ���� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
					$APPLICATION->SetPageProperty("description", "������� ������������� ��� ���� �� ������ �����, ������ ������ ��� ���� � ������ - ��������-������� Arbist.ru");
					$APPLICATION->SetPageProperty("h1", "������������ ��� ����");
					if(!isset($_GET['PAGEN_1']))
						echo '� ������� ������������ ��� ���� �� ������ �����. ��������� �������� � ��������������.<br/><br/>';
				} 
				elseif (preg_match('|/catalog/plitka_clinker/.*|i', $_SERVER['REQUEST_URI'])) 
				{
					$APPLICATION->SetPageProperty("title", "������� ��� ���� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
					$APPLICATION->SetPageProperty("description", "������� �������� ��� ���� �� ������ �����, ������ ������ ��� ���� � ������ - ��������-������� Arbist.ru");
					$APPLICATION->SetPageProperty("h1", "������� ��� ����");
					if(!isset($_GET['PAGEN_1']))
						echo '� ������� ������� ��� ���� �� ������ �����. ��������� �������� � ��������������.<br/><br/>';
				} 
				elseif (preg_match('|/catalog/plitka_premium/.*|i', $_SERVER['REQUEST_URI'])) 
				{
					$APPLICATION->SetPageProperty("title", "������������ ������ ��� ����: ������ ������ ��� ���� � ��������-�������� &laquo;������&raquo;");
					$APPLICATION->SetPageProperty("description", "������� �������� ��� ���� �� ������ �����, ������ ������ ��� ���� � ������ - ��������-������� Arbist.ru");
					$APPLICATION->SetPageProperty("h1", "������������ ������ ��� ����");
					if(!isset($_GET['PAGEN_1']))
						echo '� ������� ������������ ������ ��� ���� �� ������ �����. ��������� �������� � ��������������.<br/><br/>';
				}
            } 
			elseif($textValue == "������ ��� �����" && $iblockCode == "plitka_mosaico") 
			{
				$APPLICATION->SetPageProperty("title", "������� ��� ������: ������ ������� ��� ������ ������� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������� ��� ������ �� ������ �����, ������ ������� ��� ������ � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������� ��� ������ �������");
				if(!isset($_GET['PAGEN_1']))
                    echo '� ������� ������� ��� ������ �� ������ �����. ��������� �������� � ��������������.<br/><br/>';
            } 
			elseif($textValue == "������ ��� ����" && $iblockCode == "keramogranit") 
			{
                $APPLICATION->SetPageProperty("title", "������������ ��� ����: ������ ������������ ��� ���� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������� ��� ���� �� ������ �����, ������ ������������ ��� ���� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ��� ����");
				if(!isset($_GET['PAGEN_1']))
                    echo '� ������� ������������ ��� ���� �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';
            }
			elseif($textValue == "������ ��� ����" && $iblockCode == "plitka_clinker") 
			{
                $APPLICATION->SetPageProperty("title", "������� ��� ����: ������ ������� ��� ���� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� �������� ��� ���� �� ������ �����, ������ ������� ��� ���� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������� ��� ����");
				if(!isset($_GET['PAGEN_1']))
                    echo '� ������� ������� ��� ���� �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';
            } 
			elseif($textValue == "������ ��� �����" && $iblockCode == "keramogranit") 
			{
				$APPLICATION->SetPageProperty("title", "������������ ��� �����: ������ ������������ ��� ����� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������� ��� ����� �� ������ �����, ������ ������������ ��� ����� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ��� �����");
				if(!isset($_GET['PAGEN_1']))
					echo '� ������� ������������ ��� ����� �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';     
			} 
			elseif($textValue == "������ ��� ��������" && $iblockCode == "keramogranit") 
			{
				$APPLICATION->SetPageProperty("title", "������������ ��� ��������: ������ ������������ ��� �������� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������� ��� �������� �� ������ �����, ������ ������������ ��� �������� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ��� ��������");
				if(!isset($_GET['PAGEN_1']))
                    echo '� ������� ������������ ��� �������� �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';     
			} 
			elseif($textValue == "������ ��� �����" && $iblockCode == "keramogranit") 
			{
				$APPLICATION->SetPageProperty("title", "������������ ��� �����: ������ ������������ ��� ����� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������� ��� ����� �� ������ �����, ������ ������������ ��� ����� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ��� �����");
				if(!isset($_GET['PAGEN_1']))
                    echo '� ������� ������������ ��� ����� �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';     
			} 
			elseif($textValue == "������ ��� �����" && $iblockCode == "keramogranit") 
			{   
				$APPLICATION->SetPageProperty("title", "������������ ��� �����: ������ ������������ ��� ����� �������� � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "������� ������������� ��� ����� �� ������ �����, ������ ������������ ��� ����� � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", "������������ ��� �����");
				if(!isset($_GET['PAGEN_1']))
                    echo '� ������� ������������ ��� ����� �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';     

			} 
			else 
			{
				$APPLICATION->SetPageProperty("title", $iblockName . " " . $textValue . ": ������ " . $iblockName . " " . $textValue . " � ������ �� ������ ����� - ������� ��������-�������� Arbist");
				$APPLICATION->SetPageProperty("description", "� ������� " . $iblockName . " " . $textValue . " �� ������ �����, ������ " . $iblockName . " " . $textValue . " � ������ - ��������-������� Arbist.ru");
				$APPLICATION->SetPageProperty("h1", $iblockName . " " . $textValue);
				$printTextValue = mb_strtolower($textValue);
				if(!stristr($printTextValue, '���')) 
					$printTextValue = '��� '.$printTextValue;
				if(!isset($_GET['PAGEN_1']))
					echo '� ������� ������ �� ������� &laquo;'.$iblockName.' '.$printTextValue.'&raquo; �� ������ ����� � ��������� ��������� � ����������������.<br/><br/>';
				break;
            }
		}
		default:
	}
	if(!empty($iblockId)) 
	{
		$floorFilter = Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => 47,
			"SECTION_ID" => array(4366,6355),
			"PROPERTY_IBLOCK_ID" => $iblockId
		);

		$resFloor = CIBlockElement::GetList(Array(), $floorFilter, false, false, Array("ID"));
		if($arResFloor = $resFloor->Fetch() || ($property == "purpose" && $textValue == "����")) 
		{
			// ���� ��� �����-���� "������" ��������� ��������
			// ���� ��� �����-���� "������" ���������
			// �� ��������� ��� ������� + ������ � ��������� � ������������
			?><?$APPLICATION->IncludeComponent(
				"itc:catalog.section.filtered.list",
				"seo_list",
				Array(
					"IBLOCK_TYPE" => $iblockType,
					"IBLOCK_ID" => $iblockId,
					"SECTION_ID" => $_REQUEST["SECTION_ID"],
					"SECTION_CODE" => "",
					"SECTION_URL" => "",
					"COUNT_ELEMENTS" => "Y",
					"TOP_DEPTH" => "2",
					"SECTION_FIELDS" => "",
					"SECTION_USER_FIELDS" => "",
					"ADD_SECTIONS_CHAIN" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_NOTES" => "",
					"CACHE_GROUPS" => "Y",
					"FILTER_PROP_ID" => $map[$iblockCode][$property],
					"FILTER_PROP_VALUE" => $value
				)
			);?><?
			$seoFilter = Array(
				"PROPERTY_" . $map[$iblockCode][$property] => $value,
				"!CATALOG_PRICE_10" => false,
			);
			?><?$APPLICATION->IncludeComponent(
				"itc:catalog.section",
				"seo_section",
				Array(
					"IBLOCK_TYPE" => "catalog",
					"IBLOCK_ID" => $iblockId,
					"SECTION_ID" => $_REQUEST["SECTION_ID"],
					"SECTION_CODE" => "",
					"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
					"ELEMENT_SORT_FIELD" => "name",
					"ELEMENT_SORT_ORDER" => "asc",
					"FILTER_NAME" => "seoFilter",
					"INCLUDE_SUBSECTIONS" => "Y",
					"SHOW_ALL_WO_SECTION" => "N",
					"PAGE_ELEMENT_COUNT" => "5",
					"LINE_ELEMENT_COUNT" => "3",
					"PROPERTY_CODE" => array(0=>"",1=>"ACTION_NEW_SALE",2=>"",),
					"SECTION_URL" => "",
					"DETAIL_URL" => "",
					"BASKET_URL" => "/personal/basket.php",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "Y",
					"META_KEYWORDS" => "-",
					"META_DESCRIPTION" => "-",
					"BROWSER_TITLE" => "-",
					"ADD_SECTIONS_CHAIN" => "N",
					"DISPLAY_COMPARE" => "N",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"CACHE_FILTER" => "N",
					"FILTER_PROP_TYPE" => $property,
					"FILTER_PROP_VALUE" => $value,
					"PRICE_CODE" => array(0=>"���������",),
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"PRODUCT_PROPERTIES" => array(),
					"USE_PRODUCT_QUANTITY" => "N",
					"DISPLAY_TOP_PAGER" => "Y",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "������",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "nav_bot",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);?><?
		} 
		else 
		{
			// ���� ��� �� ��������� ��������
			//���� ��� �����
			if($iblockId == 51 || $iblockId == 50)
			{
				$APPLICATION->IncludeComponent(
					"itc:catalog.section.filtered.list", 
					"seo_list", 
					Array(
						"IBLOCK_TYPE" => $iblockType,	// ��� ����-�����
						"IBLOCK_ID" => $iblockId,	// ����-����
						"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID �������
						"SECTION_CODE" => "",	// ��� �������
						"SECTION_URL" => "",	// URL, ������� �� �������� � ���������� �������
						"COUNT_ELEMENTS" => "Y",	// ���������� ���������� ��������� � �������
						"TOP_DEPTH" => "2",	// ������������ ������������ ������� ��������
						"SECTION_FIELDS" => "",	// ���� ��������
						"SECTION_USER_FIELDS" => "",	// �������� ��������
						"ADD_SECTIONS_CHAIN" => "Y",	// �������� ������ � ������� ���������
						"CACHE_TYPE" => "A",	// ��� �����������
						"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
						"CACHE_NOTES" => "",
						"CACHE_GROUPS" => "Y",	// ��������� ����� �������
						"FILTER_PROP_ID" => $map[$iblockCode][$property],
						"FILTER_PROP_VALUE" => $value
					),
					false
				);

				if($iblockId == 51) 
				{
					$COMPLECT_IBLOCK_ID = 12;
				}
				if($iblockId == 50) 
				{
					$COMPLECT_IBLOCK_ID = 29;
				}

				$XML_ID_COMPLECT = array(
					0 => "d4f7e3e8-ea90-11e1-9c08-14dae9a723c8",// ����� ��������
					1 => "53ec260f-c448-11e1-8a68-14dae9a723c8",// �������
				);

				$seoFilter = Array(
					"PROPERTY_" . $map[$iblockCode][$property] => $value,
				);

				?><?$APPLICATION->IncludeComponent(
					"itc:catalog.section", 
					"seo_section_doors", 
					array(
						"IBLOCK_TYPE" => "catalog",
						"IBLOCK_ID" => $iblockId,
						"SECTION_ID" => $_REQUEST["SECTION_ID"],
						"SECTION_CODE" => "",
						"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
						),
						"ELEMENT_SORT_FIELD" => "name",
						"ELEMENT_SORT_ORDER" => "asc",
						"FILTER_NAME" => "seoFilter",
						"INCLUDE_SUBSECTIONS" => "Y",
						"SHOW_ALL_WO_SECTION" => "N",
						"PAGE_ELEMENT_COUNT" => "5",
						"LINE_ELEMENT_COUNT" => "4",
						"PROPERTY_CODE" => array(
						0 => "",
						1 => "ACTION_NEW_SALE",
						2 => "",
						),
						"SECTION_URL" => "",
						"DETAIL_URL" => "",
						"BASKET_URL" => "/personal/basket.php",
						"ACTION_VARIABLE" => "action",
						"PRODUCT_ID_VARIABLE" => "id",
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"SECTION_ID_VARIABLE" => "SECTION_ID",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "Y",
						"META_KEYWORDS" => "-",
						"META_DESCRIPTION" => "-",
						"BROWSER_TITLE" => "-",
						"ADD_SECTIONS_CHAIN" => "N",
						"DISPLAY_COMPARE" => "N",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"CACHE_FILTER" => "N",
						"PRICE_CODE" => array(
						0 => "���������",
						),
						"USE_PRICE_COUNT" => "N",
						"SHOW_PRICE_COUNT" => "1",
						"PRICE_VAT_INCLUDE" => "Y",
						"PRODUCT_PROPERTIES" => array(
						),
						"USE_PRODUCT_QUANTITY" => "N",
						"DISPLAY_TOP_PAGER" => "Y",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "������",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => "nav_bot",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"COMPLECT_IBLOCK_ID" => $COMPLECT_IBLOCK_ID,
						"XML_ID_COMPLECT" => $XML_ID_COMPLECT,
						"FILTER_PROP_TYPE" => $property,
						"FILTER_PROP_VALUE" => $value
					),
					false
				);?><?
			}
			else 
			{
				if($property == 'country')
				{
					$filter = array('IBLOCK_ID' => $iblockId, 'PROPERTY_'.$map[$iblockCode][$property] => $value, 'ACTIVE' => 'Y');
					$itemDB = CIBlockElement::GetList(
						Array("SORT"=>"ASC"), 
						$filter, 
						false, 
						false, 
						Array('ID','PROPERTY_'.$map[$iblockCode]['vendor'].'.NAME', 'PROPERTY_'.$map[$iblockCode]['vendor'].'.CODE')
					);

					$arVendors = array();
					while($item = $itemDB->fetch()) 
					{
						if(strlen(trim($item['PROPERTY_'.$map[$iblockCode]['vendor'].'_NAME'])) > 0) 
							$arVendors[] = $item['PROPERTY_'.$map[$iblockCode]['vendor'].'_NAME'];
						$arVendorsCODE[$item['PROPERTY_'.$map[$iblockCode]['vendor'].'_NAME']] = $item['PROPERTY_'.$map[$iblockCode]['vendor'].'_CODE'];
					}

					$arVendors = array_unique($arVendors);	
					$part = ceil(count($arVendors) / 4);
					$i=0;
					?><div class="seoTitle">�������������</div>
					<div style="width: 25%; float: left;"><?
					foreach($arVendors as $vendor)
					{
						if($i == $part) 
						{
							echo '</div><div style="width: 25%; float: left;">';
							$i=0;
						}
						?><div class="gap1" style="position: relative; margin-bottom: 5px !important;">
							<a href="/catalog/<?=$iblockCode?>/vendor/<?=$arVendorsCODE[$vendor]?>/" ><?=$vendor?></a>
						</div><?
						$i++;
					}
					echo '</div><div class="clear" style="height: 15px;"></div>';
				}

				?><?$APPLICATION->IncludeComponent(
					"itc:catalog.section.filtered.list",
					"seo",
					Array(
						"IBLOCK_TYPE" => $iblockType,
						"IBLOCK_ID" => $iblockId,
						"SECTION_CODE" => "",
						"COUNT_ELEMENTS" => "Y",
						"TOP_DEPTH" => "2",
						"SECTION_FIELDS" => array(0=>"",1=>"",),
						"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
						"SECTION_URL" => "",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"FILTER_PROP_ID" => $map[$iblockCode][$property],
						"FILTER_PROP_VALUE" => $value,
						"FILTER_PROP_TYPE" => $property
					),
					false,
					Array(
						'HIDE_ICONS' => 'Y'
					)
				);?><?
			}
		}
		if($property == "purpose" && $textValue == "������ ��� �����" && $iblockCode == "plitka_premium" && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������ ������ ��� ����� ������� ������� �������.";
		} 
		elseif($property == "purpose" && $textValue == "������ ��� �����" && $iblockCode != "keramogranit" && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������ ������ ��� ����� ������� ������� �������.";
		} 
		elseif($property == "purpose" && $textValue == "������ ��� ����" && $iblockCode == "plitka_premium" && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������ ������ ��� ���� ������� ������� �������.";
		} 
		elseif($property == "purpose" && $textValue == "������ ��� ����"  && !$_GET["PAGEN_1"]) 
		{
			if (preg_match('|/catalog/keramogranit/.*|i', $_SERVER['REQUEST_URI'])) 
			{
				echo "� ������ ������� ������������ ����� ��������� ������������� ��� ���� ������� ������� �������.";	
			} 
			elseif (preg_match('|/catalog/plitka_clinker/.*|i', $_SERVER['REQUEST_URI'])) 
			{
				echo "� ������ ������� ������������ ����� ��������� �������� ��� ���� ������� ������� �������.";
			}
		} 
		elseif($property == "purpose" && $textValue == "������ ��� �����" && $iblockCode == "plitka_mosaico"  && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������� ��� ������ ������� ������� �������.";
		} 
		elseif($property == "purpose" && $textValue == "������ ��� ����" && $iblockCode == "keramogranit"  && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������� ��� ����  ������� ������� �������.";
		} 
		elseif($property == "purpose" && $textValue == "������ ��� �����" && $iblockCode == "keramogranit"  && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������� ��� �����  ������� ������� �������.";
		} 
		elseif($property == "purpose" && $textValue == "������ ��� ��������" && $iblockCode == "keramogranit"  && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������� ��� ��������  ������� ������� �������.";
		} 
		elseif($property == "purpose" && $textValue == "������ ��� �����" && $iblockCode == "keramogranit"  && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������� ��� �����  ������� ������� �������.";
		}
		elseif($property == "purpose" && $textValue == "������ ��� �����" && $iblockCode == "keramogranit"  && !$_GET["PAGEN_1"]) 
		{
			echo "� ������ ������� ������������ ����� ��������� ������������� ��� �����  ������� ������� �������.";
		} 
		else if(!$_GET["PAGEN_1"])
		{
			echo '<br>� ����� ��������-�������� Arbist.ru ����������� ������� ����� �������� ��������� ������� ������� �������.<br/><br/>';
		}
	}
	?><br /><?

	$arFilter = Array(
		"IBLOCK_ID" => 49,
		//"PROPERTY_IB_CODE" => $iblockCode, // ������ ��������, ���������� ������ �.�. � ������� ��������� ID �������� ����������
		"ACTIVE" => "Y",
		"%PROPERTY_PROP_ID" => serialize(array((string)$map[$iblockCode][$property])),
		"PROPERTY_PROP_VALUE" => $value
	);
	
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID", "DETAIL_TEXT"));
	if($arFields = $res->GetNext()) {
		if((empty($_REQUEST['PAGEN_1']) || $_REQUEST['PAGEN_1'] == 1) && (empty($_REQUEST['PAGEN_2']) || $_REQUEST['PAGEN_2'] == 1) && (empty($_REQUEST['PAGEN_3']) || $_REQUEST['PAGEN_3'] == 1) ) {
			echo $arFields["DETAIL_TEXT"] . "<br>";
		}
	}
} 
else 
{
	//LocalRedirect("/404.php");
	/*
	echo '<pre>';
	print_r($_REQUEST);
	echo '</pre>';
	*/
	/*CHTTP::SetStatus("404 Not Found");
	echo '� ������� ������ �� ������� &laquo;'.$iblockName.'&raquo; �� ������ ����� � ��������� ��������� � ����������������.<br/>';
	echo '<br>� ����� ��������-�������� Arbist.ru ����������� ������� ����� �������� ��������� ������� ������� �������.<br/><br/>';*/
}
if(!empty($iblockType) && isset($map[$iblockCode][$property])) 
{
	$APPLICATION->AddChainItem($iblockName, "/" . $iblockType . "/" . $iblockCode . "/");
	$typeChainItem = '';
	switch($property) 
	{
		case 'purpose':
			$typeChainItem = '����������';
			break;
		case 'country':
			$typeChainItem = '������';
			break;
		case 'vendor':
			$typeChainItem = '�������������';
			break;
		default:
	}
	$APPLICATION->AddChainItem($typeChainItem, "");
	$APPLICATION->AddChainItem($textValue, "");
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?></div>