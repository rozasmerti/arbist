<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	
	CModule::IncludeModule('iblock');
	$dbElement = CIBlockElement::GetList(
		Array(
			'ID' => 'ASC',
		),
		Array(
			'ID' => $_REQUEST['ELEMENT_ID'],
		),
		false,
		false,
		Array(
			'ID',
			'IBLOCK_SECTION_ID',
		)
	);

	if($arElement = $dbElement->Fetch())
	{
		$url = '/catalog/'.$_REQUEST['IBLOCK_CODE'].'/'.$arElement['IBLOCK_SECTION_ID'].'/'.$_REQUEST['ELEMENT_ID'].'/';
		LocalRedirect($url, false, '301 Moved permanently');
	}
	else
	{
		$url = '/catalog/'.$_REQUEST['IBLOCK_CODE'].'/'.$_REQUEST['SECTION_CODE'].'/'.$_REQUEST['ELEMENT_ID'].'/';
		LocalRedirect($url);
	}
?>