$(function(){
//    if js is available
    $('body').addClass('js');

//    inputValueToggle
    $(".toggleMe").focus(function(){if(this.value==this.defaultValue)this.value='';}).bind('blur',function(){ if(this.value=='')this.value=this.defaultValue;});

    //    inputFocusHelper
    $(".input").on('click', function(e){
        e.stopPropagation();
        $(this).find('input, textarea').trigger('focus')
    });
    $('.input input, .input textarea').on('click', function(e){
        e.stopPropagation();
    });

    $('.input input, .input textarea').on('focus', function(){
        $(this).closest('.input').addClass('focusInput');
    });
    $('.input input, .input textarea').on('focusout', function(){
        $(this).closest('.input').removeClass('focusInput');
    });

//    inputHintToggle

    $('.inputHinted input').focus(function() {
        $(this).closest('.inputHinted').find('.hint').addClass('hidden');
    })
    $('.inputHinted input').blur(function() {
        if (!$(this).attr('value')) {
            $(this).closest('.inputHinted').find('.hint').removeClass('hidden');
        }
    });
    $('.inputHinted input').change(function() {
        if ($(this).attr('value')) {
            $(this).closest('.inputHinted').find('.hint').addClass('hidden');
        }
    })
    $('.inputHinted input').mouseover(function() {
        if ($(this).attr('value')) {
            $(this).closest('.inputHinted').find('.hint').addClass('hidden');
        }
    })



//    button focus helper
    $('.button button').focusin(function(){
        $(this).closest('.button').addClass('focus')
    })
    $('.button button').focusout(function(){
        $(this).closest('.button').removeClass('focus')
    })

//    button hover helper
    $('.button button').mouseenter(function(){
        $(this).closest('.button').addClass('hover')
    })
    $('.button button').mouseleave(function(){
        $(this).closest('.button').removeClass('hover')
    })

    //    button active

    $('.button button').mousedown(function(){
        $(this).closest('.button').addClass('active')
    })
    $('.button button').mouseup(function(){
        $(this).closest('.button').removeClass('active')
    })
    $('.button button').mouseleave(function(){
        $(this).closest('.button').removeClass('active')
    })


//    call popup
    var trgCont;
    $('.popup .trg').click(function(e){
        e.preventDefault();
        trgCont = $(this).closest('.popup');
        $('.popup').not(trgCont).find('.pop:visible').hide();
        trgCont.find('.pop').toggle();
        $('.popup .lower').removeClass('lower');
        $('.popup .trg').not($(this)).toggleClass('lower');
    })
	$('#mainRightBlock').find('li span').mouseover(function(){
		timer = setTimeout(showLinks, 450, $(this))	;
			$('#mainRightBlock').find('li span').mouseout(function(){
			clearTimeout(timer);
	});
		});

    $('body').click(function(){
        $('.popup').find('.pop:visible').removeClass('blockShow').addClass('blockHide');
        $('#mainRightBlock').find('.pop:visible').removeClass('blockShow').addClass('blockHide');
            
    })
    $('.popup .pop, .popup .trg').click(function(e){
        e.stopPropagation();
    })

//    menu expand
    /*var speed = 150,
        current,
        li;
    $('.menu').not('.menu-main').children('li').children('a').click(function(e){
        e.preventDefault();
        li = $(this).closest('li');
        li.find('.level0').slideToggle(speed, function() {
            li.toggleClass('expanded');
            li.closest('.menu').removeClass('menu-alt')
        });
        li.siblings().find('.level0').slideUp(speed, function() {
            li.siblings().removeClass('expanded');
        });
        $(this).blur();
    })
    $('.menu').not('.menu-main').siblings('.panel').find('.panel-cont').click(function(){
        current = $(this).closest('.panel').siblings('.menu').find('.current');
        current.closest('.menu').addClass('menu-alt').end().find('.level0:hidden').slideDown(speed);
        current.siblings().removeClass('expanded').find('.level0').slideUp(speed);
    })*/

//    filter
    $('.filter .filter-cont a, .sub-filter a').data('role', 'checkbox');
    $('.filter .choose-all button').data('role', 'choose-all');
    $('.filter .reset-all button').data('role', 'reset-all');
//    $('.filter .reset-all button, .filter .simplemodal-close').data('role', 'reset-all');
    $('.filter .reset button').data('role', 'reset');

    function flagIfFiltered(source){
        var container = source.closest('.filter');
        if (container.find('.chosen').size() > 0 && !container.hasClass('filtered')) {
            container.addClass('filtered');
        } else if (container.find('.chosen').size() <= 0) {
            container.removeClass('filtered');
        }
    }

    function indexify(elems, index, n) {
        if (!(index === -1)) {
            var selected = $(elems).eq(index - 1).find('.sel');
            if (n === 0) {
                selected.addClass('sel-h');

            } else {
                selected.text('(' + n + ')').removeClass('sel-h');
            }
        } else {
            $(elems).find('.sel').text('(' + n + ')').addClass('sel-h');
        }
    }

    function showChosen(source){
        var containers = $(source).closest('.filter').find('.filter-cont'),
            n = 0,
            index = -1;
        if(!($(source).data('role') == 'reset-all')) {
            //n = containers.not(':hidden').find('.chosen').size();
	    n = source.closest('.filter-cont').not(':hidden').find('.chosen').size();

	    var panel = containers.not(':hidden').closest('.ui-tabs-panel');
	    if (panel.length == 0)
	    {
		var tabRel = $(source).closest('.filter-cont').attr('rel');
		index = $('div[rel='+tabRel+']').attr('id').split('tab-')[1];
            } else
	    {
	            index = panel.attr('id').split('tab-')[1];
	    }
        }
        $('.filter-items, .tabsNav').each(function(){
            indexify($(this).find('.filter-item'), index, n);
        })
    }
	/**/
	function hiddenInputUpdate(target, number) {
		pcode = target[number].attributes['pcode'].value;
		pid = target[number].attributes['pid'].value;
		if($("#input" + pcode).val() != '')
			oldValArray = $("#input" + pcode).val().split(',');
		else
			oldValArray = [];
		delNumber = -1;
		for(j = 0; j < oldValArray.length; j++) {
			if(oldValArray[j] == pid) {
				delNumber = j;
				break;
			}
		}
		if(delNumber >= 0)
			oldValArray.splice(j,1);
		else
			oldValArray.push(pid);
		if(oldValArray.length == 1)
			stringValues = oldValArray[0];
		else
			stringValues = oldValArray.join(',');
		$("#input" + pcode).val(stringValues);
	}
	/**/
    function chooseItems(target, source) {
        switch ($(source).data('role')) {
            case 'checkbox':
		var pid = target.attr('pid');
		if (!!pid)
		{
			$('[pid='+pid+']').toggleClass('chosen');
		} else
		{
	                target.toggleClass('chosen'); 
		}
				// add/reset one
				hiddenInputUpdate(target, 0);
                break;
            case 'choose-all':
                target.not('.chosen').addClass('chosen');
				// add all in prop
				arrayValues = [];
				for(i = 0; i < target.length; i++) {
					pcode = target[i].attributes['pcode'].value;
					pid = target[i].attributes['pid'].value;
					arrayValues.push(pid);
				}
				if(arrayValues.length == 1)
					stringValues = arrayValues[0];
				else
					stringValues = arrayValues.join(',');
				$("#input" + pcode).val(stringValues);
                break;
            case 'reset':
                target.removeClass('chosen');
				// reset all in prop
				pcode = target[0].attributes['pcode'].value;
				if(pcode != '')
					$("#input" + pcode).val('');
                break;
            case 'reset-all':
                target.removeClass('chosen');
				// reset all
				arrayPropCodes = [];
				for(i = 0; i < target.length; i++) {
					pcode = target[i].attributes['pcode'].value;
					pid = target[i].attributes['pid'].value;
					if(arrayPropCodes[arrayPropCodes.length-1] != pcode)
						arrayPropCodes.push(pcode);
				}
				for(j = 0; j < arrayPropCodes.length; j++) {
					$("#input" + arrayPropCodes[j]).val('');
				}
                break;
        }
        showChosen(source);
        flagIfFiltered(source);

	if ($(source).data('ajax-filter') == 'y')
	{
		//var form = $(source).closest('form');
		var form = $('form#filterForm');
		var formData = form.serializeArray();
        var catalogArea = $('div.catalog-area');

        $('html, body').animate({
            scrollTop: catalogArea.offset().top
        }, 700);

		BX.showWait(catalogArea[0]);
		$.ajax({url: form.attr('action'), data: formData}).success(function(data) {
			BX.closeWait(catalogArea[0]);
            catalogArea.html($(data).find('div.catalog-area').html());
            $('.filter-marker').replaceWith($(data).find('.filter-marker'));
		});
	}
    }

    $('.filter-cont a').click(function(e){
        e.preventDefault();
		// add/reset one //
        chooseItems($(this), $(this));
    })
    $('.choose-all button, .reset button').click(function(){
		// add/reset all in prop //
        chooseItems($(this).closest('.filter').find('.filter-cont:visible').find('a'), $(this));
    })
    $('.filter').on('click', '.reset-all button', function(){
		// reset_all //
        chooseItems($(this).closest('.filter').find('.filter-cont').find('a'), $(this));
    })
    /*$('.filter').on('click', '.reset-all button, .simplemodal-close', function(){
        chooseItems($(this).closest('.filter').find('.filter-cont').find('a'), $(this));
    })*/


    //    sliders
    function getObj(el){
        var id = el.attr('id').split('tickdiv_')[1]
        return $.trackbar.getObject(id);
    }
    function getMinAndMax(el){
        var o = getObj(el);
        var minVal = o.leftLimit;
        var maxVal = o.rightLimit;
        return [minVal, maxVal];
    }

    $(".parameter .start").change(function(){
        var container = $(this).closest('.parameter');
        var track = container.find('.tickdiv');
        var value1 = $(this).val();
        var value2 = container.find('.end').val();
        var minVal = getMinAndMax(track)[0];
        if (value1 < minVal) { value1 = minVal; $(this).val(minVal)}
        if (parseInt(value1) > parseInt(value2)) {
            value1 = value2;
            $(this).val(value1);
        }
        var o = getObj(track);
        o.updateLeftValue(value1);
    });
    $(".parameter .end").change(function(){
        var container = $(this).closest('.parameter');
        var track = container.find('.tickdiv');
        var value1 = container.find('.start').val();
        var value2 = $(this).val();
        var maxVal = getMinAndMax(track)[1];
        if (value2 > maxVal) { value2 = maxVal; $(this).val(maxVal)}
        if(parseInt(value1) > parseInt(value2)){
            value2 = value1;
            $(this).val(value2);
        }
        var o = getObj(track);
        o.updateRightValue(value2);
    });

//     filtering values
    $('.filtering').keypress(function(event){
        var key, keyChar;
        if(!event) var event = window.event;
        if (event.keyCode) key = event.keyCode;
        else if(event.which) key = event.which;
        if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
        keyChar=String.fromCharCode(key);
        if(!/\d/.test(keyChar)) return false;
    });

//    reset sliders

    $('.filter form').on('reset', function(){
        $(this).find('.parameter').each(function(){
            var $that = $(this),
                start = $that.find('.start'),
                end = $that.find('.end'),
                value1 = start[0].defaultValue ?
                            start[0].defaultValue :
                            start.data('defaultValue'),
                value2 = end[0].defaultValue ?
                            end[0].defaultValue :
                            end.data('defaultValue'),
                track = $that.find('.tickdiv'),
                o = getObj(track);
            o.updateLeftValue(value1);
            o.updateRightValue(value2);
            if (!(start[0].defaultValue)) {
                setTimeout(function(){start.val(value1)}, 0)
            }
            if (!(end[0].defaultValue)) {
                setTimeout(function(){end.val(value2)}, 0)
            }
        })
    })

//    onfocus select text in slider inputs

    $('.parameter .start, .parameter .end').on('focus', function(){
        $(this).select()
    })
    $('.parameter .start, .parameter .end').on('mouseup', function(e){
        e.preventDefault();
    })


//    content trigger

    $('.trigger a').click(function(e){
        e.preventDefault();
        var exp = $(this).closest('.exp'),
            classes = exp.attr('class'),
            set = {};
        if (exp.hasClass('exp-a')) {
            exp.siblings('.expanded').removeClass('expanded');
        }
        if (exp.hasClass('exp-dual')) {
            set = exp.closest('.filter').find('.exp-dual').filter(function(i){
                if ($(this).hasClass(classes)) {
                    return true;
                }
            });
            set.toggleClass('expanded');
        } else {
            $(this).parent().toggleClass('toggled');
            exp.find('.dest').eq(0).toggle();
        }
    })
    $('.exp-a .dest, .exp-b .dest').hide();


//    opera 10 is a very good browser

    if($.browser.opera) {
        var h, el;
        $('.block4').each(function(i){
            el = $(this).find('.block-m');
            h = el.height() - parseInt(el.find('.m0').css('top')) - parseInt(el.find('.m0').css('bottom'));
            el.find('.m0').css({'height': h});
        })
    }

//    <3 ie9

    $('.input3 input').scroll(function(e){
        $(this).scrollTop(0);
    })
})


function AddBasket(id, names) {
	var colSklad = 10;
	var colich = 1;
	var top, left, offsetY;
	if (colSklad>=colich) { 
		$.post("/personal/basketchange.php", {'id':id, 'colich':colich, 'mode':'ajax', 'operat':'add'} );
		alert('da');
	}
	else {
	}
	return false;
}

function showLinks(element) {
		key = element.attr('key');
		$('#mainRightBlock').find('.pop:visible').removeClass('blockShow').addClass('blockHide');
        //$('#block' + key).toggle();
        if($('#block' + key).hasClass('blockHide'))
            $('#block' + key).removeClass('blockHide').addClass('blockShow');
        else
            $('#block' + key).removeClass('blockShow').addClass('blockHide');
		return false;
}